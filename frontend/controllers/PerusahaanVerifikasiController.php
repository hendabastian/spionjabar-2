<?php

namespace frontend\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\perusahaan\PerusahaanDokumen;
use common\models\perusahaan\Perusahaan;
use common\models\perusahaan\PerusahaanVerifikasi;
use common\models\perusahaan\PerusahaanDomisili;
use common\models\perusahaan\PerusahaanPengurus;
use common\models\perusahaan\PerusahaanPemegangSaham;
use common\models\ActivityLog;
use common\models\perusahaan\SkPerusahaan;

class PerusahaanVerifikasiController extends \yii\web\Controller
{
  /**
   * {@inheritdoc}
   */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'actions' => ['ajukan-legalitas', 'duplicate-data'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  public function beforeAction($action)
  {
    $this->layout = '/dashboard/main';
    return parent::beforeAction($action);
  }

  /**
   * proses pengajuan dokumen ke verifikator
   * @param integer $id_perusahaan
   * @var $dokumen = Model dari PerusahaanDokumen
   * @var $perusahaan = Model dari Perusahaan
   * @var $model = Model dari PerusahaanVerifikasi
   * @var $oldVerif = Data Verifikasi sebelumnya. variable ini dipakai untuk melihat apakah perusahaan ini pernah melakukan proses verifikasi.
   * @var $getSK = mencari SK yang diupload perusahaan, jika tidak ada SK terupload akan diklasifikasikan sebagai perusahaan baru
   * @return mixed
   */
  public function actionAjukanLegalitas($perusahaan_id)
  {
    $model = new PerusahaanVerifikasi();
    $dokumen = PerusahaanDokumen::find()->where(['perusahaan_id' => $perusahaan_id, 'is_delete' => 0])->all();
    $getSK = SkPerusahaan::find()->where(['perusahaan_id' => $perusahaan_id, 'is_delete' => 0])->all();
    if (!empty($getSK)) {
      $jenisPerusahaan = 1; // perusahaan lama
    } else {
      $jenisPerusahaan = 2; // perusahaan baru
    }
    $perusahaan = Perusahaan::findOne($perusahaan_id);

    if ($model->load(Yii::$app->request->post())) {
      if ($perusahaan->is_verified == 0) {
        $model->perusahaan_id = $perusahaan_id;
        $model->status_verifikasi = 1; // status verifikasi diubah menjadi diajukan
        $model->tgl_verifikasi = new \yii\db\Expression('NOW()');
        $model->save();
        $perusahaan->jenis_perusahaan = $jenisPerusahaan; //Set jenis perusahaan lama / baru
        $perusahaan->is_verified = 1; // status perusahaan diubah menjadi diajukan
        $perusahaan->save();
      } elseif ($perusahaan->is_verified == 5) {
        // $dataBaru = $this->duplicateData($perusahaan);
        $model->perusahaan_id = $perusahaan_id;
        $model->status_verifikasi = 1; // status verifikasi diubah menjadi diajukan
        $model->tgl_verifikasi = new \yii\db\Expression('NOW()');
        $model->save();
        $perusahaan->jenis_perusahaan = $jenisPerusahaan;
        $perusahaan->is_verified = 1;
        $perusahaan->save();
        Yii::$app->session->setFlash('success', 'Legalitas Berhasil Diajukan Kembali');
        return $this->redirect(['perusahaan/profil-perusahaan', 'id' => $perusahaan_id]);
      }

      Yii::$app->session->setFlash('success', 'Legalitas Berhasil Diajukan');
      return $this->redirect(['perusahaan/profil-perusahaan', 'id' => $perusahaan_id]);

    } elseif (Yii::$app->request->isAjax) {
      return $this->renderAjax('_form', [
        'jenisPerusahaan' => $jenisPerusahaan,
        'model' => $model,
        'perusahaan' => $perusahaan,
      ]);
    }
  }

  /*
    * Fungsi untuk duplikasi Data Perusahaan.
    * fungsi ini akan dijalankan apabila pada proses verifikasi, verifikator melakukan aksi penolakan, dan perusahaan melakukan pengajuan ulang
    * @param Object $perusahaan = objek dari perusahaan
    * @var $newObject = membuat objek perusahaan baru
    * @return Object
    */
  public function DuplicateData($perusahaan)
  {
    $newObject = new Perusahaan();
    $newObject->user_id = $perusahaan->user_id;
    $newObject->jenis_badan_usaha_id = $perusahaan->jenis_badan_usaha_id;
    $newObject->kode_perusahaan = $perusahaan->kode_perusahaan;
    $newObject->nama_perusahaan = $perusahaan->nama_perusahaan;
    $newObject->npwp = $perusahaan->npwp;
    $newObject->hp = $perusahaan->hp;
    $newObject->website = $perusahaan->website;
    $newObject->is_verified = 1;
    $newObject->jenis_perusahaan = $perusahaan->jenis_perusahaan;
    $newObject->data_version = $perusahaan->data_version + 1;
    $newObject->email = $perusahaan->email;
    $newObject->status = 1;
    $newObject->save(false);
    $perusahaan->status = 0; // ubah data perusahaan sebelumnya menjadi tidak aktif (perubahan)
    $perusahaan->save(false);

    $perusahaanDomisili = PerusahaanDomisili::find()->where(['is_delete' => 0, 'perusahaan_id' => $perusahaan->id])->all();
    $this->duplicateDom($newObject->id, $perusahaanDomisili);

    $perusahaanPengurus = PerusahaanPengurus::find()->where(['is_delete' => 0, 'perusahaan_id' => $perusahaan->id])->all();
    $this->duplicateDireksi($newObject->id, $perusahaanPengurus);

    $perusahaanPemegangSaham = PerusahaanPemegangSaham::find()->where(['is_delete' => 0, 'perusahaan_id' => $perusahaan->id])->all();
    $this->duplicatePengSaham($newObject->id, $perusahaanPemegangSaham);

    $perusahaanDokumen = PerusahaanDokumen::find()->where(['is_delete' => 0, 'perusahaan_id' => $perusahaan->id])->all();
    $this->duplicateDokumen($newObject->id, $perusahaanDokumen);

    return $newObject;
  }

  /*
    * Fungsi untuk duplikasi Data Domisili Perusahaan.
    * fungsi ini akan dijalankan apabila pada proses verifikasi, verifikator melakukan aksi penolakan, dan perusahaan melakukan pengajuan ulang
    * @param Object $perusahaan = objek dari perusahaan
    * @var $newDataDom = membuat objek data domisili perusahaan baru
    * @return Object
    */
  public function DuplicateDom($idPerusahaanBaru, $perusahaanDomisili)
  {
    if ($perusahaanDomisili !== NULL) {
      foreach ($perusahaanDomisili as $key => $value) {
        $newDataDom = new PerusahaanDomisili();
        $newDataDom->perusahaan_id = $idPerusahaanBaru;
        $newDataDom->jenis_domisili = $value->jenis_domisili;
        $newDataDom->alamat = $value->alamat;
        $newDataDom->provinsi_id = $value->provinsi_id;
        $newDataDom->kabupaten_id = $value->kabupaten_id;
        $newDataDom->tlp = $value->tlp;
        $newDataDom->fax = $value->fax;
        $newDataDom->kode_pos = $value->kode_pos;
        $newDataDom->alamat_utama = $value->alamat_utama;
        $newDataDom->save(false);
      }
    }
    return TRUE;
  }

  /*
    * Fungsi untuk duplikasi Data Pengurus Perusahaan.
    * fungsi ini akan dijalankan apabila pada proses verifikasi, verifikator melakukan aksi penolakan, dan perusahaan melakukan pengajuan ulang
    * @param Object $perusahaan = objek dari perusahaan
    * @var $newDataDom = membuat objek data domisili perusahaan baru
    * @return Object
    */
  public function DuplicateDireksi($idPerusahaanBaru, $perusahaanPengurus)
  {
    if ($perusahaanPengurus !== NULL) {
      foreach ($perusahaanPengurus as $key => $value) {
        $newDataDir = new PerusahaanPengurus();
        $newDataDir->perusahaan_id = $idPerusahaanBaru;
        $newDataDir->nik = $value->nik;
        $newDataDir->npwp = $value->npwp;
        // mengambil extensi dari file yang lama dan merename ke data yang baru
        $getExtensionNpwp = explode(".", $value->file_npwp);
        $getExtensionKtp = explode(".", $value->file_ktp);
        $newDataDir->file_npwp = uniqid(time(), true) . '_dokumen_perusahaan_npwp_2' . '.' . end($getExtensionNpwp);
        $newDataDir->file_ktp = uniqid(time(), true) . '_dokumen_perusahaan_ktp_2' . '.' . end($getExtensionKtp);
        // duplikat file yang sebelumnya
        $newDirectory = Yii::getAlias('@uploads/perusahaan/' . $idPerusahaanBaru . '/legalitas/');
        FileHelper::createDirectory($newDirectory);
        if (file_exists(Yii::getAlias('@uploads/perusahaan/' . $value->perusahaan_id . '/legalitas/' . $value->file_npwp))) {
          copy(Yii::getAlias('@uploads/perusahaan/' . $value->perusahaan_id . '/legalitas/' . $value->file_npwp), Yii::getAlias('@uploads/perusahaan/' . $idPerusahaanBaru . '/legalitas/' . $newDataDir->file_npwp));
        }

        if (file_exists(Yii::getAlias('@uploads/perusahaan/' . $value->perusahaan_id . '/legalitas/' . $value->file_ktp))) {
          copy(Yii::getAlias('@uploads/perusahaan/' . $value->perusahaan_id . '/legalitas/' . $value->file_ktp), Yii::getAlias('@uploads/perusahaan/' . $idPerusahaanBaru . '/legalitas/' . $newDataDir->file_ktp));
        }

        $newDataDir->nama_lengkap = $value->nama_lengkap;
        $newDataDir->jabatan = $value->jabatan;
        $newDataDir->periode_mulai = $value->periode_mulai;
        $newDataDir->periode_selesai = $value->periode_selesai;
        $newDataDir->keterangan = $value->keterangan;
        $newDataDir->save();
      }
    }
    return TRUE;
  }

  /*
    * Fungsi untuk duplikasi Data Domisili Perusahaan.
    * fungsi ini akan dijalankan apabila pada proses verifikasi, verifikator melakukan aksi penolakan, dan perusahaan melakukan pengajuan ulang
    * @param Object $perusahaan = objek dari perusahaan
    * @var $newDataDom = membuat objek data domisili perusahaan baru
    * @return Object
    */
  public function DuplicatePengSaham($idPerusahaanBaru, $perusahaanPemegangSaham)
  {
    if ($perusahaanPemegangSaham !== NULL) {
      foreach ($perusahaanPemegangSaham as $key => $value) {
        $newDataPengSaham = new PerusahaanPemegangSaham();
        $newDataPengSaham->perusahaan_id = $idPerusahaanBaru;
        $newDataPengSaham->nama_lengkap = $value->nama_lengkap;
        $newDataPengSaham->persentase = $value->persentase;
        $newDataPengSaham->keterangan = $value->keterangan;
        $newDataPengSaham->nik = $value->nik;
        $newDataPengSaham->npwp = $value->npwp;
        // mengambil extensi dari file yang lama dan merename ke data yang baru
        $getExtensionNpwp = explode(".", $value->file_npwp);
        $getExtensionKtp = explode(".", $value->file_ktp);
        $newDataPengSaham->file_npwp = uniqid(time(), true) . '_dokumen_perusahaan_npwp_2' . '.' . end($getExtensionNpwp);
        $newDataPengSaham->file_ktp = uniqid(time(), true) . '_dokumen_perusahaan_ktp_2' . '.' . end($getExtensionKtp);
        // duplikat file yang sebelumnya
        $newDirectory = Yii::getAlias('@uploads/perusahaan/' . $idPerusahaanBaru . '/legalitas/');
        FileHelper::createDirectory($newDirectory);
        if (file_exists(Yii::getAlias('@uploads/perusahaan/' . $value->perusahaan_id . '/legalitas/' . $value->file_npwp))) {
          copy(Yii::getAlias('@uploads/perusahaan/' . $value->perusahaan_id . '/legalitas/' . $value->file_npwp), Yii::getAlias('@uploads/perusahaan/' . $idPerusahaanBaru . '/legalitas/' . $newDataPengSaham->file_npwp));
        }

        if (file_exists(Yii::getAlias('@uploads/perusahaan/' . $value->perusahaan_id . '/legalitas/' . $value->file_ktp))) {
          copy(Yii::getAlias('@uploads/perusahaan/' . $value->perusahaan_id . '/legalitas/' . $value->file_ktp), Yii::getAlias('@uploads/perusahaan/' . $idPerusahaanBaru . '/legalitas/' . $newDataPengSaham->file_ktp));
        }
        $newDataPengSaham->save();
      }
    }
    return TRUE;
  }

  /*
    * Fungsi untuk duplikasi Dokumen Perusahaan.
    * fungsi ini akan dijalankan apabila pada proses verifikasi, verifikator melakukan aksi penolakan, dan perusahaan melakukan pengajuan ulang
    * @param Object $perusahaan = objek dari perusahaan
    * @var $newDataDom = membuat objek data domisili perusahaan baru
    * @return Object
    */
  public function DuplicateDokumen($idPerusahaanBaru, $perusahaanDokumen)
  {
    if ($perusahaanDokumen !== NULL) {
      foreach ($perusahaanDokumen as $key => $value) {
        $newDataDokumen = new PerusahaanDokumen();
        $newDataDokumen->perusahaan_id = $idPerusahaanBaru;
        $newDataDokumen->jenis_dokumen_perusahaan_id = $value->jenis_dokumen_perusahaan_id;
        $newDataDokumen->nama_dokumen = $value->nama_dokumen;
        $newDataDokumen->no_dokumen = $value->no_dokumen;
        $newDataDokumen->keterangan = $value->keterangan;
        $newDataDokumen->tgl_dokumen = $value->tgl_dokumen;
        // mengambil extensi dari file yang lama dan merename ke data yang baru
        $getExtension = explode(".", $value->filename);
        $newDataDokumen->filename = uniqid(time(), true) . '_dokumen_perusahaan_2' . '.' . end($getExtension);
        // duplikat file yang sebelumnya
        $newDirectory = Yii::getAlias('@uploads/perusahaan/' . $idPerusahaanBaru . '/legalitas/');
        FileHelper::createDirectory($newDirectory);
        if (file_exists(Yii::getAlias('@uploads/perusahaan/' . $value->perusahaan_id . '/legalitas/' . $value->filename))) {
          copy(Yii::getAlias('@uploads/perusahaan/' . $value->perusahaan_id . '/legalitas/' . $value->filename), Yii::getAlias('@uploads/perusahaan/' . $idPerusahaanBaru . '/legalitas/' . $newDataDokumen->filename));
        }
        $newDataDokumen->save();
      }
    }
    return TRUE;
  }
}
