<?php

namespace frontend\controllers;

use Yii;
use common\models\perusahaan\PerusahaanDomisili;
use common\models\perusahaan\PerusahaanDomisiliSearch;
use common\models\perusahaan\Perusahaan;
use common\models\perusahaan\PerusahaanDokumen;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\ActivityLog;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * PerusahaanDokumenController implements the CRUD actions for PerusahaanDomisili model.
 */
class PerusahaanDomisiliController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'getkabupaten'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->layout = '/dashboard/main';
        return parent::beforeAction($action);
    }

    /**
     * Lists all PerusahaanDomisili models.
     * @return mixed
     */
    public function actionIndex()
    {
        ActivityLog::insertLog("View", "", "Menampilkan Daftar Domisili Perusahaan");
        $searchModel = new PerusahaanDomisiliSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PerusahaanDomisili model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->renderAjax('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new PerusahaanDomisili model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($perusahaan_id)
    {
        $model = new PerusahaanDomisili();
        $perusahaanDok = new PerusahaanDokumen();
        $perusahaanDok->scenario = 'create';
        $alamatUtama = PerusahaanDomisili::find()->where(['is_delete' => 0, 'perusahaan_id' => $perusahaan_id, 'alamat_utama' => 1])->exists();
        if ($model->load(Yii::$app->request->post()) && $perusahaanDok->load(Yii::$app->request->post())) {
            $perusahaanDok->jenis_dokumen_perusahaan_id = 9;
            $dokumen = UploadedFile::getInstance($perusahaanDok, 'filename');
            $directory = Yii::getAlias('@uploads/perusahaan/' . $perusahaan_id . '/legalitas/');

            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            if (!empty($dokumen)) {
                $uid = uniqid(time(), true);
                $fileName = $uid . '_dokumen_perusahaan_domisili' . '.' . $dokumen->extension;
                $filePath = $directory . $fileName;

                if ($dokumen->saveAs($filePath)) {
                    $perusahaanDok->filename = $fileName;
                    $perusahaanDok->save();
                }
            } else {
                $perusahaanDok->filename = $oldFileNpwp;
                $perusahaanDok->save();
            }

            if ($model->alamat_utama == 1 && $alamatUtama) {
                $model->alamat_utama = 0;
                $model->save();
                Yii::$app->session->setFlash('warning', 'Data berhasil disimpan, Alamat utama hanya boleh satu');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            } else {
                $model->save();
                Yii::$app->session->setFlash('success', 'Data berhasil disimpan');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            }
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'perusahaan_id' => $perusahaan_id,
                'perusahaanDok' => $perusahaanDok,
            ]);
        }
    }

    /**
     * Updates an existing PerusahaanDomisili model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $perusahaan_id = $model->perusahaan_id;
        $oldPerusahaanDok = PerusahaanDokumen::find()->where(['is_delete' => 0, 'jenis_dokumen_perusahaan_id' => 9, 'perusahaan_id' => $model->perusahaan_id])->orderBy(['created_at' => SORT_DESC])->one();
        if(is_null($oldPerusahaanDok)) {
            $perusahaanDok = new PerusahaanDokumen();
            $oldDomisili = '';
        } else {
            $perusahaanDok = PerusahaanDokumen::find()->where(['is_delete' => 0, 'jenis_dokumen_perusahaan_id' => 9, 'perusahaan_id' => $model->perusahaan_id])->orderBy(['created_at' => SORT_DESC])->one();
            $oldDomisili = $perusahaanDok->filename;
        }
        $alamatUtama = PerusahaanDomisili::find()->where(['is_delete' => 0, 'perusahaan_id' => $perusahaan_id, 'alamat_utama' => 1, 'id' => $model->id])->exists();
        

        if ($model->load(Yii::$app->request->post()) && $perusahaanDok->load(Yii::$app->request->post())) {
            $dokumen = UploadedFile::getInstance($perusahaanDok, 'filename');
            $directory = Yii::getAlias('@uploads/perusahaan/' . $perusahaan_id . '/legalitas/');

            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            if (!empty($dokumen)) {
                $uid = uniqid(time(), true);
                $fileName = $uid . '_dokumen_perusahaan' . '.' . $dokumen->extension;
                $filePath = $directory . $fileName;

                if ($dokumen->saveAs($filePath)) {
                    $perusahaanDok->filename = $fileName;
                    $perusahaanDok->save();
                }
            } else {
                $perusahaanDok->filename = $oldDomisili;
                $perusahaanDok->save();
            }

            if ($model->alamat_utama == 1 && $alamatUtama) {
                $model->save();
                Yii::$app->session->setFlash('success', 'Data berhasil disimpan');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            } elseif ($model->alamat_utama == 1 && PerusahaanDomisili::find()->where(['is_delete' => 0, 'perusahaan_id' => $perusahaan_id, 'alamat_utama' => 1])->exists()) {
                $model->alamat_utama = 0;
                $model->save();
                Yii::$app->session->setFlash('warning', 'Data berhasil disimpan, Alamat utama hanya boleh satu');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            } else {
                $model->save();
                Yii::$app->session->setFlash('success', 'Data berhasil disimpan');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            }
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'perusahaan_id' => $perusahaan_id,
                'perusahaanDok' => $perusahaanDok,
            ]);
        }
    }

    public function actionGetkabupaten()
    {

        if (Yii::$app->request->isAjax) {

            $provinsi_id = Yii::$app->request->post('provinsi_id');

            $kabupaten = \common\models\master\Kabupaten::find()->where(['provinsi_id' => $provinsi_id, 'is_active' => 1, 'is_delete' => 0])->all();

            return \yii\helpers\Json::encode($kabupaten);
        }
    }


    /**
     * Deletes an existing PerusahaanDomisili model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->is_delete = 1;
        $model->save();

        return $this->redirect(['perusahaan/profil-perusahaan']);
    }

    /**
     * Finds the PerusahaanDomisili model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PerusahaanDomisili the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PerusahaanDomisili::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
