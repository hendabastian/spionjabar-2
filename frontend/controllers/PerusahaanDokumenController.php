<?php

namespace frontend\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\perusahaan\PerusahaanDokumen;
use common\models\perusahaan\Perusahaan;
use common\models\master\JenisDokumenPerusahaan;
use common\models\master\JenisDokumenPerusahaanSearch;
use common\models\ActivityLog;

class PerusahaanDokumenController extends \yii\web\Controller
{
	/**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view', 'ajukan-dokumen', 'ajukan-kembali'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(), 
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	/**
     * Lists semua jenis dokumen perusahaan yang harus diupload.
     * @return mixed
     */
    public function actionIndex()
    {
        ActivityLog::insertLog("View", "", "Menampilkan Daftar Dokumen Perusahaan");
    	$this->layout = "dashboard/main";
        $perusahaan = Perusahaan::find()->where(['is_delete' => 0, 'user_id' => Yii::$app->user->identity->id])->one();
    	$searchModel = new JenisDokumenPerusahaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('is_active=1');
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'perusahaan' => $perusahaan,
        ]);
    }

	/**
     * Creates a new Dokumen Perusahaan model.
     * If creation is successful, this function send response json true
     * @return mixed
     */
    public function actionCreate($jenisDokumenPerusahaanId)
    {
    	$model = new PerusahaanDokumen();
        $model->jenis_dokumen_perusahaan_id = $jenisDokumenPerusahaanId;
        $id_perusahaan = Perusahaan::find()->where(['is_delete' => 0, 'user_id' => Yii::$app->user->identity->id])->orderBy(['id' => SORT_DESC])->one();
        $model->perusahaan_id = $id_perusahaan->id;
        if ($model->load(Yii::$app->request->post())) {

            $dokumen = UploadedFile::getInstance($model, 'filename');
            $directory = Yii::getAlias('@uploads/perusahaan/'.$id_perusahaan->id.'/legalitas/');
            
            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            if (!empty($dokumen)) {
                $uid = uniqid(time(), true);
                $fileName = $uid . '_dokumen_perusahaan' . '.' . $dokumen->extension;
                $filePath = $directory . $fileName;

                if ($dokumen->saveAs($filePath)) {
                    $model->filename = $fileName;

                    if($model->save(false)){
                        Yii::$app->session->setFlash('success', 'Dokumen Berhasil DiUpload');
                        return $this->redirect(['perusahaan/profil-perusahaan', 'id' => $id_perusahaan->id]);
                    }else{
                        Yii::$app->session->setFlash('danger', 'Dokumen Gagal DiUpload');
                        return $this->redirect(['perusahaan/profil-perusahaan', 'id' => $id_perusahaan->id]);
                    }

                }
            }

        }elseif(Yii::$app->request->isAjax){
            return $this->renderAjax('_formUploadDokumen', [
                'model' => $model,
                'jenisDokumenPerusahaanId' => $jenisDokumenPerusahaanId
            ]);
        }
    }

    /**
     * Update a new Dokumen Perusahaan model.
     * If creation is successful, this function send response json true
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	$model = $this->findModel($id);
    	$oldDokumen = $model->filename;
        $jenisDokumenPerusahaanId = $model->jenis_dokumen_perusahaan_id;
        $id_perusahaan = Perusahaan::find()->where(['is_delete' => 0, 'user_id' => Yii::$app->user->identity->id])->one();

        if ($model->load(Yii::$app->request->post())) {

            $dokumen = UploadedFile::getInstance($model, 'filename');
            $directory = Yii::getAlias('@uploads/perusahaan/'.$id_perusahaan->id.'/legalitas/');
            
            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            if (!empty($dokumen)) {
                $uid = uniqid(time(), true);
                $fileName = $uid . '_dokumen_perusahaan' . '.' . $dokumen->extension;
                $filePath = $directory . $fileName;

                if ($dokumen->saveAs($filePath)) {
                    $model->filename = $fileName;

                    if($model->save()){
                        Yii::$app->session->setFlash('success', 'Dokumen Berhasil DiUpload');
                        return $this->redirect(['perusahaan/profil-perusahaan', 'id' => $id_perusahaan->id]);
                    }else{
                        Yii::$app->session->setFlash('danger', 'Dokumen Gagal DiUpload');
                        return $this->redirect(['perusahaan/profil-perusahaan', 'id' => $id_perusahaan->id]);
                    }

                }
            }else{
            	$model->filename = $oldDokumen;
            	$model->save();
            	$response = true;
                return json_encode($response);
            }

        }elseif(Yii::$app->request->isAjax){
            return $this->renderAjax('_formUploadDokumen', [
                'model' => $model,
                'jenisDokumenPerusahaanId' => $jenisDokumenPerusahaanId
            ]);
        }
    }

    public function actionView($id)
    {
    	$model = $this->findModel($id);
    	if (Yii::$app->request->isAjax) {
    		return $this->renderAjax('view', [
    			'model' => $model,
    		]);
    	}
    }

    /**
     * proses pengajuan dokumen ke verifikator 
     * @param integer $id_perusahaan
     * @var $model = Model dari PerusahaanDokumen
     * @var $perusahaan = Model dari Perusahaan
     * @var $dokVerifikasi = Model dari PerusahaanDokumenVerifikasi
     * @var $dokVerifDetail = Model dari PerusahaanDOkumenVerifikasiDetail
     * @return mixed
     */
    public function actionAjukanDokumen($id_perusahaan)
    {
        $model = PerusahaanDokumen::find()->where(['perusahaan_id' => $id_perusahaan, 'is_delete' => 0])->all();
        $perusahaan = Perusahaan::findOne($id_perusahaan);
        $dokVerifikasi = new PerusahaanDokumenVerifikasi();

        if (count($model) !== 7) {
            echo "<h3>Silahkan lengkapi Dokumen Perusahaan terlebih dahulu</h3>";
            echo "<br>";
            echo \yii\helpers\Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger btn-sm', 'data-dismiss' => 'modal']);
            return false;   
        }

        if ($dokVerifikasi->load(Yii::$app->request->post())) {
            foreach ($model as $key => $value) {
                $value->status_verifikasi = 1; // status dari dokumennya diubah menjadi diajukan
                $value->save();
                $perusahaan->is_verified = 1; // status perusahaannya diubah menjadi diajukan
                $perusahaan->save();
                
                // merecord proses pengajuan ke tabel perusahaan dokumen verifikasi
                $dokVerifikasi->perusahaan_id = $perusahaan->id;
                $dokVerifikasi->tgl_verifikasi = new \yii\db\Expression('NOW()');
                $dokVerifikasi->status_verifikasi = 1;
                $dokVerifikasi->save();

                $dokVerifDetail = new PerusahaanDokumenVerifikasiDetail();
                // merecord proses pengajuan ke tabel perusahaan dokumen verifikasi detail
                $dokVerifDetail->perusahaan_dok_id = $value->id; // id dokumen perusahaan
                $dokVerifDetail->perusahaan_dok_verif_id = $dokVerifikasi->id; //id dokumen perusahaan verifikasi
                $dokVerifDetail->catatan = "";
                $dokVerifDetail->status = 1; // status diubah menjadi diajukan

                $response = "";
                if ($dokVerifDetail->save(false)) {
                    $response = TRUE;
                }else{
                    $response = FALSE;
                }
            }
            
            return json_encode($response);
        }elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('_formPengajuanDok', [
                'dokVerifikasi' => $dokVerifikasi,
            ]);
        }
    }

    public function actionAjukanKembali($id_perusahaan)
    {
        $model = PerusahaanDokumen::find()->where(['perusahaan_id' => $id_perusahaan, 'is_delete' => 0])->all();
        $perusahaan = Perusahaan::findOne($id_perusahaan);
        $dokVerifikasi = new PerusahaanDokumenVerifikasi();

        if ($dokVerifikasi->load(Yii::$app->request->post())) {
            foreach ($model as $key => $value) {
                if ($value->status_verifikasi == 3) {
                    $value->status_verifikasi = 1;
                }
                $value->save();
                $perusahaan->is_verified = 1; // status perusahaannya diubah menjadi diajukan
                $perusahaan->save();
                
                // merecord proses pengajuan ke tabel perusahaan dokumen verifikasi
                $dokVerifikasi->perusahaan_id = $perusahaan->id;
                $dokVerifikasi->tgl_verifikasi = new \yii\db\Expression('NOW()');
                $dokVerifikasi->status_verifikasi = 1;
                $dokVerifikasi->save();

                $dokVerifDetail = new PerusahaanDokumenVerifikasiDetail();
                // merecord proses pengajuan ke tabel perusahaan dokumen verifikasi detail
                $dokVerifDetail->perusahaan_dok_id = $value->id; // id dokumen perusahaan
                $dokVerifDetail->perusahaan_dok_verif_id = $dokVerifikasi->id; //id dokumen perusahaan verifikasi
                $dokVerifDetail->status = $value->status_verifikasi; // status di dokumen verifikasi detail diisi dengan status sebelumnya
                $dokVerifDetail->catatan = "";

                $response = "";
                if ($dokVerifDetail->save(false)) {
                    $response = TRUE;
                }else{
                    $response = FALSE;
                }
            }

            return json_encode($response);
        }elseif(Yii::$app->request->isAjax){
            return $this->renderAjax('_formPengajuanDok', [
                'dokVerifikasi' => $dokVerifikasi,
            ]);
        }
    }

    /**
     * Finds the PagesData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PagesData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PerusahaanDokumen::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
