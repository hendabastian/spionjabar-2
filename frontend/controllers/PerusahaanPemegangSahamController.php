<?php

namespace frontend\controllers;

use Yii;
use common\models\perusahaan\PerusahaanPemegangSaham;
use common\models\perusahaan\PerusahaanPemegangSahamSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\ActivityLog;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * PerusahaanPemegangSahamController implements the CRUD actions for PerusahaanPemegangSaham model.
 */
class PerusahaanPemegangSahamController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->layout = '/dashboard/main';
        return parent::beforeAction($action);
    }

    /**
     * Lists all PerusahaanPemegangSaham models.
     * @return mixed
     */
    public function actionIndex()
    {
        ActivityLog::insertLog("View", "", "Menampilkan Daftar Pemegang Saham");
        $searchModel = new PerusahaanPemegangSahamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PerusahaanPemegangSaham model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PerusahaanPemegangSaham model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($perusahaan_id)
    {
        $model = new PerusahaanPemegangSaham();
        if ($model->load(Yii::$app->request->post())) {
            $fileNpwp = UploadedFile::getInstance($model, 'file_npwp');
            $fileKtp = UploadedFile::getInstance($model, 'file_ktp');
            $directory = Yii::getAlias('@uploads/perusahaan/' . $perusahaan_id . '/legalitas/');


            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            if (!empty($fileNpwp)) {
                $uid = uniqid(time(), true);
                $fileName = $uid . '_dokumen_perusahaan_npwp' . '.' . $fileNpwp->extension;
                $filePath = $directory . $fileName;

                if ($fileNpwp->saveAs($filePath)) {
                    $model->file_npwp = $fileName;
                }
            }

            if (!empty($fileKtp)) {
                $uid = uniqid(time(), true);
                $fileNameKtp = $uid . '_dokumen_perusahaan_ktp' . '.' . $fileKtp->extension;
                $filePathKtp = $directory . $fileNameKtp;

                if ($fileKtp->saveAs($filePathKtp)) {
                    $model->file_ktp = $fileNameKtp;
                }
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Data Berhasil Disimpan');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            } else {
                Yii::$app->session->setFlash('danger', 'Data Gagal disimpan');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            }
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'perusahaan_id' => $perusahaan_id,
            ]);
        }
    }

    /**
     * Updates an existing PerusahaanPemegangSaham model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $perusahaan_id = $model->perusahaan_id;
        $oldKtp = $model->file_ktp;
        $oldNpwp = $model->file_npwp;
        if ($model->load(Yii::$app->request->post())) {
            $fileNpwp = UploadedFile::getInstance($model, 'file_npwp');
            $fileKtp = UploadedFile::getInstance($model, 'file_ktp');
            $directory = Yii::getAlias('@uploads/perusahaan/' . $perusahaan_id . '/legalitas/');

            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            if (!empty($fileNpwp)) {
                $uid = uniqid(time(), true);
                $fileName = $uid . '_dokumen_perusahaan_npwp' . '.' . $fileNpwp->extension;
                $filePath = $directory . $fileName;

                if ($fileNpwp->saveAs($filePath)) {
                    $model->file_npwp = $fileName;
                }
            } else {
                $model->file_npwp = $oldNpwp;
            }

            if (!empty($fileKtp)) {
                $uid = uniqid(time(), true);
                $fileNameKtp = $uid . '_dokumen_perusahaan_ktp' . '.' . $fileKtp->extension;
                $filePathKtp = $directory . $fileNameKtp;

                if ($fileKtp->saveAs($filePathKtp)) {
                    $model->file_ktp = $fileNameKtp;
                }
            } else {
                $model->file_ktp = $oldKtp;
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Data Berhasil Disimpan');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            } else {
                Yii::$app->session->setFlash('danger', 'Data Gagal disimpan');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            }
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'perusahaan_id' => $perusahaan_id,
            ]);
        }
    }

    /**
     * Deletes an existing PerusahaanPemegangSaham model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->is_delete = 1;
        $model->save();
        return $this->redirect(['perusahaan/profil-perusahaan']);
    }

    /**
     * Finds the PerusahaanPemegangSaham model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PerusahaanPemegangSaham the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PerusahaanPemegangSaham::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
