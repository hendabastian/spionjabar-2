<?php

namespace frontend\controllers;

use Yii;
use common\models\perusahaan\PerusahaanPengurus;
use common\models\perusahaan\PerusahaanPengurusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\ActivityLog;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * PerusahaanPengurusController implements the CRUD actions for PerusahaanPengurus model.
 */
class PerusahaanPengurusController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->layout = '/dashboard/main';
        return parent::beforeAction($action);
    }

    /**
     * Lists all PerusahaanPengurus models.
     * @return mixed
     */
    public function actionIndex()
    {
        ActivityLog::insertLog("View", "", "Menampilkan Daftar Pengurus Perusahaan");
        $searchModel = new PerusahaanPengurusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PerusahaanPengurus model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PerusahaanPengurus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($perusahaan_id)
    {
        $model = new PerusahaanPengurus();
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $fileNpwp = UploadedFile::getInstance($model, 'file_npwp');
            $fileKtp = UploadedFile::getInstance($model, 'file_ktp');
            $directory = Yii::getAlias('@uploads/perusahaan/' . $perusahaan_id . '/legalitas/');
            $model->periode_mulai = $_POST['PerusahaanPengurus']['periode_mulai'];
            $model->periode_selesai = $_POST['PerusahaanPengurus']['periode_selesai'];

            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            if (!empty($fileNpwp)) {
                $uid = uniqid(time(), true);
                $fileName = $uid . '_dokumen_perusahaan_npwp' . '.' . $fileNpwp->extension;
                $filePath = $directory . $fileName;

                if ($fileNpwp->saveAs($filePath)) {
                    $model->file_npwp = $fileName;
                }
            }

            if (!empty($fileKtp)) {
                $uid = uniqid(time(), true);
                $fileNameKtp = $uid . '_dokumen_perusahaan_ktp' . '.' . $fileKtp->extension;
                $filePathKtp = $directory . $fileNameKtp;

                if ($fileKtp->saveAs($filePathKtp)) {
                    $model->file_ktp = $fileNameKtp;
                }
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Data Berhasil Disimpan');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            } else {
                Yii::$app->session->setFlash('danger', 'Data Gagal disimpan');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            }
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'perusahaan_id' => $perusahaan_id,
            ]);
        }
    }

    /**
     * Updates an existing PerusahaanPengurus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $perusahaan_id = $model->perusahaan_id;
        $oldNpwp = $model->file_npwp;
        $oldKtp = $model->file_ktp;
        if ($model->load(Yii::$app->request->post())) {

            $fileNpwp = UploadedFile::getInstance($model, 'file_npwp');
            $fileKtp = UploadedFile::getInstance($model, 'file_ktp');
            $directory = Yii::getAlias('@uploads/perusahaan/' . $perusahaan_id . '/legalitas/');

            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            if (!empty($fileNpwp)) {
                $uid = uniqid(time(), true);
                $fileName = $uid . '_dokumen_perusahaan_npwp' . '.' . $fileNpwp->extension;
                $filePath = $directory . $fileName;

                if ($fileNpwp->saveAs($filePath)) {
                    $model->file_npwp = $fileName;
                }
            } else {
                $model->file_npwp = $oldNpwp;
            }

            if (!empty($fileKtp)) {
                $uid = uniqid(time(), true);
                $fileNameKtp = $uid . '_dokumen_perusahaan_ktp' . '.' . $fileKtp->extension;
                $filePathKtp = $directory . $fileNameKtp;

                if ($fileKtp->saveAs($filePathKtp)) {
                    $model->file_ktp = $fileNameKtp;
                }
            } else {
                $model->file_ktp = $oldKtp;
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Data Berhasil Disimpan');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            } else {
                Yii::$app->session->setFlash('danger', 'Data Gagal disimpan');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            }
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'perusahaan_id' => $perusahaan_id,
            ]);
        }
    }

    /**
     * Deletes an existing PerusahaanPengurus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->is_delete = 1;
        $model->save();

        return $this->redirect(['perusahaan/profile-perusahaan']);
    }

    /**
     * Finds the PerusahaanPengurus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PerusahaanPengurus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PerusahaanPengurus::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
