<?php

namespace frontend\controllers;

use frontend\models\perusahaan\SkPerusahaanKendaraanSearch;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use frontend\modules\permohonan\models\PermohonanSearch;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\master\Kabupaten;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\perusahaan\Perusahaan;
use yii\helpers\Json;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            // change layout for error action
            if ($action->id == 'error' && !Yii::$app->user->isGuest) $this->layout = 'dashboard/main';
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'maxLength' => 4,
                'minLength' => 4
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ContactForm();

        if ($model->load(Yii::$app->request->post()) && $model->sendEmail(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('success', 'Thank you for getting in touch!.');
            return $this->redirect(['site/index#contact']);
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['dashboard']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['dashboard']);
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionDashboard()
    {
        $this->layout = 'dashboard/main';

        if (!Yii::$app->user->isGuest) {
            return $this->render('dashboard');
        } else {
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        $listJenisBadanUsaha = \yii\helpers\ArrayHelper::map(\common\models\master\JenisBadanUsaha::find()->where(['is_delete' => 0])->all(), 'id', 'jenis_badan_usaha');
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Terima Kasih, silahkan buka email anda untuk mengetahui langkah selanjutnya.');
            return $this->refresh();
        }

        return $this->render('signup', [
            'model' => $model,
            'items' => $listJenisBadanUsaha,
        ]);
    }

    public function actionGetKabupaten()
    {
        if (Yii::$app->request->isAjax) {
            $provinsiId = Yii::$app->request->post('provinsi_id');
            $getKabupaten = Kabupaten::find()->where(['provinsi_id' => $provinsiId, 'is_active' => 1, 'is_delete' => 0])->all();
            return Json::encode($getKabupaten);
        }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Terima Kasih, silahkan buka email anda untuk mengetahui langkah selanjutnya.');

                return $this->redirect(['request-password-reset']);
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionReportKendaraan()
    {
        $this->layout = 'dashboard/main';
        $searchModel = new SkPerusahaanKendaraanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->joinWith('sk')->andWhere(['t_sk_perusahaan.perusahaan_id' => Yii::$app->user->identity->perusahaan->id]);

        return $this->render('report-kendaraan', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionReportPermohonan()
    {
        $this->layout = 'dashboard/main';
        $searchModel = new PermohonanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['perusahaan_id' => Yii::$app->user->identity->perusahaan->id]);

        return $this->render('report-permohonan', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Password Baru Berhasil Disimpan.');

            return $this->redirect(['login']);
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail() !== NULL) {
            Yii::$app->session->setFlash('success', 'Terimakasih, Akun Anda Telah Terverifikasi!');
            return $this->redirect(['login']);
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Silahkan.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    /**
     * Error action.
     * @var $exception Error handler
     * @return string
     */
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null && !Yii::$app->user->isGuest) {
            $this->layout = 'dashboard/main';
            return $this->render('error', ['exception' => $exception]);
        } elseif ($exception !== null && Yii::$app->user->isGuest) {
            $this->layout = 'main';
            return $this->render('error', ['exception' => $exception]);
        }
    }

    public function actionCariPerusahaan()
    {
        $request = Yii::$app->request;
        $name = $request->get('nama-perusahaan');
        $searchModel = new PerusahaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => 1])->andWhere(['LIKE', 'nama_perusahaan', $name]);

        return $this->render('_hasil_perusahaan', [
            'name' => $name,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCariKendaraan()
    {
        $request = Yii::$app->request;
        $name = $request->get('nomor-kendaraan');
        $searchModel = new SkPerusahaanKendaraanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['LIKE', 'no_kendaraan', $name]);

        return $this->render('_hasil_kendaraan', [
            'name' => $name,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
