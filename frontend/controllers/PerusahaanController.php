<?php

namespace frontend\controllers;

use backend\modules\master\models\JenisDokumenPerusahaanSearch;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use common\models\perusahaan\Perusahaan;
use common\models\ActivityLog;
use common\models\perusahaan\PerusahaanDokumen;
use frontend\models\perusahaan\PerusahaanDomisiliSearch;
use frontend\models\perusahaan\PerusahaanPemegangSahamSearch;
use frontend\models\perusahaan\PerusahaanPengurusSearch;
use frontend\models\perusahaan\SkPerusahaanSearch;

/**
 * Perusahaan Controller
 */
class PerusahaanController extends Controller
{

  /**
   * {@inheritdoc}
   */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'actions' => ['profil-perusahaan', 'sunting-data', 'history-verifikasi'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  public function beforeAction($action)
  {
    $this->layout = '/dashboard/main';
    return parent::beforeAction($action);
  }

  /**
   * Page Profile Perusahaan
   * @var $model refers to Perusahaan Model
   * @var perusahaan_id
   */
  public function actionProfilPerusahaan()
  {
    // ActivityLog::insertLog("view", "", "Menampilkan Profil Perusahaan");
    $perusahaan_id = User::findOne(Yii::$app->user->identity->id);
    if ($perusahaan_id !== NULL) {
      $model = Perusahaan::find()->where(['is_delete' => 0, 'user_id' => $perusahaan_id->id, 'status' => 1])->one();

      // data domisili
      $searchDomisili = new PerusahaanDomisiliSearch();
      $dataDomisili = $searchDomisili->search(Yii::$app->request->queryParams);
      $dataDomisili->query->andWhere('perusahaan_id=' . $model->id);

      // data pengurus
      $searchPengurus = new PerusahaanPengurusSearch();
      $dataPengurus = $searchPengurus->search(Yii::$app->request->queryParams);
      $dataPengurus->query->andWhere('perusahaan_id=' . $model->id);

      // data pemegang saham
      $searchPemegangSaham = new PerusahaanPemegangSahamSearch();
      $dataPemegangSaham = $searchPemegangSaham->search(Yii::$app->request->queryParams);
      $dataPemegangSaham->query->andWhere('perusahaan_id=' . $model->id);

      // dokumen perusahaan
      $searchDokumen = new JenisDokumenPerusahaanSearch();
      $dataDokumen = $searchDokumen->search(Yii::$app->request->queryParams);
      $dataDokumen->query->andWhere('is_active=1');

      //sk perusahaan
      $searchSk = new SkPerusahaanSearch();
      $dataSk = $searchSk->search(Yii::$app->request->queryParams);
      $dataSk->query->andWhere('is_active=1')->andWhere(['perusahaan_id' => $model->id]);

      // file npwp perusahaan
      $perusahaanDok = PerusahaanDokumen::find()->where(['is_delete' => 0, 'perusahaan_id' => $model->id, 'jenis_dokumen_perusahaan_id' => 2])->one();

      return $this->render('profile-perusahaan', [
        'model' => $model,
        'searchDomisili' => $searchDomisili,
        'dataDomisili' => $dataDomisili,
        'searchPengurus' => $searchPengurus,
        'dataPengurus' => $dataPengurus,
        'searchPemegangSaham' => $searchPemegangSaham,
        'dataPemegangSaham' => $dataPemegangSaham,
        'searchDokumen' => $searchDokumen,
        'dataDokumen' => $dataDokumen,
        'perusahaanDok' => $perusahaanDok,
        'dataSk' => $dataSk,
        'searchSk' => $searchSk,
      ]);
    }
  }

  /**
   * Sunting Data Profile Perusahaan
   * @var $model refers to Perusahaan Model
   * @param $id = id perusahaan
   */
  public function actionSuntingData($id)
  {
    $model = Perusahaan::findOne($id);
    $user = User::findOne($model->user_id);
    $perusahaanDok = new PerusahaanDokumen();
    $perusahaanDok->scenario = "create";
    $oldFileNpwp = "";
    if (PerusahaanDokumen::find()->where(['is_delete' => 0, 'perusahaan_id' => $id, 'jenis_dokumen_perusahaan_id' => 2])->one() !== NULL) {
      $perusahaanDok = PerusahaanDokumen::find()->where(['is_delete' => 0, 'perusahaan_id' => $id, 'jenis_dokumen_perusahaan_id' => 2])->one();
      $oldFileNpwp = $perusahaanDok->filename;
    }

    if ($model->load(Yii::$app->request->post()) && $perusahaanDok->load(Yii::$app->request->post())) {
      $dokumen = UploadedFile::getInstance($perusahaanDok, 'filename');
      $directory = Yii::getAlias('@uploads/perusahaan/' . $id . '/legalitas/');
      if ($model->validate()) {
        $model->save();
        $user->email = $model->email;
        $user->username = $model->email;
        $user->save();
        $perusahaanDok->perusahaan_id = $id;
        $perusahaanDok->nama_dokumen = "NPWP Perusahaan";
        $perusahaanDok->no_dokumen = $model->npwp;
        $perusahaanDok->jenis_dokumen_perusahaan_id = 2; // id dari jenis dokumen npwp

        if (!is_dir($directory)) {
          FileHelper::createDirectory($directory);
        }

        if (!empty($dokumen)) {
          $uid = uniqid(time(), true);
          $fileName = $uid . '_dokumen_perusahaan_npwp' . '.' . $dokumen->extension;
          $filePath = $directory . $fileName;

          if ($dokumen->saveAs($filePath)) {
            $perusahaanDok->filename = $fileName;

            if ($perusahaanDok->save(false)) {
              Yii::$app->session->setFlash('success', 'Data berhasil disimpan');
              return $this->redirect(['perusahaan/profil-perusahaan']);
            } else {
              Yii::$app->session->setFlash('danger', 'Data Gagal disimpan');
              return $this->redirect(['perusahaan/profil-perusahaan']);
            }
          }
        } else {
          $perusahaanDok->filename = $oldFileNpwp;
          $perusahaanDok->save();
        }


        Yii::$app->session->setFlash('success', 'Data berhasil disimpan');
        return $this->redirect(['perusahaan/profil-perusahaan']);
      } else {
        $model->errors !== NULL ? Yii::$app->session->setFlash('danger', 'Gagal Menyimpan perubahan, ' . $model->errors['email'][0]) : Yii::$app->session->setFlash('danger', 'Gagal Menyimpan perubahan');
        return $this->redirect(['perusahaan/profil-perusahaan']);
      }
    } elseif (Yii::$app->request->isAjax) {
      return $this->renderAjax('_formSuntingData', [
        'model' => $model,
        'perusahaanDok' => $perusahaanDok,
      ]);
    }
  }

  /**
   * Fungsi untuk menampilkan history Verifikasi
   * mengambil data perusahaan dengan user_id karena kalo dengan perusahaan id data perusahaan yang sebelumnya tidak akan muncul
   * @param int $id = user_id
   */
  public function actionHistoryVerifikasi($id)
  {
    $decoded_id = base64_decode($id) / 2;
    $perusahaan = Perusahaan::find()->where(['is_delete' => 0, 'user_id' => $decoded_id])->all();
    if ($perusahaan !== NULL) {
      return $this->renderAjax('_history_verifikasi', [
        'perusahaan' => $perusahaan,
      ]);
    } else {
      return "Empty";
    }
  }
}
