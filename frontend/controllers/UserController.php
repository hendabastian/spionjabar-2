<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use common\models\ActivityLog;

/**
 * User Controller
 */
class UserController extends Controller
{
	/*
	*
    * {@inheritdoc}
    */
	public function behaviors()
	{
		return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['profile', 'change-password'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
	}

	/**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Profile Page Perusahaan Otobus
     * @param user_id
     * @return mixed
     */
    public function actionProfile()
    {
        if (!Yii::$app->user->isGuest) {
            ActivityLog::insertLog("View", "", "Menampilkan Profil");
            $this->layout = 'dashboard/main';

            $user = \common\models\User::findOne(Yii::$app->user->identity->id);
            $po = \common\models\perusahaan\Perusahaan::find()->where(['is_delete' => 0, 'user_id' => $user->id])->one();

            if($user->load(Yii::$app->request->post()) && $po->load(Yii::$app->request->post())){
                if ($user->save()) {
                    Yii::$app->session->setFlash('success', 'Data Profile Berhasil Diupdate.');
                    $po->save();
                    return $this->refresh();
                }else{
                    Yii::$app->session->setFlash('error', 'Data Profile Gagal Diupdate.');
                }
            }

            return $this->render('profile', [
                'user' => $user,
                'po' => $po,
            ]);
        }
    }

    /**
     * Change Password Page Perusahaan Otobus
     * @param user_id
     * @return mixed
     */
    public function actionChangePassword($id)
    {
    	$this->layout = 'dashboard/main';

			$encoded_id = base64_decode($id) / 20 / 200;
    	$user = User::findOne($encoded_id);
    	$user->scenario = 'change-password';
    	if ($user->load(Yii::$app->request->post()) && $user->validate()) {
    		$user->setPassword($user->new_password);
    		if($user->validatePassword($user->old_password)){
                $user->save();
                Yii::$app->session->setFlash('success', 'Password Berhasil Diupdate.');
    		    return $this->redirect('profile');
            }else{
                Yii::$app->session->setFlash('danger', 'Password Gagal Diupdate.');
    		    return $this->redirect('profile');
            }
    	}elseif(Yii::$app->request->isAjax){
            return $this->renderAjax('_formChangePassword', [
            'user' => $user,
            ]);
        }
    }


}
