<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\master\Regulasi;
use frontend\models\master\RegulasiSearch;

/**
 * Regulasi Controller, controller ini hanya menampilkan data Regulasi Perizinan
 */
class RegulasiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'download', 'preview-dok'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->layout = '/dashboard/main';
        return parent::beforeAction($action);
    }

    /**
     * Lists all Regulasi models.
     * @param jenis_layanan_id
     * @return mixed
     */
    public function actionIndex($jenisLayananId)
    {
//        ActivityLog::insertLog("View", "", "Menampilkan Daftar Regulasi");
        $searchModel = new RegulasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['jenis_layanan_id' => $jenisLayananId]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'jenisLayananId' => $jenisLayananId
        ]);
    }

    /**
     * Displays a single Regulasi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Download dokumen regulasi.
     * @param integer $path
     * @return file
     */
    public function actionDownload($path, $file)
    {
        $path = (explode('/', $path));
        $result = array_slice($path, 5);
        $fullpath = Yii::getAlias('@uploads/') . implode("/", $result) . $file;

        if (file_exists($fullpath)) {
            return Yii::$app->response->sendFile($fullpath);
        } else {
            return 'File Not Found';
        }
    }

    /**
     * Preview dokumen regulasi.
     * @param integer $path
     * @return file
     */
    public function actionPreviewDok($path, $file)
    {
        $array = (explode('/', $path));
        $result = array_slice($array, 5);
        $fullpath = Yii::getAlias('@uploads/') . implode("/", $result) . $file;

        return $this->renderAjax('preview-dok', [
            'path' => $path,
            'file' => $file,
            'fullpath' => $fullpath,
        ]);
    }

    /**
     * Finds the Regulasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Regulasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Regulasi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
