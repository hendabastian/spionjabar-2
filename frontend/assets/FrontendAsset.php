<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class FrontendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'frontend/css/bootstrap.css',
        'frontend/css/font-awesome.css',
        'frontend/owl-carousel/assets/owl.carousel.min.css',
        'frontend/css/style.css'
    ];
    public $js = [
        'frontend/js/bootstrap.js',
        'frontend/js/popper.min.js',
        'frontend/owl-carousel/owl.carousel.min.js',
        'frontend/js/main.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
//        'yii\web\JqueryAsset',
//        'yii\bootstrap4\BootstrapAsset',
    ];
}
