<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
      /* Bootstrap */
     'gentelella-master/vendors/bootstrap/dist/css/bootstrap.min.css',
      /* Font Awesome */
      'gentelella-master/vendors/font-awesome/css/font-awesome.min.css',
      /* NProgress */
      'gentelella-master/vendors/nprogress/nprogress.css',
      /* iCheck */
      'gentelella-master/vendors/iCheck/skins/flat/green.css',

      /* bootstrap-progressbar */
      'gentelella-master/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css',
      /* JQVMap */
      'gentelella-master/vendors/jqvmap/dist/jqvmap.min.css',
      /* bootstrap-daterangepicker */
      'gentelella-master/vendors/bootstrap-daterangepicker/daterangepicker.css',

      /* custom css developer */
      'css/custom.css',

      'gentelella-master/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
      //<!-- jvectormap -->
      // 'jvectormap/jquery-jvectormap-1.2.2.css',

      /* Custom Theme Style */
      'gentelella-master/build/css/custom.min.css',

      /* CSS Custom By Digitak Team */
      'css/site.css',
      'css/timeline.css',
    ];
    public $js = [
      /* jQuery */
//      'gentelella-master/vendors/jquery/dist/jquery.min.js',
      /* Bootstrap */
      'gentelella-master/vendors/bootstrap/dist/js/bootstrap.min.js',
      /* FastClick */
      'gentelella-master/vendors/fastclick/lib/fastclick.js',

      'gentelella-master/vendors/datatables.net/js/jquery.dataTables.min.js',
      'gentelella-master/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
      /* NProgress */
      'gentelella-master/vendors/nprogress/nprogress.js',

      /* custom js developer */
      // 'js/custom.js',

      /* custom js developer */
    //   'js/upload_image.js',

      /* Chart.js */
      'gentelella-master/vendors/Chart.js/dist/Chart.min.js',

      /* gauge.js */
      'gentelella-master/vendors/gauge.js/dist/gauge.min.js',

      /* bootstrap-progressbar */
      'gentelella-master/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',

      /* iCheck */
      'gentelella-master/vendors/iCheck/icheck.min.js',

      /* Skycons */
      'gentelella-master/vendors/skycons/skycons.js',

      /* Flot */
      'gentelella-master/vendors/Flot/jquery.flot.js',
      'gentelella-master/vendors/Flot/jquery.flot.pie.js',
      'gentelella-master/vendors/Flot/jquery.flot.time.js',
      'gentelella-master/vendors/Flot/jquery.flot.stack.js',
      'gentelella-master/vendors/Flot/jquery.flot.resize.js',
      /* Flot plugins */
      'gentelella-master/vendors/flot.orderbars/js/jquery.flot.orderBars.js',
      'gentelella-master/vendors/flot-spline/js/jquery.flot.spline.min.js',
      'gentelella-master/vendors/flot.curvedlines/curvedLines.js',
      /* DateJS */
      'gentelella-master/vendors/DateJS/build/date.js',
      // 'jvectormap/jquery-jvectormap-1.2.2.min.js',
      // 'jvectormap/jquery-jvectormap-world-mill-en.js',
      //
      // 'AdminLTE/bower_components/jquery-knob/dist/jquery.knob.min.js',
      /* JQVMap */
      'gentelella-master/vendors/jqvmap/dist/jquery.vmap.js',
      'gentelella-master/vendors/jqvmap/dist/maps/jquery.vmap.world.js',
      'gentelella-master/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js',
      /* bootstrap-daterangepicker */
      'gentelella-master/vendors/moment/min/moment.min.js',
      'gentelella-master/vendors/bootstrap-daterangepicker/daterangepicker.js',

      /* Custom Theme Scripts */
      'gentelella-master/build/js/custom.min.js',


      /* Ajax Modal Popup */
      'js/ajax-modal-popup.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'unclead\multipleinput\assets\MultipleInputAsset'
    ];
}
