<?php

namespace frontend\models\master;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\master\Regulasi;

/**
 * RegulasiSearch represents the model behind the search form of `common\models\master\Regulasi`.
 */
class RegulasiSearch extends Regulasi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'jenis_layanan_id', 'is_publish', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['nama_regulasi', 'tahun', 'dok_regulasi', 'keterangan', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Regulasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'jenis_layanan_id' => $this->jenis_layanan_id,
            'is_publish' => $this->is_publish,
            'is_delete' => $this->is_delete,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nama_regulasi', $this->nama_regulasi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'dok_regulasi', $this->dok_regulasi])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}