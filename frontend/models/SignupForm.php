<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\perusahaan\Perusahaan;
use common\models\perusahaan\PerusahaanDomisili;
use common\models\perusahaan\PerusahaanPengurus;
use yii\db\Expression;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $jenis_angkutan_id;
    public $kode_perusahaan;
    public $nama_perusahaan;
    public $npwp;
    public $hp;
    public $jenis_badan_usaha_id;
    public $captcha;
    public $provinsi_id;
    public $kabupaten_id;
    public $fax;
    public $alamat;
    public $kode_pos;
    public $nama_pengurus;
    public $npwp_pengurus;
    public $nik_pengurus;
    public $hp_pengurus;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_perusahaan', 'npwp', 'hp', 'email', 'jenis_badan_usaha_id', 'provinsi_id', 'kabupaten_id', 'captcha'], 'required'],
            [['nama_perusahaan', 'npwp', 'hp', 'email'], 'trim'],
            [['npwp', 'npwp_pengurus'], 'string', 'min' => 16, 'max' => 16],
            [['hp', 'hp_pengurus'], 'string', 'min' => 10, 'max' => 20],
            [['kode_pos'], 'string', 'min' => 5, 'max' => 5],
            [['nik_pengurus'], 'string', 'min' => 16, 'max' => 16],

            ['hp', 'unique', 'targetClass' => '\common\models\perusahaan\Perusahaan', 'message' => 'Nomor Handphone ini sudah digunakan.', 'filter' => ['is_delete' => 0]],
            ['npwp', 'unique', 'targetClass' => '\common\models\perusahaan\Perusahaan', 'message' => 'Nomor NPWP ini sudah digunakan.', 'filter' => ['is_delete' => 0]],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Alamat Email ini sudah digunakan.', 'filter' => ['is_delete' => 0]],

            [['provinsi_id', 'kabupaten_id', 'jenis_angkutan_id'], 'integer'],
            ['captcha', 'captcha'],
            [['alamat', 'fax', 'nama_pengurus', 'nik_pengurus'], 'string'],
            // ['username', 'trim'],
            // ['username', 'required'],
            // ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            // ['username', 'string', 'min' => 2, 'max' => 255],

            // ['password', 'required'],
            // ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'jenis_badan_usaha_id' => 'Jenis Badan Usaha',
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            print_r($this->getErrors());;
        }
        
        $user = new User();
        $user->user_role_id = 2;
        $user->name = $this->nama_perusahaan;
        $user->username = $this->email;
        $user->email = $this->email;
        $password = $this->generatePassword();
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->status = 20; //user status unverified
        if ($user->save(false)) {
            $countPO = Perusahaan::find()->count();
            $countPO++;
            $kodePO = '00300' . sprintf("%03s", $countPO);
            $perusahaan = new Perusahaan();
            $perusahaan->kode_perusahaan =  $kodePO . 'B';
            $perusahaan->jenis_badan_usaha_id = $this->jenis_badan_usaha_id;
            $perusahaan->user_id = $user->id;
            $perusahaan->nama_perusahaan = $this->nama_perusahaan;
            $perusahaan->email = $this->email;
            $perusahaan->npwp = $this->npwp;
            $perusahaan->hp = $this->hp;
            $perusahaan->is_verified = 0;
            $perusahaan->save();

            $registeredPO = Perusahaan::find()->where(['kode_perusahaan' => $kodePO])->one();

            $domisili = new PerusahaanDomisili();
            $domisili->perusahaan_id = $perusahaan->id;
            $domisili->jenis_domisili = 'Head Office';
            $domisili->alamat = $this->alamat;
            $domisili->alamat_utama = 1;
            $domisili->provinsi_id = $this->provinsi_id;
            $domisili->kabupaten_id = $this->kabupaten_id;
            $domisili->tlp = $this->hp;
            $domisili->fax = $this->fax;
            $domisili->kode_pos = $this->kode_pos;
            $domisili->save();

            $pengurus = new PerusahaanPengurus();
            $pengurus->perusahaan_id = $perusahaan->id;
            $pengurus->nik = $this->nik_pengurus;
            $pengurus->nama_lengkap = $this->nama_pengurus;
            $pengurus->jabatan = 'Direksi';
            $pengurus->npwp = $this->npwp_pengurus;
            $pengurus->save();

            $this->sendEmail($perusahaan, $password);

            return true;
        }
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($perusahaan, $password)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['perusahaan' => $perusahaan, 'password' => $password]
            )
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name . ' bot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }

    protected function generatePassword()
    {
        $password = Yii::$app->security->generateRandomString(15);
        return $password;
    }
}
