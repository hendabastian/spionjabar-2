<?php

namespace frontend\models\perusahaan;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\perusahaan\SkPerusahaan;

/**
 * SkPerusahaanSearch represents the model behind the search form of `common\models\perusahaan\SkPerusahaan`.
 */
class SkPerusahaanSearch extends SkPerusahaan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'perusahaan_id', 'jenis_angkutan_id', 'is_active', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['no_sk', 'tgl_terbit', 'exp_sk', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SkPerusahaan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'perusahaan_id' => $this->perusahaan_id,
            'jenis_angkutan_id' => $this->jenis_angkutan_id,
            'tgl_terbit' => $this->tgl_terbit,
            'exp_sk' => $this->exp_sk,
            'is_active' => $this->is_active,
            'is_delete' => 0,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'no_sk', $this->no_sk]);

        return $dataProvider;
    }
}
