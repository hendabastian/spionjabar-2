<?php

namespace frontend\models\perusahaan;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\perusahaan\PerusahaanDomisili;

/**
 * PerusahaanDomisiliSearch represents the model behind the search form of `common\models\PerusahaanDomisili`.
 */
class PerusahaanDomisiliSearch extends PerusahaanDomisili
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'perusahaan_id', 'provinsi_id', 'kabupaten_id', 'kode_pos', 'is_delete', 'created_by', 'updated_by', 'alamat_utama'], 'integer'],
            [['jenis_domisili', 'alamat', 'tlp', 'fax', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PerusahaanDomisili::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'perusahaan_id' => $this->perusahaan_id,
            'provinsi_id' => $this->provinsi_id,
            'kabupaten_id' => $this->kabupaten_id,
            'kode_pos' => $this->kode_pos,
            'is_delete' => 0,
            'alamat_utama' => $this->alamat_utama,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'jenis_domisili', $this->jenis_domisili])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'tlp', $this->tlp])
            ->andFilterWhere(['like', 'fax', $this->fax]);

        return $dataProvider;
    }
}
