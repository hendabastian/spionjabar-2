<?php

namespace frontend\models\perusahaan;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\perusahaan\SkPerusahaanKendaraan;

/**
 * SkPerusahaanKendaraanSearch represents the model behind the search form of `common\models\perusahaan\SkPerusahaanKendaraan`.
 */
class SkPerusahaanKendaraanSearch extends SkPerusahaanKendaraan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sk_id', 'jenis_kendaraan_id', 'seat', 'created_by', 'updated_by', 'is_delete'], 'integer'],
            [['no_srut', 'tgl_srut', 'no_kendaraan', 'no_rangka', 'no_uji', 'expire_uji', 'no_mesin', 'tahun', 'merk', 'nama_pemilik', 'stnk', 'kir', 'bukti_jasa_raharja', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SkPerusahaanKendaraan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sk_id' => $this->sk_id,
            'tgl_srut' => $this->tgl_srut,
            'expire_uji' => $this->expire_uji,
            'jenis_kendaraan_id' => $this->jenis_kendaraan_id,
            'seat' => $this->seat,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            't_sk_perusahaan_kendaraan.is_delete' => 0,
        ]);

        $query->andFilterWhere(['like', 'no_srut', $this->no_srut])
            ->andFilterWhere(['like', 'no_kendaraan', $this->no_kendaraan])
            ->andFilterWhere(['like', 'no_rangka', $this->no_rangka])
            ->andFilterWhere(['like', 'no_uji', $this->no_uji])
            ->andFilterWhere(['like', 'no_mesin', $this->no_mesin])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'merk', $this->merk])
            ->andFilterWhere(['like', 'nama_pemilik', $this->nama_pemilik])
            ->andFilterWhere(['like', 'stnk', $this->stnk])
            ->andFilterWhere(['like', 'kir', $this->kir])
            ->andFilterWhere(['like', 'bukti_jasa_raharja', $this->bukti_jasa_raharja]);

        return $dataProvider;
    }
}
