<?php

namespace frontend\modules\permohonan\controllers;

use common\models\master\TipePermohonan;
use common\models\master\Trayek;
use common\models\permohonan\KendaraanBbn;
use common\models\permohonan\KendaraanInfoPenambahan;
use common\models\permohonan\KendaraanInfoRealisasi;
use common\models\permohonan\KendaraanPeremajaan;
use common\models\permohonan\KendaraanPindahTrayek;
use common\models\permohonan\Undangan;
use common\models\perusahaan\SkPerusahaanKendaraan;
use Yii;
use common\models\permohonan\Permohonan;
use common\models\permohonan\PermohonanVerifikasi;
use frontend\modules\permohonan\models\PermohonanSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * PermohonanController implements the CRUD actions for Permohonan model.
 */
class PermohonanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->layout = '@frontend/views/layouts/dashboard/main';
        return parent::beforeAction($action);
    }

    /**
     * Lists all Permohonan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PermohonanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['perusahaan_id' => Yii::$app->user->identity->perusahaan->id, 'jenis_angkutan_id' => Yii::$app->request->get('jenis_angkutan_id')]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Permohonan model.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        /**
         * Code dibawah untuk mendapatkan data kendaraan berdasarkan tipe permohonan dengan kondisi sbb:
         * @property int [3, 9] = Permohonan Info Realisasi
         * @property int [4, 10, 14, 17, 20, 22] = Permohonan Peremajaan
         * @property int [5, 11, 15, 18] = Permohonan BBN
         * @property int [6,12] = Permohonan Pindah Trayek
         * @property int [19, 21] = Permohonan Info Penambahan
         */
        if (in_array($model->tipe_permohonan_id, [3, 9, 13, 16])) { // Permohonan Info Realisasi
            $modelKendaraan = KendaraanInfoRealisasi::find()->where(['permohonan_id' => $model->id]);
            $kendaraanDataProvider = new ActiveDataProvider([
                'query' => $modelKendaraan,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);
        } elseif (in_array($model->tipe_permohonan_id, [4, 10, 14, 17, 20, 22, 24])) { // Permohonan Peremajaan
            $modelKendaraan = KendaraanPeremajaan::find()->where(['permohonan_id' => $model->id]);
            $kendaraanDataProvider = new ActiveDataProvider([
                'query' => $modelKendaraan,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);
        } elseif (in_array($model->tipe_permohonan_id, [5, 11, 15, 18])) { // Permohonan BBN
            $modelKendaraan = KendaraanBbn::find()->where(['permohonan_id' => $model->id]);
            $kendaraanDataProvider = new ActiveDataProvider([
                'query' => $modelKendaraan,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);
        } elseif (in_array($model->tipe_permohonan_id, [6, 12])) { // Permohonan Pindah Trayek
            $modelKendaraan = KendaraanPindahTrayek::find()->where(['permohonan_id' => $model->id]);
            $kendaraanDataProvider = new ActiveDataProvider([
                'query' => $modelKendaraan,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);
        } elseif (in_array($model->tipe_permohonan_id, [19, 21, 23])) { // Permohonan Info Penambahan
            $modelKendaraan = KendaraanInfoPenambahan::find()->where(['permohonan_id' => $model->id]);
            $kendaraanDataProvider = new ActiveDataProvider([
                'query' => $modelKendaraan,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);
        } else {
            $modelKendaraan = null;
        }
        return $this->render('view', [
            'model' => $model,
            'kendaraanDataProvider' => isset($kendaraanDataProvider) ? $kendaraanDataProvider : null,
        ]);
    }

    /**
     * Creates a new Permohonan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Permohonan();

        if ($model->load(Yii::$app->request->post())) {
            $model->perusahaan_id = Yii::$app->user->identity->perusahaan->id;
            $model->status_verifikasi = Permohonan::STATUS_DRAFT;
            $dokSuratPermohonan = UploadedFile::getInstance($model, 'file_surat_permohonan');
            $dokProposal = UploadedFile::getInstance($model, 'proposal_pengoperasian_angkutan');
            $dokKesanggupanKendaraan = UploadedFile::getInstance($model, 'file_pernyataan_kesanggupan_kendaraan');
            $dokKesanggupanPool = UploadedFile::getInstance($model, 'file_pernyataan_kesanggupan_pool');
            $dokPernyataan = UploadedFile::getInstance($model, 'dok_pernyataan');
            $dokSuratPersetujuan = UploadedFile::getInstance($model, 'file_surat_persetujuan');
            $dokSkKp = UploadedFile::getInstance($model, 'file_sk_kp');
            $dokAdvisAsal = UploadedFile::getInstance($model, 'file_advis_asal');
            $dokAdvisTujuan = UploadedFile::getInstance($model, 'file_advis_tujuan');
            $dokPelepasanHak = UploadedFile::getInstance($model, 'file_surat_pelepasan_hak');
            $dokKebenaranData = UploadedFile::getInstance($model, 'file_pernyataan_kebenaran_data');
            $dokPrinsip = UploadedFile::getInstance($model, 'file_prinsip_kementerian');

            $directory = Yii::getAlias('@uploads/perusahaan/' . Yii::$app->user->identity->perusahaan->id . '/permohonan/');

            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            $uid = uniqid(time(), true);

            if (!empty($dokSuratPermohonan)) {
                $fileNameSuratPermohonan = $uid . '_surat_permohonan.' . $dokSuratPermohonan->extension;
                $filePathSuratPermohonan = $directory . $fileNameSuratPermohonan;

                $dokSuratPermohonan->saveAs($filePathSuratPermohonan);
                $model->file_surat_permohonan = $fileNameSuratPermohonan;
            }

            if (!empty($dokProposal)) {
                $fileNameProposal = $uid . '_proposal.' . $dokProposal->extension;
                $filePathProposal = $directory . $fileNameProposal;

                $dokProposal->saveAs($filePathProposal);
                $model->proposal_pengoperasian_angkutan = $fileNameProposal;
            }

            if (!empty($dokKesanggupanKendaraan)) {
                $fileNameKesanggupanKendaraan = $uid . '_kesanggupan_kendaraan.' . $dokKesanggupanKendaraan->extension;
                $filePathKesanggupanKendaraan = $directory . $fileNameKesanggupanKendaraan;

                $dokKesanggupanKendaraan->saveAs($filePathKesanggupanKendaraan);
                $model->file_pernyataan_kesanggupan_kendaraan = $fileNameKesanggupanKendaraan;
            }

            if (!empty($dokKesanggupanPool)) {
                $fileNameKesanggupanPool = $uid . '_kesanggupan_pool.' . $dokKesanggupanPool->extension;
                $filePathKesanggupanPool = $directory . $fileNameKesanggupanPool;

                $dokKesanggupanPool->saveAs($filePathKesanggupanPool);
                $model->file_pernyataan_kesanggupan_pool = $fileNameKesanggupanPool;
            }

            if (!empty($dokPernyataan)) {
                $fileNamePernyataan = $uid . '_pernyataan.' . $dokPernyataan->extension;
                $filePathPernyataan = $directory . $fileNamePernyataan;

                $dokPernyataan->saveAs($filePathPernyataan);
                $model->dok_pernyataan = $fileNamePernyataan;
            }

            if (!empty($dokSuratPersetujuan)) {
                $fileNamePersetujuan = $uid . '_persetujuan.' . $dokSuratPersetujuan->extension;
                $filePathPersetujuan = $directory . $fileNamePersetujuan;

                $dokSuratPersetujuan->saveAs($filePathPersetujuan);
                $model->file_surat_persetujuan = $fileNamePersetujuan;
            }

            if (!empty($dokSkKp)) {
                $fileNameSkKp = $uid . '_sk_kp.' . $dokSkKp->extension;
                $filePathSkKp = $directory . $fileNameSkKp;

                $dokSkKp->saveAs($filePathSkKp);
                $model->file_sk_kp = $fileNameSkKp;
            }

            if (!empty($dokAdvisAsal)) {
                $fileNameAdvisAsal = $uid . '_advis_asal.' . $dokAdvisAsal->extension;
                $filePathAdvisAsal = $directory . $fileNameAdvisAsal;

                $dokAdvisAsal->saveAs($filePathAdvisAsal);
                $model->file_advis_asal = $fileNameAdvisAsal;
            }

            if (!empty($dokAdvisTujuan)) {
                $fileNameAdvisTujuan = $uid . '_advis_tujuan.' . $dokAdvisTujuan->extension;
                $filePathAdvisTujuan = $directory . $fileNameAdvisTujuan;

                $dokAdvisTujuan->saveAs($filePathAdvisTujuan);
                $model->file_advis_tujuan = $fileNameAdvisTujuan;
            }

            if (!empty($dokPelepasanHak)) {
                $fileNamePelepasanHak = $uid . '_pelepasan_hak.' . $dokPelepasanHak->extension;
                $filePathPelepasanHak = $directory . $fileNamePelepasanHak;

                $dokPelepasanHak->saveAs($filePathPelepasanHak);
                $model->file_surat_pelepasan_hak = $fileNamePelepasanHak;
            }

            if (!empty($dokKebenaranData)) {
                $fileNameKebenaranData = $uid . '_kebenaran_data.' . $dokKebenaranData->extension;
                $filePathKebenaranData = $directory . $fileNameKebenaranData;

                $dokKebenaranData->saveAs($filePathKebenaranData);
                $model->file_pernyataan_kebenaran_data = $directory . $filePathKebenaranData;
            }

            if (!empty($dokPrinsip)) {
                $fileNamePrinsip = $uid . '_prinsip_kementerian.' . $dokPrinsip->extension;
                $filePathPrinsip = $directory . $fileNamePrinsip;

                $dokPrinsip->saveAs($filePathPrinsip);
                $model->file_prinsip_kementerian = $fileNamePrinsip;
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Data Permohonan Berhasil Disimpan');
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('danger', $model->getErrorSummary(true));
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Permohonan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Permohonan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();

        $model = $this->findModel($id);
        $model->is_delete = 1;

        if ($model->save()) {
            return $this->redirect(['index', 'jenis_angkutan_id' => $model->jenis_angkutan_id]);
        }
    }

    public function actionUndangan()
    {
        $undangan = Undangan::find()->where(['perusahaan_id' => Yii::$app->user->identity->perusahaan->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $undangan,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC
                ]
            ],
        ]);
        return $this->render('index-undangan', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPreviewDok($id, $filename)
    {
        $model = $this->findModel($id);
        return $this->renderAjax('preview_dok', [
            'model' => $model,
            'filename' => $filename
        ]);
    }

    public function actionDetailKendaraan($id, $tipe_permohonan_id)
    {

    }

    public function actionTambahKendaraan($permohonan_id)
    {
        $modelPermohonan = $this->findModel($permohonan_id);
        /**
         * Code dibawah untuk mendapatkan data kendaraan berdasarkan tipe permohonan dengan kondisi sbb:
         * @property int [3, 9] = Permohonan Info Realisasi
         * @property int [4, 10, 14, 17, 20, 22] = Permohonan Peremajaan
         * @property int [5, 11, 15, 18] = Permohonan BBN
         * @property int [6,12] = Permohonan Pindah Trayek
         * @property int [19, 21] = Permohonan Info Penambahan
         */
        if (in_array($modelPermohonan->tipe_permohonan_id, [3, 9, 13])) { // Permohonan Info Realisasi
            $modelKendaraan = new KendaraanInfoRealisasi();

            if ($modelKendaraan->load(Yii::$app->request->post())) {
                $modelKendaraan->permohonan_id = $permohonan_id;
                $modelKendaraan->perusahaan_id = Yii::$app->user->identity->perusahaan->id;
                if ($modelKendaraan->save()) {
                    Yii::$app->session->setFlash('success', 'Data Kendaraan Berhasil Ditambahkan');
                    return $this->redirect(['view', 'id' => $modelPermohonan->id]);
                } else {
                    Yii::$app->session->setFlash('danger', $modelKendaraan->getErrorSummary(true));
                    return $this->redirect(['view', 'id' => $modelPermohonan->id]);
                }
            }

            return $this->renderAjax('_form_kendaraan_info_realisasi', [
                'model' => $modelKendaraan
            ]);
        } elseif (in_array($modelPermohonan->tipe_permohonan_id, [4, 10, 14, 17, 20, 22, 24])) { // Permohonan Peremajaan
            $modelKendaraan = new KendaraanPeremajaan();

            if ($modelKendaraan->load(Yii::$app->request->post())) {
                $modelKendaraan->permohonan_id = $permohonan_id;
                $modelKendaraan->perusahaan_id = Yii::$app->user->identity->perusahaan->id;
                if ($modelKendaraan->save()) {
                    Yii::$app->session->setFlash('success', 'Data Kendaraan Berhasil Ditambahkan');
                    return $this->redirect(['view', 'id' => $modelPermohonan->id]);
                } else {
                    Yii::$app->session->setFlash('danger', $modelKendaraan->getErrorSummary(true));
                    return $this->redirect(['view', 'id' => $modelPermohonan->id]);
                }
            }

            return $this->renderAjax('_form_kendaraan_peremajaan', [
                'model' => $modelKendaraan
            ]);
        } elseif (in_array($modelPermohonan->tipe_permohonan_id, [5, 11, 15, 18])) { // Permohonan BBN
            $modelKendaraan = new KendaraanBbn();

            if ($modelKendaraan->load(Yii::$app->request->post())) {
                $modelKendaraan->permohonan_id = $permohonan_id;
                $modelKendaraan->perusahaan_id = Yii::$app->user->identity->perusahaan->id;
                if ($modelKendaraan->save()) {
                    Yii::$app->session->setFlash('success', 'Data Kendaraan Berhasil Ditambahkan');
                    return $this->redirect(['view', 'id' => $modelPermohonan->id]);
                } else {
                    Yii::$app->session->setFlash('danger', $modelKendaraan->getErrorSummary(true));
                    return $this->redirect(['view', 'id' => $modelPermohonan->id]);
                }
            }

            return $this->renderAjax('_form_kendaraan_bbn', [
                'model' => $modelKendaraan
            ]);
        } elseif (in_array($modelPermohonan->tipe_permohonan_id, [6, 12])) { // Permohonan Pindah Trayek
            $modelKendaraan = new KendaraanPindahTrayek();

            if ($modelKendaraan->load(Yii::$app->request->post())) {
                $modelKendaraan->permohonan_id = $permohonan_id;
                $modelKendaraan->perusahaan_id = Yii::$app->user->identity->perusahaan->id;
                if ($modelKendaraan->save()) {
                    Yii::$app->session->setFlash('success', 'Data Kendaraan Berhasil Ditambahkan');
                    return $this->redirect(['view', 'id' => $modelPermohonan->id]);
                } else {
                    Yii::$app->session->setFlash('danger', $modelKendaraan->getErrorSummary(true));
                    return $this->redirect(['view', 'id' => $modelPermohonan->id]);
                }
            }

            return $this->renderAjax('_form_kendaraan_pindah_trayek', [
                'model' => $modelKendaraan
            ]);
        } elseif (in_array($modelPermohonan->tipe_permohonan_id, [19, 21, 23])) { // Permohonan Info Penambahan
            $modelKendaraan = new KendaraanInfoPenambahan();

            if ($modelKendaraan->load(Yii::$app->request->post())) {
                $modelKendaraan->permohonan_id = $permohonan_id;
                $modelKendaraan->perusahaan_id = Yii::$app->user->identity->perusahaan->id;
                if ($modelKendaraan->save()) {
                    Yii::$app->session->setFlash('success', 'Data Kendaraan Berhasil Ditambahkan');
                    return $this->redirect(['view', 'id' => $modelPermohonan->id]);
                } else {
                    Yii::$app->session->setFlash('danger', $modelKendaraan->getErrorSummary(true));
                    return $this->redirect(['view', 'id' => $modelPermohonan->id]);
                }
            }

            return $this->renderAjax('_form_kendaraan_info_penambahan', [
                'model' => $modelKendaraan
            ]);
        } else {
            $modelKendaraan = null;
        }
    }

    public function actionAjukanVerifikasi($permohonan_id)
    {
        $model = new PermohonanVerifikasi();

        if ($model->load(Yii::$app->request->post())) {
            $model->permohonan_id = $permohonan_id;
            $model->tgl_verifikasi = new \yii\db\Expression('NOW()');
            $model->diverifikasi_oleh = Yii::$app->user->identity->id;
            $model->save();
            $modelPermohonan = $this->findModel($permohonan_id);
            $modelPermohonan->status_verifikasi = 1;
            $modelPermohonan->save();
            Yii::$app->session->setFlash('success', 'Permohonan Berhasil Diajukan');
            return $this->redirect(['view', 'id' => $permohonan_id]);
        }
        return $this->renderAjax('_form_ajukan_verifikasi', [
            'permohonan_id' => $permohonan_id,
            'model' => $model,
        ]);
    }

    public function actionGetKendaraan()
    {
        $kendaraan = SkPerusahaanKendaraan::find()->where(['no_kendaraan' => Yii::$app->request->post('no_kendaraan')])->with('sk')->asArray()->one();
        return Json::encode($kendaraan);
    }

    public function actionGetTipePermohonan()
    {
        if (Yii::$app->request->isPost) {
            $tipePermohonan = TipePermohonan::find()->where(['jenis_permohonan_id' => Yii::$app->request->post('jenis_permohonan_id')])->all();
            return \yii\helpers\Json::encode($tipePermohonan);
        }
    }

    public function actionGetField()
    {
        if (Yii::$app->request->isPost) {
            $model = new Permohonan();
            $tipePermohonanId = Yii::$app->request->post('tipe_permohonan_id');
            $jenisAngkutanId = Yii::$app->request->post('jenis_angkutan_id');
            switch ($tipePermohonanId) {
                case 1:
                    return $this->renderAjax('akdp/_form_prinsip_ijin_baru', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 2:
                    return $this->renderAjax('akdp/_form_prinsip_perpanjangan', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 3:
                    return $this->renderAjax('akdp/_form_info_realisasi', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 4:
                    return $this->renderAjax('akdp/_form_peremajaan', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 5:
                    return $this->renderAjax('akdp/_form_bbn', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                case 6:
                    return $this->renderAjax('akdp/_form_pindah_trayek', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 7:
                    return $this->renderAjax('ajdp/_form_prinsip_ijin_baru', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 8:
                    return $this->renderAjax('ajdp/_form_prinsip_perpanjangan', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 9:
                    return $this->renderAjax('ajdp/_form_info_realisasi', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 10:
                    return $this->renderAjax('ajdp/_form_peremajaan', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 11:
                    return $this->renderAjax('ajdp/_form_bbn', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;

                case 12:
                    return $this->renderAjax('ajdp/_form_pindah_trayek', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 13:
                    return $this->renderAjax('ask/_form_prinsip_ijin_baru', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 14:
                    return $this->renderAjax('ask/_form_peremajaan', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 15:
                    return $this->renderAjax('ask/_form_bbn', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 16:
                    return $this->renderAjax('taxi/_form_prinsip_ijin_baru', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 17:
                    return $this->renderAjax('taxi/_form_peremajaan', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 18:
                    return $this->renderAjax('taxi/_form_bbn', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 19:
                    return $this->renderAjax('akap/_form_info_penambahan', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 20:
                    return $this->renderAjax('akap/_form_peremajaan', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 21:
                    return $this->renderAjax('pariwisata/_form_info_penambahan', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 22:
                    return $this->renderAjax('pariwisata/_form_peremajaan', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 23:
                    return $this->renderAjax('ajap/_form_info_penambahan', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                case 24:
                    return $this->renderAjax('ajap/_form_peremajaan', [
                        'model' => $model,
                        'jenisAngkutanId' => $jenisAngkutanId,
                        'tipePermohonanId' => $tipePermohonanId
                    ]);
                    break;
                // default:
                //     return '<h2>Menu Belum tersedia</h2>';
                //     break;
            }
        }
    }


    /**
     * Finds the Permohonan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Permohonan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Permohonan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
