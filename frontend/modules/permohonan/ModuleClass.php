<?php

namespace frontend\modules\permohonan;

/**
 * Permohonan module definition class
 */
class ModuleClass extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\permohonan\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
