<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\permohonan\Permohonan */

$this->title = $model->tipePermohonan->tipe_permohonan . ' - ' . $model->tipePermohonan->jenisPermohonan->jenisAngkutan->jenis_angkutan;
$this->params['breadcrumbs'][] = ['label' => 'Permohonan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="permohonan-view">

    <div class="x_panel">
        <div class="x_title">
            <h2><?= Html::encode($this->title) ?> </h2>
            <ul class="nav navbar-right panel_toolbox">
                <h2 class="text-white"></h2>
                <li><h2>Status: <?= $model->labelStatus() ?></h2></li>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content">

            <p>
                <?= Html::a('Daftar', ['index', 'id' => $model->id, 'jenis_angkutan_id' => $model->tipePermohonan->jenisPermohonan->jenisAngkutan->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>

            </p>

            <?php switch ($model->tipe_permohonan_id) {
                case 1:
                    echo $this->render('akdp/_view_prinsip_ijin_baru', [
                        'model' => $model
                    ]);
                    break;
                case 2:
                    echo $this->render('akdp/_view_prinsip_perpanjangan', [
                        'model' => $model
                    ]);
                    break;
                case 3:
                    echo $this->render('akdp/_view_info_realisasi', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 4:
                    echo $this->render('akdp/_view_peremajaan', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 5:
                    echo $this->render('akdp/_view_bbn', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 6:
                    echo $this->render('akdp/_view_pindah_trayek', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 7:
                    echo $this->render('ajdp/_view_prinsip_ijin_baru', [
                        'model' => $model
                    ]);
                    break;
                case 8:
                    echo $this->render('ajdp/_view_prinsip_perpanjangan', [
                        'model' => $model
                    ]);
                    break;
                case 9:
                    echo $this->render('ajdp/_view_info_realisasi', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 10:
                    echo $this->render('ajdp/_view_peremajaan', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 11:
                    echo $this->render('ajdp/_view_bbn', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 12:
                    echo $this->render('ajdp/_view_pindah_trayek', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 13:
                    echo $this->render('ask/_view_prinsip_ijin_baru', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 14:
                    echo $this->render('ask/_view_peremajaan', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 15:
                    echo $this->render('ask/_view_bbn', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 16:
                    echo $this->render('taxi/_view_prinsip_ijin_baru', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 17:
                    echo $this->render('taxi/_view_peremajaan', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 18:
                    echo $this->render('taxi/_view_bbn', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 19:
                    echo $this->render('akap/_view_info_penambahan', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 20:
                    echo $this->render('akap/_view_peremajaan', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 21:
                    echo $this->render('pariwisata/_view_info_penambahan', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 22:
                    echo $this->render('pariwisata/_view_peremajaan', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 23:
                    echo $this->render('ajap/_view_info_penambahan', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
                case 24:
                    echo $this->render('ajap/_view_peremajaan', [
                        'model' => $model,
                        'kendaraanDataProvider' => $kendaraanDataProvider
                    ]);
                    break;
            } ?>

            <?php if (empty($model->status_verifikasi) || $model->status_verifikasi == 0 || $model->status_verifikasi == 9) :
                echo Html::button('<i class="fa fa-paper-plane"></i> Ajukan Verifikasi', ['value' => \yii\helpers\Url::toRoute(['ajukan-verifikasi', 'permohonan_id' => $model->id]), 'title' => 'Ajukan Verifikasi', 'class' => 'btn btn-primary btn-round ml-auto showModalButton pull-right']);
            endif ?>
        </div>
    </div>
    <?php
    Modal::begin([
        'headerOptions' => ['id' => 'modalHeader'],
        'id' => 'modal',
        'size' => 'modal-lg',
        'options' => ['tabindex' => false],
        'closeButton' => ['tag' => 'close', 'label' => 'x'],
        //keeps from closing modal with esc key or by clicking out of the modal.
        // user must click cancel or X to close
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
    ]);
    echo "<div id='modalContent' style='padding:2px;'></div>";
    Modal::end();
    ?>
</div>