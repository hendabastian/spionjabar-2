<?php

use common\models\master\JenisAngkutan;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\models\PermohonanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$jenisAngkutan = JenisAngkutan::findOne(Yii::$app->request->get('jenis_angkutan_id'));

$this->title = 'Undangan Masuk';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="permohonan-index">

    <div class="x_panel">
        <div class="x_title">
            <h2><?= Html::encode($this->title) ?></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content">

            <?php Pjax::begin(); ?>
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        'waktu_undangan',
                        'acara',
                        'tempat',
                        // 'jumlah_kendaraan',
                        //'is_delete',
                        //'created_by',
                        //'updated_by',
                        //'created_at',
                        //'updated_at',

                        ['class' => 'yii\grid\ActionColumn', 'header' => 'Aksi', 'contentOptions' => ['style' => 'width:80px; text-align:center;']],
                    ],
                ]); ?>
            </div>
            <?php Pjax::end(); ?>

        </div>
    </div>
    <?php
    Modal::begin([
        'headerOptions' => ['id' => 'modalHeader'],
        'id' => 'modal',
        'size' => 'modal-lg',
        'options' => ['tabindex' => false],
        'closeButton' => ['tag' => 'close', 'label' => 'x'],
        //keeps from closing modal with esc key or by clicking out of the modal.
        // user must click cancel or X to close
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
    ]);
    echo "<div id='modalContent' style='padding:2px;'></div>";
    Modal::end();
    ?>
</div>