<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

\frontend\assets\AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\permohonan\KendaraanInfoRealisasi */
/* @var $form ActiveForm */
?>
<div class="form_kendaraan_info_penambahan">

    <?php $form = ActiveForm::begin([
        'id' => 'kendaraan_info_penambahan-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => '',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'nama_pemilik') ?>

    <?= $form->field($model, 'alamat_pemilik') ?>

    <?= $form->field($model, 'no_kendaraan') ?>

    <?= $form->field($model, 'no_uji_kendaraan') ?>

    <?= $form->field($model, 'merk_kendaraan') ?>

    <?= $form->field($model, 'tahun_kendaraan') ?>


    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a('<i class="fa fa-remove"></i> Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>