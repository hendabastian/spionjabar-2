<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

\frontend\assets\AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\permohonan\KendaraanInfoRealisasi */
/* @var $form ActiveForm */

$dataSk = \yii\helpers\ArrayHelper::map(\common\models\perusahaan\SkPerusahaan::find()->where(['perusahaan_id' => Yii::$app->user->identity->perusahaan->id])->all(), 'id', 'id');
$dataKendaraan = \yii\helpers\ArrayHelper::map(\common\models\perusahaan\SkPerusahaanKendaraan::find()->where(['sk_id' => array_values($dataSk)])->all(), 'no_kendaraan', 'no_kendaraan');
?>
<div class="form_kendaraan_info_realisasi">
    <?php $form = ActiveForm::begin([
        'id' => 'kendaraan_info_realisasi-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => '',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'no_kendaraan')->widget(\kartik\select2\Select2::class, [
        'data' => $dataKendaraan,
        'options' => ['placeholder' => '-Pilih Kendaraan-'],
        'pluginOptions' => ['allowClear' => true]]) ?>

    <div id="detail-kendaraan" style="display: none;"></div>

    <?= $form->field($model, 'no_rangka')->textInput() ?>

    <?= $form->field($model, 'no_mesin')->textInput() ?>

    <?= $form->field($model, 'merk_kendaraan')->textInput() ?>

    <?= $form->field($model, 'tahun_kendaraan')->textInput() ?>


    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a('<i class="fa fa-remove"></i> Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>