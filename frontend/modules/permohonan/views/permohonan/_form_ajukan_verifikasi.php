<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\perijinan\IjinPenyelenggaraanVerifikasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ijin-penyelenggaraan-verifikasi-form">

  <?php $form = ActiveForm::begin(['id' => 'ijin-penyelenggaraan-verifikasi']); ?>

  <?= $form->field($model, 'permohonan_id')->hiddenInput(['value' => $permohonan_id])->label(false) ?>

  <?= $form->field($model, 'status_verifikasi')->hiddenInput(['value' => 1])->label(false) ?>

  <?= $form->field($model, 'diverifikasi_oleh')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>

  <?= $form->field($model, 'catatan_verifikasi')->textarea(['rows' => 6])->label('Catatan Verifikasi <i>* (Tidak wajib diisi)</i>') ?>

  <div class="form-group">
    <div class="col-md-offset-3 col-md-9">
      <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
      <?= Html::button('<i class="fa fa-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']) ?>
    </div>
  </div>

  <?php ActiveForm::end(); ?>

</div>