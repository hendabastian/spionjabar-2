<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

\frontend\assets\AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\permohonan\KendaraanInfoRealisasi */
/* @var $form ActiveForm */

$dataSk = \yii\helpers\ArrayHelper::map(\common\models\perusahaan\SkPerusahaan::find()->where(['perusahaan_id' => Yii::$app->user->identity->perusahaan->id])->all(), 'id', 'id');
$dataKendaraan = \yii\helpers\ArrayHelper::map(\common\models\perusahaan\SkPerusahaanKendaraan::find()->where(['sk_id' => array_values($dataSk)])->all(), 'no_kendaraan', 'no_kendaraan');
?>
<div class="form_kendaraan_info_realisasi">
    <?php $form = ActiveForm::begin([
        'id' => 'kendaraan_info_realisasi-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => '',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'no_kendaraan_lama')->widget(\kartik\select2\Select2::class, [
        'data' => $dataKendaraan,
        'options' => ['placeholder' => '-Pilih Kendaraan-'],
        'pluginOptions' => ['allowClear' => true]]) ?>

    <div id="detail-kendaraan" style="display: none;"></div>

    <div style="display: none;">
        <?= $form->field($model, 'nama_pemilik_lama')->hiddenInput() ?>

        <?= $form->field($model, 'no_uji_kendaraan_lama')->hiddenInput() ?>

        <?= $form->field($model, 'merk_kendaraan_lama')->hiddenInput() ?>

        <?= $form->field($model, 'tahun_kendaraan_lama')->hiddenInput() ?>

        <?= $form->field($model, 'lintasan_trayek_lama')->hiddenInput() ?>
    </div>

    <?= $form->field($model, 'alamat_pemilik_lama')->textInput() ?>

    <?= $form->field($model, 'status_kendaraan_lama')->widget(\kartik\select2\Select2::class, [
        'data' => [
            1 => 'Dihitamkan',
            2 => 'Scraping / Besi Tua',
            3 => 'Pindah Jalur'
        ],
        'options' => ['placeholder' => '-Pilih Kendaraan-'],
        'pluginOptions' => ['allowClear' => true]]) ?>

    <?= $form->field($model, 'nama_pemilik_baru')->textInput() ?>

    <?= $form->field($model, 'alamat_pemilik_baru')->textInput() ?>

    <?= $form->field($model, 'no_rangka_baru')->textInput() ?>

    <?= $form->field($model, 'no_mesin_kendaraan_baru')->textInput() ?>

    <?= $form->field($model, 'merk_kendaraan_baru')->textInput() ?>

    <?= $form->field($model, 'tahun_kendaraan_baru')->textInput() ?>

    <?= $form->field($model, 'lintasan_trayek_baru')->widget(\kartik\select2\Select2::class, [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\master\Trayek::find()->where(['is_delete' => 0, 'is_active' => 1])->all(), 'id', 'nama_trayek'),
        'options' => ['placeholder' => '-Pilih Wilayah Operasi-'],
        'pluginOptions' => ['allowClear' => true]]) ?>

    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a('<i class="fa fa-remove"></i> Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs("
    $('#kendaraanperemajaan-no_kendaraan_lama').change(function() {
        var noKendaraan = $('#kendaraanperemajaan-no_kendaraan_lama').val();
          $.post({
          url: '" . \yii\helpers\Url::toRoute('get-kendaraan') . "',
          data: {
            'no_kendaraan': noKendaraan
          },
          error: function(e) {
            console.log(e);
          },
          success: function(response) {
            var data = JSON.parse(response);
            console.log(data);
            $('#kendaraanperemajaan-nama_pemilik_lama').val(data.nama_pemilik)
//            $('#kendaraanperemajaan-alamat_pemilik_lama').val(data.alamat_pemilik_lama)
            $('#kendaraanperemajaan-no_uji_kendaraan_lama').val(data.no_uji)
            $('#kendaraanperemajaan-merk_kendaraan_lama').val(data.merk)
            $('#kendaraanperemajaan-tahun_kendaraan_lama').val(data.tahun)
            $('#kendaraanperemajaan-lintasan_trayek_lama').val(data.sk.trayek)
          }
        })
    })
");