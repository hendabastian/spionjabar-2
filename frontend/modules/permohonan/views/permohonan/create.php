<?php

use common\models\master\JenisAngkutan;
use common\models\master\JenisPermohonan;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\permohonan\Permohonan */

$jenisAngkutan = JenisAngkutan::findOne(Yii::$app->request->get('jenis_angkutan_id'));

$this->title = 'Tambah Permohonan - ' . $jenisAngkutan->jenis_angkutan;
$this->params['breadcrumbs'][] = ['label' => 'Permohonans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="permohonan-create">

    <div class="x_panel">
        <div class="x_title">
            <h2><?= Html::encode($this->title) ?></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="row">
                <div class="col-sm-12 col-md-3 text-right">
                    <label for="jenis-permohonan-id">Jenis Permohonan</label>
                </div>
                <div class="col-sm-12 col-md-9">
                    <div class="form-group">
                        <?= Select2::widget([
                            'name' => 'jenis-permohonan-id',
                            'id' => 'jenis-permohonan-id',
                            'data' => ArrayHelper::map(JenisPermohonan::find()->where(['jenis_angkutan_id' => Yii::$app->request->get('jenis_angkutan_id')])->all(), 'id', 'jenis_permohonan'),
                            'options' => ['placeholder' => '-Pilih Jenis Permohonan-']
                        ]) ?>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3 text-right">
                    <label for="tipe-permohonan-id">Tipe Permohonan</label>
                </div>
                <div class="col-sm-12 col-md-9">
                    <div class="form-group">
                        <?= Select2::widget([
                            'disabled' => true,
                            'name' => 'tipe-permohonan-id',
                            'id' => 'tipe-permohonan-id',
                            'data' => [],
                            'options' => ['placeholder' => '-Pilih Tipe Permohonan-']
                        ]) ?>
                    </div>
                </div>
                <?= $this->render('_form') ?>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs("
  $('#jenis-permohonan-id').change(function() {
  $('#permohonan-form').empty()
  $('#tipe-permohonan-id')
  .prop('disabled', true)
  .empty()
    var tipePermohonan = '<option></option>';
    $.post({
      url: '" . \yii\helpers\Url::toRoute('get-tipe-permohonan') . "',
      data: {
        'jenis_permohonan_id': $(this).val()
      },
      error: function(e) {
        console.log(e);
      },
      success: function(response) {
        var data = JSON.parse(response);
        data.forEach(data => {
          tipePermohonan += '<option value=' + data.id + '>' + data.tipe_permohonan + '</option>';
          $('#tipe-permohonan-id')
          .empty()
          .append(tipePermohonan)
           .prop('disabled', false);
        });
      }
    })
  })
  $('#tipe-permohonan-id').change(function() {
  $('#permohonan-form')
  .empty()
  .append('." . '<div class="text-center"><i class="fa fa-spin fa-spinner fa-5x"></i> <h3> Memuat... </h3> </div>' . ".')
    var tipePermohonan = $(this).val();
    $.post({
      url: '" . \yii\helpers\Url::toRoute('get-field') . "',
      data: {
        tipe_permohonan_id: tipePermohonan,
        jenis_angkutan_id: " . Yii::$app->request->get('jenis_angkutan_id') . "
      },
      error: function(e) {
        console.log(e);
      },
      success: function(response) {
        $('#permohonan-form')
        .empty()
        .append(response);
      }
    })
  })
") ?>

<script>


</script>