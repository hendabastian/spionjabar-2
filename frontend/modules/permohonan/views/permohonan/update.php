<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\permohonan\Permohonan */

$this->title = 'Update Permohonan: ' . $model->no_surat_permohonan;
$this->params['breadcrumbs'][] = ['label' => 'Permohonans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="permohonan-update">

    <div class="x_panel">
        <div class="x_title">
            <h2><?= Html::encode($this->title) ?></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content">

            <?php
            switch ($model->tipe_permohonan_id) {
                case 1:
                    echo $this->render('akdp/_form_prinsip_ijin_baru', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 2:
                    echo $this->render('akdp/_form_prinsip_perpanjangan', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 3:
                    echo $this->render('akdp/_form_info_realisasi', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 4:
                    echo $this->render('akdp/_form_peremajaan', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 5:
                    echo $this->render('akdp/_form_bbn', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                case 6:
                    echo $this->render('akdp/_form_pindah_trayek', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 7:
                    echo $this->render('ajdp/_form_prinsip_ijin_baru', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 8:
                    echo $this->render('ajdp/_form_prinsip_perpanjangan', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 9:
                    echo $this->render('ajdp/_form_info_realisasi', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 10:
                    echo $this->render('ajdp/_form_peremajaan', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 11:
                    echo $this->render('ajdp/_form_bbn', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;

                case 12:
                    echo $this->render('ajdp/_form_pindah_trayek', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 13:
                    echo $this->render('ask/_form_prinsip_ijin_baru', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 14:
                    echo $this->render('ask/_form_peremajaan', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 15:
                    echo $this->render('ask/_form_bbn', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 16:
                    echo $this->render('taxi/_form_prinsip_ijin_baru', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 17:
                    echo $this->render('taxi/_form_peremajaan', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 18:
                    echo $this->render('taxi/_form_bbn', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 19:
                    echo $this->render('akap/_form_info_penambahan', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 20:
                    echo $this->render('akap/_form_peremajaan', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 21:
                    echo $this->render('pariwisata/_form_info_penambahan', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                case 22:
                    echo $this->render('pariwisata/_form_peremajaan', [
                        'model' => $model,
                        'jenisAngkutanId' => $model->jenis_angkutan_id,
                        'tipePermohonanId' => $model->tipe_permohonan_id
                    ]);
                    break;
                // default:
                //     return '<h2>Menu Belum tersedia</h2>';
                //     break;
            }
            ?>

        </div>
    </div>

</div>
