<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;
?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        //'id',
        'tipePermohonan.tipe_permohonan',
        'no_surat_permohonan',
        'tgl_surat_permohonan',
        [
            'attribute' => 'file_surat_permohonan',
            'format' => 'RAW',
            'value' => function ($data) {
                return Html::button('<i class="fa fa-search"></i> Preview', ['value' => Url::toRoute(['preview-dok', 'id' => $data->id, 'filename' => $data->file_surat_permohonan]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-xs showModalButton',]);
            }
        ],
        'no_surat_persetujuan_pengusahaan_lama',
        [
            'attribute' => 'file_surat_persetujuan_pengusahaan_lama',
            'format' => 'RAW',
            'value' => function ($data) {
                return Html::button('<i class="fa fa-search"></i> Preview', ['value' => Url::toRoute(['preview-dok', 'id' => $data->id, 'filename' => $data->file_surat_persetujuan_pengusahaan_lama]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-xs showModalButton',]);
            }
        ],
        // 'jumlah_kendaraan',
        //'is_delete',
        //'created_by',
        //'updated_by',
        //'created_at',
        //'updated_at',
    ],
]) ?>