function upload_image(id,url,space_id){
      $(document).ready(function(){
        var name = document.getElementById(id).files[0].name;
        var form_data = new FormData();
        var ext = name.split('.').pop().toLowerCase();
        if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1)
        {
             alert("Invalid Image File");
        }
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById(id).files[0]);
        var f = document.getElementById(id).files[0];
        var fsize = f.size||f.fileSize;
        if(fsize > 2000000)
        {
             alert("Image File Size is very big");
        }
        else
        {
          console.log(space_id);
             form_data.append('file', document.getElementById(id).files[0]);
             $.ajax({
              url:url,
              method:"POST",
              data: form_data,
              contentType: false,
              cache: false,
              processData: false,
              beforeSend:function(){
               // $('#'+space_id).append("<label class='text-success'>Image Uploading...</label>");
               $('.loadingset').show();
              },
              success:function(data)
              {
                var obj = jQuery.parseJSON(data);
                // console.log(document.getElementById(id).files[0]);
                // console.log(obj.image);
                $('.loadingset').hide();
                 $('#'+space_id).append(obj.image);
              }
             });
        }
      });
}
function delete_image(id,url){
  $(document).ready(function(){
      if (confirm('Apakah Anda Yakin Ingin Menghapus Gambar Ini?')) {
            var form_data = new FormData();
            $.ajax({
             url:url,
             method:"POST",
             data: form_data,
             contentType: false,
             cache: false,
             processData: false,
             beforeSend:function(){
              // $('#'+space_id).append("<label class='text-success'>Image Uploading...</label>");
              $('.loadingset').show();
             },
             success:function(data)
             {
               var obj = jQuery.parseJSON(data);
               if(obj.success === true){
                 $( "."+id ).remove();
               }
               $('.loadingset').hide();
             }
            });
      }
  });
}
