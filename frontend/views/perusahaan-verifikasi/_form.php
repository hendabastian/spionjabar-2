<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\master\JenisLayanan;
use common\models\master\JenisAngkutan;
use yii\db\Expression;
/* @var $this yii\web\View */
/* @var $model common\models\ActivityLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="perusahaan-verifikasi-log-form">

    <?php $form = ActiveForm::begin(['id' => 'perusahaan-verifikasi']); ?>

    <div style="display: none;">
        <?= $form->field($model, 'jenis_perusahaan')->textInput(['value' => $jenisPerusahaan])->label(false) ?>

        <?= $form->field($model, 'user_id')->textInput(['value' => $perusahaan->user_id])->label(false) ?>

        <?= $form->field($model, 'perusahaan_id')->textInput(['value' => $perusahaan->id])->label(false) ?>
    </div>

    <?= $form->field($model, 'tgl_verifikasi')->textInput(['value' => date('Y-m-d'), 'disabled' => 'disabled'])->label("Diajukan Pada") ?>

    <?= $form->field($model, 'catatan_verifikasi')->textArea(['row' => 3])->label('Catatan Verifikasi <i>* (Tidak wajib diisi)</i>') ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="fa fa-save"></i> Registrasi', ['class' => 'btn btn-success', 'id' => 'btn-submit']) ?>
        <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal', 'id' => 'btn-close']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs('
jQuery(document).ready(function($){
    $(document).ready(function () {
        $("body").on("beforeSubmit", "form#perusahaan-verifikasi", function () {
            var form = $(this);
            var data = new FormData( this );
            // return false if form still have some validation errors
            if (form.find(".has-error").length)
            {
                return false;
            }
            // submit form
            $.ajax({
                url         : form.attr("action"),
                data        : data,
                type        : form.attr("method"),
                cache       : false,
                contentType : false,
                processData : false,
                beforeSend  : function(){
                    $("#btn-submit").prop("disabled", true);
                    $("#btn-close").prop("disabled", true);
                },
                success: function (response)
                {
                    $("#modal").modal("toggle");
                },
                error  : function ()
                {
                    console.log("internal server error");
                }
            });
            return false;
            });
    });
});
'); ?>