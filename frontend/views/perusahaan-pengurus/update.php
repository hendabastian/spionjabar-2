<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PerusahaanPengurus */

$this->title = 'Update Perusahaan Pengurus: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Perusahaan Penguruses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="perusahaan-pengurus-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
