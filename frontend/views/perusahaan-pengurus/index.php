<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\PerusahaanPengurusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Perusahaan Penguruses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perusahaan-pengurus-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Perusahaan Pengurus', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'perusahaan_id',
            'nik',
            'nama_lengkap',
            'jabatan',
            //'periode_mulai',
            //'periode_selesai',
            //'keterangan:ntext',
            //'is_delete',
            //'is_active',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
