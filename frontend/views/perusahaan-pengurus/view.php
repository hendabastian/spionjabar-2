<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PerusahaanPengurus */

$this->title = $model->nama_lengkap;
$this->params['breadcrumbs'][] = ['label' => 'Perusahaan Penguruses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Html::encode($this->title) ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>

        <div class="clearfix"></div>

    </div>

    <div class="x_content">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                // 'perusahaan_id',
                'nik',
                'nama_lengkap',
                'jabatan',
                'npwp',
                'periode_mulai',
                'periode_selesai',
                'keterangan:ntext',
                // 'is_delete',
                // 'is_active',
                // 'created_at',
                // 'updated_at',
                // 'created_by',
                // 'updated_by',
            ],
        ]) ?>

        <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger pull-right', 'data-dismiss' => 'modal']) ?>
    </div>
</div>