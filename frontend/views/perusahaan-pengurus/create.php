<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PerusahaanPengurus */

$this->title = 'Create Perusahaan Pengurus';
$this->params['breadcrumbs'][] = ['label' => 'Perusahaan Penguruses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perusahaan-pengurus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
