<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use common\models\master\Provinsi;
use common\models\master\Kabupaten;
/* @var $this yii\web\View */
/* @var $model common\models\PerusahaanDomisili */
/* @var $form yii\bootstrap\ActiveForm */
\kartik\select2\Select2Asset::register($this);
if (!$model->isNewRecord) {
    if ($model->kabupaten_id != null) {
        $dataKabupaten = ArrayHelper::map(Kabupaten::find()->where(['is_active' => 1, 'is_delete' => 0, 'id' => $model->kabupaten_id])->all(), 'id', 'nama');
    } else {
        $dataKabupaten = [];
    }
} else {
    $dataKabupaten = [];
}

?>
<div class="perusahaan-domisili-form">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'perusahaan-domisili-form',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => '',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="display: none;">
        <?= $form->field($model, 'perusahaan_id')->hiddenInput(['value' => $perusahaan_id])->label(false) ?>
    </div>

    <?= $form->field($model, 'jenis_domisili')->textInput(['maxlength' => true, 'placeholder' => 'Contoh: Kantor Pusat, Kantor Cabang, dll']) ?>

    <?= $form->field($model, 'alamat')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'provinsi_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Provinsi::find()->where(['is_active' => 1, 'is_delete' => 0])->All(), 'id', 'nama'),
        'language' => 'en',
        'options' => ['placeholder' => '-Pilih Provinsi-', 'id' => 'provinsi'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'kabupaten_id')->widget(Select2::classname(), [
        'data' => $dataKabupaten,
        'language' => 'en',
        'options' => ['placeholder' => '-Pilih Kabupaten-', 'id' => 'kabupaten'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div style="display: none;">
        <?= $form->field($perusahaanDok, 'jenis_dokumen_perusahaan_id')->hiddenInput(['value' => 9])->label(false) ?>
        <?= $form->field($perusahaanDok, 'perusahaan_id')->hiddenInput(['value' => $perusahaan_id])->label(false) ?>
        <?= $form->field($perusahaanDok, 'nama_dokumen')->hiddenInput(['value' => "Domisili"])->label(false) ?>
    </div>

    <?= $form->field($perusahaanDok, 'no_dokumen')->textInput(['maxlength' => true]) ?>

    <?= $form->field($perusahaanDok, 'filename')->fileInput()->label("Dokumen Domisili") ?>
    <?php if ($perusahaanDok !== NULL) : ?>
        <div class="col-md-offset-3 col-md-9" style="margin-top: -5px;">
            <a href="<?= Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $model->perusahaan_id . '/legalitas/' . $perusahaanDok->filename ?>" target="_blank"><?= $perusahaanDok->filename ?></a>
        </div>
        <br>
    <?php endif; ?>

    <?= $form->field($perusahaanDok, 'tgl_dokumen')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Masukkan tanggal ...'],
        'name' => 'check_issue_date',
        'value' => date('yyyy-mm-dd'),
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true
        ]
    ])->label('Tgl Exp Domisili');
    ?>

    <?= $form->field($model, 'tlp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kode_pos')->textInput() ?>

    <?= $form->field($model, 'alamat_utama')->radioList([1 => 'Ya', 0 => 'Bukan'])->label('Apakah ini alamat utama?') ?>

    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success', 'id' => 'btn-submit']) ?>
            <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal', 'id' => 'btn-close']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs('
$("#provinsi").change(function () {
    var dataKabupaten="";
        $.ajax({
            type:"POST",
            url:"' . Yii::$app->getUrlManager()->createUrl('perusahaan-domisili/getkabupaten') . '",
            data:{provinsi_id:$(this).val()},
            success:function(respone){
            var data = JSON.parse(respone);
                for(var i=0; data.length > i; i++){
                    dataKabupaten+="<option value="+data[i].id+" >"+data[i].nama+"</option>";
                }
                $("#kabupaten")
                .empty()
                .append(dataKabupaten);
            }
        })
  });
');
?>