<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\PerusahaanDomisiliSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Perusahaan Domisilis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perusahaan-domisili-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Perusahaan Domisili', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'jenis_domisili',
            'alamat:ntext',
            'provinsi_id',
            'kabupaten_id',
            //'tlp',
            //'fax',
            //'kode_pos',
            //'is_active',
            //'is_delete',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
