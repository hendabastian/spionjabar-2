<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PerusahaanDomisili */

$this->title = 'Tambah Perusahaan Domisili';
$this->params['breadcrumbs'][] = ['label' => 'Perusahaan Domisilis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perusahaan-domisili-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'perusahaan_id' => $perusahaan_id,
    ]) ?>

</div>
