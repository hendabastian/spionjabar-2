<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PerusahaanDomisili */

$this->title = 'Update Perusahaan Domisili: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Perusahaan Domisilis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="perusahaan-domisili-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'perusahaan_id' => $perusahaan_id,
    ]) ?>

</div>
