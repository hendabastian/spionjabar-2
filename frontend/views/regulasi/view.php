<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;
use yii\helpers\Url;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $model common\models\Regulasi */

$this->title = $model->nama_regulasi;
$this->params['breadcrumbs'][] = ['label' => 'Regulasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="x_panel">
    <div class="x_title">
      <h2><?= Html::encode($this->title) ?></h2>
      <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
      </ul>

      <div class="clearfix"></div>

    </div>
    <?= Html::a('Daftar', ['index', 'jenisLayananId' => $model->jenis_layanan_id], ['class' => 'btn btn-primary']) ?>
    <div class="x_content">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            [
              'attribute' => 'jenis_layanan_id',
              'value' => function($data){
                return $data->jenisLayanan->jenis_layanan;
              }
            ],
            'nama_regulasi',
            'tahun',
            [
               'attribute' => 'dok_regulasi',
               'filter' => false,
               'format' => 'RAW',
               'value' => function($data){
                $button = "";
                $dokumen = \common\models\perusahaan\Regulasi::find()->where(['is_delete' => 0, 'is_publish' => 1, 'id' => $data->id])->orderBy(['id' => 'SORT_DESC'])->one();
                if (!empty($data->dok_regulasi)) {
                    $button = Html::button('<i class="fa fa-file"></i> Preview', ['value' => Url::toRoute(['regulasi/preview-dok', 'path' => Yii::$app->urlManagerUpload->baseUrl.'/regulasi/', 'file' => $dokumen->dok_regulasi]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-xs showModalButton']);
                }

                return $button;

               }, 
               
            ],
            'keterangan:ntext',
            [
                'attribute'=>'is_publish',
                'format'=>'raw',
                'value'=>function($data){
                    return $data->is_publish == 0 ? '<span class="label label-danger">UnPublish</span>' : '<span class="label label-success">Publish</span>';
                },
            ],
            // 'is_delete',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
        ],
    ]) ?>

        <div class="box-footer">
          <p style="font-size: 11px; text-align: center;">Data dibuat pada <?= $model->created_at;?> oleh <?= User::findOne($model->created_by) == !NULL ? User::findOne($model->created_by)->username : '-' ;?> | diupdate pada <?= $model->updated_at;?> oleh <?= User::findOne($model->updated_by) == !NULL ? User::findOne($model->updated_by)->username : '-'; ?>.</p>
        </div>
    </div>
</div>

<?php
  Modal::begin([
      'headerOptions' => ['id' => 'modalHeader'],
      'id' => 'modal',
      'size' => 'modal-lg',
      'options' => ['tabindex' => ''],
      'closeButton' =>['tag'=>'close', 'x'],
      //keeps from closing modal with esc key or by clicking out of the modal.
      // user must click cancel or X to close
      'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
  ]);
    echo "<div id='modalContent'></div>";
  Modal::end();
?>