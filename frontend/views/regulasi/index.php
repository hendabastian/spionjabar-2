<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel common\models\RegulasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Regulasi';
?>
<div class="x_panel">
  <div class="x_title">
    <h2><?= Html::encode($this->title) ?></h2>
    <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
    </ul>

    <div class="clearfix"></div>

  </div>

  <div class="x_content">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
     <div class="table-responsive">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
              'attribute' => 'jenis_layanan_id',
              'value' => function($data){
                return $data->jenisLayanan->jenis_layanan;
              }
            ],
            'nama_regulasi',
            'tahun',
            [
               'attribute' => 'dok_regulasi',
               'filter' => false,
               'format' => 'RAW',
               'value' => function($data){
                $button = "";
                $dokumen = \common\models\perusahaan\Regulasi::find()->where(['is_delete' => 0, 'is_publish' => 1, 'id' => $data->id])->orderBy(['id' => 'SORT_DESC'])->one();
                if (!empty($data->dok_regulasi)) {
                    $button = Html::button('<i class="fa fa-file"></i> Preview', ['value' => Url::toRoute(['regulasi/preview-dok', 'path' => Yii::$app->urlManagerUpload->baseUrl.'/regulasi/', 'file' => $dokumen->dok_regulasi]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-xs showModalButton']);
                }

                return $button;

               }, 
               'contentOptions' => ['style' => 'width: 90px'],
            ],
            //'keterangan:ntext',
            //'is_publish',
            //'is_delete',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Aksi',
                'template' => '{view}',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

            </div>
        </div>
    </div>
</div>

<?php
  Modal::begin([
      'headerOptions' => ['id' => 'modalHeader'],
      'id' => 'modal',
      'size' => 'modal-lg',
      'options' => ['tabindex' => ''],
      'closeButton' =>['tag'=>'close', 'x'],
      //keeps from closing modal with esc key or by clicking out of the modal.
      // user must click cancel or X to close
      'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
  ]);
    echo "<div id='modalContent'></div>";
  Modal::end();
?>