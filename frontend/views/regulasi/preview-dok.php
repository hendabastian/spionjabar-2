<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>

<?php if (file_exists($fullpath)) : ?>
  <?= Html::a('<i class="fa fa-download"></i> Download', Url::toRoute(['regulasi/download', 'path' => $path, 'file' => $file]), ['class' => 'btn btn-primary', 'data-pjax' => 0]); ?>
<?php endif; ?>

<?= Html::button('<i class="glyphicon glyphicon-remove"></i> Close', ['class' => 'btn btn-danger pull-right', 'data-dismiss' => 'modal']) ?>
<hr>

<?php if (file_exists($fullpath)) :
  $pathinfo = pathinfo($fullpath);
  if ($pathinfo['extension'] == 'jpg' || $pathinfo['extension'] == 'jpeg') : ?>

    <div class="swiper-container" style="width: 100%; height: 100%;">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <div class="swiper-zoom-container">
            <img src="<?= Url::toRoute(['regulasi/download', 'path' => $path, 'file' => $file]) ?>">
          </div>
        </div>
      </div>
    </div>
  <?php else :
    echo \yii2assets\pdfjs\PdfJs::widget([
      'url' => Url::toRoute(['regulasi/download', 'path' => $path, 'file' => $file]),
      'buttons' => [
        'presentationMode' => false,
        'openFile' => false,
        'print' => true,
        'download' => false,
        'viewBookmark' => false,
        'secondaryToolbarToggle' => false
      ]
    ]);
  endif;
  ?>
<?php else : ?>
  <h2>File not found</h2>
<?php endif; ?>

<?php $this->registerJs("
  var swiper = new Swiper('.swiper-container', {
    zoom: true,
  });
") ?>