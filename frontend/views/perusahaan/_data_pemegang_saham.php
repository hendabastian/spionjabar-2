<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<?php Pjax::begin(['id' => 'data-pemegang-saham']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataPemegangSaham,
        'filterModel' => $searchPemegangSaham,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'perusahaan_id',
            'nik',
            [
                'attribute' => 'file_ktp',
                'format' => 'RAW',
                'value' => function ($data) {
                    $button = "";
                    if ($data->file_ktp !== NULL) {
                        $button = Html::button('<i class="fa fa-file-pdf-o"></i>', ['value' => Url::toRoute(['regulasi/preview-dok', 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $data->perusahaan_id . '/legalitas/', 'file' => $data->file_ktp]), 'title' => 'Preview Dokumen', 'class' => 'showModalButton btn btn-primary btn-sm']);
                    }

                    return $button;
                },
                'contentOptions' => ['style' => 'width: 50px;font-align: center;'],
            ],
            'npwp',
            [
                'attribute' => 'file_npwp',
                'format' => 'RAW',
                'value' => function ($data) {
                    $button = "";
                    if ($data->file_npwp !== NULL) {
                        $button = Html::button('<i class="fa fa-file-pdf-o"></i>', ['value' => Url::toRoute(['regulasi/preview-dok', 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $data->perusahaan_id . '/legalitas/', 'file' => $data->file_npwp]), 'title' => 'Preview Dokumen', 'class' => 'showModalButton btn btn-primary btn-sm']);
                    }

                    return $button;
                },
                'contentOptions' => ['style' => 'width: 50px;font-align: center;'],
            ],
            'nama_lengkap',
            [
                'attribute' => 'persentase',
                'value' => function ($data) {
                    return $data->persentase . "%";
                }
            ],
            'keterangan:ntext',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Aksi',
                'template' => '{view}{update}{delete}',
                'contentOptions' => ['width' => "120px"],
                'buttons' => [
                    'view' => function ($url, $model) use ($perusahaan) {
                        return $button = Html::button('<i class="fa fa-eye"></i>', ['value' => Url::toRoute(['perusahaan-pemegang-saham/view', 'id' => $model->id]), 'title' => 'Detail View Pemegang Saham', 'class' => 'showModalButton btn btn-primary btn-xs']);
                    },
                    'update' => function ($url, $model) use ($perusahaan) {
                        $button = "";
                        if ($perusahaan->is_verified == 0 || $perusahaan->is_verified == 2 || $perusahaan->is_verified == 4) {
                            $button = Html::button('<i class="fa fa-edit"></i>', ['value' => Url::toRoute(['perusahaan-pemegang-saham/update', 'id' => $model->id]), 'title' => 'Update', 'class' => 'showModalButton btn btn-warning btn-xs', 'style' => ['margin-left' => '5px;']]);
                        }

                        return $button;
                    },
                    'delete' => function ($url, $model) use ($perusahaan) {
                        $button = "";
                        if ($perusahaan->is_verified == 0 || $perusahaan->is_verified == 2 || $perusahaan->is_verified == 4) {
                            $button = Html::a('<i class="fa fa-trash"></i>', ['perusahaan-pemegang-saham/delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger btn-xs',
                                'style' => ['margin-left' => '5px'],
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]);
                        }
                        return $button;
                    }
                ],
            ],
        ],
    ]); ?>

<?php Pjax::end(); ?>
<?php $this->registerJs('
jQuery(document).ready(function($){
    $(document).ready(function () {
        $("body").on("beforeSubmit", "form#perusahaan-pemegang-saham-form", function () {
            var form = $(this);
            var data = new FormData( this );
            // return false if form still have some validation errors
            if (form.find(".has-error").length)
            {
                return false;
            }
            // submit form
            $.ajax({
                url         : form.attr("action"),
                data        : data,
                type        : form.attr("method"),
                cache       : false,
                contentType : false,
                processData : false,
                beforeSend  : function(){
                    $("#btn-submit").prop("disabled", true);
                    $("#btn-close").prop("disabled", true);
                },
                success: function (response)
                {
                    $("#modal").modal("toggle");
                },
                error  : function ()
                {
                    console.log("internal server error");
                }
            });
            return false;
            });
    });
});
'); ?>