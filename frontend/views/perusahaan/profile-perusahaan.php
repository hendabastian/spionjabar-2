<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = "Profil Perusahaan";
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="perusahaan">
    <!-- Data Perusahaan -->
    <div class="x_panel">
        <div class="x_title">
            <h2># DATA PERUSAHAAN :</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content">
            <?= $this->render('_data_perusahaan', ['model' => $model, 'perusahaanDok' => $perusahaanDok]) ?>
            <?php Pjax::begin(); ?>
            <?php if ($model->is_verified == 0 || $model->is_verified == 2 || $model->is_verified == 4) : ?>
                <?= Html::button('<i class="fa fa-edit"></i> Sunting Data Perusahaan', ['value' => Url::toRoute(['perusahaan/sunting-data', 'id' => $model->id]), 'title' => 'Sunting Data Perusahaan', 'class' => 'btn btn-primary btn-round ml-auto showModalButton pull-right']); ?>
            <?php endif ?>
            <?php Pjax::end() ?>
        </div>
    </div>

    <!-- Domisili -->
    <div class="x_panel">
        <div class="x_title">
            <h2># DOMISILI :</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content">
            <?= $this->render('_data_domisili', [
                'perusahaan' => $model,
                'searchDomisili' => $searchDomisili,
                'dataDomisili' => $dataDomisili,
            ]) ?>

            <?php Pjax::begin(); ?>
            <?php if ($model->is_verified == 0 || $model->is_verified == 2 || $model->is_verified == 4) : ?>
                <?= Html::button('<i class="fa fa-plus"></i> Tambah Domisili', ['value' => Url::toRoute(['perusahaan-domisili/create', 'perusahaan_id' => $model->id]), 'title' => 'Tambah Domisili', 'class' => 'btn btn-primary btn-round ml-auto showModalButton pull-right']); ?>
            <?php endif ?>
            <?php Pjax::end() ?>
        </div>
    </div>

    <!-- Direksi/Pengurus -->
    <div class="x_panel">
        <div class="x_title">
            <h2># DIREKSI/PENGURUS :</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content">
            <?= $this->render('_data_pengurus', [
                'perusahaan' => $model,
                'searchPengurus' => $searchPengurus,
                'dataPengurus' => $dataPengurus,
            ]) ?>
            <?php Pjax::begin(); ?>
            <?php if ($model->is_verified == 0 || $model->is_verified == 2 || $model->is_verified == 4) : ?>
                <?= Html::button('<i class="fa fa-plus"></i> Tambah Direksi / Pengurus', ['value' => Url::toRoute(['perusahaan-pengurus/create', 'perusahaan_id' => $model->id]), 'title' => 'Tambah Pengurus', 'class' => 'btn btn-primary btn-round ml-auto showModalButton pull-right']); ?>
            <?php endif ?>
            <?php Pjax::end() ?>
        </div>
    </div>


    <!-- Pemegang Saham -->
    <div class="x_panel">
        <div class="x_title">
            <h2># PEMEGANG SAHAM :</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content">
            <?= $this->render('_data_pemegang_saham', [
                'perusahaan' => $model,
                'searchPemegangSaham' => $searchPemegangSaham,
                'dataPemegangSaham' => $dataPemegangSaham,
            ]) ?>
        </div>
        <?php Pjax::begin(); ?>
        <?php if ($model->is_verified == 0 || $model->is_verified == 2 || $model->is_verified == 4) : ?>
            <?= Html::button('<i class="fa fa-plus"></i> Tambah Pemegang Saham', ['value' => Url::toRoute(['perusahaan-pemegang-saham/create', 'perusahaan_id' => $model->id]), 'title' => 'Tambah Pemegang Saham', 'class' => 'btn btn-primary btn-round ml-auto showModalButton pull-right']); ?>
        <?php endif ?>
        <?php Pjax::end() ?>
    </div>

    <section id="sk">
        <div class="x_panel">
            <div class="x_title">
                <h2># SK PERUSAHAAN (Khusus Perusahaan Lama):</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->render('_sk_perusahaan', [
                    'perusahaan' => $model,
                    'searchSk' => $searchSk,
                    'dataSk' => $dataSk,
                ])
                ?>
            </div>
            <?php Pjax::begin(); ?>
            <?php if ($model->is_verified == 0 || $model->is_verified == 2 || $model->is_verified == 4 || $model->is_verified == 5) : ?>
                <?= Html::button('<i class="fa fa-plus"></i> Tambah SK', ['value' => Url::toRoute(['sk-perusahaan/create', 'perusahaan_id' => $model->id]), 'title' => 'Tambah SK Perusahaan', 'class' => 'btn btn-primary btn-round ml-auto showModalButton pull-right']); ?>
            <?php endif ?>
            <?php Pjax::end() ?>
        </div>
    </section>

    <!-- Dokumen Perusahaan -->
    <div class="x_panel">
        <div class="x_title">
            <h2># DATA DOKUMEN KELENGKAPAN PERUSAHAAN :</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content">
            <?= $this->render('_dokumen_perusahaan', [
                'perusahaan' => $model,
                'searchDokumen' => $searchDokumen,
                'dataDokumen' => $dataDokumen,
            ]) ?>
        </div>
        <?php Pjax::begin(); ?>
        <?php if ($model->is_verified == 0 || $model->is_verified == 4 || $model->is_verified == 5) : ?>
            <?= Html::button('<i class="fa fa-paper-plane"></i> Registrasi Legalitas', ['value' => Url::toRoute(['perusahaan-verifikasi/ajukan-legalitas', 'perusahaan_id' => $model->id]), 'title' => 'Pengajuan Legalitas', 'class' => 'btn btn-primary btn-round ml-auto showModalButton pull-right']); ?>
        <?php endif ?>
        <?php Pjax::end() ?>
    </div>

</div>

<?php
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'closeButton' => ['tag' => 'close', 'x'],
    'options' => [
        'tabindex' => false,
    ],
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>
