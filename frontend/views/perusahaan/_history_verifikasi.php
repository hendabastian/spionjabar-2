<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;
use common\models\perusahaan\Perusahaan;
use common\models\perusahaan\PerusahaanVerifikasi;
?>
<table class="table table-striped">
  <tr>
    <td>Tanggal Verifikasi</td>
    <td>Diverifikasi Oleh</td>
    <td>Status Verifikasi</td>
    <td>Catatan</td>
  </tr>
  <?php foreach ($perusahaan as $key => $value) : ?>
    <?php foreach (PerusahaanVerifikasi::find()->where(['is_delete' => 0, 'perusahaan_id' => $value->id])->all() as $key => $value) : ?>
      <tr>
        <td><?= $value->tgl_verifikasi ?></td>
        <td><?= $value->diverifikasi_oleh !== NULL ? User::findOne($value->diverifikasi_oleh)->name : "" ?></td>
        <td><?= $value->checkStatus($value->status_verifikasi) ?></td>
        <td><?= $value->catatan_verifikasi ?></td>
      </tr>
    <?php endforeach; ?>
  <?php endforeach; ?>
</table>

<?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']) ?>