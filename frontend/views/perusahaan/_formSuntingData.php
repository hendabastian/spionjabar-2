<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\MaskedInput;
use common\models\master\JenisBadanUsaha;
/* @var $this yii\web\View */
/* @var $model common\models\cms\PagesData */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="alert alert-warning" style="color: white;">
  <strong>Perhatian!</strong> Jika anda mengubah email anda, username anda akan ikut terubah.
</div>
<?php $form = ActiveForm::begin([
      'layout'=>'horizontal',
      'id' => 'sunting-data',
      'fieldConfig' => [
          'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
          'horizontalCssClasses' => [
              'label' => 'col-sm-3',
              'offset' => '',
              'wrapper' => 'col-sm-9',
              'error' => '',
              'hint' => '',
          ],
      ],
    ]); ?>

	<?= $form->field($model, 'jenis_badan_usaha_id')->widget(Select2::classname(), [
                   'data' => ArrayHelper::map(JenisBadanUsaha::find()->where(['is_active' => 1, 'is_delete' => 0])->All(), 'id', 'jenis_badan_usaha'),
                   'language' => 'en',
                   'options' => ['placeholder' => '-Pilih Jenis Badan Usaha-'],
                   'pluginOptions' => [
                       'allowClear' => true
                   ],
                ])->label('Jenis Badan Usaha');?>

    <?= $form->field($model, 'nama_perusahaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'npwp')->widget(MaskedInput::className(), [
        'mask' => '99.999.999.9-999.999',
    ]) ?>

		<div style="display: none;">
			<?= $form->field($perusahaanDok, 'perusahaan_id')->hiddenInput(['value' => $model->id])->label(false) ?>
      <?= $form->field($perusahaanDok, 'jenis_dokumen_perusahaan_id')->hiddenInput(['value' => 2])->label(false) ?>
			<?= $form->field($perusahaanDok, 'nama_dokumen')->hiddenInput(['value' => "NPWP Perusahaan"])->label(false) ?>
			<?= $form->field($perusahaanDok, 'no_dokumen')->hiddenInput(['value' => $model->npwp])->label(false) ?>
		</div>

		<?= $form->field($perusahaanDok, 'filename')->fileInput()->label('File NPWP Perusahaan') ?>
    
    <?php if($perusahaanDok !== NULL): ?>
        <div class="col-md-offset-3 col-md-9" style="margin-top: -5px;">
            <a href="<?= Yii::$app->urlManagerUpload->baseUrl.'/perusahaan/'.$model->id.'/legalitas/'.$perusahaanDok->filename ?>" target="_blank"><?= $perusahaanDok->filename ?></a>
        </div>
        <br>
    <?php endif; ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hp')->textInput(['maxlength' => true])->label('No. HP') ?>

    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
      <div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
        <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']) ?>
      </div>
    </div>

<?php ActiveForm::end(); ?>
