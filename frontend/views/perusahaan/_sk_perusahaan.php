<?php

use common\models\perusahaan\SkPerusahaanKendaraan;
use common\models\perusahaan\SkPerusahaan;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataSk,
        'filterModel' => $searchSk,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'jenis_kendaraan_id',
                'label' => 'Jenis Angkutan',
                'value' => function ($data) {
                    return $data->jenisAngkutan->jenis_angkutan;
                }
            ],
            [
                'attribute' => 'trayek',
                'value' => function ($data) {
                    return $data->trayek->nama_trayek ?? '-';
                }
            ],
            [
                'attribute' => 'file_sk',
                'format' => 'RAW',
                'value' => function ($data) {
                    $button = "";
                    if ($data->file_sk !== NULL) {
                        $button = Html::a('<i class="fa fa-download"></i> Download', Url::toRoute(['regulasi/download', 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $data->perusahaan_id . '/legalitas/', 'file' => $data->file_sk]), ['class' => ' btn btn-primary btn-xs']);
                    }

                    return $button;
                },
                'contentOptions' => ['style' => 'width: 50px;font-align: center;'],
            ],
            'no_sk',
            'tgl_terbit',
            [
                'attribute' => 'id',
                'format' => 'RAW',
                'label' => 'Jml. Kendaraan',
                'value' => function ($data) use ($perusahaan) {
                    $count = SkPerusahaanKendaraan::find()->where(['sk_id' => $data->id])->count();
                    $text = ' Kendaraan';
                    if ($perusahaan->is_verified == 0 || $perusahaan->is_verified == 4 || $perusahaan->is_verified == 5) :
                        $button = Html::a('<i class="fa fa-plus"></i> Tambah Kendaraan', Url::toRoute(['sk-kendaraan/index', 'sk_id' => $data->id, 'perusahaan_id' => $data->perusahaan_id]), ['class' => 'btn btn-xs btn-primary']);
                    else :
                        $button = Html::a('<i class="fa fa-car"></i> Daftar Kendaraan', Url::toRoute(['sk-kendaraan/index', 'sk_id' => $data->id, 'perusahaan_id' => $data->perusahaan_id]), ['class' => 'btn btn-xs btn-primary']);
                    endif;
                    return $count . $text . '<br>' . $button;
                },
                'contentOptions' => ['style' => 'align-items: center;'],
            ],
            //'exp_sk',
            //'is_active',
            //'is_delete',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Aksi',
                'contentOptions' => ['style' => 'text-align: center; width: 120px;'],
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $data) use ($perusahaan) {
                        $button = "";
                        $sk = SkPerusahaan::find()->where(['is_delete' => 0, 'id' => $data->id])->one();
                        if ($sk !== null) {
                            $button = Html::button('<i class="fa fa-eye"></i>', [
                                'value' => Url::toRoute([
                                    'sk-perusahaan/view', 'id' => $data->id
                                ]),
                                'title' => 'Lihat Detail SK',
                                'class' => 'showModalButton btn btn-primary btn-xs',
                                'style' => [
                                    'margin-left' => '5px',
                                ],
                            ]);
                        }
                        return $button;
                    },
                    'update' => function ($url, $data) use ($perusahaan) {
                        $button = "";
                        if ($perusahaan->is_verified == 0 || $perusahaan->is_verified == 2 || $perusahaan->is_verified == 4 || $perusahaan->is_verified == 5) :
                            $sk = SkPerusahaan::find()->where(['is_delete' => 0, 'id' => $data->id])->one();
                            if ($sk !== null) {
                                $button = Html::button('<i class="fa fa-edit"></i>', [
                                    'value' => Url::toRoute([
                                        'sk-perusahaan/update', 'id' => $data->id, 'perusahaan_id' => $data->perusahaan_id
                                    ]),
                                    'title' => 'Update SK',
                                    'class' => 'showModalButton btn btn-warning btn-xs',
                                    'style' => ['margin-left' => '5px;'],
                                ]);
                            }
                        endif;
                        return $button;
                    },
                    'delete' => function ($url, $data) use ($perusahaan) {
                        $button = "";
                        if ($perusahaan->is_verified == 0 || $perusahaan->is_verified == 2 || $perusahaan->is_verified == 4 || $perusahaan->is_verified == 5) :
                            $sk = SkPerusahaan::find()->where(['is_delete' => 0, 'id' => $data->id])->one();
                            if ($sk !== null) {
                                $button = Html::button('<i class="fa fa-trash"></i>', [
                                    'value' => Url::toRoute([
                                        'sk-perusahaan/delete', 'id' => $data->id
                                    ]),
                                    'class' => 'showModalButton btn btn-danger btn-xs',
                                    'style' => ['margin-left' => '5px'],
                                    'data' => [
                                        'confirm' => 'Yakin akan menghapus data ini?',
                                        'method' => 'post',
                                    ]
                                ]);
                            }
                        endif;
                        return $button;
                    }
                ]
            ],
        ],
    ]); ?>

