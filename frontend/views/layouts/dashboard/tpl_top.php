<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="javascript:document.getElementById('logout-form').submit()">
                        <i class="fa fa-sign-out"></i> Log Out</a>
                </li>

                <li>
                    <a href="<?= yii\helpers\Url::to(['../frontend/site/index']); ?>"
                       class="dropdown-toggle info-number" target="_blank">
                        <i class="fa fa-globe"></i> Frontend
                    </a>
                </li>

            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->