<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);

$baseTheme = Yii::$app->getUrlManager()->getBaseUrl().'/gentelella-master/';

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?= $baseTheme;?>favicon.ico" type="image/x-icon">
  <?= Html::csrfMetaTags() ?>
  <title><?= Yii::$app->name.' | '.Html::encode($this->title) ?></title>

  <?php $baseTheme = Yii::$app->getUrlManager()->getBaseUrl().'/gentelella-master';?>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <?php $this->head() ?>


</head>
<body class="nav-md">
<?php $this->beginBody() ?>
<div class="container body">
  <div class="main_container">
