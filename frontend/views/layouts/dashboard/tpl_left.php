<?php

use yii\helpers\Url;
use common\models\User;
use common\models\UserRole;
use common\models\master\JenisLayanan;
use common\models\master\JenisPerizinan;

?>

<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">

            <a href="<?= Url::toRoute('/site/dashboard'); ?>" class="site_title">
                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/frontend/images/logo.png' ?>" width="90%;"
                     align="center" alt="">
            </a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="<?= $baseTheme; ?>/images/avatar.png" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2 style="word-wrap: break-word;"><?php if (!Yii::$app->user->isGuest) {
                        echo ucwords(Yii::$app->user->identity->perusahaan->nama_perusahaan);
                    } ?></h2>
                <p><?php if (!Yii::$app->user->isGuest) {
                        echo ucwords(UserRole::findOne(User::findOne(Yii::$app->user->identity->id)->user_role_id)->role);
                    } ?></p>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Main Menu</h3>
                <ul class="nav side-menu">
                    <li><a href="<?= Url::toRoute('/site/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                </ul>

                <ul class="nav side-menu">
                    <li><a href="<?= Url::toRoute('/perusahaan/profil-perusahaan'); ?>"><i class="fa fa-building"></i>
                            Profil Perusahaan</a></li>
                </ul>

                <ul class="nav side-menu">
                    <li class="<?= Yii::$app->controller->id == "regulasi" ? 'active' : ''; ?>"><a><i
                                    class="fa fa-balance-scale"></i> Informasi Regulasi <span
                                    class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu"
                            style="<?= Yii::$app->controller->id == "regulasi" ? 'display: block;' : 'display: none;'; ?>">
                            <?php foreach (JenisLayanan::find()->where(['is_delete' => 0, 'is_active' => 1])->all() as $key => $value) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/regulasi/index', 'jenisLayananId' => $value->id]); ?>"><?= $value->jenis_layanan ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                </ul>
                <?php if (Yii::$app->user->identity->perusahaan->is_verified == 2) : ?>
                    <ul class="nav side-menu">
                        <?php foreach (JenisLayanan::find()->where(['is_active' => 1, 'is_delete' => 0])->all() as $index => $data) : ?>
                            <li><a><i class="fa fa-file"></i> <?= $data->jenis_layanan ?> <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <?php foreach ($data->jenisAngkutan as $indexAngkutan => $dataAngkutan) : ?>
                                        <li <?= (Yii::$app->request->get('jenis_angkutan_id') == $dataAngkutan->id) ? "class='active '" : '' ?>>
                                            <a href="<?= Url::toRoute(['/permohonan/permohonan/index', 'jenis_angkutan_id' => $dataAngkutan->id]) ?>"><?= $dataAngkutan->jenis_angkutan ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        <?php endforeach; ?>
                    </ul>

                    <ul class="nav side-menu">
                        <li><a><i class="fa fa-book"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?= Url::toRoute('/site/report-permohonan'); ?>"> Data Perijinan</a></li>
                                <li><a href="<?= Url::toRoute('/site/report-kendaraan'); ?>"> Data Kendaraan</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav side-menu">
                        <li><a href="<?= Url::toRoute('/permohonan/permohonan/undangan'); ?>"><i
                                        class="fa fa-mail-reply"></i>
                                Undangan</a></li>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- /sidebar menu -->
</div>