<!-- page content -->
<div class="right_col" role="main">
    <?php if ($this->title != 'Dashboard') { ?>
        <div class="page-title">
            <div class="title_left">
                <h3><?= $this->title; ?></h3>
            </div>

            <div class="title_right">
                <?= yii\widgets\Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
            </div>
        </div>
    <?php } ?>

    <div class="clearfix"></div>

    <div class="row">
        <div class="container">
            <?= common\widgets\Alert::widget() ?>
            <?= $content; ?>
        </div>
    </div>
    <?= yii\helpers\Html::beginForm(['/site/logout'], 'post', ['id' => 'logout-form']); ?>
    <?= yii\helpers\Html::endForm(); ?>
</div>
<!-- /page content -->
