<?php

use frontend\assets\AppAsset;
use frontend\assets\FrontendAsset;
use yii\helpers\Html;
use yii\helpers\Url;
FrontendAsset::register($this);
$baseTheme = Yii::$app->getUrlManager()->getBaseUrl() . '/frontend/';
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <!-- awesone fonts css-->
    <link rel="shortcut icon" href="<?= $baseTheme; ?>/images/icon.png">


    <title><?= Html::encode($this->title) ?> | DISHUB JABAR</title>
    <?php $this->head() ?>

    <style>
        .btn {
            border-radius: 30px;
            font-size: 15px;
        }

        .img {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
</head>

<body>
    <?php $this->beginBody() ?>
    <nav style="padding: 1%;" class="navbar navbar-expand-sm navbar-light bg-light shadow bg-white rounded">
        <!-- <div id="gtco-main-nav"></div> -->
        <div class="container-fluid">
            <a class="navbar-brand" style="width: 19%;"><img src="<?= $baseTheme ?>/images/logo.png" style="width: 100%" /></a>
            <button class="navbar-toggler" data-target="#my-nav" onclick="myFunction(this)" data-toggle="collapse">
                <i class="fa fa-bars" style="color: black;"></i>
            </button>
            <div id="my-nav" class="collapse navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['site/index']); ?>">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['site/index#panduan']); ?>">Panduan</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['site/index#cek']); ?>">Cek Perusahaan & Kendaraan</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['site/index#kontak']); ?>">Kontak</a></li>
                </ul>
                <?php if (Yii::$app->user->isGuest) { ?>
                    <a href="<?= Url::toRoute(['site/login']) ?>" class="btn btn-lg btn-outline-success">LOGIN</a> &nbsp; &nbsp;
                    <a href="<?= Url::toRoute(['site/signup']) ?>" class="btn btn-lg btn-outline-primary">PENDAFTARAN AKUN</a>
                <?php } else { ?>
                    <a href="<?= Url::toRoute(['site/dashboard']) ?>" class="btn btn-lg btn-outline-primary"><i class="fa fa-home"></i> DASHBOARD</a>&nbsp; &nbsp;
                    <a href="javascript:document.getElementById('logout-form').submit()" class="btn btn-lg btn-outline-danger "><i class="fa fa-sign-out"></i> Log Out</a>
                    <?= Html::beginForm(['site/logout'], 'post', ['id' => 'logout-form']); ?>
                    <?= Html::endForm(); ?>
                <?php } ?>

            </div>
        </div>
    </nav>

    <?= $content ?>

    <br>
    <br>
    <section id="kontak">
        <footer class="container-fluid" id="gtco-footer" style="padding-top:100px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6" id="contact">
                        <h4> Contact Us </h4>
                        <input type="text" class="form-control" placeholder="Full Name">
                        <input type="email" class="form-control" placeholder="Email Address">
                        <textarea class="form-control" placeholder="Message"></textarea>
                        <a href="#" class="submit-button">Send <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-6">
                                <h4>Menu</h4>
                                <ul class="nav flex-column company-nav">
                                    <li class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['site/index']); ?>">Home</a></li>
                                    <li class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['site/index#panduan']); ?>">Panduan</a></li>
                                    <li class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['site/index#cek']); ?>">Cek Perusahaan & Kendaraan</a></li>
                                    <!-- <li class="nav-item"><a class="nav-link" href="#">News</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#">FAQ's</a></li> -->
                                    <li class="nav-item"><a class="nav-link" href="<?= Url::toRoute(['site/index#kontak']); ?> ">Contact</a></li>
                                </ul>
                                <h4 class="mt-5">Follow Us</h4>
                                <ul class="nav follow-us-nav">
                                    <li class="nav-item"><a class="nav-link pl-0" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
                                    <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                            <div class="col-6">
                                <h4>Dinas Perhubungan Jawa Barat</h4>
                                <ul class="nav flex-column services-nav">
                                    <p><strong>Jl. Sukabumi No. 1, Kota Bandung</strong><br>
                                        Jawa Barat, 40271 - Indonesia <br>
                                        (022) 7207257 - 7272258 <br>
                                        dishub@jabarprov.go.id</p>
                                </ul>
                            </div>
                            <div class="col-12">
                                <p>&copy; 2019. All Rights Reserved | Kementerian Perhubungan.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </section>


    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>