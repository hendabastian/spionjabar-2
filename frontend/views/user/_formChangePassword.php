<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user common\models\user */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['id' => 'update-password-form']); ?>

	<?= $form->field($user, 'old_password')->passwordInput() ?>

    <?= $form->field($user, 'new_password')->passwordInput() ?>

    <?= $form->field($user, 'new_password_repeat')->passwordInput() ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']) ?>
        <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']) ?>
    </div>

<?php ActiveForm::end(); ?>