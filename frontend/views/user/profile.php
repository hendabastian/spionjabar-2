<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Modal;

$this->title = "Profile";
 ?>
<div class="x_panel">
    <div class="x_title">
      <h2><?= Html::encode($this->title) ?></h2>
      <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
      </ul>

      <div class="clearfix"></div>

    </div>

    <div class="x_content">
       <?php $form = ActiveForm::begin([
        'layout'=>'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => '',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
      ]); ?>
          <?= $form->field($user, 'username')->textInput(['maxlength' => true]) ?>

          <?= $form->field($user, 'email')->textInput(['maxlength' => true]) ?>

          <?= $form->field($po, 'nama_perusahaan')->textInput(['maxlength' => true]) ?>

          <?= $form->field($po, 'hp')->textInput(['maxlength' => true])->label('No HP') ?>

          <div class="form-group">
            <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-warning']) ?>
                        <!-- Button to Open the Modal -->
            <?= Html::button('Update Password', ['value' => Url::to(['user/change-password', 'id' => base64_encode($user->id * 20 * 200)]), 'title' => 'Ubah Kata Sandi', 'class' => 'showModalButton btn btn-warning']); ?>
            </div>
          </div>

          <?php ActiveForm::end(); ?>
    </div>
</div>

<?php
  Modal::begin([
      'headerOptions' => ['id' => 'modalHeader'],
      'id' => 'modal',
      'size' => 'modal-lg',
      'options' => ['tabindex' => ''],
      'closeButton' =>['tag'=>'close', 'x'],
      //keeps from closing modal with esc key or by clicking out of the modal.
      // user must click cancel or X to close
      'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
  ]);
    echo "<div id='modalContent'></div>";
  Modal::end();
?>

<?php $this->registerJs('
jQuery(document).ready(function($){
        $(document).ready(function () {
            $("body").on("beforeSubmit", "form#update-password-form", function () {
                var form = $(this);
                var data = new FormData( this );
                // return false if form still have some validation errors
                if (form.find(".has-error").length)
                {
                    return false;
                }
                // submit form
                $.ajax({
                    url         : form.attr("action"),
                    data        : data,
                    type        : form.attr("method"),
                    cache       : false,
                    contentType : false,
                    processData : false,
                    success: function (response)
                    {
                        $("#modal").modal("toggle");
                    },
                    error  : function ()
                    {
                        console.log("internal server error");
                    }
                });
                return false;
                });
        });
    });
    '); ?>
