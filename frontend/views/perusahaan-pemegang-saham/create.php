<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PerusahaanPemegangSaham */

$this->title = 'Create Perusahaan Pemegang Saham';
$this->params['breadcrumbs'][] = ['label' => 'Perusahaan Pemegang Sahams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perusahaan-pemegang-saham-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
