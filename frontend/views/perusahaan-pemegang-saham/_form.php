<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\MaskedInput;
/* @var $this yii\web\View */
/* @var $model common\models\PerusahaanPemegangSaham */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="perusahaan-pemegang-saham-form">
    <?php $form = ActiveForm::begin([
      'layout'=>'horizontal',
      'id' => 'perusahaan-pemegang-saham-form',
      'fieldConfig' => [
          'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
          'horizontalCssClasses' => [
              'label' => 'col-sm-3',
              'offset' => '',
              'wrapper' => 'col-sm-9',
              'error' => '',
              'hint' => '',
          ],
      ],
    ]); ?>
    <div style="display: none;">
        <?= $form->field($model, 'perusahaan_id')->hiddenInput(['value' => $perusahaan_id])->label(false) ?>
    </div>

    <?= $form->field($model, 'nama_lengkap')->textInput(['maxlength' => true, 'autofocus' => true]) ?>

    <?= $form->field($model, 'persentase')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nik')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_ktp')->fileInput() ?>
    
    <?php if($model !== NULL): ?>
        <div class="col-md-offset-3 col-md-9" style="margin-top: -5px;">
            <a href="<?= Yii::$app->urlManagerUpload->baseUrl.'/perusahaan/'.$perusahaan_id.'/legalitas/'.$model->file_ktp ?>" target="_blank"><?= $model->file_ktp ?></a>
        </div>
        <br>
    <?php endif; ?>

    <?= $form->field($model, 'npwp')->widget(MaskedInput::className(), [
        'mask' => '99.999.999.9-999.999',
    ]) ?>

    <?= $form->field($model, 'file_npwp')->fileInput() ?>

    <?php if($model !== NULL): ?>
        <div class="col-md-offset-3 col-md-9" style="margin-top: -5px;">
            <a href="<?= Yii::$app->urlManagerUpload->baseUrl.'/perusahaan/'.$perusahaan_id.'/legalitas/'.$model->file_npwp ?>" target="_blank"><?= $model->file_npwp ?></a>
        </div>
        <br>
    <?php endif; ?>
    
    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

    <div class="form-group">
      <div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success', 'id' => 'btn-submit']) ?>
        <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal', 'id' => 'btn-close']) ?>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
