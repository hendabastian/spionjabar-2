<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;
use yii\helpers\Url;
use common\components\Helper;
/* @var $this yii\web\View */
/* @var $model common\models\master\JenisLayanan */

$this->title = $model->nama_dokumen;
?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Jenis Dokumen</th>
        <td><?= $model->jenisDokumen->jenis_dokumen ?></td>
    </tr>
    <tr>
        <th>Nama Dokumen</th>
        <td><?= $model->nama_dokumen ?></td>
    </tr>
    <tr>
        <th>No Dokumen</th>
        <td><?= $model->no_dokumen ?></td>
    </tr>

    <?php if($model->jenis_dokumen_perusahaan_id == 3 || $model->jenis_dokumen_perusahaan_id == 5 || $model->jenis_dokumen_perusahaan_id == 7 || $model->jenis_dokumen_perusahaan_id == 8): ?>
        <tr>
            <th><?= $model->getLabelTgl($model->jenis_dokumen_perusahaan_id) ?></th>
            <td>
                <?php 
                    $today = Date('Y-m-d');
                    $label = "";

                    if($model->jenis_dokumen_perusahaan_id == 7){
                        if($model->tgl_dokumen == $today){
                            $label = "<span class='label label-danger' style='font-size: 10px;'>".Helper::getTanggalBiasa($model->tgl_dokumen)." | Expired</span>";
                        }else{
                            $label = "<span class='label label-success' style='font-size: 10px;'>".Helper::getTanggalBiasa($model->tgl_dokumen)."</span>";
                        }
                    }elseif($model->jenis_dokumen_perusahaan_id == 3 || $model->jenis_dokumen_perusahaan_id == 5 || $model->jenis_dokumen_perusahaan_id == 8){
                        $label = Helper::getTanggalBiasa($model->tgl_dokumen);
                    }

                    echo $label;
                ?>
            </td>
        </tr>
    <?php endif; ?>
    <tr>
        <th>Keterangan</th>
        <td><?= $model->keterangan ?></td>
    </tr>
</table>
<?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']) ?>