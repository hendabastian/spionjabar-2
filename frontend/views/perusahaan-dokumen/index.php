<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use common\models\PerusahaanDokumen;
/* @var $this yii\web\View */
/* @var $searchModel common\models\master\JenisDokumenPerusahaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dokumen Perusahaan';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php endif; ?>
<div class="card">
    <div class="card-header">
        <div class="card-title"><?= Html::encode($this->title) ?></div>
    </div>
    <div class="card-body">
    <p>
        <?php Pjax::begin(['id' => 'button-ajukan']) ?>
            <?php if($perusahaan->is_verified == 0): ?>
                <?= Html::button('<i class="fa fa-paper-plane"></i> Ajukan Dokumen', ['value' => Url::toRoute(['ajukan-dokumen', 'id_perusahaan' => $perusahaan->id]), 'title' => 'Ajukan Dokumen', 'class' => 'showModalButton btn btn-success pull-left', 'style' => ['margin-right: 10px;']]); ?>

            <?php elseif($perusahaan->is_verified == 3): ?>
                    <?= Html::button('<i class="fa fa-paper-plane"></i> Ajukan Dokumen Kembali', ['value' => Url::toRoute(['ajukan-kembali', 'id_perusahaan' => $perusahaan->id]), 'title' => 'Ajukan Dokumen Kembali', 'class' => 'showModalButton btn btn-success pull-right']); ?>
            <?php endif; ?>
            <?= Html::a('<i class="fa fa-arrow-left"></i> Kembali', Yii::$app->request->referrer, ['class' => 'btn btn-danger', 'style' => ['margin-left' => '10px;']]) ?>
        <?php Pjax::end(); ?>
    </p>
    <?php Pjax::begin(['id' => 'dokumen-perusahaan-grid-view']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'jenis_dokumen',
            // 'keterangan:ntext',
            // [
            //    'attribute' => 'is_active',
            //    'label' => 'Status',
            //    'format' => 'RAW',
            //    'value' => function($data){
            //        return $data->is_active  == 1 ? "<span class='badge badge-success'>Active</span>" : "<span class='badge badge-danger'>InActive</span>";
            //    }
            // ],
            // 'is_delete',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            [
            	'class' => 'yii\grid\ActionColumn',
            	'header' => 'Aksi',
            	'contentOptions' => ['width' => '70px'],
            	'template' => '{create}{view}{update}',
            	'buttons' => [
            		'create' => function($url, $model){
                        $perusahaan = \common\models\Perusahaan::find()->where(['is_delete' => 0, 'user_id' => Yii::$app->user->identity->id])->one();
                        $dokumen = PerusahaanDokumen::find()->where(['is_delete' => 0, 'jenis_dokumen_perusahaan_id' => $model->id, 'perusahaan_id' => $perusahaan->id])->all();
                        $button = "";
                        if($dokumen == NULL && $perusahaan->is_verified !== 2){
                        	$button = Html::button('<i class="fa fa-upload"></i> Upload Dokumen', ['value' => Url::toRoute(['perusahaan-dokumen/create', 'jenisDokumenPerusahaanId' => $model->id]), 'title' => 'Upload Dokumen', 'class' => 'showModalButton btn btn-warning btn-xs']);
                        }

                        return $button;
                    },
	                'view' => function($url, $model){
                        $perusahaan = \common\models\Perusahaan::find()->where(['is_delete' => 0, 'user_id' => Yii::$app->user->identity->id])->one();
                        $dokumen = PerusahaanDokumen::find()->where(['is_delete' => 0, 'jenis_dokumen_perusahaan_id' => $model->id, 'perusahaan_id' => $perusahaan->id])->one();
                        $button = "";
                        if($dokumen !== NULL){
                        	$button = Html::button('<i class="fa fa-eye"></i>', ['value' => Url::toRoute(['perusahaan-dokumen/view', 'id' => $dokumen->id]), 'title' => 'View', 'class' => 'showModalButton btn btn-primary btn-xs', 'style' => ['margin-right' => '12px;']]);
                        }

                        return $button;
                    },
                    'update' => function($url, $model){
                        $perusahaan = \common\models\Perusahaan::find()->where(['is_delete' => 0, 'user_id' => Yii::$app->user->identity->id])->one();
                        $dokumen = PerusahaanDokumen::find()->where(['is_delete' => 0, 'jenis_dokumen_perusahaan_id' => $model->id, 'perusahaan_id' => $perusahaan->id])->one();
                        $button = "";
                        if($dokumen !== NULL){
                            if ($dokumen->status_verifikasi == NULL || $dokumen->status_verifikasi == 3) {
                            	$button = Html::button('<i class="fa fa-edit"></i>', ['value' => Url::toRoute(['perusahaan-dokumen/update', 'id' => $dokumen->id]), 'title' => 'Update', 'class' => 'showModalButton btn btn-warning btn-xs']);
                            }
                        }

                        return $button;
                    },
	            ], 
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
</div>

<?php
  Modal::begin([
      'headerOptions' => ['id' => 'modalHeader'],
      'id' => 'modal',
      'size' => 'modal-lg',
      'options' => ['tabindex' => ''],
      'closeButton' =>['tag'=>'close', 'x'],
      //keeps from closing modal with esc key or by clicking out of the modal.
      // user must click cancel or X to close
      'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
  ]);
    echo "<div id='modalContent'></div>";
  Modal::end();
?>

<!-- Upload Dokumen -->
<?php $this->registerJs('
jQuery(document).ready(function($){
        $(document).ready(function () {
            $("body").on("beforeSubmit", "form#perusahaan-dokumen-form", function () {
                var form = $(this);
                var data = new FormData( this );
                // return false if form still have some validation errors
                if (form.find(".has-error").length)
                {
                    return false;
                }
                // submit form
                $.ajax({
                    url         : form.attr("action"),
                    data        : data,
                    type        : form.attr("method"),
                    cache       : false,
                    contentType : false,
                    processData : false,
                    success: function (response)
                    {
                        $("#modal").modal("toggle");
                        console.log(response === "true");
                        if(response === "true"){
                            $.pjax.reload({container:"#dokumen-perusahaan-grid-view"}); //for pjax update
                            swal("Dokumen Berhasil Disimpan", {
                                icon : "success",
                                buttons: {                  
                                    confirm: {
                                        className : "btn btn-success"
                                    }
                                },
                            });
                        }else{
                            console.log(response);
                            $.pjax.reload({container:"#dokumen-perusahaan-grid-view"}); //for pjax update
                            swal("Dokumen Gagal Disimpan", {
                                icon : "error",
                                buttons: {                  
                                    confirm: {
                                        className : "btn btn-danger"
                                    }
                                },
                            });
                        }
                    },
                    error  : function ()
                    {
                        console.log("internal server error");
                    }
                });
                return false;
                });
        });
    });
    '); ?>

<!-- Pengajuan Dokumen -->
<?php $this->registerJs('
jQuery(document).ready(function($){
        $(document).ready(function () {
            $("body").on("beforeSubmit", "form#pengajuan-dokumen-form", function () {
                var form = $(this);
                var data = new FormData( this );
                // return false if form still have some validation errors
                if (form.find(".has-error").length)
                {
                    return false;
                }
                // submit form
                $.ajax({
                    url         : form.attr("action"),
                    data        : data,
                    type        : form.attr("method"),
                    cache       : false,
                    contentType : false,
                    processData : false,
                    success: function (response)
                    {
                        $("#modal").modal("toggle");
                        console.log(response === "true");
                        if(response === "true"){
                            $.pjax.reload({container:"#dokumen-perusahaan-grid-view", async: false}); //for pjax update
                            $.pjax.reload({container:"#button-ajukan", async: false}); //for pjax update
                            swal("Dokumen Berhasil Diajukan", {
                                icon : "success",
                                buttons: {                  
                                    confirm: {
                                        className : "btn btn-success"
                                    }
                                },
                            });
                        }else{
                            console.log(response);
                            $.pjax.reload({container:"#dokumen-perusahaan-grid-view", async: false}); //for pjax update

                            swal("Dokumen Gagal Diajukan", {
                                icon : "error",
                                buttons: {                  
                                    confirm: {
                                        className : "btn btn-danger"
                                    }
                                },
                            });
                        }
                    },
                    error  : function ()
                    {
                        console.log("internal server error");
                    }
                });
                return false;
                });
        });
    });
    '); ?>