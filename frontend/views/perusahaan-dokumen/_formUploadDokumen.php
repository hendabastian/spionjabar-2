<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use common\models\master\JenisDokumenPerusahaan;

/* @var $this yii\web\View */
/* @var $model common\models\cms\PagesData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-data-form">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'perusahaan-dokumen-form',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => '',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div style="display: none;">
        <?= $form->field($model, 'jenis_dokumen_perusahaan_id')->widget(Select2::classname(), [
            'theme' => Select2::THEME_BOOTSTRAP,
            'data' => ArrayHelper::map(JenisDokumenPerusahaan::find()->where(['is_active' => 1, 'is_delete' => 0])->All(), 'id', 'jenis_dokumen'),
            'language' => 'en',
            'options' => ['value' => $jenisDokumenPerusahaanId, 'disabled' => 'disabled'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(false); ?>
    </div>

    <?= $form->field($model, 'nama_dokumen')->textInput(['maxlength' => true, 'value' => $model->jenisDokumen->jenis_dokumen]) ?>

    <?= $form->field($model, 'no_dokumen')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'filename')->fileInput()->label('File Dokumen') ?>

    <?php if ($model->filename !== NULL): ?>
        <div class="col-md-offset-3 col-md-9" style="margin-top: -5px;">
            <a href="<?= Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $model->perusahaan_id . '/legalitas/' . $model->filename ?>"
               target="_blank"><?= $model->filename ?></a>
        </div>
        <br>
        <br>
    <?php endif; ?>

    <?= $form->field($model, 'tgl_dokumen')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Masukkan tanggal ...'],
        'name' => 'check_issue_date',
        'value' => date('yyyy-mm-dd'),
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true
        ]
    ])->label($model->getLabelTgl($jenisDokumenPerusahaanId));
    ?>

    <?= $form->field($model, 'keterangan')->textArea(['row' => 3]) ?>

    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success', 'id' => 'btn-submit']) ?>
            <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal', 'id' => 'btn-close']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
