<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\cms\PagesData */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['id' => 'pengajuan-dokumen-form']); ?>

<?= $form->field($dokVerifikasi, 'catatan_verifikasi')->textArea(['row' => 6])->label('Catatan') ?>

<div class="form-group">
    <?= Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']) ?>
    <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']) ?>
</div>

<?php ActiveForm::end(); ?>