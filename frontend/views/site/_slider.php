<?php $baseTheme = Yii::$app->getUrlManager()->getBaseUrl() . '/frontend/';
?>
<!-- Start WOWSlider.com HEAD section -->
<!-- add to the <head> of your page -->
<link rel="stylesheet" type="text/css" href="<?= $baseTheme ?>wow-slider/engine1/style.css" />
<script type="text/javascript" src="<?= $baseTheme ?>wow-slider/engine1/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->

<!-- Start WOWSlider.com BODY section -->
<!-- add to the <body> of your page -->
<div id="wowslider-container1">
	<div class="ws_images">
		<ul>
			<li><img src="<?= $baseTheme ?>wow-slider/data1/images/slider1.jpg" alt="1" title="1" id="wows1_0" /></li>
			<li><img src="<?= $baseTheme ?>wow-slider/data1/images/slider2.jpg" alt="javascript image slider" title="2" id="wows1_1" /></a></li>
			<!-- <li><img src="<?= $baseTheme ?>wow-slider/data1/images/3.jpg" alt="3" title="3" id="wows1_2"/></li> -->
		</ul>
	</div>
	<div class="ws_bullets">
		<div>
			<a href="#" title="1"><span><img src="<?= $baseTheme ?>wow-slider/data1/tooltips/1.jpg" alt="1" />1</span></a>
			<a href="#" title="2"><span><img src="<?= $baseTheme ?>wow-slider/data1/tooltips/2.jpg" alt="2" />2</span></a>
			<!-- <a href="#" title="3"><span><img src="<?= $baseTheme ?>wow-slider/data1/tooltips/3.jpg" alt="3"/>3</span></a> -->
		</div>
	</div>
	<div class="ws_script" style="position:absolute;left:-99%"></div>
	<div class="ws_shadow"></div>
</div>
<script type="text/javascript" src="<?= $baseTheme ?>wow-slider/engine1/wowslider.js"></script>
<script type="text/javascript" src="<?= $baseTheme ?>wow-slider/engine1/script.js"></script>
<!-- End WOWSlider.com BODY section -->