<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\perusahaan\PerusahaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hasil Pencarian | ' . $name;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container" style="padding-top: 2%;">
    <h2><?= Html::encode($this->title) ?></h2>
    <hr>
    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                //'data_version',
                //'user_id',
                [
                    'attribute' => 'jenis_badan_usaha_id',
                    'value' => function ($data) {
                        return $data->jenisBadanUsaha->jenis_badan_usaha;
                    }
                ],
                //'kode_perusahaan',
                'nama_perusahaan',
                'npwp',
                'email:email',
                'hp',
                'website:ntext',
                //'is_verified',

                //'is_delete',
                //'created_at',
                //'updated_at',
                //'created_by',
                //'updated_by',

                // [
                //     'class' => 'yii\grid\ActionColumn', 
                //     'header' => 'Aksi', 
                //     'template' => '{view}',
                //     'buttons' => [
                //         'view' => function ($data) {

                //         }
                //     ],
                //     'contentOptions' => [
                //         'style' => 'width:80px; text-align:center;'
                //     ],
                // ],
            ],
        ]); ?>
</div>