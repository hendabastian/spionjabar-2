<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'SPIONJABAR';
$baseTheme = Yii::$app->getUrlManager()->getBaseUrl() . '/frontend/';
?>

<style>
</style>

<?php echo $this->render('_slider'); ?>
<!-- <div class="container-fluid gtco-banner-area">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1> PORTAL PERIZINAN ONLINE ANGKUTAN DAN MULTIMODA</h1>
                <p> Portal perizinan online untuk Angkutan Provinsi Jawa Barat. </p>
                <a href="#">Contact Us <i class="fa fa-angle-right" aria-hidden="true"></i></a>
            </div> 
            <div class="col-md-6">
                <div class="card">
                    <img class="card-img-top img-fluid" src="<?= $baseTheme; ?>images/section.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div> -->
<div class="container-fluid gtco-feature" id="services">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="cover">
                    <div class="card">
                        <svg class="back-bg" width="100%" viewBox="0 -110 1000 900" style="position:absolute; z-index: -1">
                            <defs>
                                <linearGradient id="PSgrad_01" x1="64.279%" x2="0%" y1="76.604%" y2="0%">
                                    <stop offset="0%" stop-color="rgb(1,230,248)" stop-opacity="1" />
                                    <stop offset="100%" stop-color="rgb(29,62,222)" stop-opacity="1" />
                                </linearGradient>
                            </defs>
                            <path fill-rule="evenodd" opacity="0.102" fill="url(#PSgrad_01)" d="M616.656,2.494 L89.351,98.948 C19.867,111.658 -16.508,176.639 7.408,240.130 L122.755,546.348 C141.761,596.806 203.597,623.407 259.843,609.597 L697.535,502.126 C748.221,489.680 783.967,441.432 777.751,392.742 L739.837,95.775 C732.096,35.145 677.715,-8.675 616.656,2.494 Z" />
                        </svg>
                        <img src="<?= $baseTheme ?>/images/section.png" width="70%" style="align-items: center" />
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <h2>PORTAL PELAYANAN ONLINE TRANSPORTASI DARAT</h2>
                <p> Dinas Perhubungan Provinsi Jawa Barat </p>
                <p>
                    <small>
                        Portal Perizinan Online Angkutan Umum adalah sebuah sistem informasi yang digunakan untuk membantu proses perizinan penyelenggaraan angkutan dan multimoda dilingkungan Dinas Perhubungan Provinsi Jawa Barat.
                    </small>
                </p>
                <!-- <a href="#" class="btn btn-outline-primary">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a> -->
            </div>
        </div>
    </div>
</div>
<section id="panduan">
    <div class="container-fluid gtco-features" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <h2> Tahapan Proses </h2>
                    <p> Untuk menggunakan SPIONJABAR terdapat beberapa tahapan proses yang terdiri sebagai berikut <br><br> Anda juga dapat mengunduh manual book pada tombol di bawah ini </p>
                    <a href="<?= $baseTheme ?>User Manual SPIONJABAR.pdf" class="btn btn-outline-primary" target="_blank">Download Manual Book <i class="fa fa-download" aria-hidden="true"></i></a>
                </div>
                <div class="col-lg-8">
                    <svg id="bg-services" width="100%" viewBox="0 0 1000 800">
                        <defs>
                            <linearGradient id="PSgrad_02" x1="64.279%" x2="0%" y1="76.604%" y2="0%">
                                <stop offset="0%" stop-color="rgb(1,230,248)" stop-opacity="1" />
                                <stop offset="100%" stop-color="rgb(29,62,222)" stop-opacity="1" />
                            </linearGradient>
                        </defs>
                        <path fill-rule="evenodd" opacity="0.102" fill="url(#PSgrad_02)" d="M801.878,3.146 L116.381,128.537 C26.052,145.060 -21.235,229.535 9.856,312.073 L159.806,710.157 C184.515,775.753 264.901,810.334 338.020,792.380 L907.021,652.668 C972.912,636.489 1019.383,573.766 1011.301,510.470 L962.013,124.412 C951.950,45.594 881.254,-11.373 801.878,3.146 Z" />
                    </svg>
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="card text-center">
                                <div class="oval"><img class="card-img-top" src="<?= $baseTheme ?>images/form.png" alt=""></div>
                                <div class="card-body">
                                    <h3 class="card-title">Registrasi Perusahaan</h3>
                                    <p class="card-text">Tahap Pertama : Untuk mendapatkan username dan password silahkan daftarkan perusahaan anda dengan cara mengisi form Pandaftaran Perusahaan.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="card text-center">
                                <div class="oval"><img class="card-img-top" src="<?= $baseTheme ?>images/upload.png" alt=""></div>
                                <div class="card-body">
                                    <h3 class="card-title">Penyampaian Legalitas</h3>
                                    <p class="card-text">Tahap Kedua : Jika anda telah memiliki username dan password selanjutnya login dan uploudkan dokumen legalitas perusahaan anda dan ajukan.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="card text-center">
                                <div class="oval"><img class="card-img-top" src="<?= $baseTheme ?>images/request1.png" alt=""></div>
                                <div class="card-body">
                                    <h3 class="card-title">Penyampaian Permohonan Perijinan</h3>
                                    <p class="card-text">Tahap Ketiga : Ajukan permohonan izin penyelenggaran dengan melampirkan dokumen permohon dan monitor progres permohonan anda malalui fitur Monitoring Progres.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="card text-center">
                                <div class="oval"><img class="card-img-top" src="<?= $baseTheme ?>images/request2.png" alt=""></div>
                                <div class="card-body">
                                    <h3 class="card-title">Penyampaian Permohonan Realisasi Kendaraan</h3>
                                    <p class="card-text">Tahap Keempat : Perusahaan mengajukan permohonan izin realisisi kendaraan dengan menyertakan data-data kendaraan yang ingin diajukan.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<br>
<section id="cek">
    <div class="container-fluid gtco-feature" id="cek-perusahaan">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="cover">
                            <div class="card" style="padding-bottom:80px;">
                                <svg class="back-bg" width="100%" viewBox="0 0 900 700" style="position:absolute; z-index: -1">
                                    <defs>
                                        <linearGradient id="PSgrad_01" x1="64.279%" x2="0%" y1="76.604%" y2="0%">
                                            <stop offset="0%" stop-color="rgb(1,230,248)" stop-opacity="1" />
                                            <stop offset="100%" stop-color="rgb(29,62,222)" stop-opacity="1" />
                                        </linearGradient>
                                    </defs>
                                    <path fill-rule="evenodd" opacity="0.102" fill="url(#PSgrad_01)" d="M616.656,2.494 L89.351,98.948 C19.867,111.658 -16.508,176.639 7.408,240.130 L122.755,546.348 C141.761,596.806 203.597,623.407 259.843,609.597 L697.535,502.126 C748.221,489.680 783.967,441.432 777.751,392.742 L739.837,95.775 C732.096,35.145 677.715,-8.675 616.656,2.494 Z" />
                                </svg>
                                <!-- *************-->
                                <img src="<?= $baseTheme ?>images/hosting.png" width="70%" style="align-items: center" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h2>Cek Perusahaan </h2>
                        <div class="alert alert-warning" role="alert">
                            <strong>Info!</strong> Nama Perusahaan tidak harus lengkap.
                        </div>
                        <form action="<?= Url::toRoute(['cari-perusahaan']) ?>">
                            <div class="form-group">
                                <input type="text" class="form-control shadow p-3 mb-5 bg-white rounded" placeholder="Masukkan Nama Perusahaan" id="nama-perusahaan" name="nama-perusahaan" style="border-radius: 25px;">
                                <br>
                                <button type="submit" class="btn btn-outline-primary btn-block" style="margin-top: -60px; ">Cari <i class="fa fa-angle-right" aria-hidden="true"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="cover">
                            <div class="card" style="padding-bottom:80px;">
                                <svg class="back-bg" width="100%" viewBox="0 0 900 700" style="position:absolute; z-index: -1">
                                    <defs>
                                        <linearGradient id="PSgrad_01" x1="64.279%" x2="0%" y1="76.604%" y2="0%">
                                            <stop offset="0%" stop-color="rgb(1,230,248)" stop-opacity="1" />
                                            <stop offset="100%" stop-color="rgb(29,62,222)" stop-opacity="1" />
                                        </linearGradient>
                                    </defs>
                                    <path fill-rule="evenodd" opacity="0.102" fill="url(#PSgrad_01)" d="M616.656,2.494 L89.351,98.948 C19.867,111.658 -16.508,176.639 7.408,240.130 L122.755,546.348 C141.761,596.806 203.597,623.407 259.843,609.597 L697.535,502.126 C748.221,489.680 783.967,441.432 777.751,392.742 L739.837,95.775 C732.096,35.145 677.715,-8.675 616.656,2.494 Z" />
                                </svg>
                                <!-- *************-->
                                <img src="<?= $baseTheme ?>images/search.png" width="70%" style="align-items: center" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <h2>Cek Kendaraan </h2>
                        <div class="alert alert-warning" role="alert">
                            Contoh: <strong>X-1234-XX</strong>
                        </div>
                        <form action="<?= Url::toRoute(['cari-kendaraan']) ?>">
                            <div class="form-group">
                                <input type="text" class="form-control shadow p-3 mb-5 bg-white rounded" placeholder="Masukkan Nomor Kendaraan" id="nomor-kendaraan" name="nomor-kendaraan" style="border-radius: 25px;">
                                <br>
                                <button type="submit" class="btn btn-outline-primary btn-block" style="margin-top: -60px">Cari <i class="fa fa-angle-right" aria-hidden="true"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>