<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Kirim Ulang Link Aktivasi';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
<div class="site-resend-verification-email" style="padding-top:150px; padding-bottom:50px">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Masukan Email anda, kami akan mengirimkan link aktivasi untuk anda.</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'resend-verification-email-form']); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::classname())->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</div>
