<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Hasil Pencarian';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
<div class="site-about" style="padding-top:150px; padding-bottom:50px">
    <h1><?= Html::encode($this->title).' - "'.$name.'"';?></h1>
        <table class="table table-striped">
            <thead>
                <th>Nama Perusahaan</th>
                <th>Telepon</th>
                <th>Alamat</th>
                <th>Provinsi</th>
            </thead>
            <tbody>
            <?php foreach ($hasil as $data) { ?>
                <tr>
                    <td><?=$data->nama_perusahaan ?></td>
                    <td><?=$data->hp ?></td>
                    <td><?=$data->domisili->alamat ?></td>
                    <td><?=$data->domisili->provinsi->nama.', '.$data->domisili->provinsi->negara->nama ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    
            
    
    <p></p>

    <code><?= __FILE__ ?></code>
</div>
</div>
