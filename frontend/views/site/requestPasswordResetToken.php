<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="site-request-password-reset" style="padding-top:150px; padding-bottom:50px">
<?php if (Yii::$app->session->hasFlash('success')): ?>
  <div class="alert alert-success alert-dismissable">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      <h4><?= Yii::$app->session->getFlash('success') ?></h4>
  </div>
<?php endif; ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Silahkan masukan email anda, kami akan mengirimkan link reset password untuk anda.</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::classname())->label(false) ?>

                <div class="form-group">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</div>
