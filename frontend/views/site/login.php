<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
$baseTheme = Yii::$app->getUrlManager()->getBaseUrl() . '/frontend/';
?>
<?php if (Yii::$app->session->hasFlash('success')) : ?>
    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><?= Yii::$app->session->getFlash('success') ?></h4>
    </div>
<?php endif; ?>
<div class="container-fluid gtco-banner-area">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h1><?= Html::encode($this->title) ?></h1>
                <p>Please fill out the following fields to login:</p>
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>
                <?php Pjax::begin() ?>
                <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::classname())->label(false) ?>
                <?php Pjax::end() ?>
                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-info', 'name' => 'login-button']) ?>
                </div>

                <div style="color:#999;margin:1em 0">
                    Lupa Password? <?= Html::a('Reset Password', ['site/request-password-reset']) ?>.
                    <br>
                    Kirim Ulang Link Verifikasi Email <?= Html::a('Resend', ['site/resend-verification-email', 'class' => 'btn btn-sm']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-6 offset-md-1">
                <div class="card">
                    <img class="card-img-top img-fluid" src="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/public/img/City_SVG.svg' ?> " alt="">
                </div>
            </div>
        </div>
    </div>
</div>