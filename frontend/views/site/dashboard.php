<?php

/* @var $this yii\web\View */

use common\models\perijinan\IjinPenyelenggaraan;
use common\models\perijinan\IjinPersetujuanPenambahan;
use common\models\perijinan\IjinRealisasi;
use common\models\perijinan\RekomendasiPerpanjanganSk;
use yii\db\conditions\BetweenColumnsCondition;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-create">

  <div class="x_panel">
    <div class="x_content">
      <div class="row">
        <div class="col-md-2">
          <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/gentelella-master/'; ?>/images/avatar.png" alt="..." class="img-circle profile_img">
        </div>
        <div class="col-md-10">
          <div class="row">
            <div class="col-md-12">
              <h4 style="color:#1ABB9C">PERUSAHAAN</h4>
                <p><?= Yii::$app->user->identity->perusahaan->nama_perusahaan ?></p>
            </div>
          </div>
          <hr>
        </div>
      </div>
    </div>
  </div>

</div>

<?php

use common\components\Helper;
use common\models\master\JenisAngkutan;
use common\models\permohonan\Permohonan;
use common\models\UserRole;
use yii\helpers\Url;

/** @var \common\models\permohonan\PermohonanVerifikasi $permohonanVerifikasi */
$permohonan = Permohonan::find()->where(['is_delete' => 0, 'perusahaan_id' => Yii::$app->user->identity->perusahaan->id]);
$listUser = \common\models\User::find()->where(['user_role_id' => [UserRole::VERIFIKATOR, UserRole::KEPALA_SEKSI, UserRole::KEPALA_BIDANG, UserRole::KEPALA_DINAS]])->select(['id', 'name'])->asArray()->all();
?>

<div class="row">
    <div class="col-lg-3">
        <div class="x_panel">
            <div class="x_content">
                <div class="card-title">
                    Menunggu Verifikasi Verifikator
                </div>
                <a href="<?= Url::toRoute(['report/permohonan']) ?>">
                    <h1><?= $permohonan->where(['status_verifikasi' => Permohonan::STATUS_PERUSAHAAN_SUBMITTED])->count() ?></h1>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="x_panel">
            <div class="x_content">
                <div class="card-title">
                    Menunggu Verifikasi Kepala Seksi
                </div>
                <a href="<?= Url::toRoute(['report/permohonan']) ?>">
                    <h1><?= $permohonan->where(['status_verifikasi' => Permohonan::STATUS_VERIFIKATOR_APPROVED])->count() ?></h1>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="x_panel">
            <div class="x_content">
                <div class="card-title">
                    Menunggu Verifikasi Kepala Bidang Angkutan
                </div>
                <a href="<?= Url::toRoute(['report/permohonan']) ?>">
                    <h1><?= $permohonan->where(['status_verifikasi' => Permohonan::STATUS_KASI_APPROVED])->count() ?></h1>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="x_panel">
            <div class="x_content">
                <div class="card-title">
                    Menunggu Verifikasi Kepala Dinas
                </div>
                <a href="<?= Url::toRoute(['report/permohonan']) ?>">
                    <h1><?= $permohonan->where(['status_verifikasi' => Permohonan::STATUS_KABID_APPROVED])->count() ?></h1>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="row justify-content-center">
    <!--    <div class="col-lg-5">-->
    <!--        <?//= $this->render('_table_unprocessed', [
    //            'permohonan' => $permohonan
    //        ]) ?>-->
    <!--    </div>-->
    <div class="col-lg-12">
        <div id="performa-chart"></div>
    </div>
</div>

<link rel="stylesheet" href="<?= Yii::$app->urlManager->createAbsoluteUrl('highcharts/css/highcharts.css') ?>">

<script src="<?= Yii::$app->urlManager->createAbsoluteUrl('highcharts/highcharts.js') ?>"></script>
<script>
    Highcharts.chart('performa-chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Jumlah Permohonan Diverifikasi pada <?=Helper::getBulanLengkap(date('m')) . ' ' . date('Y')?>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Verifikator',
            colorByPoint: true,
            data: [{
                name: 'Menunggu Verifikasi Verifikator',
                y: <?= $permohonan->where(['status_verifikasi' => Permohonan::STATUS_PERUSAHAAN_SUBMITTED])->count() ?>,
            }, {
                name: 'Menunggu Verifikasi Kepala Seksi',
                y: <?= $permohonan->where(['status_verifikasi' => Permohonan::STATUS_VERIFIKATOR_APPROVED])->count() ?>,
            }, {
                name: 'Menunggu Verifikasi Kepala Bidang Angkutan',
                y: <?= $permohonan->where(['status_verifikasi' => Permohonan::STATUS_KASI_APPROVED])->count() ?>,
            }, {
                name: 'Menunggu Verifikasi Kepala Dinas',
                y: <?= $permohonan->where(['status_verifikasi' => Permohonan::STATUS_KABID_APPROVED])->count() ?>,
            }]
        }]
    });
</script>