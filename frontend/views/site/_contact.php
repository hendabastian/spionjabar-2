<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
?>
<style>
  #contactform-verifycode{
    margin-top: 10px;
  }
</style>
<!-- start contact Area -->
<section class="contact-area section-gap" id="contact">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-30 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Kontak Kami</h1>
                    <p>Jika memiliki pertanyaan jangan sungkan untuk menghubungi kami.</p>
                </div>
            </div>
        </div>
          <?php $form = ActiveForm::begin(['class' => 'form-area mt-60 contact-form text-right']); ?>
            <div class="row">
            <div class="col-lg-6 form-group">
                <?= $form->field($model, 'name')->textInput(['class' => 'common-input mb-20 form-control', 'placeholder' => 'Masukan Nama Anda'])->label(false) ?>

                <?= $form->field($model, 'email')->textInput(['class' => 'common-input mb-20 form-control', 'placeholder' => 'Masukan Email'])->label(false) ?>

                <?= $form->field($model, 'subject')->textInput(['class' => 'common-input mb-20 form-control', 'placeholder' => 'Subject'])->label(false) ?>

            </div>
            <div class="col-lg-6 form-group">
              <?= $form->field($model, 'body')->textArea(['class' => 'common-input mb-20 form-control', 'placeholder' => 'Masukan isi pesan', 'rows' => 5])->label(false) ?>

              <?= $form->field($model, 'verifyCode')->widget(\yii\captcha\Captcha::classname())->label(false) ?>

                <?= Html::submitButton('Submit', ['class' => 'primary-btn', 'name' => 'contact-button']) ?>
                <div class="mt-10 alert-msg">
                </div>
            </div></div>
        <?php ActiveForm::end(); ?>

    </div>
</section>
<!-- end contact Area -->
