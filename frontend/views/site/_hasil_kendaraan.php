<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\perusahaan\PerusahaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hasil Pencarian | ' . $name;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container" style="padding-top: 2%;">
    <h2><?= Html::encode($this->title) ?></h2>
    <hr>
    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                // 'id',
                // 'sk_id',
                // 'perusahaan_id',
                [
                    'attribute' => 'perusahaan_id',
                    'label' => 'Perusahaan Pemilik',
                    'value' => function ($data) {
                        return $data->perusahaan->nama_perusahaan;
                    }
                ],
                // 'no_srut',
                // 'tgl_srut',
                'no_kendaraan',
                'no_rangka',
                //'no_uji',
                //'expire_uji',
                //'no_mesin',
                'tahun',
                'merk',
                //'nama_pemilik',
                //'jenis_kendaraan_id',
                //'seat',
                //'stnk',
                //'kir',
                //'bukti_jasa_raharja',
                //'created_at',
                //'updated_at',
                //'created_by',
                //'updated_by',
                //'is_delete',

                // [
                //     'class' => 'yii\grid\ActionColumn',
                //     'header' => 'Aksi',
                //     'contentOptions' => ['style' => 'text-align:center; width: 70px;'],
                //     'template' => '{view}{update}{delete}',
                //     'buttons' => [
                //         'view' => function ($url, $data) {
                //             return Html::button('<i class="fa fa-eye"></i>', ['value' => Url::toRoute(['sk-kendaraan/view', 'id' => $data->id]), 'title' => 'Detail Kendaraan', 'class' => 'showModalButton btn btn-primary btn-xs']);
                //         },
                //         'update' => function ($url, $data) {
                //             return Html::button('<i class="fa fa-edit"></i>', ['value' => Url::toRoute(['sk-kendaraan/update', 'id' => $data->id]), 'title' => 'Update Kendaraan', 'class' => 'showModalButton btn btn-warning btn-xs', 'data-pjax' => 'button-action']);
                //         },
                //         'delete' => function ($url, $data) {
                //             return Html::a(
                //                 '<i class="fa fa-trash"></i>',
                //                 ['sk-kendaraan/delete', 'id' => $data->id],
                //                 [
                //                     'class' => 'btn btn-danger btn-xs',
                //                     'data-pjax' => 'button-action',
                //                     'data' => [
                //                         'confirm' => 'Yakin akan menghapus data kendaraan ini',
                //                         'method' => 'post',
                //                     ],
                //                 ]
                //             );
                //         }
                //     ]
                // ],
            ],
        ]); ?>
</div>