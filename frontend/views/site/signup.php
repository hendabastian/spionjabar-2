<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use common\models\master\JenisAngkutan;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\master\JenisBadanUsaha;
use common\models\master\Kabupaten;
use common\models\master\Provinsi;

$this->title = 'Pendaftaran Akun';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container" style="padding-top: 5%;">
    <?php if (Yii::$app->session->hasFlash('success')) : ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><?= Yii::$app->session->getFlash('success') ?></h4>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-sm-12">
            <h1 class="text-center"><?= Html::encode($this->title) ?></h1>
            <p class="text-center">Silakan isi form berikut untuk registrasi:</p>
        </div>
    </div>
    <hr>
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-6">
            <?php $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{hint}",
                    'errorOptions' => ['class' => 'help-block'],
                ],
            ]); ?>

            <?= $form->field($model, 'jenis_badan_usaha_id')->dropDownList($items, ['prompt' => 'Jenis Badan Usaha', 'required' => true]); ?>

            <?= $form->field($model, 'nama_perusahaan')->textInput(['maxlength' => true, 'required' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'required' => true, 'type' => 'email'])->label('Email Perusahaan') ?>

            <?= $form->field($model, 'npwp')->textInput(['maxlength' => 16, 'minlength' => 16, 'required' => true])->label('NPWP') ?>

            <?= $form->field($model, 'hp')->textInput(['required' => true,  'maxlength' => 13, 'minlength' => 10])->label('No. Telp / Handphone Perusahaan') ?>

            <?= $form->field($model, 'fax')->textInput(['required' => true]) ?>

            <?= $form->field($model, 'alamat')->textarea(['required' => true])->label('Alamat Perusahaan') ?>
        </div>

        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'provinsi_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(Provinsi::find()->where(['is_active' => 1, 'is_delete' => 0])->all(), 'id', 'nama'),
                    'options' => [
                        'placeholder' => '-Pilih Provinsi-',
                        'required' => true
                    ],
                    'bsVersion' => '4.x'
                ])->label('Provinsi') ?>
            </div>
            <?= $form->field($model, 'kabupaten_id')->widget(Select2::class, [
                'data' => [],
                'options' => [
                    'placeholder' => '-Pilih Kabupaten / Kota -',
                    'required' => true
                ],
                'bsVersion' => '4.x'
            ])->label('Pilih Kabupaten / Kota'); ?>

            <?= $form->field($model, 'kode_pos')->textInput(['maxlength' => true, 'required' => true]) ?>

            <?= $form->field($model, 'nama_pengurus')->textInput(['required' => true])->label('Nama Penanggung Jawab / Direksi') ?>

            <?= $form->field($model, 'nik_pengurus')->textInput(['required' => true, 'type' => 'text', 'maxlength' => 16, 'minlength' => 16])->label('NIK Penanggung Jawab / Direksi') ?>

            <?= $form->field($model, 'hp_pengurus')->textInput(['required' => true, 'maxlength' => 13, 'minlength' => 10])->label('No. HP Penanggung Jawab / Direksi') ?>

            <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::class)->label(false) ?>
        </div>

        <div class="col-sm-12 col-md-3 form-group">
            <?= Html::submitButton('Daftar Akun', ['class' => 'btn btn-primary btn-lg btn-block']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php $this->registerJs('
$("#signupform-provinsi_id").change(function() {
        var dataKabupaten = "";
        $.ajax({
            type: "POST",
            url: "' . \yii\helpers\Url::toRoute(['site/get-kabupaten']) . '",
            data: {
                provinsi_id: $(this).val()
            },
            error: function(e) {
                console.log(e);
            },
            success: function(response) {
                var data = JSON.parse(response);
                for (var i = 0; data.length > i; i++) {
                    dataKabupaten += "<option value=" + data[i].id + ">" + data[i].nama + "</option>"
                }
                $("#signupform-kabupaten_id")
                .empty()
                .append(dataKabupaten);
                console.log(dataKabupaten);
            }
        })
    });
') ?>

