<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\perusahaan\SkPerusahaan */

$this->title = 'Tambah SK Perusahaan';
$this->params['breadcrumbs'][] = ['label' => 'Sk Perusahaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sk-perusahaan-create">
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Html::encode($this->title) ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?= $this->render('_form', [
                'perusahaan_id' => $perusahaan_id,
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>