<?php

use common\models\master\JenisAngkutan;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\perusahaan\SkPerusahaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sk-perusahaan-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'sk-perusahaan-form',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => '',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>
    <div style="display: none;">
        <?= $form->field($model, 'perusahaan_id')->textInput(['value' => $perusahaan_id]) ?>
    </div>

    <?= $form->field($model, 'jenis_angkutan_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(JenisAngkutan::find()->where(['is_active' => 1, 'is_delete' => 0])->all(), 'id', 'jenis_angkutan'),
        'language' => 'en',
        'options' => ['prompt' => 'Pilih Jenis Angkutan'],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'trayek')->widget(Select2::class, [
        'data' => ArrayHelper::map(\common\models\master\Trayek::find()->where(['is_active' => 1, 'is_delete' => 0])->all(), 'id', 'nama_trayek'),
        'language' => 'en',
        'options' => ['prompt' => 'Pilih Wilayah Operasi'],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ])->label('Wilayah Operasi') ?>

    <?= $form->field($model, 'file_sk')->fileInput() ?>

    <?= $form->field($model, 'no_sk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tgl_terbit')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Masukkan tanggal ...'],
        'name' => 'check_issue_date',
        'value' => date('yyyy-mm-dd'),
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true,
        ]
    ]) ?>

    <?= $form->field($model, 'exp_sk')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Masukkan tanggal ...'],
        'name' => 'check_issues_date',
        'value' => date('yyyy-mm-dd'),
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true,
        ]
    ]) ?>


    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success', 'id' => 'btn-submit']) ?>
            <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal', 'id' => 'btn-close']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>