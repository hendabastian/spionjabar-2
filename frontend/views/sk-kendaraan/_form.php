<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\master\JenisAngkutan;
use common\models\master\Trayek;
use common\models\master\JenisKendaraan;
use kartik\date\DatePicker;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model common\models\perusahaan\SkPerusahaanKendaraan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sk-kendaraan-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'options' => ['id' => 'sk-kendaraan-form'],
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => '',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div style="display:none;">
        <?= $form->field($model, 'sk_id')->textInput(['value' => $sk_id]) ?>

        <?= $form->field($model, 'perusahaan_id')->textInput(['value' => $perusahaan_id]) ?>
    </div>

    <?= $form->field($model, 'no_kendaraan')->widget(MaskedInput::className(), [
        'mask' => 'A-9999-AA'
    ]); ?>

    <?= $form->field($model, 'no_rangka')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_uji')->textInput(['maxlength' => true])->label('Nomor Uji') ?>

    <?= $form->field($model, 'expire_uji')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Masukkan tanggal ...'],
        'name' => 'check_issue_date',
        'value' => date('yyyy-mm-dd'),
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true
        ]
    ])->label('Expire Uji');
    ?>

    <?= $form->field($model, 'no_mesin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tahun')->textInput() ?>

    <?= $form->field($model, 'merk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_pemilik')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenis_kendaraan_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(JenisKendaraan::find()->where(['is_active' => 1, 'is_delete' => 0])->all(), 'id', 'jenis_kendaraan'),
        'options' => ['placeholder' => '--Pilih Jenis Kendaraan--'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])->label('Jenis Kendaraan') ?>

    <?= $form->field($model, 'seat')->textInput() ?>

    <?= $form->field($model, 'stnk')->fileInput() ?>

    <?= $form->field($model, 'kir')->fileInput() ?>

    <?= $form->field($model, 'bukti_jasa_raharja')->fileInput() ?>

    <?= $form->field($model, 'is_active')->widget(Select2::className(), [
        'data' => ['1' => 'Aktif', '0' => 'Tidak Aktif'],
        'options' => ['placeholder' => 'Aktif / Tidak Aktif'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])->label('Status Kendaraan') ?>

    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success', 'id' => 'btn-submit']) ?>
            <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal', 'id' => 'btn-close']) ?>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>