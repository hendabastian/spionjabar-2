<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\perusahaan\SkPerusahaanKendaraan */

$this->title = 'Tambah Kendaraan Terdaftar';
$this->params['breadcrumbs'][] = ['label' => 'Sk Kendaraan'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sk-kendaraan-create">
    <div class="x_panel">
        <div class="x_title">
            <h2>Data SK Perusahaan</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?= $this->render('_sk_perusahaan', [
                'perusahaan_id' => $perusahaan_id,
                'model' => $skPerusahaan,
            ]) ?>
        </div>
    </div>

    <div class="x_panel">
        <div class="x_title">
            <h2><?= Html::encode($this->title) ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    // 'sk_id',
                    // 'perusahaan_id',
                    // 'no_srut',
                    // 'tgl_srut',
                    'no_kendaraan',
                    'no_rangka',
                    //'no_uji',
                    //'expire_uji',
                    //'no_mesin',
                    'tahun',
                    'merk',
                    //'nama_pemilik',
                    //'jenis_kendaraan_id',
                    //'seat',
                    //'stnk',
                    //'kir',
                    //'bukti_jasa_raharja',
                    //'created_at',
                    //'updated_at',
                    //'created_by',
                    //'updated_by',
                    //'is_delete',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?= Html::button('<i class="fa fa-plus"></i> Tambah Kendaraan', ['value' => Url::toRoute(['sk_kendaraan/create', 'perusahaan_id' => $perusahaan_id]), 'class' => 'btn btn-primary pull-right']) ?>
        </div>

    </div>
</div>