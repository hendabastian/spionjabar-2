<?php

use common\models\perusahaan\Perusahaan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\perusahaan\SkPerusahaanKendaraan */

$this->title = 'Tambah Kendaraan Terdaftar';
$this->params['breadcrumbs'][] = ['label' => 'Sk Kendaraan'];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="sk-kendaraan-create">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data SK Perusahaan <?= $perusahaan->nama_perusahaan ?> </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= Html::a('<i class="fa fa-arrow-left"></i> Kembali', Url::toRoute('perusahaan/profil-perusahaan#sk'), ['class' => 'btn btn-danger']) ?>
                <?= Html::button('<i class="fa fa-upload"></i> Upload Data Kendaraan', ['value' => Url::toRoute(['import-excel', 'sk_id' => $skPerusahaan->id, 'perusahaan_id' => $perusahaan_id]), 'title' => 'Detail Kendaraan', 'class' => 'showModalButton btn btn-primary']); ?>
                <?= $this->render('_sk_perusahaan', [
                    'perusahaan_id' => $perusahaan_id,
                    'model' => $skPerusahaan,
                ]) ?>
            </div>
        </div>

        <div class="x_panel">
            <div class="x_title">
                <h2><?= Html::encode($this->title) ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        // 'id',
                        // 'sk_id',
                        // 'perusahaan_id',
                        // 'no_srut',
                        // 'tgl_srut',
                        'no_kendaraan',
                        'no_rangka',
                        'no_uji',
                        //'expire_uji',
                        'no_mesin',
                        'tahun',
                        'merk',
                        //'nama_pemilik',
                        //'jenis_kendaraan_id',
                        //'seat',
                        //'stnk',
                        //'kir',
                        //'bukti_jasa_raharja',
                        //'created_at',
                        //'updated_at',
                        //'created_by',
                        //'updated_by',
                        //'is_delete',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'Aksi',
                            'contentOptions' => ['style' => 'text-align:center; width: 130px;'],
                            'template' => '{view}{update}{delete}',
                            'buttons' => [
                                'view' => function ($url, $data) {
                                    return Html::button('<i class="fa fa-eye"></i>', ['value' => Url::toRoute(['sk-kendaraan/view', 'id' => $data->id]), 'title' => 'Detail Kendaraan', 'class' => 'showModalButton btn btn-primary btn-xs']);
                                },
                                'update' => function ($url, $data) {
                                    return Html::button('<i class="fa fa-edit"></i>', ['value' => Url::toRoute(['sk-kendaraan/update', 'id' => $data->id, 'sk_id' => $data->sk_id]), 'title' => 'Update Kendaraan', 'class' => 'showModalButton btn btn-warning btn-xs', 'data-pjax' => 'button-action']);
                                },
                                'delete' => function ($url, $data) {
                                    return Html::a(
                                        '<i class="fa fa-trash"></i>',
                                        ['sk-kendaraan/delete', 'id' => $data->id],
                                        [
                                            'class' => 'btn btn-danger btn-xs',
                                            'data-pjax' => 'button-action',
                                            'data' => [
                                                'confirm' => 'Yakin akan menghapus data kendaraan ini',
                                                'method' => 'post',
                                            ],
                                        ]
                                    );
                                }
                            ]
                        ],
                    ],
                ]); ?>
                <?php
                    echo Html::button('<i class="fa fa-plus"></i> Tambah Kendaraan', ['value' => Url::toRoute(['sk-kendaraan/create', 'perusahaan_id' => $perusahaan_id, 'sk_id' => $skPerusahaan->id]), 'title' => 'Tambah Kendaraan', 'class' => 'btn showModalButton btn-primary pull-right btn-round']);
                    ?>
            </div>

        </div>
    </div>

<?php
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'closeButton' => ['tag' => 'close', 'x'],
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>