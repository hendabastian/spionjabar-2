<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\perusahaan\SkPerusahaanKendaraanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sk-kendaraan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sk_id') ?>

    <?= $form->field($model, 'perusahaan_id') ?>

    <?= $form->field($model, 'no_srut') ?>

    <?= $form->field($model, 'tgl_srut') ?>

    <?php // echo $form->field($model, 'no_kendaraan') ?>

    <?php // echo $form->field($model, 'no_rangka') ?>

    <?php // echo $form->field($model, 'no_uji') ?>

    <?php // echo $form->field($model, 'expire_uji') ?>

    <?php // echo $form->field($model, 'no_mesin') ?>

    <?php // echo $form->field($model, 'tahun') ?>

    <?php // echo $form->field($model, 'merk') ?>

    <?php // echo $form->field($model, 'nama_pemilik') ?>

    <?php // echo $form->field($model, 'jenis_kendaraan_id') ?>

    <?php // echo $form->field($model, 'seat') ?>

    <?php // echo $form->field($model, 'stnk') ?>

    <?php // echo $form->field($model, 'kir') ?>

    <?php // echo $form->field($model, 'bukti_jasa_raharja') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'is_delete') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
