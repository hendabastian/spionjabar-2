<?php

namespace backend\controllers;

use common\models\perusahaan\Perusahaan;
use Yii;
use backend\models\UserManagement;
use backend\models\UserManagementSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserManagementController implements the CRUD actions for UserManagement model.
 */
class UserManagementController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return (Yii::$app->user->identity->userRole->id == 1);
                        }
                    ],
                    [
                        'actions' => ['profile', 'change-password'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return (Yii::$app->user->identity->userRole->id == 1
                                or Yii::$app->user->identity->userRole->id == 2
                                or Yii::$app->user->identity->userRole->id == 3
                                or Yii::$app->user->identity->userRole->id == 4
                                or Yii::$app->user->identity->userRole->id == 5
                                or Yii::$app->user->identity->userRole->id == 6
                                or Yii::$app->user->identity->userRole->id == 7
                                or Yii::$app->user->identity->userRole->id == 8
                                or Yii::$app->user->identity->userRole->id == 9);
                        }
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserManagement models.
     * @return mixed
     */
    public function actionIndex()
    {
        // \common\models\ActivityLog::insertLog('view', '', 'Menampilkan Daftar User');
        $searchModel = new UserManagementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserManagement model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserManagement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserManagement();
        $model->scenario = 'admin-create-user';
        if ($model->load(Yii::$app->request->post())) {
            $model->generateAuthKey();
            $model->setPassword($model->password);
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UserManagement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->password);
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UserManagement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();

        $model = $this->findModel($id);
        $perusahaan = Perusahaan::findOne(['user_id' => $id]);
        if ($perusahaan != null) {
            $perusahaan->is_delete = 1;
            $perusahaan->save();
        }
        $model->is_delete = 1;

        if ($model->save()) {
            return $this->redirect(['index']);
        }
    }

    /**
     * Profile Page Perusahaan Otobus
     * @param user_id
     * @return mixed
     */
    public function actionProfile()
    {
        if (!Yii::$app->user->isGuest) {
            // \common\models\ActivityLog::insertLog("View", "", "Menampilkan Profil");
            $user = \common\models\User::findOne(Yii::$app->user->identity->id);

            if ($user->load(Yii::$app->request->post())) {
                if ($user->save()) {
                    Yii::$app->session->setFlash('success', 'Data Profile Berhasil Diupdate.');
                    return $this->refresh();
                } else {
                    Yii::$app->session->setFlash('error', 'Data Profile Gagal Diupdate.');
                }
            }

            return $this->render('profile', [
                'user' => $user,
            ]);
        }
    }

    /**
     * Change Password Page Perusahaan Otobus
     * @param user_id
     * @return mixed
     */
    public function actionChangePassword($id)
    {
        $user = $this->findModel(base64_decode($id) / 10 / 100);
        $user->scenario = 'change-password';
        if ($user->load(Yii::$app->request->post()) && $user->validate()) {
            if ($user->validatePassword($user->old_password)) {
                $user->setPassword($user->new_password);
                $user->save();
                Yii::$app->session->setFlash('success', 'Password Berhasil Diupdate.');
                return $this->redirect('profile');
            } else {
                Yii::$app->session->setFlash('danger', 'Password Gagal Diupdate.');
                return $this->redirect('profile');
            }
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('_formChangePassword', [
                'user' => $user,
            ]);
        }
    }

    /**
     * Finds the UserManagement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserManagement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserManagement::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
