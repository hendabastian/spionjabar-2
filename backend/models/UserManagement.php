<?php

namespace backend\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $verification_token
 */
class UserManagement extends \yii\db\ActiveRecord
{
    public $old_password;
    public $new_password;
    public $new_password_repeat;
    public $password;
    public $password_confirm;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'user_role_id'], 'required'],
            [['status'], 'default', 'value' => null],
            [['status'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'verification_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 32],
            [['password_reset_token'], 'unique'],
            [['username'], 'unique'],
            [['created_at', 'updated_at'], 'safe'],
            [['password', 'password_confirm'], 'required', 'on' => 'admin-create-user'],
            ['password', 'string', 'min' => 6],
            ['password_confirm', 'compare', 'compareAttribute' => 'password', 'message' => "Kata Sandi Tidak Cocok"],
            [['new_password', 'new_password_repeat', 'old_password'], 'required', 'on' => 'change-password'],
            ['new_password', 'string', 'min' => 6],
            ['new_password_repeat', 'compare', 'compareAttribute' => 'new_password', 'message' => "Kata Sandi Tidak Cocok"],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'name' => 'Nama Lengkap',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'verification_token' => 'Verification Token',
            'user_role_id' => 'User Role',
            'password' => 'Kata Sandi',
            'password_confirm' => 'Konfirmasi Kata Sandi',
            'new_password' => 'Kata Sandi',
            'new_password_repeat' => 'Konfirmasi Kata Sandi',
            'old_password' => 'Kata Sandi Saat Ini',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord) {
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        } else {
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function getRole()
    {
        return $this->hasOne(\common\models\UserRole::class, ['id' => 'user_role_id']);
    }
}
