<?php

namespace backend\assets;

use yii\bootstrap4\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

class ArgonAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
//        'argon/vendor/nucleo/css/nucleo.css',
        'argon/css/fontawesome/css/all.min.css',
        'argon/css/argon.min5438.css',
    ];
    public $js = [
//        'argon/vendor/bootstrap/dist/js/bootstrap.bundle.min.js',
        'argon/vendor/js-cookie/js.cookie.js',
        'argon/vendor/jquery.scrollbar/jquery.scrollbar.min.js',
        'argon/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js',
        'argon/vendor/chart.js/dist/Chart.min.js',
        'argon/vendor/chart.js/dist/Chart.extension.js',
        'argon/js/argon.min5438.js',
        'js/ajax-modalbs4-popup.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset'
    ];
}