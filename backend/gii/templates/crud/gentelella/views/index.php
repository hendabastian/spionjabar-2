<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "yii\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>


      <div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">

        <div class="x_panel">
          <div class="x_title">
            <h2><?= "<?= " ?>Html::encode($this->title) ?></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>

            <div class="clearfix"></div>

          </div>

          <div class="x_content">

            <?= $generator->enablePjax ? "    <?php Pjax::begin(); ?>\n" : '' ?>
            <?php if(!empty($generator->searchModelClass)): ?>
            <?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
            <?php endif; ?>

                <p class="text-muted font-13 m-b-30">
                    <?= "<?= " ?>Html::a(<?= $generator->generateString('Tambah ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['create'], ['class' => 'btn btn-success']) ?>
                </p>
          <div class="table-responsive">
            <?php if ($generator->indexWidgetType === 'grid'): ?>
                <?= "<?= " ?>GridView::widget([
                    'dataProvider' => $dataProvider,
                    <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
                        ['class' => 'yii\grid\SerialColumn'],

                      <?php
                      $count = 0;
                      if (($tableSchema = $generator->getTableSchema()) === false) {
                          foreach ($generator->getColumnNames() as $name) {
                              if (in_array($name, ['id', 'is_active', 'is_delete', 'created_at', 'updated_at', 'created_by', 'updated_by'])) {
                                  echo "            //'" . $name . "',\n";
                              } else {
                                  echo "            '" . $name . "',\n";
                              }
                          }
                      } else {
                          foreach ($tableSchema->columns as $column) {
                              $format = $generator->generateColumnFormat($column);

                              if (in_array($column->name, ['id', 'is_active', 'is_delete', 'created_at', 'updated_at', 'created_by', 'updated_by'])) {
                                  echo "            //'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                              } else {
                                  echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                              }
                          }
                      }
                      ?>

                        ['class' => 'yii\grid\ActionColumn', 'header' => 'Aksi', 'contentOptions' =>['style' =>'width:80px; text-align:center;']],
                    ],
                ]); ?>
            <?php else: ?>
                <?= "<?= " ?>ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => function ($model, $key, $index, $widget) {
                        return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
                    },
                ]) ?>
            <?php endif; ?>
          </div>
            <?= $generator->enablePjax ? "    <?php Pjax::end(); ?>\n" : '' ?>

          </div>
        </div>

      </div>
