<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin([
      'layout'=>'horizontal',
      'fieldConfig' => [
          'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
          'horizontalCssClasses' => [
              'label' => 'col-sm-3',
              'offset' => '',
              'wrapper' => 'col-sm-9',
              'error' => '',
              'hint' => '',
          ],
      ],
    ]); ?>

    <?php foreach ($generator->getColumnNames() as $attribute) {
        if (in_array($attribute, ['is_delete', 'created_at', 'updated_at', 'created_by', 'updated_by'])) {
            continue;
        }
        if (in_array($attribute, $safeAttributes)) {
          if (in_array($attribute, ['is_active'])) {?>
            
            <div class="form-group">
              <div class="col-md-offset-3 col-md-9">
                  <?= "    <?= " ?> $form->field($model, 'is_active')->checkbox() ?>
              </div>
            </div>

          <?php  continue;
          }else{
            echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
          }
        }
    } ?>



    <div class="form-group">
      <div class="col-md-offset-3 col-md-9">
        <?= "<?= " ?>Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
        <?= "<?= "?>Html::a('<i class="fa fa-remove"></i> Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
      </div>
    </div>

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
