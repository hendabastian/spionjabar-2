<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\UserRole;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\UserManagement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-management-form">

    <?php $form = ActiveForm::begin([
      'layout'=>'horizontal',
      'fieldConfig' => [
          'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
          'horizontalCssClasses' => [
              'label' => 'col-sm-3',
              'offset' => '',
              'wrapper' => 'col-sm-9',
              'error' => '',
              'hint' => '',
          ],
      ],
    ]); ?>

    <?= $form->field($model, 'user_role_id')->widget(Select2::classname(), [
               'data' => ArrayHelper::map(UserRole::find()->where(['is_delete' => 0])->All(), 'id', 'role'),
               'language' => 'en',
               'options' => ['placeholder' => '-Pilih User Role-'],
               'pluginOptions' => [
                   'allowClear' => true
               ],
            ]);?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php if ($model->isNewRecord): ?>

      <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'password_confirm')->passwordInput(['maxlength' => true]) ?>

    <?php else: ?>

      <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'placeholder' => 'Kosongkan jika tidak ingin merubah password']) ?>

      <?= $form->field($model, 'password_confirm')->passwordInput(['maxlength' => true, 'placeholder' => 'Kosongkan jika tidak ingin merubah password']) ?>

    <?php endif; ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
               'data' => [10 => 'Active', 20 => 'UnVerified', 0 => 'InActive'],
               'language' => 'en',
               'options' => ['placeholder' => '-Pilih Status-'],
               'pluginOptions' => [
                   'allowClear' => true
               ],
            ]);?>

    <div class="form-group">
      <div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('<i class="fa fa-remove"></i> Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
