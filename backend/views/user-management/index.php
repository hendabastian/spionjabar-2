<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserManagementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Management';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="user-management-index">

    <div class="card">
        <div class="card-header">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>

        <div class="card-body">
            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p class="text-muted font-13 m-b-30">
                <?= Html::a('<span class="fa fa-plus-circle"></span> Tambah User', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        // 'id',
                        [
                            'attribute' => 'user_role_id',
                            'filter' => \yii\helpers\ArrayHelper::map(\common\models\UserRole::find()->where(['is_delete' => 0])->all(), 'id', 'role'),
                            'value' => function ($data) {
                                return $data->role->role;
                            }
                        ],
                        'first_name',
                        'last_name',
                        'username',
                        // 'auth_key',
                        // 'password_hash',
                        // 'password_reset_token',
                        'email:email',
                        [
                            'attribute' => 'status',
                            'format' => 'RAW',
                            'value' => function ($data) {
                                $label = "";
                                if ($data->status == 10) {
                                    $label = "<span class='label label-success'>Active</span>";
                                } elseif ($data->status == 20) {
                                    $label = "<span class='label label-danger'>UnVerified</span>";
                                } elseif ($data->status == 0) {
                                    $label = "<span class='label label-danger'>Inactive</span>";
                                }

                                return $label;
                            },
                            'contentOptions' => ['style' => 'width: 80px'],
                        ],
                        //'created_at',
                        //'updated_at',
                        //'verification_token',

                        ['class' => 'kartik\grid\ActionColumn', 'header' => 'Aksi', 'contentOptions' => ['style' => 'width:80px; text-align:center;']],
                    ],
                ]); ?>
            </div>
            <?php Pjax::end(); ?>

        </div>
    </div>

</div>
