<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\UserManagement */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'User Managements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="x_panel">
  <div class="x_title">
    <h2><?= Html::encode($this->title) ?></h2>
    <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
    </ul>

    <div class="clearfix"></div>

  </div>
  <p>
        <?= Html::a('Daftar', ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
  <div class="x_content">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            [
               'attribute' => 'user_role_id',
               'filter' => \yii\helpers\ArrayHelper::map(\common\models\UserRole::find()->where(['is_delete'=>0])->all(), 'id', 'role'),
               'value' => function($data){
                 return $data->role->role;
               }
            ],
            'first_name',
            'last_name',
            'username',
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            'email:email',
            [
               'attribute' => 'status',
               'format' => 'RAW',
               'value' => function($data){
                $label = "";
                if ($data->status == 10) {
                    $label = "<span class='label label-success'>Active</span>";
                }elseif ($data->status == 20) {
                    $label = "<span class='label label-danger'>UnVerified</span>";
                }elseif ($data->status == 0) {
                    $label = "<span class='label label-danger'>Inactive</span>";
                }

                return $label;
               },
            ],
            // 'created_at',
            // 'updated_at',
            // 'verification_token',
        ],
    ]) ?>

    </div>
  </div>
</div>
