<?php

use yii\helpers\Url;
use common\models\master\JenisLayanan;

?>

<div class="navbar-inner">
    <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Main Menu</span>
            <span class="docs-mini"><i class="fas fa-menu"></i></span>
        </h6>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link <?= Yii::$app->controller->action->id == "index" ? 'active' : ''; ?>"
                   href="<?= Url::toRoute('/site/index'); ?>">
                    <i class="fas fa-home"></i> <span class="nav-link-text">Dashboard</span>
                </a>
            </li>
            <li class="nav-item <?= Yii::$app->controller->id == "regulasi" ? 'active' : ''; ?>">
                <a class="nav-link" href="#regulasi-menu" data-toggle="collapse" role="button" aria-expanded="false"
                   aria-controls="regulasi-menu">
                    <i class="fas fa-balance-scale"></i> <span class="nav-link-text">Regulasi</span>
                </a>
                <div class="collapse" id="regulasi-menu">
                    <ul class="nav nav-sm flex-column">
                        <?php foreach (JenisLayanan::find()->where(['is_delete' => 0, 'is_active' => 1])->all() as $key => $value) : ?>
                            <li class="nav-item">
                                <a class="nav-link"
                                   href="<?= Url::toRoute(['/perusahaan/regulasi', 'jenisLayananId' => $value->id]); ?>">
                                    <span class="sidenav-mini-icon"></span>
                                    <span class="sidenav-normal">
                                        <?= $value->jenis_layanan ?>
                                    </span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </li>
            <li class="nav-item <?php if (Yii::$app->controller->id == 'perusahaan') {
                echo 'active';
            } ?>">
                <a class="nav-link" href="#perusahaan-menu" data-toggle="collapse" role="button" aria-expanded="false"
                   aria-controls="perusahaan-menu">
                    <i class="fas fa-building"></i> <span class="nav-link-text">Perusahaan</span>
                </a>
                <div class="collapse" id="perusahaan-menu">
                    <ul class="nav nav-sm flex-column">
                        <li class="<?php if (Yii::$app->controller->id == 'perusahaan') {
                            echo 'active';
                        } ?>">
                            <a class="nav-link" href="<?= Url::toRoute('/perusahaan/perusahaan'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">Data Perusahaan</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <hr class="my-3">
        <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">System</span>
            <span class="docs-mini"><i class="fas fa-menu"></i></span>
        </h6>
        <ul class="navbar-nav">
            <li class="nav-item <?= Yii::$app->controller->id == "master" ? 'active' : ''; ?>">
                <a class="nav-link" href="#master-menu" data-toggle="collapse" role="button" aria-expanded="false"
                   aria-controls="master-menu">
                    <i class="fas fa-database"></i> <span class="nav-link-text">Pengaturan Data</span>
                </a>
                <div class="collapse" id="master-menu">
                    <ul class="nav nav-sm flex-column">
                        <li>
                            <a class="nav-link" href="<?= Url::toRoute('/master/terminal'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">
                                Data Terminal
                                </span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="<?= Url::toRoute('/master/trayek'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">
                                Data Trayek
                                </span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="<?= Url::toRoute('/master/tipe-terminal'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">
                                Tipe Terminal
                                </span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="<?= Url::toRoute('/master/jenis-badan-usaha'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">
                                Jenis Badan Usaha
                                </span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="<?= Url::toRoute('/master/jenis-dokumen-perusahaan'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">
                                Jenis Dokumen Perusahaan
                                </span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="<?= Url::toRoute('/master/jenis-kendaraan'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">
                                Jenis Kendaraan
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item <?= Yii::$app->controller->id == "master" ? 'active' : ''; ?>">
                <a class="nav-link" href="#master-permohonan-menu" data-toggle="collapse" role="button" aria-expanded="false"
                   aria-controls="master-permohonan-menu">
                    <i class="fas fa-envelope-open-text"></i> <span class="nav-link-text">Jenis Permohonan</span>
                </a>
                <div class="collapse" id="master-permohonan-menu">
                    <ul class="nav nav-sm flex-column">
                        <li>
                            <a class="nav-link" href="<?= Url::toRoute('/master/jenis-layanan'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">
                                Jenis Layanan
                                </span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="<?= Url::toRoute('/master/jenis-angkutan'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">
                                Jenis Angkutan
                                </span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="<?= Url::toRoute('/master/jenis-permohonan'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">
                                Jenis Permohonan
                                </span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="<?= Url::toRoute('/master/tipe-permohonan'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">
                                Tipe Permohonan
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item <?= Yii::$app->controller->id == "master" ? 'active' : ''; ?>">
                <a class="nav-link" href="#user-menu" data-toggle="collapse" role="button" aria-expanded="false"
                   aria-controls="user-menu">
                    <i class="fas fa-user-circle"></i> <span class="nav-link-text">User Accounts</span>
                </a>
                <div class="collapse" id="user-menu">
                    <ul class="nav nav-sm flex-column">
                        <li>
                            <a class="nav-link" href="<?= Url::toRoute('user-management/index'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">
                                User Login
                                </span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="<?= Url::toRoute('user-category/index'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">
                                User Role
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
