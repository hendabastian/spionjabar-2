<?php

use yii\helpers\Url;
use common\models\master\JenisLayanan;
?>

<div class="menu_section">
  <h3>Main Menu</h3>
  <ul class="nav side-menu">
    <li><a href="<?= Url::toRoute('/site/index'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
  </ul>
</div>

<div class="menu_section">
  <h3>Cek IWKBU Permohonan</h3>
  <ul class="nav side-menu">
    <li><a href="<?= Url::toRoute('/verifikator/verifikasi-ijin-prinsip/index'); ?>"><i class="fa fa-check-circle"></i> Ijin Prinsip</a></li>
    <li><a href="<?= Url::toRoute('/verifikator/verifikasi-info-kepengusahaan/index'); ?>"><i class="fa fa-user"></i> Info Kepengusahaan</a></li>
    <li><a href="<?= Url::toRoute('/verifikator/verifikasi-permohonan-rekomendasi/index'); ?>"><i class="fa fa-archive"></i> Permohonan Rekomendasi</a></li>
  </ul>
</div>

<div class="menu_section">
  <h3>Cek Perusahaan & Kendaraan</h3>
  <ul class="nav side-menu">
    <li><a href="<?= Url::toRoute('/cari-perusahaan/index'); ?>"><i class="fa fa-building"></i> Cek Perusahaan</a></li>
    <li><a href="<?= Url::toRoute('/cari-kendaraan/index'); ?>"><i class="fa fa-bus"></i> Cek Kendaraan</a></li>
  </ul>
</div>