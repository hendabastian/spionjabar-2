<footer class="footer pt-0 mx-4">
    <div class="row justify-content-end">
        <div class="col-lg-6 align-self-end">
            <div class="copyright text-right text-muted">
                SPIONJABAR - &copy; 2020 <a href="http://dishub.jabarprov.go.id/" class="font-weight-bold ml-1" target="_blank">Dinas Perhubungan Provinsi Jawa Barat</a>
            </div>
        </div>
    </div>
</footer>