<?php

use yii\helpers\Url;
use common\models\master\JenisLayanan;

?>

<div class="navbar-inner">
    <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Main Menu</span>
            <span class="docs-mini"><i class="fas fa-menu"></i></span>
        </h6>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link <?= Yii::$app->controller->action->id == "index" ? 'active' : ''; ?>"
                   href="<?= Url::toRoute('/site/index'); ?>">
                    <i class="fas fa-home"></i> <span class="nav-link-text">Dashboard</span>
                </a>
            </li>
            <?php foreach (JenisLayanan::find()->where(['is_active' => 1, 'is_delete' => 0])->all() as $index => $data) : ?>
                <li class="nav-item">
                    <a class="nav-link"
                       href="#<?= strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $data->jenis_layanan), '-')) ?>-menu"
                       data-toggle="collapse" role="button"
                       aria-expanded="false"
                       aria-controls="<?= strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $data->jenis_layanan), '-')) ?>-menu">
                        <i class="fas fa-file"></i> <span
                                class="nav-link-text"><?= $data->jenis_layanan ?></span>
                    </a>
                    <div class="collapse"
                         id="<?= strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $data->jenis_layanan), '-')) ?>-menu">
                        <ul class="nav nav-sm flex-column">
                            <?php foreach ($data->jenisAngkutan as $indexAngkutan => $dataAngkutan) : ?>
                                <li class="nav-item">
                                    <a class="nav-link"
                                       href="<?= Url::toRoute(['/permohonan/permohonan/index', 'jenis_angkutan_id' => $dataAngkutan->id]) ?>">
                                        <span class="sidenav-mini-icon"></span>
                                        <span class="sidenav-normal">
                                        <?= $dataAngkutan->jenis_angkutan ?>
                                    </span>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </li>
            <?php endforeach; ?>
            <li class="nav-item <?php if (Yii::$app->controller->id == 'perusahaan') {
                echo 'active';
            } ?>">
                <a class="nav-link" href="#perusahaan-menu" data-toggle="collapse" role="button" aria-expanded="false"
                   aria-controls="perusahaan-menu">
                    <i class="fas fa-building"></i> <span class="nav-link-text">Perusahaan</span>
                </a>
                <div class="collapse" id="perusahaan-menu">
                    <ul class="nav nav-sm flex-column">
                        <li class="<?php if (Yii::$app->controller->id == 'perusahaan') {
                            echo 'active';
                        } ?>">
                            <a class="nav-link" href="<?= Url::toRoute('/approval/perusahaan'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">Data Perusahaan</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item <?php if (Yii::$app->controller->id == 'report') {
                echo 'active';
            } ?>">
                <a class="nav-link" href="#report-menu" data-toggle="collapse" role="button" aria-expanded="false"
                   aria-controls="report-menu">
                    <i class="fas fa-book"></i> <span class="nav-link-text">Report</span>
                </a>
                <div class="collapse" id="report-menu">
                    <ul class="nav nav-sm flex-column">
                        <li class="<?php if (Yii::$app->controller->id == 'report') {
                            echo 'active';
                        } ?>">
                            <a class="nav-link" href="<?= Url::toRoute('/report/report/permohonan-terverifikasi'); ?>">
                                <span class="sidenav-mini-icon"></span>
                                <span class="sidenav-normal">Permohonan Diverifikasi</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>

