<!-- Main content -->
<div class="main-content" id="panel">
    <!--Topnav-->
    <?= $this->render('tpl_top') ?>
    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6">
                        <h6 class="h2 text-white d-inline-block mb-0"><?= $this->title ?></h6>
                    </div>
                    <div class="col-lg-6">
                        <div class="float-right">
                            <?= common\widgets\Bs4Breadcrumbs::widget([
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                'navOptions' => ['class' => 'd-none d-md-inline-block ml-md-4']
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <?= \common\widgets\Bs4Alert::widget() ?>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6 h-100">
        <div class="row">
            <div class="col-lg-12">

                <?= $content ?>
            </div>
        </div>
        <!-- Footer -->
        <?= $this->render('tpl_footer') ?>
    </div>
</div>