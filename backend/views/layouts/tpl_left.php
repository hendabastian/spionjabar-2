<?php

use yii\helpers\Url;
use common\models\User;
use common\models\UserRole;

?>

<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  d-flex  align-items-center">
            <a class="navbar-brand" href="dashboard.html">
                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/img/logo.png" class="navbar-brand-img"
                     alt="...">
            </a>
            <div class=" ml-auto ">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!Yii::$app->user->isGuest && User::findOne(Yii::$app->user->identity->id)->user_role_id == 1) : ?>
            <?= $this->render('_menu_administrator'); ?>
        <?php elseif (!Yii::$app->user->isGuest && User::findOne(Yii::$app->user->identity->id)->user_role_id == 3 || User::findOne(Yii::$app->user->identity->id)->user_role_id == 5 || User::findOne(Yii::$app->user->identity->id)->user_role_id == 6 || User::findOne(Yii::$app->user->identity->id)->user_role_id == 7 || User::findOne(Yii::$app->user->identity->id)->user_role_id == 9) : ?>
            <?= $this->render('_menu_approval'); ?>
        <?php elseif (!Yii::$app->user->isGuest && User::findOne(Yii::$app->user->identity->id)->user_role_id == 4) : ?>
            <?= $this->render('_menu_jr'); ?>
        <?php elseif (!Yii::$app->user->isGuest && User::findOne(Yii::$app->user->identity->id)->user_role_id == 8) : ?>
            <?= $this->render('_menu_cetak') ?>
        <?php endif; ?>

    </div>
</nav>