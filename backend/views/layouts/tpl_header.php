<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use backend\assets\ArgonAsset;
use yii\helpers\Html;

ArgonAsset::register($this);

$baseTheme = Yii::$app->getUrlManager()->getBaseUrl() . '/gentelella-master/';

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= $baseTheme; ?>favicon.ico" type="image/x-icon">
    <?= Html::csrfMetaTags() ?>
    <title><?= Yii::$app->name . ' | ' . Html::encode($this->title) ?></title>
    <style>
        .card .table td, .card .table th {
            font-size: 12pt;
            padding-right: 1.5rem;
            padding-left: 1.5rem;
        }
        .table td, .table th {
            font-size: 12pt;
            white-space: nowrap;
        }

        .badge {
            font-size: 100%;
            font-weight: 600;
            line-height: 1;
            display: inline-block;
            padding: .35rem .375rem;
            transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            text-align: center;
            vertical-align: baseline;
            white-space: nowrap;
            border-radius: .375rem;
        }
    </style>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php include_once('tpl_left.php') ?>
<?php include_once('tpl_right.php') ?>
<?php include_once('tpl_footer.php') ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
