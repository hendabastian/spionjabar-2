<hr>
<div class="row">
    <h4 class="text-primary">Tindak Lanjut Permohonan</h4> <br>
    <div class="col-md-4">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h3 class="panel-title">Menunggu Dicetak</h3>
            </div>
            <div class="panel-body">
                <li>Ijin Prinsip: <?= common\models\perijinan\IjinPenyelenggaraan::find()->where(['status_verifikasi' => 7])->andWhere(['is_delete' => 0])->count(); ?></li>
                <li>Info Kepengusahaan: <?= common\models\perijinan\IjinPersetujuanPenambahan::find()->where(['status_verifikasi' => 7])->andWhere(['is_delete' => 0])->count(); ?></li>
                <li>Permohonan Rekomendasi Realisasi Ijin Prinsip: <?= common\models\perijinan\IjinRealisasi::find()->where(['status_verifikasi' => 7])->andWhere(['is_delete' => 0])->count(); ?></li>
                <li>Permohonan Rekomendasi Perpanjangan SK: <?= common\models\perijinan\RekomendasiPerpanjanganSk::find()->where(['status_verifikasi' => 7])->andWhere(['is_delete' => 0])->count(); ?></li>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <h4 style="color:#1ABB9C">Aktivitas Terakhir</h4>
        <p><?= date('Y-m-d h:m:s') ?></p>
    </div>
</div>