<table class="table table-bordered table-striped table-lg">
    <tr>
        <th colspan="2" scope="col">Dokumen Yang Harus Diproses</th>
        <th scope="col">Jumlah</th>
    </tr>
    <tr>
        <th colspan="2">Permohonan Izin</th>
        <td></td>
    </tr>
    <tr>
        <th colspan="2" class="text-right">AKDP</th>
        <td><?= $permohonan->where(['jenis_angkutan_id' => \common\models\master\JenisAngkutan::AKDP])->count() ?></td>
    </tr>
    <tr>
        <th colspan="2" class="text-right">AJDP</th>
        <td><?= $permohonan->where(['jenis_angkutan_id' => \common\models\master\JenisAngkutan::AJDP])->count() ?></td>
    </tr>
    <tr>
        <th colspan="2" class="text-right">ASK</th>
        <td><?= $permohonan->where(['jenis_angkutan_id' => \common\models\master\JenisAngkutan::ASK])->count() ?></td>
    </tr>
    <tr>
        <th colspan="2" class="text-right">TAXI</th>
        <td><?= $permohonan->where(['jenis_angkutan_id' => \common\models\master\JenisAngkutan::TAXI])->count() ?></td>
    </tr>
    <tr>
        <th colspan="2">Permohonan Rekomendasi</th>
        <td></td>
    </tr>
    <tr>
        <th colspan="2" class="text-right">AKAP</th>
        <td><?= $permohonan->where(['jenis_angkutan_id' => \common\models\master\JenisAngkutan::AKAP])->count() ?></td>
    </tr>
    <tr>
        <th colspan="2" class="text-right">Pariwisata</th>
        <td><?= $permohonan->where(['jenis_angkutan_id' => \common\models\master\JenisAngkutan::PARIWISATA])->count() ?></td>
    </tr>
    <tr>
        <th colspan="2" class="text-right">AJAP</th>
        <td><?= $permohonan->where(['jenis_angkutan_id' => \common\models\master\JenisAngkutan::AJAP])->count() ?></td>
    </tr>
    <tr>
        <th colspan="2" class="text-right">Lainnya</th>
        <td><?= $permohonan->where(['jenis_angkutan_id' => \common\models\master\JenisAngkutan::LAINNYA])->count() ?></td>
    </tr>
</table>
