<?php

use common\components\Helper;
use common\models\master\JenisAngkutan;
use common\models\permohonan\Permohonan;
use common\models\UserRole;
use yii\helpers\Url;

/** @var \common\models\permohonan\PermohonanVerifikasi $permohonanVerifikasi */
$permohonan = Permohonan::find()->where(['is_delete' => 0, 'status_verifikasi' => Permohonan::STATUS_KABID_APPROVED]);
$listUser = \common\models\User::find()->where(['user_role_id' => [UserRole::VERIFIKATOR, UserRole::KEPALA_SEKSI, UserRole::KEPALA_BIDANG, UserRole::KEPALA_DINAS]])->select(['id', 'name'])->asArray()->all();
$verifikator = $permohonanVerifikasi->find()->where(['diverifikasi_oleh' => array_values($listUser)])->count();
?>

<div class="row">
    <div class="col-lg-3">
        <div class="card border border-light rounded">
            <div class="card-body">
                <div class="card-title">
                    Permohonan AKDP belum diproses
                </div>
                <a href="<?= Url::toRoute(['permohonan/permohonan/index', 'jenis_angkutan_id' => JenisAngkutan::AKDP]) ?>">
                    <h1><?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::AKDP, 'status_verifikasi' => Permohonan::STATUS_KABID_APPROVED])->count() ?></h1>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card border border-light rounded">
            <div class="card-body">
                <div class="card-title">
                    Permohonan AJDP belum diproses
                </div>
                <a href="<?= Url::toRoute(['permohonan/permohonan/index', 'jenis_angkutan_id' => JenisAngkutan::AJDP]) ?>">
                    <h1><?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::AJDP, 'status_verifikasi' => Permohonan::STATUS_KABID_APPROVED])->count() ?></h1>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card border border-light rounded">
            <div class="card-body">
                <div class="card-title">
                    Permohonan ASK belum diproses
                </div>
                <a href="<?= Url::toRoute(['permohonan/permohonan/index', 'jenis_angkutan_id' => JenisAngkutan::ASK]) ?>">
                    <h1><?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::ASK, 'status_verifikasi' => Permohonan::STATUS_KABID_APPROVED])->count() ?></h1>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card border border-light rounded">
            <div class="card-body">
                <div class="card-title">
                    Permohonan TAXI belum diproses
                </div>
                <a href="<?= Url::toRoute(['permohonan/permohonan/index', 'jenis_angkutan_id' => JenisAngkutan::TAXI]) ?>">
                    <h1><?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::TAXI, 'status_verifikasi' => Permohonan::STATUS_KABID_APPROVED])->count() ?></h1>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card border border-light rounded">
            <div class="card-body">
                <div class="card-title">
                    Permohonan AKAP belum diproses
                </div>
                <a href="<?= Url::toRoute(['permohonan/permohonan/index', 'jenis_angkutan_id' => JenisAngkutan::AKAP]) ?>">
                    <h1><?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::AKAP, 'status_verifikasi' => Permohonan::STATUS_KABID_APPROVED])->count() ?></h1>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card border border-light rounded">
            <div class="card-body">
                <div class="card-title">
                    Permohonan Pariwisata belum diproses
                </div>
                <a href="<?= Url::toRoute(['permohonan/permohonan/index', 'jenis_angkutan_id' => JenisAngkutan::PARIWISATA]) ?>">
                    <h1><?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::PARIWISATA, 'status_verifikasi' => Permohonan::STATUS_KABID_APPROVED])->count() ?></h1>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card border border-light rounded">
            <div class="card-body">
                <div class="card-title">
                    Permohonan AJAP belum diproses
                </div>
                <a href="<?= Url::toRoute(['permohonan/permohonan/index', 'jenis_angkutan_id' => JenisAngkutan::AJAP]) ?>">
                    <h1><?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::AJAP, 'status_verifikasi' => Permohonan::STATUS_KABID_APPROVED])->count() ?></h1>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="row justify-content-center">
<!--    <div class="col-lg-5">-->
<!--        <?//= $this->render('_table_unprocessed', [
//            'permohonan' => $permohonan
//        ]) ?>-->
<!--    </div>-->
    <div class="col-lg-7">
        <div id="performa-chart"></div>
    </div>
</div>

<link rel="stylesheet" href="<?= Yii::$app->urlManager->createAbsoluteUrl('highcharts/css/highcharts.css') ?>">

<script src="<?= Yii::$app->urlManager->createAbsoluteUrl('highcharts/highcharts.js') ?>"></script>
<script>
    Highcharts.chart('performa-chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Jumlah Permohonan Diverifikasi pada <?=Helper::getBulanLengkap(date('m')) . ' ' . date('Y')?>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Verifikator',
            colorByPoint: true,
            data: [{
                name: 'AKDP',
                y: <?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::AKDP])->count() ?>,
            }, {
                name: 'AJDP',
                y: <?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::AJDP])->count() ?>
            }, {
                name: 'ASK',
                y: <?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::ASK])->count() ?>
            }, {
                name: 'TAXI',
                y: <?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::TAXI])->count() ?>
            }, {
                name: 'AKAP',
                y: <?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::AKAP])->count() ?>
            }, {
                name: 'PARIWISATA',
                y: <?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::PARIWISATA])->count() ?>
            }, {
                name: 'AJAP',
                y: <?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::AJAP])->count() ?>
            }, {
                name: 'Lainnya',
                y: <?= $permohonan->where(['jenis_angkutan_id' => JenisAngkutan::LAINNYA])->count() ?>
            }]
        }]
    });
</script>