<?php

/* @var $this yii\web\View */

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
$baseTheme = Yii::$app->getUrlManager()->getBaseUrl() . '/dashjr/';
$role_id = Yii::$app->user->identity->user_role_id;

$status_verifikasi = 0;

switch ($role_id) {
    case 3:
        $status_verifikasi = 1;
        break;
    case 5:
        $status_verifikasi = 3;
        break;
    case 6:
        $status_verifikasi = 4;
        break;
    case 9:
        $status_verifikasi = 5;
        break;
    case 7:
        $status_verifikasi = 6;
        break;
    case 8:
        $status_verifikasi = 7;
}
?>


<?php if ((\common\models\User::findOne(Yii::$app->user->identity->id)->user_role_id == 4)) : ?>

    <section id="cek-kendaraan" class="info-area relative section-gap" style="padding-top: 15%;">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row justify-content-start">
                <div class="col-lg-12">

                    <h1 class="text-white">Cek Kendaraan</h1>
                    <br>
                    <div class="alert alert-warning" role="alert">
                        <strong>Contoh:</strong> X-1234-XX
                    </div>
                    <form class="form-area" action="<?= \yii\helpers\Url::toRoute(['cari-kendaraan/search']); ?>">
                        <input name="nopol" id="nopol" placeholder="Masukan Nama Perusahaan"
                               onfocus="this.placeholder = ''"
                               onblur="this.placeholder = 'Masukan Nomor Polisi Kendaraan'"
                               class="common-input mb-20 form-control" required="" type="text">
                        <button type="submit" class="primary-btn header-btn text-uppercase">Cari</button>
                        <a href="<?= \yii\helpers\Url::toRoute(['cari-kendaraan/index']); ?>"
                           class="primary-btn header-btn text-uppercase">Lihat Daftar Kendaraan</a>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="cek-perusahaan" class="call-action-area section-gap">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-9">
                    <h1 class="text-white">Cek Perusahaan</h1>

                    <br>
                    <form class="form-area" action="<?= \yii\helpers\Url::toRoute(['cari-perusahaan/search']); ?>">
                        <div class="alert alert-warning" role="alert">
                            <strong>Info!</strong> Nama Perusahaan tidak harus lengkap, pencarian berdasarkan keyword
                            yang dimasukkan.
                        </div>
                        <input name="name" placeholder="Masukan Nama Kendaraan" onfocus="this.placeholder = ''"
                               onblur="this.placeholder = 'Masukan Nama Kendaraan'"
                               class="common-input mb-20 form-control" required="" type="text">
                        <button type="submit" class="primary-btn header-btn text-uppercase">Cari</button>
                        <a href="<?= \yii\helpers\Url::toRoute(['cari-perusahaan/index']); ?>"
                           class="primary-btn header-btn text-uppercase">Lihat Daftar Perusahaan</a>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="details-area pt-50">
        <div class="container">
            <div class="details-section">
                <div class="row align-items-center">
                    <div class="col-lg-3 detials-left">
                        <h5 class="text-uppercase">SPIONJABAR</h5>
                        <p>
                            Direktorat Angkutan Jalan.
                        </p>
                    </div>
                    <div class="col-lg-9 detials-right">
                        <p>
                            Sistem Perijinan Online Angkutan dan Multimoda (SPIONJABAR) adalah sebuah sistem informasi
                            yang digunakan untuk membantu proses perizinan penyelenggaraan angkutan dan multimoda
                            dilingkungan Kementerian Perhubungan.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about-area" id="about">
        <div class="container-fluid">
            <div class="row d-flex justify-content-end align-items-center">
                <div class="col-lg-6 col-md-12 about-left">
                    <h1>Tentang SPIONJABAR</h1>
                    <p>
                        Sistem Perijinan Online Angkutan dan Multimoda (SPIONJABAR) adalah sebuah sistem informasi yang
                        digunakan untuk membantu proses perizinan penyelenggaraan angkutan dan multimoda dilingkungan
                        Kementerian Perhubungan.
                    </p>
                    <a href="#" class="primary-btn header-btn text-uppercase">See Details</a>
                </div>
                <div class="col-lg-6 col-md-12 about-right no-padding">
                    <img class="img-fluid" src="<?= $baseTheme; ?>/img/c1.jpg" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- End Dashboard JR -->

<?php else : ?>
    <!-- Dashboard DISHUB -->
    <div class="dashboard-create">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Selamat Datang,</h4>
                        <h2 style="color:#1ABB9C"><?= Yii::$app->user->identity->name ?></h2>
                        <p>Bag. <?= Yii::$app->user->identity->userRole->role ?></p>
                        <hr>
                        <?php if (($role_id) == 3) :
                            echo $this->render('_dashboard_verifikator', [
                                'permohonanVerifikasi' => $permohonanVerifikasi
                            ]);

                        elseif (($role_id) == 5) :
                            echo $this->render('_dashboard_kasi', [
                                'permohonanVerifikasi' => $permohonanVerifikasi
                            ]);

                        elseif (($role_id) == 6) :
                            echo $this->render('_dashboard_kabid', [
                                'permohonanVerifikasi' => $permohonanVerifikasi
                            ]);

                        elseif (($role_id) == 9) :
                            echo $this->render('_dashboard_sekretaris', [
                                'permohonanVerifikasi' => $permohonanVerifikasi
                            ]);

                        elseif (($role_id) == 7) :
                            echo $this->render('_dashboard_kadis', [
                                'permohonanVerifikasi' => $permohonanVerifikasi
                            ]);

                        elseif (($role_id) == 8) :
                            echo $this->render('_dashboard_penerbit', [
                                'permohonanVerifikasi' => $permohonanVerifikasi
                            ]);

                        endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif
?>

</div>