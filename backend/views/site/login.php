<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php /*
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to login:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

*/ ?>

<div class="animate form login_form">
    <section class="login_content">

        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <h1>Login Form</h1>

        <div class="form-group" style="margin-bottom: 24pt;">
            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/img/logo.png" style="max-width: 100%;" alt="">
        </div>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Username'])->label(false) ?>
        <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password'])->label(false) ?>

        <div style="text-align:left; margin-bottom:20px">
            <?= $form->field($model, 'rememberMe')->checkbox() ?>
        </div>
        <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::classname())->label(false) ?>

        <div style="text-align:left; margin-bottom:10px">
            <?= Html::submitButton('<i class="fa fa-lock"></i> Login', ['class' => 'btn btn-default', 'name' => 'login-button']) ?>
        </div>

        <div class="clearfix"></div>

        <div class="separator">


            <div class="clearfix"></div>
            <div>
                <p>© 2020 Dinas Perhubungan Jawa Barat.</p>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </section>
</div>