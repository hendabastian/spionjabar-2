<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserRoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Roles';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="user-role-index">

    <div class="card">
        <div class="card-body">
            <div class="card-title">
                <h2><?= Html::encode($this->title) ?></h2>
            </div>
            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p class="text-muted font-13 m-b-30">
                <?= Html::a('Tambah User Role', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        'role',
                        'keterangan:ntext',
                        //'is_delete',
                        //'created_at',
                        //'updated_at',
                        //'created_by',
                        //'updated_by',

                        ['class' => 'yii\grid\ActionColumn', 'header' => 'Aksi', 'contentOptions' => ['style' => 'width:80px; text-align:center;']],
                    ],
                ]); ?>
            </div>
            <?php Pjax::end(); ?>

        </div>
    </div>

</div>
