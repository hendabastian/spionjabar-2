<?php

use yii\helpers\Url;
use common\models\master\JenisLayanan;
?>

<div class="menu_section">
  <h3>Main Menu</h3>
  <ul class="nav side-menu">
    <li><a href="<?= Url::toRoute('/site/index'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
  </ul>
</div>

<div class="menu_section">
  <h3>Cetak Permohonan</h3>
  <ul class="nav side-menu">
    <li><a><i class="fa fa-print"></i> Cetak Permohonan <span class="fa fa-chevron-down"></span></a>
      <ul class="nav child_menu">
        <!-- <li><a href="<?= Url::toRoute('/verifikator/verifikasi-ijin-prinsip/index'); ?>">Ijin Prinsip</a></li> -->
        <li><a href="<?= Url::toRoute('/verifikator/verifikasi-info-kepengusahaan/index'); ?>">Permohonan Ijin Prinsip</a></li>
        <li><a href="<?= Url::toRoute('/verifikator/verifikasi-permohonan-rekomendasi/index'); ?>">Info Realisasi</a></li>
        <!-- <li><a href="<?= Url::toRoute('/verifikator/verifikasi-rekom-perpanjangan-sk/index'); ?>">Rekomendasi Perpanjangan SK</a></li> -->
      </ul>
    </li>
    <li><a><i class="fa fa-check-square-o"></i> Upload Persetujuan <span class="fa fa-chevron-down"></span></a>
      <ul class="nav child_menu">
        <!-- <li><a href="<?= Url::toRoute('/verifikator/verifikasi-ijin-prinsip/monitoring-index'); ?>">Ijin Prinsip</a></li> -->
        <li><a href="<?= Url::toRoute('/verifikator/verifikasi-info-kepengusahaan/monitoring-index'); ?>">Permohonan Ijin Prinsip</a></li>
        <li><a href="<?= Url::toRoute('/verifikator/verifikasi-permohonan-rekomendasi/monitoring-index'); ?>">Info Realisasi</a></li>
        <!-- <li><a href="<?= Url::toRoute('/verifikator/verifikasi-rekom-perpanjangan-sk/monitoring-index'); ?>">Rekomendasi Perpanjangan SK</a></li> -->
      </ul>
    </li>
  </ul>
</div>