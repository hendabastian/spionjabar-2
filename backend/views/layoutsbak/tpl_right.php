<!-- page content -->
<div class="right_col" role="main">
  <?php if($this->title != 'Dashboard'){?>
  <div class="page-title">
      <div class="title_left">
        <h3><?= $this->title;?></h3>
      </div>

      <div class="title_right">
<!--
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
        </div>
-->
      <?= yii\widgets\Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
      </div>
    </div>
  <?php }?>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <?= common\widgets\Alert::widget() ?>

        <?= $content ;?>
      </div>
    </div>

</div>
<!-- /page content -->
