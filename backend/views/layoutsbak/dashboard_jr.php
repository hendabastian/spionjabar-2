<?php

use yii\helpers\Html;

$baseTheme = Yii::$app->getUrlManager()->getBaseUrl() . '/dashjr/';
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="<?= $baseTheme; ?>/img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="codepixer">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
			CSS
			============================================= -->
    <link rel="stylesheet" href="<?= $baseTheme; ?>/css/linearicons.css">
    <link rel="stylesheet" href="<?= $baseTheme; ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $baseTheme; ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $baseTheme; ?>/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= $baseTheme; ?>/css/nice-select.css">
    <link rel="stylesheet" href="<?= $baseTheme; ?>/css/animate.min.css">
    <link rel="stylesheet" href="<?= $baseTheme; ?>/css/owl.carousel.css">
    <link rel="stylesheet" href="<?= $baseTheme; ?>/css/main.css">
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/css/public_custom.css">

    <link rel="apple-touch-icon" sizes="57x57" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico//apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico//apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico//apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico//apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico//apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico//apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico//apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico//apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico//android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico//favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico//favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico//favicon-16x16.png">
    <link rel="manifest" href="<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>/favico//manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>
    <header id="header" id="home">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-sm-8 col-4 header-top-left no-padding">

                    </div>
                    <div class="col-lg-6 col-sm-4 col-8 header-top-right no-padding">
                        <?php if (Yii::$app->user->isGuest) { ?>
                            <a class="text-uppercase top-head-btn" href="<?= \yii\helpers\Url::toRoute(['site/login']); ?>">Sign In</a>
                        <?php } else { ?>
                            <a href="javascript:document.getElementById('logout-form').submit()" class="text-uppercase top-head-btn"> Logout</a>
                            <?= yii\helpers\Html::beginForm(['/site/logout'], 'post', ['id' => 'logout-form']); ?>
                            <?= yii\helpers\Html::endForm(); ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container main-menu">
            <div class="row align-items-center justify-content-between d-flex">
                <div id="logo">
                    <a href="<?= \yii\helpers\Url::toRoute(['site/index']); ?>"><img src="<?= $baseTheme; ?>/img/logo.png" alt="" title="" style="height:40px" /></a>
                </div>
                <nav id="nav-menu-container">
                    <ul class="nav-menu">
                        <li><a href="<?= Yii::$app->controller->action->id == "signup" || Yii::$app->controller->action->id == "login" ? \yii\helpers\Url::toRoute(['site/index']) : \yii\helpers\Url::toRoute(['site/index']) . '#home' ?>">Home</a></li>
                        <li><a href="<?= \yii\helpers\Url::toRoute(['cari-perusahaan/index']); ?>">Cek Perusahaan</a></li>
                        <li><a href="<?= \yii\helpers\Url::toRoute(['cari-kendaraan/index']); ?>">Cek Kendaraan</a></li>
                        <li><a href="<?= \yii\helpers\Url::toRoute(['activity-log/index']); ?>">Aktivitas Terakhir</a></li>
                    </ul>
                </nav><!-- #nav-menu-container -->
            </div>
        </div>
    </header><!-- #header -->
    <?= $content ?>
    <!-- start footer Area -->
    <footer class="footer-area section-gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-4  col-md-12">
                    <div class="single-footer-widget">
                        <h6>Dinas Perhubungan Jawa Barat</h6>
                        Jl. Sukabumi No. 1, Kota Bandung<br>
                        Jawa Barat, 40271 - Indonesia<br>
                        (022) 7207257 - 7272258<br>
                        <p>dishub@jabarprov.go.id</p>
                    </div>
                </div>
                <div class="col-lg-4  col-md-12">
                    <div class="single-footer-widget">
                        <h6>Main Menu </h6>
                        <ul class="footer-nav">
                            <li><a href="<?= \yii\helpers\Url::toRoute(['site/index']); ?>">Home</a></li>
                            <li><a href="<?= \yii\helpers\Url::toRoute(['cari-perusahaan/index']); ?>">Cek Perusahaan</a></li>
                            <li><a href="<?= \yii\helpers\Url::toRoute(['cari-kendaraan/index']); ?>">Cek Kendaraan</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4  col-md-12">
                    <div class="single-footer-widget">
                        <h6>Eksternal Link </h6>
                        <ul class="footer-nav">
                            <li><a href="https://www.jasaraharja.co.id/">Portal Jasa Raharja</a></li>
                            <li><a href="http://dishub.jabarprov.go.id/">Portal Dinas Perhubungan Jawa Barat</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
                <p class="col-lg-12 col-sm-12 footer-text m-0 text-white">Copyright &copy;<script>
                        document.write(new Date().getFullYear());
                    </script> All rights reserved | <a href="http://dishub.jabarprov.go.id/" target="_blank">Dinas Perhubungan Jawa Barat</a> </p>
                <?php /*
						<div class="footer-social d-flex align-items-center">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-instagram"></i></a>
							<a href="#"><i class="fa fa-youtube"></i></a>
						</div>
						*/ ?>
            </div>
        </div>
    </footer>
    <!-- End footer Area -->

    <script src="<?= $baseTheme; ?>/js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="<?= $baseTheme; ?>/js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
    <script src="<?= $baseTheme; ?>/js/easing.min.js"></script>
    <script src="<?= $baseTheme; ?>/js/hoverIntent.js"></script>
    <script src="<?= $baseTheme; ?>/js/superfish.min.js"></script>
    <script src="<?= $baseTheme; ?>/js/jquery.ajaxchimp.min.js"></script>
    <script src="<?= $baseTheme; ?>/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= $baseTheme; ?>/js/owl.carousel.min.js"></script>
    <script src="<?= $baseTheme; ?>/js/jquery.nice-select.min.js"></script>
    <script src="<?= $baseTheme; ?>/js/jquery.counterup.min.js"></script>
    <script src="<?= $baseTheme; ?>/js/waypoints.min.js"></script>
    <script src="<?= $baseTheme; ?>/js/mail-script.js"></script>
    <script src="<?= $baseTheme; ?>/js/main.js"></script>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>