<?php

use yii\helpers\Url;
use common\models\master\JenisLayanan;
?>

<div class="menu_section">
  <h3>Main Menu</h3>
  <ul class="nav side-menu">
    <li><a href="<?= Url::toRoute('/site/index'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
  </ul>
</div>
<?php if (Yii::$app->user->identity->user_role_id == 3) : ?>
  <div class="menu_section">
    <h3>Verifikasi Perusahaan</h3>
    <ul class="nav side-menu">
      <li><a href="<?= Url::toRoute(['/approval/perusahaan/index', 'jenis_perusahaan' => 2]); ?>"><i class="fa fa-archive"></i> Dok. Perusahaan Baru</a></li>
      <li><a href="<?= Url::toRoute(['/approval/perusahaan/index', 'jenis_perusahaan' => 1]); ?>"><i class="fa fa-archive"></i> Dok. Perusahaan Lama</a></li>
    </ul>
  </div>
<?php endif; ?>

<div class="menu_section">
<h3>Verifikasi Perusahaan</h3>
  <ul class="nav side-menu">
    <?php foreach (JenisLayanan::find()->where(['is_active' => 1, 'is_delete' => 0])->all() as $index => $data) : ?>
      <li><a><i class="fa fa-file"></i> <?= $data->jenis_layanan ?> <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <?php foreach ($data->jenisAngkutan as $indexAngkutan => $dataAngkutan) : ?>
            <li <?= (Yii::$app->request->get('jenis_angkutan_id') ==  $dataAngkutan->id) ? "class='active '" : '' ?>><a href="<?= Url::toRoute(['/permohonan/permohonan/index', 'jenis_angkutan_id' => $dataAngkutan->id]) ?>"><?= $dataAngkutan->jenis_angkutan ?></a></li>
          <?php endforeach; ?>
        </ul>
      </li>
    <?php endforeach; ?>
  </ul>

  <ul class="nav side-menu">
    <li><a><i class="fa fa-book"></i> Laporan <span class="fa fa-chevron-down"></span></a>
      <ul class="nav child_menu">
        <li><a href="<?= Url::toRoute('/site/report'); ?>"><i class="fa fa-book"></i> Status Perijinan</a></li>
        <li><a href="<?= Url::toRoute('/site/report'); ?>"><i class="fa fa-book"></i> Report</a></li>
      </ul>
    </li>
  </ul>
</div>