<?php

use yii\helpers\Url;
use common\models\User;
use common\models\UserRole;
?>

<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="<?= Url::toRoute('/site/index'); ?>" class="site_title">
        <i class="fa fa-car"></i> <span><?= Yii::$app->name; ?></span>
      </a>
      <?php /*
      <a href="<?= \yii\helpers\Url::toRoute(['site/dashboard']);?>">
        <img src="<?= Yii::$app->getUrlManager()->getBaseUrl();?>/img/logo.png" alt="" title="" style="height:55px; padding-top:20px"/>
      </a>
      */ ?>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="<?= $baseTheme; ?>/images/avatar.png" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Welcome,</span>
        <h2><?php if (!Yii::$app->user->isGuest) {
              echo ucwords(Yii::$app->user->identity->username);
            } ?></h2>
        <p><?php if (!Yii::$app->user->isGuest) {
              echo ucwords(UserRole::findOne(User::findOne(Yii::$app->user->identity->id)->user_role_id)->role);
            } ?></p>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <?php if (!Yii::$app->user->isGuest && User::findOne(Yii::$app->user->identity->id)->user_role_id == 1) : ?>
        <?= $this->render('_menu_administrator'); ?>
      <?php elseif (!Yii::$app->user->isGuest && User::findOne(Yii::$app->user->identity->id)->user_role_id == 3 || User::findOne(Yii::$app->user->identity->id)->user_role_id == 5 || User::findOne(Yii::$app->user->identity->id)->user_role_id == 6 || User::findOne(Yii::$app->user->identity->id)->user_role_id == 7 || User::findOne(Yii::$app->user->identity->id)->user_role_id == 9) : ?>
        <?= $this->render('_menu_approval'); ?>
      <?php elseif (!Yii::$app->user->isGuest && User::findOne(Yii::$app->user->identity->id)->user_role_id == 4) : ?>
        <?= $this->render('_menu_jr'); ?>
      <?php elseif (!Yii::$app->user->isGuest && User::findOne(Yii::$app->user->identity->id)->user_role_id == 8) : ?>
        <?= $this->render('_menu_cetak') ?>
      <?php endif; ?>
    </div>
    <!-- /sidebar menu -->
  </div>
</div>