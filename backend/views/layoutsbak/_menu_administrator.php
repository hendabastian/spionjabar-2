<?php

use yii\helpers\Url;
use common\models\master\JenisLayanan;
?>

<div class="menu_section">
  <h3>Main Menu</h3>
  <ul class="nav side-menu">
    <li><a href="<?= Url::toRoute('/site/index'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="<?= Yii::$app->controller->id == "regulasi" ? 'active' : ''; ?>"><a><i class="fa fa-balance-scale"></i> Regulasi <span class="fa fa-chevron-down"></span></a>
      <ul class="nav child_menu" style="<?= Yii::$app->controller->id == "regulasi" ? 'display: block;' : 'display: none;'; ?>">
        <?php foreach (JenisLayanan::find()->where(['is_delete' => 0, 'is_active' => 1])->all() as $key => $value) : ?>
          <li><a href="<?= Url::toRoute(['/perusahaan/regulasi', 'jenisLayananId' => $value->id]); ?>"><?= $value->jenis_layanan ?></a></li>
        <?php endforeach; ?>
      </ul>
    </li>
    <li class="<?php if (Yii::$app->controller->id == 'perusahaan') {
                  echo 'active';
                } ?>">
      <a><i class="fa fa-bank"></i> Perusahaan <span class="fa fa-chevron-down"></span></a>
      <ul class="nav child_menu">
        <li class="<?php if (Yii::$app->controller->id == 'perusahaan') {
                      echo 'active';
                    } ?>"><a href="<?= Url::toRoute('/perusahaan/perusahaan'); ?>">Data Perusahaan</a></li>
      </ul>
    </li>

    <li><a><i class="fa fa-th"></i> Tracking Permohonan <span class="fa fa-chevron-down"></span></a>
      <ul class="nav child_menu">
        <li><a href="<?= Url::toRoute('/verifikator/verifikasi-ijin-prinsip/monitoring-index'); ?>">Ijin Prinsip</a></li>
        <li><a href="<?= Url::toRoute('/verifikator/verifikasi-info-kepengusahaan/monitoring-index'); ?>">Info Kepengusahaan</a></li>
        <li><a href="<?= Url::toRoute('/verifikator/verifikasi-permohonan-rekomendasi/monitoring-index'); ?>">Rekomendasi Realisasi Ijin Prinsip</a></li>
        <li><a href="<?= Url::toRoute('/verifikator/verifikasi-rekom-perpanjangan-sk/monitoring-index'); ?>">Rekomendasi Perpanjangan SK</a></li>
      </ul>
    </li>
    <li><a href="<?= Url::toRoute('/reporting/reporting/index'); ?>"><i class="fa fa-newspaper-o"></i> Reporting</a></li>

  </ul>
</div>


<div class="menu_section">
  <h3>System</h3>
  <ul class="nav side-menu">
    <li class="">
      <a><i class="fa fa-database"></i> Master <span class="fa fa-chevron-down"></span></a>
      <ul class="nav child_menu">
        <li><a href="<?= Url::toRoute('/master/terminal'); ?>">Data Terminal</a></li>
        <li><a href="<?= Url::toRoute('/master/trayek'); ?>">Data Trayek</a></li>
        <li><a href="<?= Url::toRoute('/master/tipe-terminal'); ?>">Tipe Terminal</a></li>
        <li><a href="<?= Url::toRoute('/master/jenis-badan-usaha'); ?>">Jenis Badan Usaha</a></li>
        <li><a href="<?= Url::toRoute('/master/jenis-dokumen-perusahaan'); ?>">Jenis Dokumen Perusahaan</a></li>
        <li><a href="<?= Url::toRoute('/master/jenis-kendaraan'); ?>">Jenis Kendaraan</a></li>
        <li><a>Master Permohonan<span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="<?= Url::toRoute('/master/jenis-layanan'); ?>">Jenis Layanan</a> </li>
            <li><a href="<?= Url::toRoute('/master/jenis-angkutan'); ?>">Jenis Angkutan</a> </li>
            <li><a href="<?= Url::toRoute('/master/jenis-permohonan'); ?>">Jenis Permohonan</a> </li>
            <li><a href="<?= Url::toRoute('/master/tipe-permohonan'); ?>">Tipe Permohonan</a> </li>
          </ul>
        </li>
        <li class="<?php if (
                      Yii::$app->controller->id == 'provinsi'  ||
                      Yii::$app->controller->id == 'kabupaten' ||
                      Yii::$app->controller->id == 'kecamatan' ||
                      Yii::$app->controller->id == 'kelurahan'
                    ) {
                      echo 'active';
                    } ?>">
          <a>Master Wilayah<span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu" style="<?php if (
                                              Yii::$app->controller->id == 'provinsi'  ||
                                              Yii::$app->controller->id == 'kabupaten' ||
                                              Yii::$app->controller->id == 'kecamatan' ||
                                              Yii::$app->controller->id == 'kelurahan'
                                            ) {
                                              echo 'display:block';
                                            } ?>">
            <li class="<?php if (Yii::$app->controller->id == 'provinsi') {
                          echo 'current-page';
                        } ?>">
              <a href="<?= Url::toRoute('/master/provinsi'); ?>">Provinsi</a>
            </li>
            <li class="<?php if (Yii::$app->controller->id == 'kabupaten') {
                          echo 'current-page';
                        } ?>">
              <a href="<?= Url::toRoute('/master/kabupaten'); ?>">Kabupaten/Kota</a>
            </li>
            <li class="<?php if (Yii::$app->controller->id == 'kecamatan') {
                          echo 'current-page';
                        } ?>">
              <a href="<?= Url::toRoute('/master/kecamatan'); ?>">Kecamatan</a>
            </li>
            <li class="<?php if (Yii::$app->controller->id == 'kelurahan') {
                          echo 'current-page';
                        } ?>">
              <a href="<?= Url::toRoute('/master/kelurahan'); ?>">Kelurahan/Desa</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>

    <li><a><i class="fa fa-users"></i> Users <span class="fa fa-chevron-down"></span></a>
      <ul class="nav child_menu">
        <li><a href="<?= Url::toRoute('user-management/index'); ?>">Users</a></li>
        <li><a href="<?= Url::toRoute('user-category/index'); ?>">User Role</a></li>
      </ul>
    </li>

    <li><a><i class="fa fa-cogs"></i> System Config <span class="fa fa-chevron-down"></span></a>
      <ul class="nav child_menu">
        <li><a href="<?= Url::toRoute('activity-log/index'); ?>">User Log Activity</a></li>
      </ul>
    </li>
  </ul>
</div>