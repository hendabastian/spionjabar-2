$(function () {
    //get the click of modal button to create / update item
    //we get the button by class not by ID because you can only have one id on a page and you can
    //have multiple classes therefore you can have multiple open modal buttons on a page all with or without
    //the same link.
    //we use on so the dom element can be called again if they are nested, otherwise when we load the content once it kills the dom element and wont let you load anther modal on click without a page refresh
    $(document).on('click', '.showModalButton', function (e, form, $con, url) {
        //check if the modal is open. if it's open just reload content not whole modal
        //also this allows you to nest buttons inside of modals to reload the content it is in
        //the if else are intentionally separated instead of put into a function to get the
        //button since it is using a class not an #id so there are many of them and we need
        //to ensure we get the right button and content.
        $('#modal').data('resource-id', this.dataset.resourceId)
        $con = $('#modal').modal('show').find('#modalContent')
        url = $(this).attr('value');
        $con.html("<div class='text-center'><i class='fa fa-spin fa-spinner fa-5x'></i> <h3> Memuat... </h3> </div>")

        if ($(this).data('modal-type') === 'embed-pdf') {
            $con.html('<object type="application/pdf" data="' + url + '" width="100%" height="500" style="height: 85vh;">No Support</object>')
        } else if ((form = $(this).data('form-src')) || (form = $(this).data('update-form'))) {
            $con.html($('#' + form).clone().show().removeAttr('id')[0].outerHTML)
        } else {
            $con.load(url);
        }
        //dynamiclly set the header for the modal
        var closeBtn = '<button type="button" id="close-button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'
        $('#modalHeader').html('<h4>' + ($(this).data('modal-title') || $(this).attr('title')) + closeBtn + '</h4>');
    })
})