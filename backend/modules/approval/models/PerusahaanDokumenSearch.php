<?php

namespace backend\modules\approval\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\perusahaan\PerusahaanDokumen;

/**
 * PerusahaanDokumenSearch represents the model behind the search form of `common\models\PerusahaanDokumen`.
 */
class PerusahaanDokumenSearch extends PerusahaanDokumen
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'perusahaan_id', 'jenis_dokumen_perusahaan_id', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['nama_dokumen', 'no_dokumen', 'keterangan', 'filename', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PerusahaanDokumen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'perusahaan_id' => $this->perusahaan_id,
            'jenis_dokumen_perusahaan_id' => $this->jenis_dokumen_perusahaan_id,
            'is_delete' => $this->is_delete,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'nama_dokumen', $this->nama_dokumen])
            ->andFilterWhere(['like', 'no_dokumen', $this->no_dokumen])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'filename', $this->filename]);

        return $dataProvider;
    }
}
