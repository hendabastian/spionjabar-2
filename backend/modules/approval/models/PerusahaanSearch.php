<?php
namespace backend\modules\approval\models;

use common\models\perusahaan\Perusahaan;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PerusahaanSearch extends Perusahaan {
    public $alamat;
    public $provinsi;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'is_delete', 'created_by', 'updated_by', 'is_verified'], 'integer'],
            [['kode_perusahaan', 'nama_perusahaan', 'npwp', 'email', 'hp', 'created_at', 'updated_at', 'alamat', 'provinsi'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Perusahaan::find();

        $query->joinWith(['domisili']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['alamat'] = [
            'asc' => ['t_perusahaan_domisili.alamat' => SORT_ASC],
            'desc' => ['t_perusahaan_domisili.alamat' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['provinsi'] = [
            'asc' => ['t_perusahaan_domisili.provinsi_id' => SORT_ASC],
            'desc' => ['t_perusahaan_domisili.provinsi_id' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            't_perusahaan.is_delete' => 0,
            'is_verified'=> $this->is_verified,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'kode_perusahaan', $this->kode_perusahaan])
            ->andFilterWhere(['like', 'nama_perusahaan', $this->nama_perusahaan])
            ->andFilterWhere(['like', 'npwp', $this->npwp])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 't_perusahaan_domisili.alamat', $this->alamat])
            ->andFilterWhere(['like', 't_perusahaan_domisili.provinsi_id', $this->provinsi])
            ->andFilterWhere(['like', 'hp', $this->hp]);

        return $dataProvider;
    }
}