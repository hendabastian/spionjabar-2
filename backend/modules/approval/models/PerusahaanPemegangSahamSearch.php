<?php

namespace backend\modules\approval\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\perusahaan\PerusahaanPemegangSaham;

/**
 * PerusahaanPemegangSahamSearch represents the model behind the search form of `common\models\PerusahaanPemegangSaham`.
 */
class PerusahaanPemegangSahamSearch extends PerusahaanPemegangSaham
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'perusahaan_id', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['nama_lengkap', 'keterangan', 'created_at', 'updated_at'], 'safe'],
            [['persentase'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PerusahaanPemegangSaham::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'perusahaan_id' => $this->perusahaan_id,
            'persentase' => $this->persentase,
            'is_delete' => 0,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'nama_lengkap', $this->nama_lengkap])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
