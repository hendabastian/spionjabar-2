<?php

namespace backend\modules\approval\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\perusahaan\PerusahaanVerifikasi;

/**
 * PerusahaanVerifikasiSearch represents the model behind the search form of `common\models\perusahaan\PerusahaanVerifikasi`.
 */
class PerusahaanVerifikasiSearch extends PerusahaanVerifikasi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'perusahaan_id', 'jenis_layanan_id', 'status_verifikasi', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['tgl_verifikasi', 'catatan_verifikasi', 'diverifikasi_oleh', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PerusahaanVerifikasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load ($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'perusahaan_id' => $this->perusahaan_id,
            'jenis_layanan_id' => $this->jenis_layanan_id,
            'tgl_verifikasi' => $this->tgl_verifikasi,
            'status_verifikasi' => $this->status_verifikasi,
            'is_delete' => $this->is_delete,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'catatan_verifikasi', $this->catatan_verifikasi])
            ->andFilterWhere(['like', 'diverifikasi_oleh', $this->diverifikasi_oleh]);

        return $dataProvider;
    }
}