<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<?php if (file_exists($fullpath)): ?>
  <?= \yii2assets\pdfjs\PdfJs::widget([
    'url'=> Url::toRoute(['perusahaan-dokumen/download', 'path' => $path, 'file' => $file]),
    'buttons'=>[
      'presentationMode' => false,
      'openFile' => false,
      'print' => true,
      'download' => true,
      'viewBookmark' => false,
      'secondaryToolbarToggle' => false
    ]
  ]); ?>
<?php else: ?>
  <h2>File not found</h2>
<?php endif; ?>
