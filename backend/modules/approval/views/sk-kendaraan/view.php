<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\perusahaan\SkPerusahaanKendaraan */
?>
<div class="sk-kendaraan-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            // 'sk_id',
            [
                'attribute' => 'perusahaan_id',
                'label' => 'Perusahaan',
                'value' => function ($data) {
                    return $data->sk->perusahaan->nama_perusahaan;
                }
            ],
            // 'no_srut',
            // 'tgl_srut',
            'no_kendaraan',
            'no_rangka',
            'no_uji',
            'expire_uji',
            'no_mesin',
            'tahun',
            'merk',
            'nama_pemilik',
            [
                'attribute' => 'jenis_kendaraan_id',
                'value' => function ($data) {
                    return $data->jenisKendaraan->jenis_kendaraan;
                }
            ],
            'seat',
            [
                'attribute' => 'stnk',
                'format' => 'RAW',
                'value' => function ($data) {
                    if ($data->stnk !== NULL) {
                        return Html::a('Download', \yii\helpers\Url::toRoute(['sk-kendaraan/download', 'file' => Yii::getAlias('@uploads') . '/perusahaan/' . $data->sk->perusahaan_id . '/legalitas/' . $data->stnk]), ['class' => 'btn btn-primary btn-xs', 'target' => '_blank', 'data-pjax' => 0]);
                    }
                },
            ],
            [
                'attribute' => 'kir',
                'format' => 'RAW',
                'value' => function ($data) {
                    if ($data->kir !== NULL) {
                        return Html::a('Download', \yii\helpers\Url::toRoute(['sk-kendaraan/download', 'file' => Yii::getAlias('@uploads') . '/perusahaan/' . $data->sk->perusahaan_id . '/legalitas/' . $data->kir]), ['class' => 'btn btn-primary btn-xs', 'target' => '_blank', 'data-pjax' => 0]);
                    }
                },
            ],
            [
                'attribute' => 'bukti_jasa_raharja',
                'format' => 'RAW',
                'value' => function ($data) {
                    if ($data->bukti_jasa_raharja !== NULL) {
                        return Html::a('Download', \yii\helpers\Url::toRoute(['sk-kendaraan/download', 'file' => Yii::getAlias('@uploads') . '/perusahaan/' . $data->sk->perusahaan_id . '/legalitas/' . $data->bukti_jasa_raharja]), ['class' => 'btn btn-primary btn-xs', 'target' => '_blank', 'data-pjax' => 0]);
                    }
                },
            ],
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
            // 'is_delete',
        ],
    ]) ?>
    <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Close', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']) ?>


</div>