<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="container">
    <?= Html::beginForm(Url::toRoute(['import-excel', 'sk_id' => $sk_id, 'perusahaan_id' => $perusahaan_id]), 'post', [
        'enctype' => 'multipart/form-data'
    ]) ?>
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="alert alert-info">
                <h3>Tata Cara Penggunaan</h3>
                <li>Download terlebih dahulu template data kendaraan pada tombol 'Download Template' Di bawah</li>
                <li>Isi data kendaraan sesuai kolom</li>
                <li>Upload kembali template yang telah terisi</li>
            </div>
            <?= Html::a('<i class="fa fa-download"></i> Download Template', Yii::$app->urlManager->createAbsoluteUrl('/public/template_data_kendaraan.xlsx'), ['class' => 'btn btn-success', 'download']) ?>

        </div>
        <div class="col-sm-6 col-sm-offset-3">
            <div class="form-group">
                <label for="xlsx_guru">Data Excel Kendaraan (.xlsx)</label>
                <input type="file" name="file_excel" id="file_excel" class="form-control" required>
            </div>
        </div>
        <div class="col-sm-6 col-sm-offset-3 form-group">
            <?= Html::submitButton('Import', ['class' => 'btn btn-primary btn-block']) ?>
        </div>
    </div>


    <?= Html::endForm() ?>
</div>
