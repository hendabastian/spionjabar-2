<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        // 'id',
        [
            'attribute' => 'perusahaan_id',
            'label' => 'Perusahaan',
            'value' => function ($data) {
                return $data->perusahaan->nama_perusahaan;
            }
        ],
        [
            'attribute' => 'trayek',
            'value' => function ($data) {
                return $data->trayek->nama_trayek ?? '-';
            }
        ],
        [
            'attribute' => 'jenis_angkutan_id',
            'label' => 'Jenis Angkutan',
            'value' => function ($data) {
                return $data->jenisAngkutan->jenis_angkutan;
            }
        ],
        'no_sk',
        [
            'attribute' => 'file_sk',
            'label' => 'File SK',
            'format' => 'RAW',
            'value' => function ($data) {
                $button = "";
                if ($data->file_sk !== NULL) {
                    $button = Html::a('<i class="fa fa-download"></i> Download', Url::toRoute(['regulasi/download', 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $data->perusahaan_id . '/legalitas/', 'file' => $data->file_sk]), ['class' => ' btn btn-primary btn-xs']);
                }
                return $button;
            },
        ],
        'tgl_terbit',
        'exp_sk',
        // 'is_active',
        // 'is_delete',
        // 'created_at',
        // 'updated_at',
        // 'created_by',
        // 'updated_by',
    ],
]) ?>