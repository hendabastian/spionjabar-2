<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\perusahaan\SkPerusahaanKendaraan */

$this->title = 'Update Sk Kendaraan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sk Kendaraans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sk-kendaraan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'sk_id' => $sk_id,
        'perusahaan_id' => $perusahaan_id,
    ]) ?>

</div>