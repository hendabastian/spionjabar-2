<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PerusahaanDomisili */

$this->title = $model->jenis_domisili;
$this->params['breadcrumbs'][] = ['label' => 'Perusahaan Domisilis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="card">
    <div class="card-header">
        <h2><?= Html::encode($this->title) ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>

        <div class="clearfix"></div>

    </div>

    <div class="card-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                'jenis_domisili',
                'alamat:ntext',
                [
                    'attribute' => 'provinsi_id',
                    'value' => function ($data) {
                        return $data->provinsi->nama;
                    }
                ],
                [
                    'attribute' => 'kabupaten_id',
                    'value' => function ($data) {
                        return $data->kabupaten->nama;
                    }
                ],
                'tlp',
                'fax',
                'kode_pos',
                [
                    'attribute' => 'tgl_exp_domisili',
                    'format' => 'RAW',
                    'value' => function ($data) {
                        $today = date('d-m-Y');
                        $tglDok = $data->getDokumenDomisili($data->perusahaan_id);
                        $label = "";

                        if ($tglDok !== NULL) {
                            if ($tglDok->tgl_dokumen > $today) {
                                $label = "<span class='label label-danger' style='font-size: 10px;'>" . date('d-m-Y', strtotime($tglDok->tgl_dokumen)) . " | Expired</span>";
                            } else {
                                $label = "<span class='label label-success' style='font-size: 10px;'>" . date('d-m-Y', strtotime($tglDok->tgl_dokumen)) . "</span>";
                            }
                        }

                        return $label;
                    },
                ],
                [
                    'attribute' => 'alamat_utama',
                    'format' => 'raw',
                    'filter' => ['0' => 'Bukan Alamat Utama', '1' => 'Alamat Utama'],
                    'value' => function ($data) {
                        if ($data->alamat_utama == 0) {
                            return '<span class="label label-danger">Bukan</span>';
                        } else {
                            return '<span class="label label-success">Ya</span>';
                        }
                    },
                ],
                // 'is_delete',
                // 'created_at',
                // 'updated_at',
                // 'created_by',
                // 'updated_by',
            ],
        ]) ?>
        <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger pull-right', 'data-dismiss' => 'modal']) ?>
    </div>
</div>