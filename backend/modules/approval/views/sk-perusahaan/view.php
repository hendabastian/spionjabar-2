<?php

use common\models\perusahaan\SkPerusahaanKendaraan;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\perusahaan\SkPerusahaan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sk Perusahaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sk-perusahaan-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            [
                'attribute' => 'perusahaan_id',
                'label' => 'Perusahaan',
                'value' => function ($data) {
                    return $data->perusahaan->nama_perusahaan;
                }
            ],
            [
                'attribute' => 'trayek',
                'value' => function ($data) {
                    return $data->trayek->nama_trayek ?? '-';
                }
            ],
            [
                'attribute' => 'jenis_angkutan_id',
                'label' => 'Jenis Angkutan',
                'value' => function ($data) {
                    return $data->jenisAngkutan->jenis_angkutan;
                }
            ],
            'no_sk',
            [
                'attribute' => 'file_sk',
                'format' => 'RAW',
                'value' => function ($data) {
                    $button = "";
                    if ($data->file_sk !== NULL) {
                        $button = Html::a('<i class="fa fa-download"></i>', Url::toRoute(['regulasi/download', 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $data->perusahaan_id . '/legalitas/', 'file' => $data->file_sk]), ['class' => ' btn btn-primary btn-sm']);
                    }

                    return $button;
                }
            ],
            'tgl_terbit',
            'exp_sk',
            [
                'attribute' => 'id',
                'format' => 'RAW',
                'label' => 'Jml. Kendaraan',
                'value' => function ($data) use ($perusahaan) {
                    $count = SkPerusahaanKendaraan::find()->where(['sk_id' => $data->id])->count();
                    $text = ' Kendaraan';
                    if ($perusahaan->is_verified == 0 || $perusahaan->is_verified == 4 || $perusahaan->is_verified == 5) :
                        $button = Html::a('<i class="fa fa-plus"></i> Tambah Kendaraan', Url::toRoute(['sk-kendaraan/index', 'sk_id' => $data->id, 'perusahaan_id' => $data->perusahaan_id]), ['class' => 'btn btn-xs btn-primary']);
                    else :
                        $button = Html::a('<i class="fa fa-car"></i> Daftar Kendaraan', Url::toRoute(['sk-kendaraan/index', 'sk_id' => $data->id, 'perusahaan_id' => $data->perusahaan_id]), ['class' => 'btn btn-xs btn-primary']);
                    endif;
                    return $count . $text . '<br>' . $button;
                },
                'contentOptions' => ['style' => 'align-items: center;'],
            ],
            // 'is_active',
            // 'is_delete',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
        ],
    ]) ?>
    <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal', 'id' => 'btn-close']) ?>

</div>