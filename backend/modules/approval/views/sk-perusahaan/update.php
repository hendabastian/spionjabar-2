<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\perusahaan\SkPerusahaan */

$this->title = 'Update Sk Perusahaan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sk Perusahaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sk-perusahaan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
