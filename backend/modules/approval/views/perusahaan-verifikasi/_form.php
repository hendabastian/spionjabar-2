<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\master\JenisLayanan;
/* @var $this yii\web\View */
/* @var $model common\models\ActivityLog */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="perusahaan-verifikasi-log-form">

    <?php $form = ActiveForm::begin(['id' => 'perusahaan-verifikasi']); ?>

    <div class="alert alert-warning">Pastikan seluruh dokumen yang diupload oleh perusahaan telah diperiksa</div>

    <div style="display: none;">
        <?= $form->field($model, 'user_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>

        <?= $form->field($model, 'perusahaan_id')->hiddenInput(['value' => $perusahaan->id])->label(false) ?>

        <?= $form->field($model, 'tgl_verifikasi')->hiddenInput(['value' => date('Y-m-d'), 'disabled' => 'disabled'])->label("Diproses Pada") ?>
    </div>

    <?php if ($perusahaan->jenis_perusahaan == 2) : ?>

        <?= $form->field($model, 'status_verifikasi')->radioList([2 => 'Terregistrasi', 5 => 'Belum Dapat Terregistrasi']); ?>

    <?php else : ?>

        <?= $form->field($model, 'status_verifikasi')->radioList([2 => 'Terregistrasi', 5 => 'Belum Dapat Terregistrasi']); ?>

    <?php endif ?>

    <?= $form->field($model, 'catatan_verifikasi')->textArea(['row' => 3]) ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success', 'id' => 'btn-submit']) ?>
        <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal', 'id' => 'btn-close']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs('
jQuery(document).ready(function($){
    $(document).ready(function () {
        $("body").on("beforeSubmit", "form#perusahaan-verifikasi", function () {
            var form = $(this);
            var data = new FormData( this );
            // return false if form still have some validation errors
            if (form.find(".has-error").length)
            {
                return false;
            }
            // submit form
            $.ajax({
                url         : form.attr("action"),
                data        : data,
                type        : form.attr("method"),
                cache       : false,
                contentType : false,
                processData : false,
                beforeSend  : function(){
                    $("#btn-submit").prop("disabled", true);
                    $("#btn-close").prop("disabled", true);
                },
                success: function (response)
                {
                    $("#modal").modal("toggle");
                },
                error  : function ()
                {
                    console.log("internal server error");
                }
            });
            return false;
            });
    });
});
'); ?>