<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\perusahaan\PerusahaanDokumen;
?>
<?php Pjax::begin(['id' => 'data-domisili']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataDomisili,
        'filterModel' => $searchDomisili,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'jenis_domisili',
            'alamat:ntext',
            // 'provinsi_id',
            // 'kabupaten_id',
            // 'tlp',
            // 'fax',
            // 'kode_pos',

            [
                'attribute' => 'dokumen_domisili',
                'format' => 'RAW',
                'value' => function ($data) {
                    $dokumen = PerusahaanDokumen::find()->where(['is_delete' => 0, 'jenis_dokumen_perusahaan_id' => 9, 'perusahaan_id' => $data->perusahaan_id])->one();
                    $button = "";
                    if ($dokumen !== NULL) {
                        $button = Html::button('<i class="fa fa-file-pdf-o"></i>', ['value' => Url::toRoute(['perusahaan-dokumen/preview-dok', 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $data->perusahaan_id . '/legalitas/', 'file' => $dokumen->filename]), 'title' => 'Preview Dokumen', 'class' => 'showModalButton btn btn-primary btn-sm']);
                    }
                    return $button;
                },
                'contentOptions' => ['style' => 'width: 30px;text-align: center;'],
            ],
            [
                'attribute' => 'tgl_exp_domisili',
                'format' => 'RAW',
                'value' => function ($data) {
                    $today = date('d-m-Y');
                    $dokumen = $data->getDokumenDomisili($data->perusahaan_id);
                    $label = "";

                    if ($dokumen !== NULL) {
                        if ($dokumen->tgl_dokumen > $today) {
                            $label = "<span class='label label-danger' style='font-size: 10px;'>" . date('d-m-Y', strtotime($dokumen->tgl_dokumen)) . " | Expired</span>";
                        } else {
                            $label = "<span class='label label-success' style='font-size: 10px;'>" . date('d-m-Y', strtotime($dokumen->tgl_dokumen)) . "</span>";
                        }
                    }

                    return $label;
                },
                'contentOptions' => ['style' => 'width: 120px;text-align: center;'],
            ],
            [
                'attribute' => 'alamat_utama',
                'format' => 'raw',
                'filter' => ['0' => 'Bukan Alamat Utama', '1' => 'Alamat Utama'],
                'value' => function ($data) {
                    if ($data->alamat_utama == 0) {
                        return '<span class="label label-danger">Bukan</span>';
                    } else {
                        return '<span class="label label-success">Ya</span>';
                    }
                },
                'contentOptions' => ['style' => 'width:80px; text-align:center'],
            ],
            //'is_delete',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Aksi',
                'template' => '{view}{update}{delete}',
                'contentOptions' => ['width' => "80px"],
                'buttons' => [
                    'view' => function ($url, $model) use ($perusahaan) {
                        return $button = Html::button('<i class="fa fa-eye"></i>', ['value' => Url::toRoute(['perusahaan-domisili/view', 'id' => $model->id]), 'title' => 'Detail View Domisili', 'class' => 'showModalButton btn btn-primary btn-sm']);
                    },
                    'update' => function ($url, $model) use ($perusahaan) {
                        $button = "";
                        if ($perusahaan->is_verified == 0 || $perusahaan->is_verified == 3) {
                            $button = Html::button('<i class="fa fa-edit"></i>', ['value' => Url::toRoute(['perusahaan-domisili/update', 'id' => $model->id]), 'title' => 'Update', 'class' => 'showModalButton btn btn-warning btn-sm', 'data-pjax' => 'button-action', 'style' => ['margin-left' => '5px;']]);
                        }

                        return $button;
                    },
                    'delete' => function ($url, $model) use ($perusahaan) {
                        $button = "";
                        if ($perusahaan->is_verified == 0 || $perusahaan->is_verified == 3) {
                            return Html::a('<i class="fa fa-trash"></i>', ['perusahaan-domisili/delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger btn-sm',
                                'style' => ['margin-left' => '5px'],
                                'data-pjax' => 'button-action',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]);
                        }
                    }
                ]
            ],
        ],
    ]); ?>

<?php Pjax::end(); ?>

<?php $this->registerJs('
jQuery(document).ready(function($){
    $(document).ready(function () {
        $("body").on("beforeSubmit", "form#perusahaan-domisili-form", function () {
            var form = $(this);
            var data = new FormData( this );
            // return false if form still have some validation errors
            if (form.find(".has-error").length)
            {
                return false;
            }
            // submit form
            $.ajax({
                url         : form.attr("action"),
                data        : data,
                type        : form.attr("method"),
                cache       : false,
                contentType : false,
                processData : false,
                success: function (response)
                {
                    $("#modal").modal("toggle");
                    console.log(response === "true");
                    if(response === "true"){
                        $.pjax.reload({container:"#data-domisili", async: false}); //for pjax update
                        swal("Data Berhasil Disimpan", {
                            icon : "success",
                            buttons: {
                                confirm: {
                                    className : "btn btn-success"
                                }
                            },
                        });
                    }else{
                        console.log(response);
                        $.pjax.reload({container:"#data-domisili", async: false}); //for pjax update

                        swal("Data Gagal Disimpan", {
                            icon : "error",
                            buttons: {
                                confirm: {
                                    className : "btn btn-danger"
                                }
                            },
                        });
                    }
                },
                error  : function ()
                {
                    console.log("internal server error");
                }
            });
            return false;
            });
    });
});
'); ?>
