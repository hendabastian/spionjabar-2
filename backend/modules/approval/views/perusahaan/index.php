<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\master\TrayekSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Data Perusahaan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="verifikasi-dokumen">

    <div class="card">
        <div class="card-header">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>

        <div class="card-body">
            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'nama_perusahaan',
                    'npwp',
                    'email:email',
                    [
                        'attribute' => 'alamat',
                        'value' => function ($data) {
                            return $data->domisili !== NULL ? $data->domisili->alamat : "-";
                        },
                    ],
                    [
                        'attribute' => 'provinsi',
                        'value' => function ($data) {
                            return $data->domisili !== NULL ? $data->domisili->provinsi->nama : "-";
                        },
                    ],
                    [
                        'attribute' => 'is_verified',
                        'filter' => [0 => 'Belum Terverifikasi', 1 => 'Diajukan', 2 => 'Terdaftar', 3 => 'Ditolak'],
                        'label' => 'Status',
                        'format' => 'RAW',
                        'value' => function ($data) {
                            return $data->checkStatus($data->is_verified);
                        }
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Aksi',
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye"></i>', Url::toRoute(['view', 'id' => base64_encode($model->id * 2)]), ['class' => 'btn btn-primary btn-sm', 'data-pjax' => 0]);
                            }
                        ],
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</div>