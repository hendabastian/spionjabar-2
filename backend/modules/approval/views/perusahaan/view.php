<?php

use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = "Profil Perusahaan";
?>
    <!-- Data Perusahaan -->

    <div class="card">
        <div class="card-header">
            <h2># DATA PERUSAHAAN :</h2>
            <p>
                <?= Html::a('<i class="fa fa-arrow-left"></i> kembali', Url::toRoute(['index', 'jenis_perusahaan' => $model->jenis_perusahaan]), ['class' => 'btn btn-primary btn-xs pull-right']) ?>
                <?php if ($model->is_verified == 1) {
                    echo Html::button('<i class="fa fa-check"></i> Verifikasi', ['value' => Url::toRoute(['perusahaan-verifikasi/verifikasi', 'perusahaan_id' => base64_encode($model->id * 2)]), 'title' => 'Verifikasi', 'class' => 'btn btn-info btn-xs ml-auto showModalButton pull-right']);
                }
                ?>
            </p>
        </div>

        <div class="card-body">
            <?= $this->render('_data_perusahaan', ['model' => $model, 'perusahaanDok' => $perusahaanDok]) ?>
        </div>
    </div>

    <!-- Domisili -->
    <div class="card">
        <div class="card-header">
            <h2># DOMISILI :</h2>


        </div>

        <div class="card-body">
            <?= $this->render('_data_domisili', [
                'perusahaan' => $model,
                'searchDomisili' => $searchDomisili,
                'dataDomisili' => $dataDomisili,
            ]) ?>
        </div>
    </div>

    <!-- Direksi/Pengurus -->
    <div class="card">
        <div class="card-header">
            <h2># DIREKSI/PENGURUS :</h2>


        </div>

        <div class="card-body">
            <?= $this->render('_data_pengurus', [
                'perusahaan' => $model,
                'searchPengurus' => $searchPengurus,
                'dataPengurus' => $dataPengurus,
            ]) ?>
        </div>
    </div>


    <!-- Pemegang Saham -->
    <div class="card">
        <div class="card-header">
            <h2># PEMEGANG SAHAM :</h2>


        </div>

        <div class="card-body">
            <?= $this->render('_data_pemegang_saham', [
                'perusahaan' => $model,
                'searchPemegangSaham' => $searchPemegangSaham,
                'dataPemegangSaham' => $dataPemegangSaham,
            ]) ?>
        </div>
    </div>

    <section id="sk">
        <div class="card">
            <div class="card-header">
                <h2># SK PERUSAHAAN (Khusus Perusahaan Lama):</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                <?= $this->render('_sk_perusahaan', [
                    'perusahaan' => $model,
                    'searchSk' => $searchSk,
                    'dataSk' => $dataSk,
                ])
                ?>
            </div>
        </div>
    </section>

    <!-- Dokumen Perusahaan -->
    <div class="card">
        <div class="card-header">
            <h2># DATA DOKUMEN KELENGKAPAN PERUSAHAAN :</h2>
        </div>

        <div class="card-body">
            <?= $this->render('_dokumen_perusahaan', [
                'perusahaan' => $model,
                'searchDokumen' => $searchDokumen,
                'dataDokumen' => $dataDokumen,
            ]) ?>
        </div>
    </div>

<?php
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'options' => ['tabindex' => ''],
    'closeButton' => ['tag' => 'close', 'x'],
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>