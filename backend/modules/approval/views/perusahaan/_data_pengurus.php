<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<?php Pjax::begin(['id' => 'data-pengurus']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

	<?= GridView::widget([
        'dataProvider' => $dataPengurus,
        'filterModel' => $searchPengurus,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'perusahaan_id',
            'nik',
            [
                'attribute' => 'file_ktp',
                'format' => 'RAW',
                'value' => function ($data) {
                    $button = "";
                    if ($data->file_ktp !== NULL) {
                        $button = Html::button('<i class="fa fa-file-pdf-o"></i>', ['value' => Url::toRoute(['perusahaan-dokumen/preview-dok', 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $data->perusahaan_id . '/legalitas/', 'file' => $data->file_ktp]), 'title' => 'Preview Dokumen', 'class' => 'showModalButton btn btn-primary btn-sm']);
                    }

                    return $button;
                },
                'contentOptions' => ['style' => 'width: 50px;font-align: center;'],
            ],
            'npwp',
            [
                'attribute' => 'file_npwp',
                'format' => 'RAW',
                'value' => function ($data) {
                    $button = "";
                    if ($data->file_npwp !== NULL) {
                        $button = Html::button('<i class="fa fa-file-pdf-o"></i>', ['value' => Url::toRoute(['perusahaan-dokumen/preview-dok', 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $data->perusahaan_id . '/legalitas/', 'file' => $data->file_npwp]), 'title' => 'Preview Dokumen', 'class' => 'showModalButton btn btn-primary btn-sm']);
                    }

                    return $button;
                },
                'contentOptions' => ['style' => 'width: 50px;font-align: center;'],
            ],
            'nama_lengkap',
            'jabatan',
            //'periode_mulai',
            //'periode_selesai',
            //'keterangan:ntext',
            //'is_delete',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Aksi',
                'template' => '{view}{update}{delete}',
                'contentOptions' => ['width' => '80px'],
                'buttons' => [
                    'view' => function ($url, $model) use ($perusahaan) {
                        return Html::button('<i class="fa fa-eye"></i>', ['value' => Url::toRoute(['perusahaan-pengurus/view', 'id' => $model->id]), 'title' => 'Detail view Pengurus/Direksi', 'class' => 'showModalButton btn btn-primary btn-sm']);
                    },
                    'update' => function ($url, $model) use ($perusahaan) {
                        $button = "";
                        if ($perusahaan->is_verified == 0 || $perusahaan->is_verified == 3) {
                            $button = Html::button('<i class="fa fa-edit"></i>', ['value' => Url::toRoute(['perusahaan-pengurus/update', 'id' => $model->id]), 'title' => 'Update', 'class' => 'showModalButton btn btn-warning btn-sm', 'style' => ['margin-left' => '5px;']]);
                        }

                        return $button;
                    },
                    'delete' => function ($url, $model) use ($perusahaan) {
                        $button = "";
                        if ($perusahaan->is_verified == 0 || $perusahaan->is_verified == 3) {
                            $button = Html::a('<i class="fa fa-trash"></i>', ['perusahaan-pengurus/delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger btn-sm',
                                'style' => ['margin-left' => '5px'],
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]);
                        }
                        return $button;
                    }
                ]
            ],
        ],
    ]); ?>

<?php Pjax::end(); ?>