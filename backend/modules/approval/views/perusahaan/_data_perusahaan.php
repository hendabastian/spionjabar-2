<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Url;
 ?>
<?php Pjax::begin(['id' => 'data-perusahaan']) ?>
<table class="table table-striped">
    <tr>
        <td style="width: 25%;font-weight: bold;">Status Perusahaan</td>
        <td style="width: 7%;font-weight: bold;">:</td>
        <td><?= $model->checkStatus($model->is_verified); ?></td>
    </tr>
    <tr>
        <td style="width: 25%;font-weight: bold;">Kode Perusahaan</td>
        <td style="width: 7%;font-weight: bold;">:</td>
        <td><?= $model->kode_perusahaan == NULL ? "-" : $model->kode_perusahaan;?></td>
    </tr>
    <tr>
        <td style="width: 25%;font-weight: bold;">Jenis Badan Usaha</td>
        <td style="width: 7%;font-weight: bold;">:</td>
        <td><?= $model->jenisBadanUsaha == NULL ? "-" : $model->jenisBadanUsaha->jenis_badan_usaha;?></td>
    </tr>
    <tr>
        <td style="width: 25%;font-weight: bold;">Nama Perusahaan</td>
        <td style="width: 7%;font-weight: bold;">:</td>
        <td><?= $model->nama_perusahaan;?></td>
    </tr>
    <tr>
        <td style="width: 25%;font-weight: bold;">NPWP Perusahaan</td>
        <td style="width: 7%;font-weight: bold;">:</td>
        <td><?= $model->npwp;?></td>
    </tr>
    <tr>
        <td style="width: 25%;font-weight: bold;">File NPWP</td>
        <td style="width: 7%;font-weight: bold;">:</td>
        <td><?= $perusahaanDok !== NULL ? Html::button('<i class="fa fa-file-pdf-o"></i>', ['value' => Url::toRoute(['perusahaan-dokumen/preview-dok', 'path' => Yii::$app->urlManagerUpload->baseUrl.'/perusahaan/'.$model->id.'/legalitas/', 'file' => $perusahaanDok->filename]), 'title' => 'Preview Dokumen', 'class' => 'showModalButton btn btn-primary btn-sm']) : "dokumen tidak ada" ;?></td>
    </tr>
    <tr>
        <td style="width: 25%;font-weight: bold;">Email</td>
        <td style="width: 7%;font-weight: bold;">:</td>
        <td><?= $model->email;?></td>
    </tr>
    <tr>
        <td style="width: 25%;font-weight: bold;">No. HP</td>
        <td style="width: 7%;font-weight: bold;">:</td>
        <td><?= $model->hp;?></td>
    </tr>
    <tr>
        <td style="width: 25%;font-weight: bold;">Website</td>
        <td style="width: 7%;font-weight: bold;">:</td>
        <td><?= $model->website == NULL ? "-" : $model->website;?></td>
    </tr>
    <tr>
        <td style="width: 25%;font-weight: bold;">History Verifikasi</td>
        <td style="width: 7%;font-weight: bold;">:</td>
        <td><?= Html::button('<i class="fa fa-history"></i> History Verifikasi', ['value' => Url::toRoute(['perusahaan/history-verifikasi', 'id' => $model->user_id]), 'title' => 'History Verifikasi', 'class' => 'showModalButton btn btn-primary btn-sm']) ?></td>
    </tr>
</table>
<?php Pjax::end() ?>
<?php $this->registerJs('
jQuery(document).ready(function($){
        $(document).ready(function () {
            $("body").on("beforeSubmit", "form#sunting-data", function () {
                var form = $(this);
                var data = new FormData( this );
                // return false if form still have some validation errors
                if (form.find(".has-error").length)
                {
                    return false;
                }
                // submit form
                $.ajax({
                    url         : form.attr("action"),
                    data        : data,
                    type        : form.attr("method"),
                    cache       : false,
                    contentType : false,
                    processData : false,
                    success: function (response)
                    {
                        $("#modal").modal("toggle");
                        console.log(response === "true");
                        if(response === "true"){
                            $.pjax.reload({container:"#data-perusahaan", async: false}); //for pjax update
                            Swal.fire(
                                  "Berhasil!",
                                  "Perubahan Berhasil Disimpan.",
                                  "success"
                                )
                        }else{
                            $.pjax.reload({container:"#data-perusahaan", async: false}); //for pjax update

                            Swal.fire(
                                  "Gagal!",
                                  "Perubahan Gagal Disimpan.",
                                  "danger"
                                )
                        }
                    },
                    error  : function ()
                    {
                        console.log("internal server error");
                    }
                });
                return false;
                });
        });
    });
'); ?>
