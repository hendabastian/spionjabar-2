<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\perusahaan\PerusahaanDokumen;
?>

<?php Pjax::begin(['id' => 'data-dokumen']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataDokumen,
        'filterModel' => $searchDokumen,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'jenis_dokumen',

            [
            	'class' => 'yii\grid\ActionColumn',
            	'header' => 'Aksi',
            	'contentOptions' => ['style' => 'text-align: center;width: 120px;'],
            	'template' => '{view}{preview-dok}',
                'buttons' => [

                    'view' => function($url, $model) use ($perusahaan){
                        $button = "Dokumen Kosong";
                        $dokumen = PerusahaanDokumen::find()->where(['is_delete' => 0, 'perusahaan_id' => $perusahaan->id, 'jenis_dokumen_perusahaan_id' => $model->id])->one();
                        if ($dokumen !== NULL) {
                            $button = Html::button('<i class="fa fa-eye"></i>', ['value' => Url::toRoute(['perusahaan-dokumen/view', 'id' => $dokumen->id]), 'title' => 'View', 'class' => 'showModalButton btn btn-primary btn-sm']);
                        }
                        return $button;
                    },
                    'preview-dok' => function($url, $model) use ($perusahaan){
                        $button = "";
                        $dokumen = PerusahaanDokumen::find()->where(['is_delete' => 0, 'perusahaan_id' => $perusahaan->id, 'jenis_dokumen_perusahaan_id' => $model->id])->one();
                        if ($dokumen !== NULL) {
                            $button = Html::button('<i class="fa fa-file-pdf"></i>', ['value' => Url::toRoute(['perusahaan-dokumen/preview-dok', 'path' => Yii::$app->urlManagerUpload->baseUrl.'/perusahaan/'.$perusahaan->id.'/legalitas/', 'file' => $dokumen->filename]), 'title' => 'Preview Dokumen', 'class' => 'showModalButton btn btn-primary btn-sm']);
                        }
                        return $button;
                    },
                ],
            ],
        ],
    ]); ?>

<?php Pjax::end(); ?>

<!-- Upload Dokumen -->
<?php $this->registerJs('
jQuery(document).ready(function($){
        $(document).ready(function () {
            $("body").on("beforeSubmit", "form#perusahaan-dokumen-form", function () {
                var form = $(this);
                var data = new FormData( this );
                // return false if form still have some validation errors
                if (form.find(".has-error").length)
                {
                    return false;
                }
                // submit form
                $.ajax({
                    url         : form.attr("action"),
                    data        : data,
                    type        : form.attr("method"),
                    cache       : false,
                    contentType : false,
                    processData : false,
                    success: function (response)
                    {
                        $("#modal").modal("toggle");
                        console.log(response === "true");
                        if(response === "true"){
                            $.pjax.reload({container:"#data-dokumen"}); // pjax gridview data dokumen
                            button-data-perusahaan
                            swal("Dokumen Berhasil Disimpan", {
                                icon : "success",
                                buttons: {
                                    confirm: {
                                        className : "btn btn-success"
                                    }
                                },
                            });
                        }else{
                            console.log(response);
                            $.pjax.reload({container:"#data-dokumen"}); //for pjax update
                            swal("Dokumen Gagal Disimpan", {
                                icon : "error",
                                buttons: {
                                    confirm: {
                                        className : "btn btn-danger"
                                    }
                                },
                            });
                        }
                    },
                    error  : function ()
                    {
                        console.log("internal server error");
                    }
                });
                return false;
                });
        });
    });
'); ?>

<!-- Pengajuan Dokumen -->
<?php $this->registerJs('
jQuery(document).ready(function($){
        $(document).ready(function () {
            $("body").on("beforeSubmit", "form#pengajuan-dokumen-form", function () {
                var form = $(this);
                var data = new FormData( this );
                // return false if form still have some validation errors
                if (form.find(".has-error").length)
                {
                    return false;
                }
                // submit form
                $.ajax({
                    url         : form.attr("action"),
                    data        : data,
                    type        : form.attr("method"),
                    cache       : false,
                    contentType : false,
                    processData : false,
                    success: function (response)
                    {
                        $("#modal").modal("toggle");
                        console.log(response === "true");
                        if(response === "true"){
                            swal("Dokumen Berhasil Diajukan", {
                                icon : "success",
                                buttons: {
                                    confirm: {
                                        className : "btn btn-success"
                                    }
                                },
                            });

                            $.pjax.reload({container:"#data-dokumen", async: false}); //for pjax update
                            $.pjax.reload({container:"#data-perusahaan", async: false}); //for pjax update
                            $.pjax.reload({container:"#data-domisili", async: false}); //for pjax update
                            $.pjax.reload({container:"#data-pengurus", async: false}); //for pjax update
                            $.pjax.reload({container:"#data-pemegang-saham", async: false}); //for pjax update
                            $.pjax.reload({container:"#button-ajukan", async: false}); //for pjax update

                            var i;
                            for(i = 0;i <= 3; i++){
                                $.pjax.reload({container:"#p"+i}); // button tambah data
                            }


                        }else{
                            console.log(response);
                            $.pjax.reload({container:"#data-dokumen", async: false}); //for pjax update

                            swal("Dokumen Gagal Diajukan", {
                                icon : "error",
                                buttons: {
                                    confirm: {
                                        className : "btn btn-danger"
                                    }
                                },
                            });
                        }
                    },
                    error  : function ()
                    {
                        console.log("internal server error");
                    }
                });
                return false;
                });
        });
    });
'); ?>
