<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<?php Pjax::begin(['id' => 'data-pemegang-saham']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataPemegangSaham,
        'filterModel' => $searchPemegangSaham,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'perusahaan_id',
            'nik',
            [
                'attribute' => 'file_ktp',
                'format' => 'RAW',
                'value' => function($data){
                  $button = "";
                  if ($data->file_ktp !== NULL) {
                    $button = Html::button('<i class="fa fa-file-pdf-o"></i>', ['value' => Url::toRoute(['perusahaan-dokumen/preview-dok', 'path' => Yii::$app->urlManagerUpload->baseUrl.'/perusahaan/'.$data->perusahaan_id.'/legalitas/', 'file' => $data->file_ktp]), 'title' => 'Preview Dokumen', 'class' => 'showModalButton btn btn-primary btn-sm']);
                  }
    
                  return $button;
                },
                'contentOptions' => ['style' => 'width: 50px;font-align: center;'],
            ],
            'npwp',
            [
                'attribute' => 'file_npwp',
                'format' => 'RAW',
                'value' => function($data){
                $button = "";
                if ($data->file_npwp !== NULL) {
                    $button = Html::button('<i class="fa fa-file-pdf-o"></i>', ['value' => Url::toRoute(['perusahaan-dokumen/preview-dok', 'path' => Yii::$app->urlManagerUpload->baseUrl.'/perusahaan/'.$data->perusahaan_id.'/legalitas/', 'file' => $data->file_npwp]), 'title' => 'Preview Dokumen', 'class' => 'showModalButton btn btn-primary btn-sm']);
                }

                return $button;
                },
                'contentOptions' => ['style' => 'width: 50px;font-align: center;'],
            ],
            'nama_lengkap',
            [
               'attribute' => 'persentase',
               'value' => function($data){
                return $data->persentase . "%";
               }
            ],
            'keterangan:ntext',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            [
            	'class' => 'yii\grid\ActionColumn',
            	'header' => 'Aksi',
            	'template' => '{view}',
                'contentOptions' => ['width' => '80px'],
            	'buttons' => [
            		'view' => function($url, $model) use ($perusahaan){
                        return $button = Html::button('<i class="fa fa-eye"></i>', ['value' => Url::toRoute(['perusahaan-pemegang-saham/view', 'id' => $model->id]), 'title' => 'Detail View Pemegang Saham', 'class' => 'showModalButton btn btn-primary btn-sm']);
                    },
            	],
        	],
        ],
    ]); ?>

<?php Pjax::end(); ?>
<?php $this->registerJs('
jQuery(document).ready(function($){
    $(document).ready(function () {
        $("body").on("beforeSubmit", "form#perusahaan-pemegang-saham-form", function () {
            var form = $(this);
            var data = new FormData( this );
            // return false if form still have some validation errors
            if (form.find(".has-error").length)
            {
                return false;
            }
            // submit form
            $.ajax({
                url         : form.attr("action"),
                data        : data,
                type        : form.attr("method"),
                cache       : false,
                contentType : false,
                processData : false,
                success: function (response)
                {
                    $("#modal").modal("toggle");
                    console.log(response === "true");
                    if(response === "true"){
                        $.pjax.reload({container:"#data-pemegang-saham", async: false}); //for pjax update
                        swal("Data Berhasil Disimpan", {
                            icon : "success",
                            buttons: {
                                confirm: {
                                    className : "btn btn-success"
                                }
                            },
                        });
                    }else{
                        console.log(response);
                        $.pjax.reload({container:"#data-pemegang-saham", async: false}); //for pjax update

                        swal("Data Gagal Disimpan", {
                            icon : "error",
                            buttons: {
                                confirm: {
                                    className : "btn btn-danger"
                                }
                            },
                        });
                    }
                },
                error  : function ()
                {
                    console.log("internal server error");
                }
            });
            return false;
            });
    });
});
'); ?>
