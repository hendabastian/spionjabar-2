<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\perusahaan\SkKendaraan */
?>
<div class="sk-kendaraan-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            // 'sk_id',
            [
                'attribute' => 'perusahaan_id',
                'label' => 'Perusahaan',
                'value' => function ($data) {
                    return $data->perusahaan->nama_perusahaan;
                }
            ],
            // 'no_srut',
            // 'tgl_srut',
            'no_kendaraan',
            'no_rangka',
            'no_uji',
            'expire_uji',
            'no_mesin',
            'tahun',
            'merk',
            'nama_pemilik',
            'jenis_kendaraan_id',
            'seat',
            [
                'attribute' => 'stnk',
                'format' => 'RAW',
                'value' => function ($data) {
                    if ($data->stnk !== NULL) {
                        return Html::a('Download', \yii\helpers\Url::toRoute(['download', 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $data->perusahaan_id . '/legalitas/', 'file' => $data->stnk]), ['class' => 'btn btn-primary btn-sm', 'target' => '_blank', 'data-pjax' => 0]);
                    }
                },
            ],
            [
                'attribute' => 'kir',
                'format' => 'RAW',
                'value' => function ($data) {
                    if ($data->kir !== NULL) {
                        return Html::a('Download', \yii\helpers\Url::toRoute(['download', 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $data->perusahaan_id . '/legalitas/', 'file' => $data->kir]), ['class' => 'btn btn-primary btn-sm', 'target' => '_blank', 'data-pjax' => 0]);
                    }
                },
            ],
            [
                'attribute' => 'bukti_jasa_raharja',
                'format' => 'RAW',
                'value' => function ($data) {
                    if ($data->bukti_jasa_raharja !== NULL) {
                        return Html::a('Download', \yii\helpers\Url::toRoute(['download', 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $data->perusahaan_id . '/legalitas/', 'file' => $data->bukti_jasa_raharja]), ['class' => 'btn btn-primary btn-sm', 'target' => '_blank', 'data-pjax' => 0]);
                    }
                },
            ],
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
            // 'is_delete',
        ],
    ]) ?>
    <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Close', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']) ?>


</div>