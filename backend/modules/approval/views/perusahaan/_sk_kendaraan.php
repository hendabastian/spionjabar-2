<?php

use common\models\perusahaan\Perusahaan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\perusahaan\SkKendaraan */

$this->title = 'Tambah Kendaraan Terdaftar';
$this->params['breadcrumbs'][] = ['label' => 'Sk Kendaraan'];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::a('<i class="fa fa-arrow-left"></i> Kembali', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
<div class="sk-kendaraan-create">
    <div class="card">
        <div class="card-header">
            <h2>Data SK Perusahaan <?= $skPerusahaan->perusahaan->nama_perusahaan ?> </h2>
            <div class="clearfix"></div>
        </div>
        <div class="card-body">
            <?= DetailView::widget([
                'model' => $skPerusahaan,
                'attributes' => [
                    // 'id',
                    [
                        'attribute' => 'perusahaan_id',
                        'label' => 'Perusahaan',
                        'value' => function ($data) {
                            return $data->perusahaan->nama_perusahaan;
                        }
                    ],
                    'trayek',
                    [
                        'attribute' => 'jenis_angkutan_id',
                        'label' => 'Jenis Angkutan',
                        'value' => function ($data) {
                            return $data->jenisAngkutan->jenis_angkutan;
                        }
                    ],
                    'no_sk',
                    [
                        'attribute' => 'file_sk',
                        'label' => 'File SK',
                        'format' => 'RAW',
                        'value' => function ($data) {
                            $button = "";
                            if ($data->file_sk !== NULL) {
                                $button = Html::a('<i class="fa fa-download"></i> Download', Url::toRoute(['download', 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $data->perusahaan_id . '/legalitas/', 'file' => $data->file_sk]), ['class' => ' btn btn-primary btn-sm']);
                            }
                            return $button;
                        },
                    ],
                    'tgl_terbit',
                    'exp_sk',
                    // 'is_active',
                    // 'is_delete',
                    // 'created_at',
                    // 'updated_at',
                    // 'created_by',
                    // 'updated_by',
                ],
            ]) ?>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h2><?= Html::encode($this->title) ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    // 'sk_id',
                    // 'perusahaan_id',
                    // 'no_srut',
                    // 'tgl_srut',
                    'no_kendaraan',
                    'no_rangka',
                    //'no_uji',
                    //'expire_uji',
                    //'no_mesin',
                    'tahun',
                    'merk',
                    //'nama_pemilik',
                    //'jenis_kendaraan_id',
                    //'seat',
                    //'stnk',
                    //'kir',
                    //'bukti_jasa_raharja',
                    //'created_at',
                    //'updated_at',
                    //'created_by',
                    //'updated_by',
                    //'is_delete',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Aksi',
                        'contentOptions' => ['style' => 'text-align:center; width: 70px;'],
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, $data) {
                                return Html::button('<i class="fa fa-eye"></i>', ['value' => Url::toRoute(['view-kendaraan', 'id' => $data->id]), 'title' => 'Detail Kendaraan', 'class' => 'showModalButton btn btn-primary btn-sm']);
                            },
                        ]
                    ],
                ],
            ]); ?>

        </div>

    </div>
</div>

<?php
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'closeButton' => ['tag' => 'close', 'x'],
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>