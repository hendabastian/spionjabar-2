<?php

namespace backend\modules\approval\controllers;

use common\models\perusahaan\Perusahaan;
use Yii;
use common\models\perusahaan\SkPerusahaan;
use common\models\perusahaan\SkPerusahaanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * SkPerusahaanController implements the CRUD actions for SkPerusahaan model.
 */
class SkPerusahaanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SkPerusahaan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SkPerusahaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SkPerusahaan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $perusahaan = Perusahaan::find()->where(['id' => $model->perusahaan_id])->one();
        return $this->renderAjax('view', [
            'perusahaan' => $perusahaan,
            'model' => $model,
        ]);
    }

    /**
     * Creates a new SkPerusahaan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($perusahaan_id)
    {
        $this->layout = 'dashboard/main';
        $model = new SkPerusahaan();
        $model->scenario = "create";

        if ($model->load(Yii::$app->request->post())) {
            $dokSK = UploadedFile::getInstance($model, 'file_sk');
            $directory = Yii::getAlias('@uploads/perusahaan/' . $perusahaan_id . '/legalitas/');

            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            $uid = uniqid(time(), true);

            if (!empty($dokSK)) {
                $fileSK = $uid . '_sk' . '.' . $dokSK->extension;
                $filePathSK = $directory . $fileSK;

                if ($dokSK->saveAs($filePathSK)) {
                    $model->file_sk = $fileSK;
                    $perusahaan = Perusahaan::find()->where(['id' => $perusahaan_id])->one();
                    $perusahaan->jenis_perusahaan = 1; //set jenis perusahaan menjadi perusahaan lama
                    $perusahaan->save();
                    if ($model->save()) {
                        Yii::$app->session->setFlash('success', 'Data berhasil disimpan');
                        return $this->redirect(['perusahaan/profil-perusahaan']);
                    }
                }
            }
        }

        return $this->renderAjax('_form', [
            'perusahaan_id' => $perusahaan_id,
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SkPerusahaan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $perusahaan_id)
    {
        $model = $this->findModel($id);
        $oldSk = $model->file_sk;
        if ($model->load(Yii::$app->request->post())) {
            $dokSk = UploadedFile::getInstance($model, 'file_sk');
            $directory = Yii::getAlias('@uploads/perusahaan/' . $perusahaan_id . '/legalitas/');
            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            $uid = uniqid(time(), true);

            if (!empty($dokSk)) {
                $fileSk = $uid . '_sk' . '.' . $dokSk->extension;
                $filePathSk = $directory . $fileSk;

                if ($dokSk->saveAs($filePathSk)) {
                    $model->file_sk = $fileSk;
                }
            } else {
                $model->file_sk = $oldSk;
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Data berhasil disimpan');
                return $this->redirect(['perusahaan/profil-perusahaan']);
            }
        }

        return $this->renderAjax('_form', [
            'perusahaan_id' => $perusahaan_id,
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SkPerusahaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->is_delete = 1;

        return $this->redirect(['perusahaan/profile-perusahaan']);
    }

    /**
     * Finds the SkPerusahaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SkPerusahaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SkPerusahaan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
