<?php

namespace backend\modules\approval\controllers;

use backend\modules\approval\models\PerusahaanDomisiliSearch;
use backend\modules\approval\models\PerusahaanPemegangSahamSearch;
use backend\modules\approval\models\PerusahaanPengurusSearch;
use backend\modules\approval\models\PerusahaanSearch;
use backend\modules\approval\models\SkPerusahaanSearch;
use backend\modules\master\models\JenisDokumenPerusahaanSearch;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use common\models\perusahaan\Perusahaan;
use common\models\perusahaan\PerusahaanDokumen;
use common\models\perusahaan\PerusahaanVerifikasi;
use common\models\perusahaan\SkPerusahaan;

/**
 * Perusahaan Controller
 */
class PerusahaanController extends Controller
{

  /**
   * {@inheritdoc}
   */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'actions' => ['index', 'view', 'history-verifikasi', 'sk-kendaraan', 'download', 'view-kendaraan', 'perusahaan-belum-approved', 'perusahaan-sudah-approved'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  public function actionIndex()
  {
    $searchModel = new PerusahaanSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionView($id)
  {
    $decoded_id = base64_decode($id);
    $model = Perusahaan::find()->where(['is_delete' => 0, 'id' => $decoded_id / 2, 'status' => 1])->one();
    if ($model !== NULL) {
      // data domisili
      $searchDomisili = new PerusahaanDomisiliSearch();
      $dataDomisili = $searchDomisili->search(Yii::$app->request->queryParams);
      $dataDomisili->query->andWhere('perusahaan_id=' . $model->id);

      // data pengurus
      $searchPengurus = new PerusahaanPengurusSearch();
      $dataPengurus = $searchPengurus->search(Yii::$app->request->queryParams);
      $dataPengurus->query->andWhere('perusahaan_id=' . $model->id);

      // data pemegang saham
      $searchPemegangSaham = new PerusahaanPemegangSahamSearch();
      $dataPemegangSaham = $searchPemegangSaham->search(Yii::$app->request->queryParams);
      $dataPemegangSaham->query->andWhere('perusahaan_id=' . $model->id);

      //sk perusahaan
      $searchSk = new SkPerusahaanSearch();
      $dataSk = $searchSk->search(Yii::$app->request->queryParams);
      $dataSk->query->andWhere(['perusahaan_id' => $model->id, 'is_active' => 1, 'is_delete' => 0]);

      // dokumen perusahaan
      $searchDokumen = new JenisDokumenPerusahaanSearch();
      $dataDokumen = $searchDokumen->search(Yii::$app->request->queryParams);
      $dataDokumen->query->andWhere('is_active=1');

      // file npwp
      $perusahaanDok = PerusahaanDokumen::find()->where(['is_delete' => 0, 'perusahaan_id' => $model->id, 'jenis_dokumen_perusahaan_id' => 2])->one();

      return $this->render('view', [
        'model' => $model,
        'searchDomisili' => $searchDomisili,
        'dataDomisili' => $dataDomisili,
        'searchPengurus' => $searchPengurus,
        'dataPengurus' => $dataPengurus,
        'searchPemegangSaham' => $searchPemegangSaham,
        'dataPemegangSaham' => $dataPemegangSaham,
        'searchDokumen' => $searchDokumen,
        'dataDokumen' => $dataDokumen,
        'perusahaanDok' => $perusahaanDok,
        'searchSk' => $searchSk,
        'dataSk' => $dataSk,
      ]);
    } else {
      throw new \yii\web\NotFoundHttpException();
    }
  }

  public function actionDownload($path, $file)
  {
    $path = (explode('/', $path));
    $result = array_slice($path, 5);
    $fullpath = Yii::getAlias('@uploads/') . implode("/", $result) . $file;

    if (file_exists($fullpath)) {
      return Yii::$app->response->sendFile($fullpath);
    } else {
      return 'File Not Found';
    }
  }

  public function actionSkKendaraan($sk_id, $perusahaan_id)
  {
    $skPerusahaan = SkPerusahaan::findOne($sk_id);
    $searchModel = new SkKendaraanSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $dataProvider->query->andWhere(['sk_id' => $sk_id]);

    return $this->render('_sk_kendaraan', [
      'skPerusahaan' => $skPerusahaan,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Fungsi untuk menampilkan history Verifikasi
   * mengambil data perusahaan dengan user_id karena kalo dengan perusahaan id data perusahaan yang sebelumnya tidak akan muncul
   * @param int $id = user_id
   */
  public function actionHistoryVerifikasi($id)
  {
    $perusahaan = Perusahaan::find()->where(['is_delete' => 0, 'user_id' => $id])->all();
    if ($perusahaan !== NULL) {
      return $this->renderAjax('_history_verifikasi', [
        'perusahaan' => $perusahaan,
      ]);
    } else {
      return "Empty";
    }
  }

  public function actionViewKendaraan($id)
  {
    $model = SkKendaraan::findOne($id);
    return $this->renderAjax('_detail_kendaraan', [
      'model' => $model,
    ]);
  }

  public function actionPerusahaanBelumApproved()
  {
    $jenis_perusahaan = '';
    $searchModel = new PerusahaanSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $dataProvider->query
      ->andWhere(['status' => 1, 'is_verified' => 1])
      ->andWhere(['not', ['jenis_perusahaan' => NULL]]);
    return $this->render('index', [
      'jenis_perusahaan' => $jenis_perusahaan,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionPerusahaanSudahApproved()
  {
    $jenis_perusahaan = '';
    $searchModel = new PerusahaanSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $dataProvider->query
      ->andWhere(['status' => 1, 'is_verified' => 2])
      ->andWhere(['not', ['jenis_perusahaan' => NULL]]);
    return $this->render('index', [
      'jenis_perusahaan' => $jenis_perusahaan,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }
}
