<?php

namespace backend\modules\approval\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\perusahaan\PerusahaanDokumen;
use common\models\perusahaan\Perusahaan;
use common\models\master\JenisDokumenPerusahaan;
use common\models\master\JenisDokumenPerusahaanSearch;
use common\models\ActivityLog;
use yii\web\NotFoundHttpException;

class PerusahaanDokumenController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['view', 'preview-dok', 'download'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Download dokumen regulasi.
     * @param integer $path
     * @return file
     */
    public function actionDownload($path, $file)
    {
        $path = (explode('/', $path));
        $result = array_slice($path, 5);
        $fullpath = Yii::getAlias('@uploads/') . implode("/", $result) . $file;

        if (file_exists($fullpath)) {
            return Yii::$app->response->sendFile($fullpath);
        } else {
            return 'File Not Found';
        }
    }

    /**
     * Preview dokumen.
     * @param integer $path
     * @return file
     */
    public function actionPreviewDok($path, $file)
    {
        $array = (explode('/', $path));
        $result = array_slice($array, 5);
        $fullpath = Yii::getAlias('@uploads/') . implode("/", $result) . $file;

        return $this->renderAjax('preview-dok', [
            'path' => $path,
            'file' => $file,
            'fullpath' => $fullpath,
        ]);
    }

    /**
     * Finds the PagesData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PagesData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PerusahaanDokumen::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
