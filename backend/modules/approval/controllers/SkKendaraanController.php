<?php

namespace backend\modules\approval\controllers;

use common\models\perusahaan\Perusahaan;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Yii;
use common\models\perusahaan\SkPerusahaanKendaraan;
use frontend\models\perusahaan\SkPerusahaanKendaraanSearch;
use common\models\perusahaan\SkPerusahaan;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * SkPerusahaanKendaraanController implements the CRUD actions for SkPerusahaanKendaraan model.
 */
class SkKendaraanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SkPerusahaanKendaraan models.
     * @return mixed
     */
    public function actionIndex($sk_id, $perusahaan_id)
    {
        $model = new SkPerusahaanKendaraan();
        $skPerusahaan = SkPerusahaan::findOne($sk_id);
        $searchModel = new SkPerusahaanKendaraanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['sk_id' => $sk_id]);
        $perusahaan = Perusahaan::findOne($perusahaan_id);

        return $this->render('index', [
            'model' => $model,
            'perusahaan_id' => $perusahaan_id,
            'skPerusahaan' => $skPerusahaan,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'perusahaan' => $perusahaan,
        ]);
    }

    /**
     * Displays a single SkPerusahaanKendaraan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SkPerusahaanKendaraan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($sk_id, $perusahaan_id)
    {
        $model = new SkPerusahaanKendaraan();
        $skPerusahaan = SkPerusahaan::findOne($sk_id);
        $searchModel = new SkPerusahaanKendaraanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['sk_id' => $sk_id]);

        if ($model->load(Yii::$app->request->post())) {
            $stnk = UploadedFile::getInstance($model, 'stnk');
            $kir = UploadedFile::getInstance($model, 'kir');
            $buktiJasaRaharja = UploadedFile::getInstance($model, 'bukti_jasa_raharja');
            $directory = Yii::getAlias('@uploads/perusahaan/' . $perusahaan_id . '/legalitas/');

            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            $uid = uniqid(time(), true);

            if (!empty($stnk) && !empty($kir) && !empty($buktiJasaRaharja)) {
                $fileStnk = $uid . '_file_stnk' . '.' . $stnk->extension;
                $filePathStnk = $directory . $fileStnk;

                $fileKir = $uid . '_file_kir' . '.' . $kir->extension;
                $filePathKir = $directory . $fileKir;

                $fileBuktiJasaRaharja = $uid . '_file_bukti_jasa_raharja' . '.' . $buktiJasaRaharja->extension;
                $filePathJasaRaharja = $directory . $fileBuktiJasaRaharja;

                if ($stnk->saveAs($filePathStnk) && $kir->saveAs($filePathKir) && $buktiJasaRaharja->saveAs($filePathJasaRaharja)) {
                    $model->no_kendaraan = strtoupper($model->no_kendaraan);
                    $model->stnk = $fileStnk;
                    $model->kir = $fileKir;
                    $model->bukti_jasa_raharja = $fileBuktiJasaRaharja;

                    if ($model->save()) {
                        return $this->redirect(['index', 'sk_id' => $sk_id, 'perusahaan_id' => $perusahaan_id]);
                    }
                }
            }
        }

        return $this->renderAjax('_form', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'skPerusahaan' => $skPerusahaan,
            'sk_id' => $sk_id,
            'perusahaan_id' => $perusahaan_id,
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SkPerusahaanKendaraan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $sk_id)
    {
        $model = $this->findModel($id);
        $oldSkId = $model->sk_id;
        $perusahaan_id = $model->sk->perusahaan_id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
            'sk_id' => $sk_id,
            'perusahaan_id' => $perusahaan_id,
        ]);
    }

    /**
     * Deletes an existing SkPerusahaanKendaraan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->is_delete = 1;
        $model->save();

        return $this->redirect(['index', 'sk_id' => $model->sk_id, 'perusahaan_id' => $model->perusahaan_id]);
    }

    public function actionDownload($file)
    {
        $response = "File Not Found";
        if (file_exists($file)) {
            $response = Yii::$app->response->sendFile($file);
        }

        return $response;
    }

    public function actionImportExcel($sk_id, $perusahaan_id)
    {
        if (Yii::$app->request->isPost) {
            $fileExcel = UploadedFile::getInstanceByName('file_excel');
            $reader = new Xlsx();
            $getSheet = $reader->load($fileExcel->tempName);
            $sheetData = $getSheet->getActiveSheet()->toArray();
            array_shift($sheetData);
//            return Json::encode($sheetData);
            foreach ($sheetData as $index => $data) {
                $nopol = $data[0];
                $no_rangka = $data[1];
                $no_uji = $data[2];
                $expire_uji = $data[3];
                switch ($data[4]) {
                    case "BUS BESAR":
                        $jenis_kendaraan = 1;
                    break;
                    case "BUS SEDANG":
                        $jenis_kendaraan = 2;
                        break;
                    case "BUS KECIL":
                        $jenis_kendaraan = 3;
                        break;
                }
                $no_mesin = $data[5];
                $tahun = $data[6];
                $merk = $data[7];
                $nama_pemilik = $data[8];
                $jumlah_seat = $data[9];

                $kendaraan[$index] = new SkPerusahaanKendaraan();
                $kendaraan[$index]->sk_id = $sk_id;
                $kendaraan[$index]->no_kendaraan = $nopol;
                $kendaraan[$index]->no_rangka = $no_rangka;
                $kendaraan[$index]->no_uji = $no_uji;
                $kendaraan[$index]->expire_uji = date('Y-m-d', strtotime($expire_uji));
                $kendaraan[$index]->no_mesin = $no_mesin;
                $kendaraan[$index]->tahun = $tahun;
                $kendaraan[$index]->merk = $merk;
                $kendaraan[$index]->nama_pemilik = $nama_pemilik;
                $kendaraan[$index]->jenis_kendaraan_id = $jenis_kendaraan;
                $kendaraan[$index]->seat = $jumlah_seat;
                $kendaraan[$index]->is_active = 1;
                $kendaraan[$index]->insert();
            }

            Yii::$app->session->setFlash('success', 'Data Kendaraan berhasil diimport');
            return $this->redirect(['index', 'sk_id' => $sk_id, 'perusahaan_id' => $perusahaan_id]);
        } else {
            return $this->renderAjax('_form_import', [
                'sk_id' => $sk_id,
                'perusahaan_id' => $perusahaan_id
            ]);
        }

    }

    /**
     * Finds the SkPerusahaanKendaraan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SkPerusahaanKendaraan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SkPerusahaanKendaraan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
