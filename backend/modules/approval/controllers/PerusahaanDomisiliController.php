<?php

namespace backend\modules\approval\controllers;

use Yii;
use common\models\perusahaan\PerusahaanDomisili;
use common\models\perusahaan\PerusahaanDomisiliSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\ActivityLog;

/**
 * PerusahaanDokumenController implements the CRUD actions for PerusahaanDomisili model.
 */
class PerusahaanDomisiliController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays a single PerusahaanDomisili model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->renderAjax('view', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the PerusahaanDomisili model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PerusahaanDomisili the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PerusahaanDomisili::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
