<?php

namespace backend\modules\approval\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\perusahaan\PerusahaanDokumen;
use common\models\perusahaan\Perusahaan;
use common\models\perusahaan\PerusahaanVerifikasi;
use common\models\ActivityLog;

class PerusahaanVerifikasiController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['verifikasi'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionVerifikasi($perusahaan_id)
    {
        $model = new PerusahaanVerifikasi();
        $decoded_id = base64_decode($perusahaan_id) / 2;
        $verifSebelumnya = PerusahaanVerifikasi::find()->orderBy(['id' => SORT_DESC])->where(['is_delete' => 0, 'perusahaan_id' => $decoded_id])->one();
        $dokumen = PerusahaanDokumen::find()->where(['perusahaan_id' => $decoded_id, 'is_delete' => 0])->all();
        $perusahaan = Perusahaan::findOne($decoded_id);

        if ($model->load(Yii::$app->request->post())) {
            //jika status nya ditolak maka, set status perusahaan menjadi ditolak, begitupun sebaliknya
            $perusahaan->is_verified = $model->status_verifikasi;
            $model->diverifikasi_oleh = Yii::$app->user->identity->id;
            $model->tgl_verifikasi = new \yii\db\Expression('NOW()');
            if ($model->save()) {
                $perusahaan->save();
                if ($model->status_verifikasi == 2) {
                    $this->sendEmailApprove($perusahaan);
                } else {
                    $this->sendEmailNotApprove($perusahaan);
                }
                Yii::$app->session->setFlash('success', 'Dokumen Berhasil Ditindaklanjuti');
                return $this->redirect(['perusahaan/view', 'id' => base64_encode($decoded_id * 2)]);
            }
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'perusahaan' => $perusahaan,
                'verifSebelumnya' => $verifSebelumnya,
            ]);
        }
    }

    protected function sendEmailApprove($perusahaan)
    {
        $perusahaanVerifikasi = PerusahaanVerifikasi::find()->orderBy(['id' => SORT_DESC])->where(['perusahaan_id' => $perusahaan->id])->one();
        return Yii::$app->mailer->compose(
            [
                'html' => 'emailApproval-html'
            ],
            [
                'perusahaan' => $perusahaan,
                'perusahaanVerifikasi' => $perusahaanVerifikasi,
            ]
        )
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name . ' bot'])
            ->setTo($perusahaan->email)
            ->setSubject('Legalitas Perusahaan Diterima')
            ->send();
    }

    protected function sendEmailNotApprove($perusahaan)
    {
        $perusahaanVerifikasi = PerusahaanVerifikasi::find()->orderBy(['id' => SORT_DESC])->where(['perusahaan_id' => $perusahaan->id])->one();
        return Yii::$app->mailer->compose(
            [
                'html' => 'emailApprovalTolak-html'
            ],
            [
                'perusahaan' => $perusahaan,
                'perusahaanVerifikasi' => $perusahaanVerifikasi,
            ]
        )
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name . ' bot'])
            ->setTo($perusahaan->email)
            ->setSubject('Legalitas Perusahaan Dikembalikan')
            ->send();
    }
}
