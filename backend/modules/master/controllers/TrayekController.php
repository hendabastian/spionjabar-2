<?php

namespace backend\modules\master\controllers;

use common\models\master\JenisAngkutan;
use Yii;
use common\models\master\Trayek;
use backend\modules\master\models\TrayekSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrayekController implements the CRUD actions for Trayek model.
 */
class TrayekController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Trayek models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrayekSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Trayek model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Trayek model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Trayek();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Trayek model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Trayek model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();

        $model = $this->findModel($id);
        $model->is_delete = 1;

        if ($model->save()) {
            return $this->redirect(['index']);
        }
    }

    public function actionImportData()
    {
        $json = file_get_contents(Yii::getAlias('@root/trayek.json'));
        $encodedJson = Json::decode($json);
//        return var_dump($encodedJson);
        foreach ($encodedJson as $index => $data) {
            $kuota1[$index] = isset($data['BUS_10_16']) ? $data['BUS_10_16'] : 0;
            $kuota2[$index] = isset($data['BUS_17_24']) ? $data['BUS_17_24'] : 0;
            $kuota3[$index] = isset($data['BUS_UP_24']) ? $data['BUS_UP_24'] : 0;
            $trayek[$index] = new Trayek();
            $trayek[$index]->kode_trayek = $data['KODE'];
            $trayek[$index]->nama_trayek = $data['TRAYEK'];
            $trayek[$index]->jenis_angkutan_id = JenisAngkutan::AKDP;
            $trayek[$index]->kuota = 0;
            $trayek[$index]->keterangan = $data['KETERANGAN'];
            $trayek[$index]->insert();
        }
        return $this->redirect('index');
    }

    /**
     * Finds the Trayek model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Trayek the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Trayek::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
