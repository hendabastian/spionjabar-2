<?php

namespace backend\modules\master\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\master\Terminal;

/**
 * TerminalSearch represents the model behind the search form of `common\models\master\Terminal`.
 */
class TerminalSearch extends Terminal
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tipe_terminal_id', 'kabupaten_id', 'is_active', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['kode_terminal', 'nama_terminal', 'alamat', 'latitude', 'longitude', 'kepala_terminal', 'no_tlp', 'keterangan', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Terminal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tipe_terminal_id' => $this->tipe_terminal_id,
            'kabupaten_id' => $this->kabupaten_id,
            'is_active' => $this->is_active,
            'is_delete' => $this->is_delete,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'kode_terminal', $this->kode_terminal])
            ->andFilterWhere(['like', 'nama_terminal', $this->nama_terminal])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'latitude', $this->latitude])
            ->andFilterWhere(['like', 'longitude', $this->longitude])
            ->andFilterWhere(['like', 'kepala_terminal', $this->kepala_terminal])
            ->andFilterWhere(['like', 'no_tlp', $this->no_tlp])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
