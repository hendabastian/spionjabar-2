<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\master\JenisBadanUsaha */

$this->title = 'Update Jenis Badan Usaha: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Jenis Badan Usahas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jenis-badan-usaha-update">

  <div class="x_panel">
    <div class="x_title">
      <h2><?= Html::encode($this->title) ?></h2>
      <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
      </ul>

      <div class="clearfix"></div>

    </div>

    <div class="x_content">

      <?= $this->render('_form', [
          'model' => $model,
      ]) ?>

    </div>
  </div>

</div>
