<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\master\models\TerminalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Terminals';
$this->params['breadcrumbs'][] = $this->title;
?>


      <div class="terminal-index">

        <div class="x_panel">
          <div class="x_title">
            <h2><?= Html::encode($this->title) ?></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>

            <div class="clearfix"></div>

          </div>

          <div class="x_content">

                <?php Pjax::begin(); ?>
                            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            
                <p class="text-muted font-13 m-b-30">
                    <?= Html::a('Tambah Terminal', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
          <div class="table-responsive">
                            <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                                  //'id',
            'tipe_terminal_id',
            'kode_terminal',
            'nama_terminal',
            'alamat:ntext',
            'kabupaten_id',
            'latitude',
            'longitude',
            'kepala_terminal',
            'no_tlp',
            'keterangan:ntext',
            //'is_active',
            //'is_delete',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',

                        ['class' => 'yii\grid\ActionColumn', 'header' => 'Aksi', 'contentOptions' =>['style' =>'width:80px; text-align:center;']],
                    ],
                ]); ?>
                      </div>
                <?php Pjax::end(); ?>

          </div>
        </div>

      </div>
