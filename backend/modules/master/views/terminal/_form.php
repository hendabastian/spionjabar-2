<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\master\Terminal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="terminal-form">

    <?php $form = ActiveForm::begin([
      'layout'=>'horizontal',
      'fieldConfig' => [
          'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
          'horizontalCssClasses' => [
              'label' => 'col-sm-3',
              'offset' => '',
              'wrapper' => 'col-sm-9',
              'error' => '',
              'hint' => '',
          ],
      ],
    ]); ?>

        <?= $form->field($model, 'tipe_terminal_id')->widget(Select2::class, [
          'data' => [1 => 'Terminal Tipe A', 2 => 'Tipe Terminal B']
        ]) ?>

    <?= $form->field($model, 'kode_terminal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_terminal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'kabupaten_id')->textInput() ?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kepala_terminal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_tlp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

            
            <div class="form-group">
              <div class="col-md-offset-3 col-md-9">
                      <?=  $form->field($model, 'is_active')->checkbox() ?>
              </div>
            </div>

          


    <div class="form-group">
      <div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('<i class="fa fa-remove"></i> Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
