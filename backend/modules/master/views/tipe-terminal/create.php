<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\master\TipeTerminal */

$this->title = 'Tambah Tipe Terminal';
$this->params['breadcrumbs'][] = ['label' => 'Tipe Terminals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipe-terminal-create">

  <div class="x_panel">
    <div class="x_title">
      <h2><?= Html::encode($this->title) ?></h2>
      <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
      </ul>

      <div class="clearfix"></div>

    </div>

    <div class="x_content">

      <?= $this->render('_form', [
          'model' => $model,
      ]) ?>

    </div>
  </div>

</div>
