<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\master\JenisLayanan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jenis-layanan-form">

    <?php $form = ActiveForm::begin([
      'layout'=>'horizontal',
      'fieldConfig' => [
          'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
          'horizontalCssClasses' => [
              'label' => 'col-sm-3',
              'offset' => '',
              'wrapper' => 'col-sm-9',
              'error' => '',
              'hint' => '',
          ],
      ],
    ]); ?>

        <?= $form->field($model, 'jenis_layanan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

            
            <div class="form-group">
              <div class="col-md-offset-3 col-md-9">
                      <?=  $form->field($model, 'is_active')->checkbox() ?>
              </div>
            </div>

          


    <div class="form-group">
      <div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('<i class="fa fa-remove"></i> Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
