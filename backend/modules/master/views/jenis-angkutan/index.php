<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\master\models\JenisAngkutanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jenis Angkutans';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="jenis-angkutan-index">

  <div class="x_panel">
    <div class="x_title">
      <h2><?= Html::encode($this->title) ?></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
      </ul>

      <div class="clearfix"></div>

    </div>

    <div class="x_content">

      <?php // echo $this->render('_search', ['model' => $searchModel]); 
      ?>

      <p class="text-muted font-13 m-b-30">
        <?= Html::a('Tambah Jenis Angkutan', ['create'], ['class' => 'btn btn-success']) ?>
      </p>
      <div class="table-responsive">
        <?= GridView::widget([
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'jenisLayanan.jenis_layanan',
            'jenis_angkutan',
            'keterangan:ntext',
            //'is_active',
            //'is_delete',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn', 'header' => 'Aksi', 'contentOptions' => ['style' => 'width:80px; text-align:center;']],
          ],
        ]); ?>
      </div>

    </div>
  </div>

</div>