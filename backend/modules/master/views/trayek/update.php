<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\master\Trayek */

$this->title = 'Update Trayek: ' . $model->nama_trayek;
$this->params['breadcrumbs'][] = ['label' => 'Trayeks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trayek-update">

    <div class="card">
        <div class="card-header">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
