<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\master\Trayek */

$this->title = 'Tambah Trayek';
$this->params['breadcrumbs'][] = ['label' => 'Trayek', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trayek-create">

    <div class="card">
        <div class="card-header">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
