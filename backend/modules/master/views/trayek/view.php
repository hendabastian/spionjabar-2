<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\master\Trayek */

$this->title = $model->nama_trayek;
$this->params['breadcrumbs'][] = ['label' => 'Trayek', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trayek-view">

    <div class="card">
        <div class="card-header">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="card-body">
            <?= Html::a('Daftar', ['index', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    'kode_trayek',
                    'nama_trayek',
                    'terminal_asal',
                    'terminal_tujuan',
                    'rute_trayek:ntext',
                    'jarak_rute',
                    'kuota',
                    'jenis_angkutan_id',
                    'keterangan:ntext',
                    //'is_active',
                    //'is_delete',
                    //'created_by',
                    //'updated_by',
                    //'created_at',
                    //'updated_at',
                ],
            ]) ?>
        </div>
    </div>
</div>
