<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\master\models\TrayekSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trayek';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="trayek-index">

    <div class="card">
        <div class="card-header">
            <h2><?= Html::encode($this->title) ?></h2>
            <?= Html::a('Tambah Trayek', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="card-body">
            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <div class="table-responsive">
                <?= GridView::widget([
                    'responsive' => false,
                    'bootstrap' => true,
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        'kode_trayek',
                        'nama_trayek',
                        'terminalAsal.nama_terminal',
                        'terminalTujuan.nama_terminal',
                        'rute_trayek:ntext',
                        'jarak_rute',
                        'kuota',
                        'jenisAngkutan.jenis_angkutan',
                        'keterangan:ntext',
                        //'is_active',
                        //'is_delete',
                        //'created_by',
                        //'updated_by',
                        //'created_at',
                        //'updated_at',

                        ['class' => 'kartik\grid\ActionColumn', 'header' => 'Aksi', 'contentOptions' => ['style' => 'width:80px; text-align:center;']],
                    ],
                ]); ?>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>

</div>
