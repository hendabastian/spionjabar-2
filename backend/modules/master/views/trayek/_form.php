<?php

use common\models\master\Terminal;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\master\Trayek */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trayek-form">
    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => '',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'kode_trayek')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_trayek')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'terminal_asal')->widget(Select2::class, [
        'data' => ArrayHelper::map(Terminal::find()->where(['is_delete' => 0])->all(), 'id', 'nama_terminal'),
        'options' => ['placeholder' => 'Pilih Terminal Asal']
    ]) ?>

    <?= $form->field($model, 'terminal_tujuan')->widget(Select2::class, [
        'data' => ArrayHelper::map(Terminal::find()->where(['is_delete' => 0])->all(), 'id', 'nama_terminal'),
        'options' => ['placeholder' => 'Pilih Terminal Asal']
    ]) ?>

    <?= $form->field($model, 'rute_trayek')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'jarak_rute')->textInput() ?>

    <?= $form->field($model, 'kuota')->textInput() ?>

    <?= $form->field($model, 'jenis_angkutan_id')->widget(Select2::class, [
        'data' => ArrayHelper::map(\common\models\master\JenisAngkutan::find()->where(['is_delete' => 0, 'is_active' => 1])->all(), 'id', 'jenis_angkutan'),
        'options' => ['placeholder' => 'Jenis Angkutan']
    ]) ?>

    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>


    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= $form->field($model, 'is_active')->checkbox() ?>
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a('<i class="fa fa-remove"></i> Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
