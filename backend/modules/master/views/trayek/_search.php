<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\master\models\TrayekSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trayek-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kode_trayek') ?>

    <?= $form->field($model, 'nama_trayek') ?>

    <?= $form->field($model, 'terminal_asal') ?>

    <?= $form->field($model, 'terminal_tujuan') ?>

    <?php // echo $form->field($model, 'rute_trayek') ?>

    <?php // echo $form->field($model, 'jarak_rute') ?>

    <?php // echo $form->field($model, 'kuota') ?>

    <?php // echo $form->field($model, 'jenis_angkutan_id') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <?php // echo $form->field($model, 'is_delete') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
