<?php

use common\models\master\JenisAngkutan;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\master\JenisPermohonan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jenis-permohonan-form">

  <?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'fieldConfig' => [
      'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
      'horizontalCssClasses' => [
        'label' => 'col-sm-3',
        'offset' => '',
        'wrapper' => 'col-sm-9',
        'error' => '',
        'hint' => '',
      ],
    ],
  ]); ?>

  <?= $form->field($model, 'jenis_angkutan_id')->widget(Select2::class, [
    'data' => ArrayHelper::map(JenisAngkutan::find()->where(['is_delete' => 0, 'is_active' => 1])->all(), 'id', 'jenis_angkutan')
  ]) ?>

  <?= $form->field($model, 'jenis_permohonan')->textInput(['maxlength' => true]) ?>


  <div class="form-group">
    <div class="col-md-offset-3 col-md-9">
      <?= $form->field($model, 'is_active')->checkbox() ?>
    </div>
  </div>




  <div class="form-group">
    <div class="col-md-offset-3 col-md-9">
      <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
      <?= Html::a('<i class="fa fa-remove"></i> Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
    </div>
  </div>

  <?php ActiveForm::end(); ?>

</div>