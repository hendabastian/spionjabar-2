<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\master\TipePermohonan */

$this->title = 'Update Tipe Permohonan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tipe Permohonans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tipe-permohonan-update">

  <div class="x_panel">
    <div class="x_title">
      <h2><?= Html::encode($this->title) ?></h2>
      <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
      </ul>

      <div class="clearfix"></div>

    </div>

    <div class="x_content">

      <?= $this->render('_form', [
          'model' => $model,
      ]) ?>

    </div>
  </div>

</div>
