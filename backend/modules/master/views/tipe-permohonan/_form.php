<?php

use common\models\master\JenisPermohonan;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\master\TipePermohonan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipe-permohonan-form">

  <?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'fieldConfig' => [
      'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
      'horizontalCssClasses' => [
        'label' => 'col-sm-3',
        'offset' => '',
        'wrapper' => 'col-sm-9',
        'error' => '',
        'hint' => '',
      ],
    ],
  ]); ?>

  <?= $form->field($model, 'jenis_permohonan_id')->widget(Select2::class, [
    'data' => ArrayHelper::map(JenisPermohonan::find()->where(['is_delete' => 0, 'is_active' => 1])->all(), 'id', function($data) {
      return $data->jenisAngkutan->jenis_angkutan . ' - ' . $data->jenis_permohonan;
    })
  ]) ?>

  <?= $form->field($model, 'tipe_permohonan')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>


  <div class="form-group">
    <div class="col-md-offset-3 col-md-9">
      <?= $form->field($model, 'is_active')->checkbox() ?>
    </div>
  </div>




  <div class="form-group">
    <div class="col-md-offset-3 col-md-9">
      <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
      <?= Html::a('<i class="fa fa-remove"></i> Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
    </div>
  </div>

  <?php ActiveForm::end(); ?>

</div>