<?php

namespace backend\modules\permohonan\controllers;

use common\models\permohonan\PermohonanAdvis;
use common\models\permohonan\PermohonanAdvisEmail;
use common\models\master\TipePermohonan;
use common\models\master\Trayek;
use common\models\permohonan\KendaraanBbn;
use common\models\permohonan\KendaraanInfoPenambahan;
use common\models\permohonan\KendaraanInfoRealisasi;
use common\models\permohonan\KendaraanPeremajaan;
use common\models\permohonan\KendaraanPindahTrayek;
use common\models\permohonan\PermohonanApproved;
use common\models\permohonan\Undangan;
use common\models\UserRole;
use kartik\mpdf\Pdf;
use Yii;
use common\models\permohonan\Permohonan;
use common\models\permohonan\PermohonanVerifikasi;
use backend\modules\permohonan\models\PermohonanSearch;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * PermohonanController implements the CRUD actions for Permohonan model.
 */
class PermohonanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Permohonan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PermohonanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->andFilterWhere(['jenis_angkutan_id' => Yii::$app->request->get('jenis_angkutan_id')]);
        switch (Yii::$app->user->identity->userRole->id) {
            case UserRole::VERIFIKATOR:
                $dataProvider->query->andFilterWhere(['status_verifikasi' => [
                    Permohonan::STATUS_PERUSAHAAN_SUBMITTED,
                    Permohonan::STATUS_ADVIS_SUBMITTED
                ]]);
                break;
            case UserRole::KEPALA_SEKSI:
                $dataProvider->query->andFilterWhere(['status_verifikasi' => Permohonan::STATUS_VERIFIKATOR_APPROVED]);
                break;
            case UserRole::KEPALA_BIDANG:
                $dataProvider->query->andFilterWhere(['status_verifikasi' => Permohonan::STATUS_KASI_APPROVED]);
                break;
            case UserRole::KEPALA_DINAS:
                $dataProvider->query->andFilterWhere(['status_verifikasi' => Permohonan::STATUS_KABID_APPROVED]);
                break;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Permohonan model.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        /**
         * Code dibawah untuk mendapatkan data kendaraan berdasarkan tipe permohonan dengan kondisi sbb:
         * @property int [3, 9] = Permohonan Info Realisasi
         * @property int [4, 10, 14, 17, 20, 22] = Permohonan Peremajaan
         * @property int [5, 11, 15, 18] = Permohonan BBN
         * @property int [6,12] = Permohonan Pindah Trayek
         * @property int [19, 21] = Permohonan Info Penambahan
         */
        if (in_array($model->tipe_permohonan_id, [3, 9, 13])) { // Permohonan Info Realisasi
            $modelKendaraan = KendaraanInfoRealisasi::find()->where(['permohonan_id' => $model->id]);
            $kendaraanDataProvider = new ActiveDataProvider([
                'query' => $modelKendaraan,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);
        } elseif (in_array($model->tipe_permohonan_id, [4, 10, 14, 17, 20, 22, 24])) { // Permohonan Peremajaan
            $modelKendaraan = KendaraanPeremajaan::find()->where(['permohonan_id' => $model->id]);
            $kendaraanDataProvider = new ActiveDataProvider([
                'query' => $modelKendaraan,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);
        } elseif (in_array($model->tipe_permohonan_id, [5, 11, 15, 18])) { // Permohonan BBN
            $modelKendaraan = KendaraanBbn::find()->where(['permohonan_id' => $model->id]);
            $kendaraanDataProvider = new ActiveDataProvider([
                'query' => $modelKendaraan,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);
        } elseif (in_array($model->tipe_permohonan_id, [6, 12])) { // Permohonan Pindah Trayek
            $modelKendaraan = KendaraanPindahTrayek::find()->where(['permohonan_id' => $model->id]);
            $kendaraanDataProvider = new ActiveDataProvider([
                'query' => $modelKendaraan,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);
        } elseif (in_array($model->tipe_permohonan_id, [19, 21, 23])) { // Permohonan Info Penambahan
            $modelKendaraan = KendaraanInfoPenambahan::find()->where(['permohonan_id' => $model->id]);
            $kendaraanDataProvider = new ActiveDataProvider([
                'query' => $modelKendaraan,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC
                    ]
                ],
            ]);
        } else {
            $modelKendaraan = null;
        }
        return $this->render('view', [
            'model' => $model,
            'kendaraanDataProvider' => isset($kendaraanDataProvider) ? $kendaraanDataProvider : null,
        ]);
    }

    /**
     * Deletes an existing Permohonan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();

        $model = $this->findModel($id);
        $model->is_delete = 1;

        if ($model->save()) {
            return $this->redirect(['index', 'jenis_angkutan_id' => $model->jenis_angkutan_id]);
        }
    }

    public function actionPreviewDok($id, $filename)
    {
        $model = $this->findModel($id);
        return $this->renderAjax('preview_dok', [
            'model' => $model,
            'filename' => $filename
        ]);
    }


    public function actionAjukanVerifikasi($permohonan_id)
    {
        $model = new PermohonanVerifikasi();
        $advis = new PermohonanAdvis();
        $email = new PermohonanAdvisEmail();
        $modelPermohonan = $this->findModel($permohonan_id);

        if ($model->load(Yii::$app->request->post())) {
            $model->permohonan_id = $permohonan_id;
            $model->tgl_verifikasi = new \yii\db\Expression('NOW()');
            $model->diverifikasi_oleh = Yii::$app->user->identity->id;
            $model->save();
            $modelPermohonan->status_verifikasi = $model->status_verifikasi;
            $modelPermohonan->save();

            if ($advis->load(Yii::$app->request->post())) {
                $dokAdvis = UploadedFile::getInstance($advis, 'file_advis');
                $directory = Yii::getAlias('@uploads/perusahaan/' . $modelPermohonan->perusahaan->id . '/advis/');

                if (!is_dir($directory)) {
                    FileHelper::createDirectory($directory);
                }

                $uid = uniqid(time(), true);

                if (!empty($dokAdvis)) {
                    $fileNameAdvis = $uid . '_advis' . $dokAdvis->extension;
                    $filePathAdvis = $directory . $fileNameAdvis;

                    $dokAdvis->saveAs($filePathAdvis);
                    $advis->file_advis = $fileNameAdvis;
                }
            }

            $advis->save();

            Yii::$app->session->setFlash('success', 'Permohonan Berhasil Diverifikasi');
            return $this->redirect(['view', 'id' => $permohonan_id]);
        }
        return $this->renderAjax('_form_ajukan_verifikasi', [
            'permohonan_id' => $permohonan_id,
            'model' => $model,
            'advis' => $advis,
            'email' => $email,
            'modelPermohonan' => $modelPermohonan
        ]);
    }

    public function actionGetTipePermohonan()
    {
        if (Yii::$app->request->isPost) {
            $tipePermohonan = TipePermohonan::find()->where(['jenis_permohonan_id' => Yii::$app->request->post('jenis_permohonan_id')])->all();
            return \yii\helpers\Json::encode($tipePermohonan);
        }
    }

    public function actionSendAdvis($permohonan_id)
    {
        $model = new PermohonanVerifikasi();
        $permohonan = $this->findModel($permohonan_id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->status_verifikasi = Permohonan::STATUS_ADVIS_SUBMITTED) {
                foreach ($permohonan->advis->advisEmail as $index => $sendMail) {
                    Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => 'emailAdvis-html'],
                            [
                                'perusahaan' => $permohonan->perusahaan,
                                'sendMail' => $sendMail,
                                'permohonan' => $permohonan
                            ]
                        )
                        ->setFrom('no-reply@spionjabar.test')
                        ->setTo($sendMail['email'])
                        ->setSubject('Permohonan Advis untuk Perusahaan ' . $permohonan->perusahaan->nama_perusahaan)
                        ->send();
                }
                $permohonan->status_verifikasi = $model->status_verifikasi;
                $permohonan->save();
                $model->permohonan_id = $permohonan_id;
                $model->tgl_verifikasi = new \yii\db\Expression('NOW()');
                $model->diverifikasi_oleh = Yii::$app->user->identity->id;
                $model->save();
                Yii::$app->session->setFlash('success', 'Permohonan Berhasil Disetujui & Permintaan Advis Telah Terkirim');
            } else {
                Yii::$app->session->setFlash('success', 'Permohonan Telah Ditolak');
            }
            return $this->redirect(['view', 'id' => $permohonan_id]);

        }
        return $this->renderAjax('_form_kirim_advis', [
            'permohonan_id' => $permohonan_id,
            'model' => $model,
            'permohonan' => $permohonan
        ]);
    }

    public function actionInputAdvis($permohonan_id)
    {
        $permohonan = $this->findModel($permohonan_id);
        $model = PermohonanAdvis::find()->where(['permohonan_id' => $permohonan_id])->one();

        if ($model->load(Yii::$app->request->post())) {
            $dokAdvis = UploadedFile::getInstance($model, 'file_advis');
            $directory = Yii::getAlias('@uploads/perusahaan/' . $permohonan->perusahaan->id . '/advis/');

            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            $uid = uniqid(time(), true);

            if (!empty($dokAdvis)) {
                $fileNameAdvis = $uid . '_advis' . $dokAdvis->extension;
                $filePathAdvis = $directory . $fileNameAdvis;

                $dokAdvis->saveAs($filePathAdvis);
                $model->file_advis = $fileNameAdvis;
            }

            if ($model->save()) {
                $permohonan->status_verifikasi = Permohonan::STATUS_ADVIS_APPROVED;
                $permohonan->save();
                Yii::$app->session->setFlash('success', 'Data Advis berhasil diinput');
                return $this->redirect(['view', 'id' => $permohonan_id]);
            } else {
                Yii::$app->session->setFlash('danger', $model->getErrorSummary(true));
                return $this->redirect(['view', 'id' => $permohonan_id]);
            }
        }

        return $this->renderAjax('_form_input_advis', [
            'permohonan' => $permohonan,
            'model' => $model
        ]);
    }

    public function actionApprovePermohonan($id)
    {
        $permohonan = $this->findModel($id);
        $model = new PermohonanApproved();
        if ($model->load(Yii::$app->request->post())) {
            $model->perusahaan_id = $permohonan->perusahaan_id;
            $model->permohonan_id = $id;
            $model->jenis_permohonan = $permohonan->tipe_permohonan_id;
            $model->tgl_cetak = new Expression('NOW()');
            $model->tgl_expire = date('Y-m-d', strtotime('+6 months'));
            $permohonan->status_verifikasi = Permohonan::STATUS_KADIS_APPROVED;
            $permohonan->save();
            $model->save();
            Yii::$app->session->setFlash('success', 'Permohonan Berhasil Disetujui');
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->renderAjax('_form_approve', [
            'permohonan' => $permohonan,
            'model' => $model
        ]);
    }

    public function actionSuratPermohonan($id)
    {
        $model = PermohonanApproved::findOne(['permohonan_id' => $id]);
        switch ($model->permohonan->tipe_permohonan_id) {
            case TipePermohonan::AKDP_IJIN_PRINSIP_BARU:
                $content = $this->renderPartial('akdp/_dok_ip', [
                    'model' => $model
                ]);
                break;
        }

        $pdf = new Pdf([
            'mode' => PDF::MODE_CORE,
            'orientation' => PDF::ORIENT_PORTRAIT,
            'format' => PDF::FORMAT_A4,
            'filename' => $model->no_surat . '.pdf',
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'destination' => PDF::DEST_DOWNLOAD,
            'content' => $content,
        ]);

        return $pdf->render();

    }

    public function actionInputUndangan($id)
    {
        $surat = new Undangan();
        if ($surat->load(Yii::$app->request->post())) {
            $role = Yii::$app->user->identity->user_role_id;
            $model = $this->findModel($id);
            $directory = Yii::getAlias('@uploads/perusahaan/' . $model->perusahaan_id . '/undangan/');
            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            $uid = uniqid(time(), true);
            $filenameDok = $uid . '_undangan' . '.pdf';
            $filePathDok = $directory . $filenameDok;

            $content = $this->renderPartial('_undangan', [
                'surat' => $surat,
                'model' => $model,
            ]);

            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_FILE,
                'filename' => $filePathDok,
                'content' => $content,
            ]);

            $perusahaan = $model->perusahaan;
            $pdf->render();
            $model->save();
            $surat->save();

            $this->sendUndangan($perusahaan, $filePathDok);

            Yii::$app->session->setFlash('success', 'Perusahaan telah diundang');
            return $this->redirect([
                'view',
                'id' => $model->id,
            ]);
        }

        return $this->renderAjax('_form_input_undangan', [
            'model' => $surat,
            'ijinPrinsipId' => $id,
        ]);
    }

    protected function sendUndangan($perusahaan, $filePathDok)
    {
        return Yii::$app->mailer->compose(
            ['html' => 'emailUndangan-html', 'text' => 'emailUndangan-text'],
            [
                'perusahaan' => $perusahaan,
            ]
        )
            ->attach($filePathDok)
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name . ' bot'])
            ->setTo($perusahaan->email)
            ->setSubject('Undangan Presentasi - DISHUB JABAR')
            ->send();
    }

    public function actionUploadBuktiRapat($id)
    {
        $model = $this->findModel($id);
        $surat = Undangan::findOne($model->undangan->id);
        if ($surat->load(Yii::$app->request->post())) {
            $dokBuktiRapat = UploadedFile::getInstance($surat, 'bukti_rapat');
            $directory = Yii::getAlias('@uploads/perusahaan/' . $model->perusahaan_id . '/undangan/');
            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            $uid = uniqid(time(), true);
            $filenameDok = $uid . '_bukti_rapat.' . $dokBuktiRapat->extension;
            $filePathDok = $directory . $filenameDok;

            if ($dokBuktiRapat->saveAs($filePathDok)) {
                $surat->bukti_rapat = $filenameDok;
                $surat->save();
            }

            Yii::$app->session->setFlash('success', 'Bukti rapat telah diupload');
            return $this->redirect([
                'view',
                'id' => $model->id,
            ]);
        }

        return $this->renderAjax('_form_upload_bukti_rapat', [
            'model' => $surat,
            'ijinPrinsipId' => $id,
        ]);
    }

    public function actionPreviewDokPath($id, $path, $filename)
    {
        $model = $this->findModel($id);
        return $this->renderAjax('preview_dok_path', [
            'model' => $model,
            'path' => $path,
            'filename' => $filename
        ]);
    }

    public function actionPreviewDokUrl($url)
    {
        return $this->renderAjax('preview_dok_url', [
            'url' => $url,
        ]);
    }

    /**
     * Finds the Permohonan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Permohonan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Permohonan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
