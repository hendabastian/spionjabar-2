<?php

namespace backend\modules\permohonan;

/**
 * Permohonan module definition class
 */
class Permohonan extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\permohonan\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
