<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;
?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        //'id',
        'tipePermohonan.tipe_permohonan',
        'no_surat_permohonan',
        'tgl_surat_permohonan',
        [
            'attribute' => 'file_surat_permohonan',
            'format' => 'RAW',
            'value' => function ($data) {
                return Html::button('<i class="fa fa-search"></i> Preview', ['value' => Url::toRoute(['preview-dok', 'id' => $data->id, 'filename' => $data->file_surat_permohonan]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-xs showModalButton',]);
            }
        ],
        [
            'attribute' => 'proposal_pengoperasian_angkutan',
            'format' => 'RAW',
            'value' => function ($data) {
                return Html::button('<i class="fa fa-search"></i> Preview', ['value' => Url::toRoute(['preview-dok', 'id' => $data->id, 'filename' => $data->proposal_pengoperasian_angkutan]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-xs showModalButton',]);
            }
        ],
        [
            'attribute' => 'file_pernyataan_kesanggupan_kendaraan',
            'format' => 'RAW',
            'value' => function ($data) {
                return Html::button('<i class="fa fa-search"></i> Preview', ['value' => Url::toRoute(['preview-dok', 'id' => $data->id, 'filename' => $data->file_pernyataan_kesanggupan_kendaraan]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-xs showModalButton',]);
            }
        ],
        [
            'attribute' => 'file_pernyataan_kesanggupan_pool',
            'format' => 'RAW',
            'value' => function ($data) {
                return Html::button('<i class="fa fa-search"></i> Preview', ['value' => Url::toRoute(['preview-dok', 'id' => $data->id, 'filename' => $data->file_pernyataan_kesanggupan_pool]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-xs showModalButton',]);
            }
        ],
        [
            'attribute' => 'dok_pernyataan',
            'format' => 'RAW',
            'value' => function ($data) {
                return Html::button('<i class="fa fa-search"></i> Preview', ['value' => Url::toRoute(['preview-dok', 'id' => $data->id, 'filename' => $data->dok_pernyataan]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-xs showModalButton',]);
            }
        ],
        'jenisKendaraan.jenis_kendaraan',
        // 'trayek.nama_trayek',
                [
            'attribute' => 'kota_asal_id',
            'value' => function ($data) {
                return $data->kotaAsal->nama;
            }
        ],
        [
            'attribute' => 'kota_tujuan_id',
            'value' => function ($data) {
                return $data->kotaTujuan->nama;
            }
        ],
        'jumlah_kendaraan',
        //'is_delete',
        //'created_by',
        //'updated_by',
        //'created_at',
        //'updated_at',
    ],
]) ?>

