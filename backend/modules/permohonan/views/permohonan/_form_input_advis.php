<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\permohonan\PermohonanAdvis */
/* @var $form ActiveForm */
?>
<div class="input_advis">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4 text-black',
                'offset' => 'offset-sm-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'permohonan_id')->hiddenInput(['value' => $permohonan->id])->label(false) ?>
    <?= $form->field($model, 'no_advis') ?>
    <?= $form->field($model, 'file_advis')->fileInput() ?>
    <?= $form->field($model, 'tgl_advis')->widget(DatePicker::class, [
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true
        ]
    ]) ?>
    <?= $form->field($model, 'kendaraan_disetujui')->textInput(['type' => 'number', 'max' => $permohonan->jumlah_kendaraan])->hint('Total Kendaraan yang diajukan: ' . $permohonan->jumlah_kendaraan) ?>


    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>