<?php

use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\perijinan\IjinPersetujuanPenambahanKendaraan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cetak-undangan-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => '',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>
    <div style="display: none;">
        <?= /** @var integer $ijinPrinsipId */
        $form->field($model, 'permohonan_id')->hiddenInput(['value' => $ijinPrinsipId]) ?>
    </div>

    <?= $form->field($model, 'no_surat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sifat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lampiran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hal')->textInput(['maxlength' => true, 'value' => 'Undangan']) ?>

    <?= $form->field($model, 'waktu_undangan')->widget(\kartik\datetime\DateTimePicker::class, [
        'options' => ['placeholder' => 'Masukkan tanggal ...'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]);
    ?>
    <?= $form->field($model, 'acara')->textInput(['value' => 'Undangan Presentasi Kepengusahaan']) ?>

    <?= $form->field($model, 'tempat')->textInput(['value' => 'Dinas Perhubungan Provinsi Jawa Barat Jl. Sukabumi No.1 Bandung']) ?>


    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
            <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal', 'id' => 'btn-close']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>