<?php

use yii2assets\pdfjs\PdfJs;
use yii2assets\pdfjs\PdfJsAsset;

?>

<?php
$fileInfo = pathinfo($path . $filename);

switch ($fileInfo['extension']) {
    case 'pdf':
        echo PdfJs::widget([
            'url' => $path . $filename,
            'buttons' => [
                'presentationMode' => false,
                'openFile' => false,
                'print' => false,
                'download' => true,
                'viewBookmark' => false,
                'secondaryToolbarToggle' => false
            ]
        ]);
        break;
    case 'mp4': ?>
        <video width="100%" height="100%" controls>
            <source src="<?= $path . $filename ?>" type="video/mp4">
            Your browser does not support the video tag.
        </video>
    <?php
        break;
}
?>