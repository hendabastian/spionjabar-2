<?php

use common\models\master\JenisAngkutan;
use common\models\master\TipePermohonan;
use common\models\perusahaan\Perusahaan;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\models\PermohonanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$jenisAngkutan = JenisAngkutan::findOne(Yii::$app->request->get('jenis_angkutan_id'));

$this->title = 'Permohonan - ' . $jenisAngkutan->jenis_angkutan;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="permohonan-index">

    <div class="card">
        <div class="card-header">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="card-body">
            <?php Pjax::begin(); ?>
            <div class="table" style="width: 100%;">
                <?= GridView::widget([
                    'bootstrap' => true,
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        [
                            'attribute' => 'perusahaan_id',
                            'value' => 'perusahaan.nama_perusahaan',
                            'filter' => \kartik\select2\Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'perusahaan_id',
                                'data' => ArrayHelper::map(Perusahaan::find()->all(), 'id', 'nama_perusahaan'),
                                'options' => ['placeholder' => 'Filter Perusahaan']
                            ]),
                            'contentOptions' => ['style' => 'width:150px;'],
                        ],
                        [
                            'attribute' => 'tipe_permohonan_id',
                            'value' => function ($data) {
                                return $data->tipePermohonan->tipe_permohonan;
                            },
                            'filter' => \kartik\select2\Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'tipe_permohonan_id',
                                'data' => ArrayHelper::map(TipePermohonan::find()->joinWith(['jenisPermohonan'])->where(['m_jenis_permohonan.jenis_angkutan_id' => Yii::$app->request->get('jenis_angkutan_id')])->all(), 'id', 'tipe_permohonan'),
                                'options' => ['placeholder' => 'Filter Jenis Permohonan']
                            ]),
                            'contentOptions' => ['style' => 'width:150px;'],
                        ],
                        'no_surat_permohonan',
                        'tgl_surat_permohonan',
                        [
                            'attribute' => 'file_surat_permohonan',
                            'format' => 'RAW',
                            'value' => function ($data) {
                                return Html::button('<i class="fa fa-search"></i> Preview', ['value' => Url::toRoute(['preview-dok', 'id' => $data->id, 'filename' => $data->file_surat_permohonan]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-sm showModalButton',]);
                            }
                        ],
                        // 'jumlah_kendaraan',
                        //'is_delete',
                        //'created_by',
                        //'updated_by',
                        //'created_at',
                        //'updated_at',

                        ['class' => 'kartik\grid\ActionColumn', 'header' => 'Aksi', 'template' => '{view}', 'contentOptions' => ['style' => 'width:80px; text-align:center;'], 'buttons' => [
                            'view' => function ($url, $model) {

                                return Html::a('<i class="fas fa-eye"></i>', $url, [
                                    'title' => Yii::t('app', 'view')
                                ]);
                            },
                        ]],
                    ],
                ]); ?>
            </div>
            <?php Pjax::end(); ?>

        </div>
    </div>

</div>

<?php
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'options' => ['tabindex' => false],
    'closeButton' => ['tag' => 'close', 'label' => 'x'],
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
]);
echo "<div id='modalContent' style='padding:2px;'></div>";
Modal::end();
?> 