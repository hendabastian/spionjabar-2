<?php

use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\perijinan\IjinPersetujuanPenambahanKendaraan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cetak-undangan-form">

    <?php $form = ActiveForm::begin([
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-6',
                'offset' => '',
                'wrapper' => 'col-sm-6',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>
    <div style="display: none;">
        <?= /** @var integer $ijinPrinsipId */
        $form->field($model, 'permohonan_id')->hiddenInput(['value' => $ijinPrinsipId]) ?>
    </div>

    <?= $form->field($model, 'bukti_rapat')->fileInput(['class' => 'form-control'])->hint('File dapat berupa video MP4 atau PDF') ?>


    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
            <?= Html::button(' <i class="glyphicon glyphicon-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal', 'id' => 'btn-close']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>