<?php

use common\models\master\Kabupaten;
use common\models\master\Terminal;
use common\models\master\Trayek;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

\frontend\assets\AppAsset::register($this);

$form = ActiveForm::begin([
    'action' => Url::toRoute('create'),
    'id' => 'form-permohonan',
    'options' => [
        'name' => 'form-permohonan'
    ],
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-3',
            'offset' => '',
            'wrapper' => 'col-sm-9',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

<?= $form->field($model, 'no_surat_permohonan')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'tgl_surat_permohonan')->widget(DatePicker::class, [
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true
    ]
]) ?>

<?= $form->field($model, 'file_surat_permohonan')->fileInput() ?>

<?= $form->field($model, 'no_surat_persetujuan') ?>

<?= $form->field($model, 'tgl_surat_persetujuan')->widget(DatePicker::class, [
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true
    ]
]) ?>

<?= $form->field($model, 'file_surat_persetujuan')->fileInput() ?>

<?= $form->field($model, 'trayek_id')->widget(Select2::class, [
    'data' => ArrayHelper::map(Trayek::find()->where(['is_delete' => 0, 'is_active' => 1, 'jenis_angkutan_id' => $jenisAngkutanId])->all(), 'id', 'nama_trayek'),
    'options' => ['placeholder' => '-Pilih Trayek-'],
    'pluginOptions' => ['allowClear' => true]
]) ?>

<?= $form->field($model, 'trayek_baru_id')->widget(Select2::class, [
    'data' => ArrayHelper::map(Trayek::find()->where(['is_delete' => 0, 'is_active' => 1, 'jenis_angkutan_id' => $jenisAngkutanId])->all(), 'id', 'nama_trayek'),
    'options' => ['placeholder' => '-Pilih Trayek Baru-'],
    'pluginOptions' => ['allowClear' => true]
]) ?>

<?= $form->field($model, 'kota_asal_id')->widget(Select2::class, [
    'data' => ArrayHelper::map(Kabupaten::find()->where(['is_delete' => 0, 'is_active' => 1])->all(), 'id', 'nama'),
    'options' => ['placeholder' => '-Pilih Kota Asal-'],
    'pluginOptions' => ['allowClear' => true]
]) ?>

<?= $form->field($model, 'kota_tujuan_id')->widget(Select2::class, [
    'data' => ArrayHelper::map(Kabupaten::find()->where(['is_delete' => 0, 'is_active' => 1])->all(), 'id', 'nama'),
    'options' => ['placeholder' => '-Pilih Kota Tujuan-'],
    'pluginOptions' => ['allowClear' => true]
]) ?>

    <div style="display:none;">
        <?= $form->field($model, 'tipe_permohonan_id')->hiddenInput(['value' => $tipePermohonanId])->label(false) ?>

        <?= $form->field($model, 'jenis_angkutan_id')->hiddenInput(['value' => $jenisAngkutanId])->label(false) ?>
    </div>

    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a('<i class="fa fa-remove"></i> Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>