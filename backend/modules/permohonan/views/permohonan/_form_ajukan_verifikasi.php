<?php

use common\models\UserRole;
use common\models\permohonan\Permohonan;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model common\models\permohonan\Permohonan
 * @var $form yii\bootstrap4\ActiveForm
 * @var int $permohonan_id
 */

$js = <<<JS
$(document).ready(function() {
    var maxFields = 15
    var wrapper = $('#field-wrap')
    var initFieldsCount = 1
    
    $('#tambah-penerima').click(function (e) {
        e.preventDefault();
        if (initFieldsCount < maxFields) {
            initFieldsCount++;
            console.log(initFieldsCount);
            $(wrapper).append('<div id="wrapper-' + initFieldsCount +'">'  +
             '<div class="form-group">' +
             '<label for="permohonanadvisemail-kepada" class="control-label col-sm-3">Kepada</label>' +
              '<div class="col-sm-9">' +
               '<input type="text" name="PermohonanAdvisEmail[kepada][]" id="kepada[]" class="form-control">' +
                '</div>' +
                 '</div>' +
                  '<div class="form-group">' +
                   '<label for="permohonanadvisemail-email" class="control-label col-sm-3">Alamat Email</label>' +
                    '<div class="col-sm-9">' +
                     '<input type="text" name="PermohonanAdvisEmail[email][]" id="email[]" class="form-control">' +
                      '</div>' +
                       '</div>')
        } else {
            alert('Penerima email tidak bisa lebih dari ' + maxFields);
        }
    })
    $('#kurangi-penerima').click(function(e) {
        $('#wrapper-'+initFieldsCount).remove();
        initFieldsCount--;
        console.log(initFieldsCount)
    })
})
JS;

//$this->registerJs($js);
?>

<div class="ijin-penyelenggaraan-verifikasi-form">

    <?php $form = ActiveForm::begin(['id' => 'ijin-penyelenggaraan-verifikasi']); ?>

    <?= $form->field($model, 'permohonan_id')->hiddenInput(['value' => $permohonan_id])->label(false) ?>
    <?php if (Yii::$app->user->identity->userRole->id == UserRole::VERIFIKATOR) {
        echo $form->field($model, 'status_verifikasi')->radioList([Permohonan::STATUS_VERIFIKATOR_APPROVED => 'Diteruskan', Permohonan::STATUS_VERIFIKATOR_REJECTED => 'Tidak dapat diteruskan']);
    } elseif (Yii::$app->user->identity->userRole->id == UserRole::KEPALA_SEKSI) { // KASI
        echo $form->field($model, 'status_verifikasi')->radioList([Permohonan::STATUS_KASI_APPROVED => 'Diteruskan', Permohonan::STATUS_KASI_REJECTED => 'Tidak Dapat Diteruskan']);
    } elseif (Yii::$app->user->identity->userRole->id == UserRole::KEPALA_BIDANG) { // KABID
        echo $form->field($model, 'status_verifikasi')->radioList([Permohonan::STATUS_KABID_APPROVED => 'Diteruskan', Permohonan::STATUS_KABID_REJECTED => 'Tidak Dapat Diteruskan']);
    } elseif (Yii::$app->user->identity->userRole->id == UserRole::KEPALA_DINAS) { //KADIS
        echo $form->field($model, 'status_verifikasi')->hiddenInput(['value' => Permohonan::STATUS_KADIS_REJECTED])->label(false);
    } ?>

    <?= $form->field($model, 'catatan_verifikasi')->textarea(['rows' => 3])->label('Catatan Verifikasi <i>* (Tidak wajib diisi)</i>') ?>

    <?php if (Yii::$app->user->identity->userRole->id == UserRole::VERIFIKATOR) : ?>


        <?= $form->field($model, 'diverifikasi_oleh')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>

        <hr>
        <h4>Input Tujuan Pengiriman Advis</h4>
        <?= $form->field($advis, 'permohonan_id')->hiddenInput(['value' => $permohonan_id])->label(false) ?>
        <?= $form->field($advis, 'no_advis') ?>
        <?= $form->field($advis, 'file_advis')->fileInput() ?>
        <?= $form->field($advis, 'tgl_advis')->widget(\kartik\date\DatePicker::class, [
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true
        ]
    ]) ?>
        <?= $form->field($advis, 'kendaraan_disetujui')->textInput(['type' => 'number', 'max' => $modelPermohonan->jumlah_kendaraan])->hint('Total Kendaraan yang diajukan: ' . $modelPermohonan->jumlah_kendaraan) ?>

<!--        <hr>-->
<!--        <div class="col-md-offset-3 col-md-9 my-4">-->
<!--            <button class="btn btn-primary btn-sm" type="button" id="tambah-penerima"><i class="fa fa-plus"></i>-->
<!--                Penerima-->
<!--            </button>-->
<!--            <button class="btn btn-danger btn-sm" type="button" id="kurangi-penerima"><i class="fa fa-minus"></i>-->
<!--                Penerima-->
<!--            </button>-->
<!--        </div>-->
<!--        <div id="field-wrap">-->
<!--            <div id="wrapper-1">-->
<!--                <div class="form-group">-->
<!--                    <label for="permohonanadvisemail-kepada" class="control-label col-sm-3">Kepada</label>-->
<!--                    <div class="col-sm-9">-->
<!--                        <input type="text" name="PermohonanAdvisEmail[kepada][]" id="kepada[]" class="form-control">-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="form-group">-->
<!--                    <label for="permohonanadvisemail-email" class="control-label col-sm-3">Alamat Email</label>-->
<!--                    <div class="col-sm-9">-->
<!--                        <input type="text" name="PermohonanAdvisEmail[email][]" id="email[]" class="form-control">-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
    <?php endif; ?>

    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
            <?= Html::button('<i class="fa fa-remove"></i> Cancel', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>