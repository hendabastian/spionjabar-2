<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;
?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        //'id',
        'tipePermohonan.tipe_permohonan',
        'no_surat_permohonan',
        'tgl_surat_permohonan',
        [
            'attribute' => 'file_surat_permohonan',
            'format' => 'RAW',
            'value' => function ($data) {
                return Html::button('<i class="fa fa-search"></i> Preview', ['value' => Url::toRoute(['preview-dok', 'id' => $data->id, 'filename' => $data->file_surat_permohonan]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-xs showModalButton',]);
            }
        ],
        [
            'attribute' => 'file_sk_kp',
            'format' => 'RAW',
            'value' => function ($data) {
                return Html::button('<i class="fa fa-search"></i> Preview', ['value' => Url::toRoute(['preview-dok', 'id' => $data->id, 'filename' => $data->file_sk_kp]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-xs showModalButton',]);
            }
        ],
        [
            'attribute' => 'file_surat_pelepasan_hak',
            'format' => 'RAW',
            'value' => function ($data) {
                return Html::button('<i class="fa fa-search"></i> Preview', ['value' => Url::toRoute(['preview-dok', 'id' => $data->id, 'filename' => $data->file_surat_pelepasan_hak]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-xs showModalButton',]);
            }
        ],
        // 'jumlah_kendaraan',
        //'is_delete',
        //'created_by',
        //'updated_by',
        //'created_at',
        //'updated_at',
    ],
]) ?>

<div class="x_panel">
    <div class="x_title">
        <h2>Data Kendaraan</h2>
        <div class="clearfix"></div>
    </div>
<div class="x_content">

        <?= \yii\grid\GridView::widget([
            'dataProvider' => $kendaraanDataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'no_kendaraan_lama',
                'no_rangka_baru',
                'merk_kendaraan_baru',
                'tahun_kendaraan_baru',
//                [
//                    'class' => 'yii\grid\ActionColumn',
//                    'header' => 'Aksi',
//                    'template' => '{view}{update}{delete}',
//                    'buttons' => [
//                        'view' => function ($url, $data) {
//                            return Html::button('<i class="fa fa-file"></i> Detail', ['value' => Url::toRoute(['detail-kendaraan', 'id' => $data->id, 'tipe_permohonan_id' => $data->permohonan->tipe_permohonan_id]), 'class' => 'btn btn-xs btn-primary showModalButton', 'title' => 'Detail Kendaraan']);
//                        },
//                        'update' => function ($url, $data) {
//                            return Html::button('<i class="fa fa-edit"></i> Update', ['value' => Url::toRoute(['detail-kendaraan', 'id' => $data->id, 'tipe_permohonan_id' => $data->permohonan->tipe_permohonan_id]), 'class' => 'btn btn-xs btn-warning showModalButton', 'title' => 'Detail Kendaraan']);
//                        },
//                        'delete' => function ($url, $data) {
//                            return Html::button('<i class="fa fa-trash"></i> Delete', ['value' => Url::toRoute(['detail-kendaraan', 'id' => $data->id, 'tipe_permohonan_id' => $data->permohonan->tipe_permohonan_id]), 'class' => 'btn btn-xs btn-danger showModalButton', 'title' => 'Detail Kendaraan']);
//                        }
//                    ],
//                    'contentOptions' => [
//                        'style' => 'width:225px; text-align:center;'
//                    ]
//                ],
            ],
        ]);
        ?>
    </div>
</div>