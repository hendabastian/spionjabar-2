<?php

use yii2assets\pdfjs\PdfJs;
use yii2assets\pdfjs\PdfJsAsset;

?>


<?php
$fileInfo = pathinfo(Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $model->perusahaan_id . '/permohonan/' . $filename);

switch ($fileInfo['extension']) {
    case 'pdf':
        echo PdfJs::widget([
            'url' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $model->perusahaan_id . '/permohonan/' . $filename,
            'buttons' => [
                'presentationMode' => false,
                'openFile' => false,
                'print' => false,
                'download' => true,
                'viewBookmark' => false,
                'secondaryToolbarToggle' => false
            ]
        ]);
        break;
    case 'mp4': ?>
        <video width="100%" height="100%" controls>
            <source src="<?= Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $model->perusahaan_id . '/permohonan/' . $filename ?>"
                    type="video/mp4">
            Your browser does not support the video tag.
        </video>
        <?php
        break;
    case 'jpg':
    case 'png':
    case 'jpeg': ?>
        <img src="<?= Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $model->perusahaan_id . '/permohonan/' . $filename ?>"
             alt="" srcset="" width="100%" style="widh: 100%;">
        <?php break;
}
?>
