<?php

use common\models\permohonan\Permohonan;
use common\models\UserRole;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\permohonan\Permohonan */

$this->title = $model->tipePermohonan->tipe_permohonan . ' - ' . $model->tipePermohonan->jenisPermohonan->jenisAngkutan->jenis_angkutan . ' - ' . $model->perusahaan->nama_perusahaan;
$this->params['breadcrumbs'][] = ['label' => 'Permohonan', 'url' => ['index', 'jenis_angkutan_id' => $model->tipePermohonan->jenisPermohonan->jenisAngkutan->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="permohonan-view">

        <div class="card">
            <div class="card-header">
                <div class="float-left">
                    <h2><?= Html::encode($this->title) ?> </h2>
                    <h4><?= $model->labelStatus() ?></h4>
                </div>
                <?= Html::a('Daftar', ['index', 'id' => $model->id, 'jenis_angkutan_id' => $model->tipePermohonan->jenisPermohonan->jenisAngkutan->id], ['class' => 'btn btn-primary float-right align-self-start']) ?>
            </div>

            <div class="card-body">
                <?php switch ($model->tipe_permohonan_id) {
                    case 1:
                        echo $this->render('akdp/_view_prinsip_ijin_baru', [
                            'model' => $model
                        ]);
                        break;
                    case 2:
                        echo $this->render('akdp/_view_prinsip_perpanjangan', [
                            'model' => $model
                        ]);
                        break;
                    case 3:
                        echo $this->render('akdp/_view_info_realisasi', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 4:
                        echo $this->render('akdp/_view_peremajaan', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 5:
                        echo $this->render('akdp/_view_bbn', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 6:
                        echo $this->render('akdp/_view_pindah_trayek', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 7:
                        echo $this->render('ajdp/_view_prinsip_ijin_baru', [
                            'model' => $model
                        ]);
                        break;
                    case 8:
                        echo $this->render('ajdp/_view_prinsip_perpanjangan', [
                            'model' => $model
                        ]);
                        break;
                    case 9:
                        echo $this->render('ajdp/_view_info_realisasi', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 10:
                        echo $this->render('ajdp/_view_peremajaan', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 11:
                        echo $this->render('ajdp/_view_bbn', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 12:
                        echo $this->render('ajdp/_view_pindah_trayek', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 13:
                        echo $this->render('ask/_view_prinsip_ijin_baru', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 14:
                        echo $this->render('ask/_view_peremajaan', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 15:
                        echo $this->render('ask/_view_bbn', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 16:
                        echo $this->render('taxi/_view_prinsip_ijin_baru', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 17:
                        echo $this->render('taxi/_view_peremajaan', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 18:
                        echo $this->render('taxi/_view_bbn', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 19:
                        echo $this->render('akap/_view_info_penambahan', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 20:
                        echo $this->render('akap/_view_peremajaan', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 21:
                        echo $this->render('pariwisata/_view_info_penambahan', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 22:
                        echo $this->render('pariwisata/_view_peremajaan', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 23:
                        echo $this->render('ajap/_view_info_penambahan', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                    case 24:
                        echo $this->render('ajap/_view_peremajaan', [
                            'model' => $model,
                            'kendaraanDataProvider' => $kendaraanDataProvider
                        ]);
                        break;
                } ?>

            </div>

            <div class="card-footer">
                <?php switch (Yii::$app->user->identity->userRole->id):
                    case UserRole::VERIFIKATOR:
                        if ($model->status_verifikasi == Permohonan::STATUS_PERUSAHAAN_SUBMITTED) {
                            if (empty($model->undangan)) {
                                echo Html::button('<i class="fa fa-mail-forward"></i> Kirim Undangan', ['value' => Url::toRoute(['input-undangan', 'id' => $model->id]), 'title' => 'Kirim Undangan Presentasi', 'class' => 'btn btn-sm btn-primary btn-round ml-auto showModalButton float-left']);
                            } else {
                                if ($model->undangan->bukti_rapat != null) {
                                    echo Html::button('<i class="fa fa-search"></i> Lihat Bukti Rapat', ['value' => Url::toRoute(['preview-dok-path', 'id' => $model->id, 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $model->perusahaan_id . '/undangan/', 'filename' => $model->undangan->bukti_rapat]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-sm showModalButton',]);
                                } else {
                                    echo Html::button('<i class="fa fa-upload"></i> Upload Bukti Rapat', ['value' => Url::toRoute(['upload-bukti-rapat', 'id' => $model->id]), 'title' => 'Upload Bukti Rapat Hasil Presentasi', 'class' => 'btn btn-sm btn-primary btn-round ml-auto showModalButton float-left']);
                                }
                            }
//                            echo Html::button('<i class="fa fa-paper-plane"></i> Input Tujuan Pengiriman Advis', ['value' => Url::toRoute(['send-advis', 'permohonan_id' => $model->id]), 'title' => 'Kirim Advis', 'class' => 'btn btn-sm btn-primary btn-round ml-auto showModalButton float-right']);
                            echo Html::button('<i class="fa fa-paper-plane"></i> Proses Verifikasi', ['value' => Url::toRoute(['ajukan-verifikasi', 'permohonan_id' => $model->id]), 'title' => 'Proses Verifikasi', 'class' => 'btn btn-sm btn-primary btn-round ml-auto showModalButton float-right mx-1']);
                        }
                        break;
                    case UserRole::KEPALA_SEKSI:
                        if (empty($model->undangan)) {
                            echo Html::button('<i class="fa fa-mail-forward"></i> Kirim Undangan', ['value' => Url::toRoute(['input-undangan', 'id' => $model->id]), 'title' => 'Kirim Undangan Presentasi', 'class' => 'btn btn-sm btn-primary btn-round ml-auto showModalButton float-left']);
                        } else {
                            if ($model->undangan->bukti_rapat != null) {
                                echo Html::button('<i class="fa fa-search"></i> Lihat Bukti Rapat', ['value' => Url::toRoute(['preview-dok-path', 'id' => $model->id, 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $model->perusahaan_id . '/undangan/', 'filename' => $model->undangan->bukti_rapat]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-sm showModalButton',]);
                            } else {
                                echo Html::button('<i class="fa fa-upload"></i> Upload Bukti Rapat', ['value' => Url::toRoute(['upload-bukti-rapat', 'id' => $model->id]), 'title' => 'Upload Bukti Rapat Hasil Presentasi', 'class' => 'btn btn-sm btn-primary btn-round ml-auto showModalButton float-left']);
                            }
                        }
                        if ($model->status_verifikasi == Permohonan::STATUS_VERIFIKATOR_APPROVED) {
                            echo Html::button('<i class="fa fa-paper-plane"></i> Proses Verifikasi', ['value' => Url::toRoute(['ajukan-verifikasi', 'permohonan_id' => $model->id]), 'title' => 'Proses Verifikasi', 'class' => 'btn btn-sm btn-primary btn-round ml-auto showModalButton float-right']);
                        }
                        break;
                    case UserRole::KEPALA_BIDANG:
                        if (empty($model->undangan)) {
                            echo Html::button('<i class="fa fa-mail-forward"></i> Kirim Undangan', ['value' => Url::toRoute(['input-undangan', 'id' => $model->id]), 'title' => 'Kirim Undangan Presentasi', 'class' => 'btn btn-sm btn-primary btn-round ml-auto showModalButton float-left']);
                        } else {
                            if ($model->undangan->bukti_rapat != null) {
                                echo Html::button('<i class="fa fa-search"></i> Lihat Bukti Rapat', ['value' => Url::toRoute(['preview-dok-path', 'id' => $model->id, 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $model->perusahaan_id . '/undangan/', 'filename' => $model->undangan->bukti_rapat]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-sm showModalButton',]);
                            } else {
                                echo Html::button('<i class="fa fa-upload"></i> Upload Bukti Rapat', ['value' => Url::toRoute(['upload-bukti-rapat', 'id' => $model->id]), 'title' => 'Upload Bukti Rapat Hasil Presentasi', 'class' => 'btn btn-sm btn-primary btn-round ml-auto showModalButton float-left']);
                            }
                        }
                        if ($model->status_verifikasi == Permohonan::STATUS_KASI_APPROVED) {
                            echo Html::button('<i class="fa fa-paper-plane"></i> Proses Verifikasi', ['value' => Url::toRoute(['send-advis', 'permohonan_id' => $model->id]), 'title' => 'Kirim Advis', 'class' => 'btn btn-sm btn-primary btn-round ml-auto showModalButton float-right']);
                        }
                        break;
                    case UserRole::KEPALA_DINAS:
                        if (empty($model->undangan)) {
                            echo Html::button('<i class="fa fa-mail-forward"></i> Kirim Undangan', ['value' => Url::toRoute(['input-undangan', 'id' => $model->id]), 'title' => 'Kirim Undangan Presentasi', 'class' => 'btn btn-sm btn-primary btn-round ml-auto showModalButton float-left']);
                        } else {
                            if ($model->undangan->bukti_rapat != null) {
                                echo Html::button('<i class="fa fa-search"></i> Lihat Bukti Rapat', ['value' => Url::toRoute(['preview-dok-path', 'id' => $model->id, 'path' => Yii::$app->urlManagerUpload->baseUrl . '/perusahaan/' . $model->perusahaan_id . '/undangan/', 'filename' => $model->undangan->bukti_rapat]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-sm showModalButton',]);
                            } else {
                                echo Html::button('<i class="fa fa-upload"></i> Upload Bukti Rapat', ['value' => Url::toRoute(['upload-bukti-rapat', 'id' => $model->id]), 'title' => 'Upload Bukti Rapat Hasil Presentasi', 'class' => 'btn btn-sm btn-primary btn-round ml-auto showModalButton float-left']);
                            }
                        }
//                        echo Html::button('<i class="fa fa-print"></i> Cetak Otomatis', ['value' => Url::toRoute(['verifikasi-ijin-prinsip/print', 'id' => $model->id]), 'class' => 'showModalButton btn btn-sm btn-success btn-round float-right', 'title' => 'Cetak Dokumen']);
                        if ($model->status_verifikasi == Permohonan::STATUS_ADVIS_APPROVED) {
                            echo Html::button('<i class="fa fa-check"></i> Setujui', ['value' => Url::toRoute(['approve-permohonan', 'id' => $model->id]), 'class' => 'showModalButton btn btn-sm btn-success btn-round float-right', 'title' => 'Persetujuan Permohonan']);
                            echo Html::button('<i class="fa fa-ban"></i> Tolak', ['value' => Url::toRoute(['ajukan-verifikasi', 'permohonan_id' => $model->id]), 'class' => 'showModalButton btn btn-sm btn-danger btn-round float-right mx-1', 'title' => 'Persetujuan Permohonan']);
                        } elseif ($model->status_verifikasi == Permohonan::STATUS_KADIS_APPROVED) {
//                            echo Html::a('Download Surat', Url::toRoute(['surat-permohonan', 'id' => $model->id]), ['class' => 'btn btn-primary', 'target' => '_blank']);
                            echo Html::button('<i class="fa fa-search"></i> Preview', ['value' => Url::toRoute(['preview-dok-url', 'url' => Url::toRoute(['surat-permohonan', 'id' => $model->id])]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-sm showModalButton',]);
                        }

                        break;
                endswitch; ?>
            </div>
        </div>

    </div>

<?php
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'options' => ['tabindex' => false],
    'closeButton' => ['tag' => 'close', 'label' => 'x'],
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
]);
echo "<div id='modalContent' style='padding:2px;'></div>";
Modal::end();
?>