<body>
<table style="width: 100%;">
    <tr>
        <td style="align-items: center;"><img
                    src="https://upload.wikimedia.org/wikipedia/commons/0/07/West_Java_coa.png" width="70"/></td>
        <td style="text-align: center;">
            <p style="text-align: center;"><b style="text-align: center;">PEMERINTAH DAERAH PROVINSI JAWA BARAT<br/>
                    <strong style="font-size: 22pt; text-align: center;">DINAS PERHUBUNGAN</strong></b>
                <br/>
            <p style="text-align: center;"> Jl. Sukabumi No. 1 Telp. : (022) 7207257 - 7272258 Fax. : (022) 7202163 <br>
                Website : dishub.jabarprov.go.id - e-mail : dishub@jabarprov.go.id <br>
                Bandung - 40271</p>
            </p>
        </td>
    </tr>
</table>
<hr/>
<table style="width: 100%;">
    <tr>
        <td>
            <p align="left" style="padding-top: 20px;">Nomor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                <?= $surat->no_surat ?><br>
                Sifat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?= $surat->sifat ?> <br>
                Lampiran&nbsp;&nbsp;&nbsp;&nbsp;: <?= $surat->lampiran ?> <br>
                Hal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?= $surat->hal ?>
            </p>
        </td>
        <td>
            <p style="padding-right: 25px; float:right;">K e p a d a : <br>
                Yth. (Terlampir) <br>
                di <br>
                TEMPAT</p>
        </td>
    </tr>
</table>
<br>
<br>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mempertimbangkan surat permohonan nomor
    <?= $model->no_surat_permohonan ?> untuk permohonan ijin prinsip, bersama ini kami mengundang Saudara untuk
    dapat hadir pada :</p>
<table style="padding-left:70px;">
    <tr>
        <td width="100">Hari</td>
        <td>: <?= Yii::$app->formatter->asDate($surat->waktu_undangan, 'php:l') ?></td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td>: <?= Yii::$app->formatter->asDate($surat->waktu_undangan, 'php:d F Y') ?></td>
    </tr>
    <tr>
        <td>Pukul</td>
        <td>: <?= Yii::$app->formatter->asDate($surat->waktu_undangan, 'php:h:i') ?></td>
    </tr>
    <tr>
        <td>Acara</td>
        <td>: <?= $surat->acara ?></td>
    </tr>
    <tr>
        <td valign="top">Tempat</td>
        <td>: <?= $surat->tempat ?></td>
    </tr>
</table>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan hal tersebut, dimohon
    kiranya Bapak/lbu/Sdr berkenan
    untuk hadir dalam acara tersebut.</p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian disampaikan, atas perhatian dan
    bantuannya diucapkan terima
    kasih.</p>

<br>
<br>
<br>
<br>
<br>
<br>
<p align="right">KUASA PENGGUNA ANGGARAN / <br> PEJABAT PEMBUAT KOMITMEN</p>
<br>
<br>
<br>
<p align="right">DIDING, S.IP., M.AP.</p>
<p align="right">NIP. 1791827389239</p>