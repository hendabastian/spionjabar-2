<?php

use common\models\master\JenisAngkutan;
use common\models\master\TipePermohonan;
use common\models\perusahaan\Perusahaan;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\report\models\PermohonanVerifikasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Permohonan Terverifikasi';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="permohonan-index">
        <div class="card">
            <div class="card-header">
                <h2><?= Html::encode($this->title) ?></h2>
            </div>
            <div class="card-body">
                <div class="table" style="width: 100%;">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            //'id',
                            [
                                'attribute' => 'perusahaan_id',
                                'value' => 'permohonan.perusahaan.nama_perusahaan',
                                'filter' => \kartik\select2\Select2::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'perusahaan_id',
                                    'data' => ArrayHelper::map(Perusahaan::find()->all(), 'id', 'nama_perusahaan'),
                                    'options' => ['placeholder' => 'Filter Perusahaan']
                                ]),
                                'contentOptions' => ['style' => 'width:150px;'],
                            ],
                            [
                                'attribute' => 'tipe_permohonan_id',
                                'value' => function ($data) {
                                    return $data->permohonan->tipePermohonan->jenisPermohonan->jenisAngkutan->jenis_angkutan . ' - ' . $data->permohonan->tipePermohonan->tipe_permohonan;
                                },
                                'filter' => \kartik\select2\Select2::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'tipe_permohonan_id',
                                    'data' => ArrayHelper::map(TipePermohonan::find()->joinWith(['jenisPermohonan'])->all(), 'id', function ($data) {
                                        return $data->jenisPermohonan->jenisAngkutan->jenis_angkutan . ' - ' . $data->tipe_permohonan;
                                    }),
                                    'options' => ['placeholder' => 'Filter Jenis Permohonan']
                                ]),
                                'contentOptions' => ['style' => 'width:150px;'],
                            ],
                            [
                                'attribute' => 'permohonan.created_at',
                                'label' => 'Diajukan Pada'
                            ],
                            [
                                'attribute' => 'created_at',
                                'label' => 'Diverifikasi Pada',
                            ],
                            [
                                'label' => 'Status Saat Ini',
                                'format' => 'RAW',
                                'value' => function ($data) {
                                    return $data->permohonan->labelStatus();
                                }
                            ],
//                            [
//                                'attribute' => 'file_surat_permohonan',
//                                'format' => 'RAW',
//                                'value' => function ($data) {
//                                    return Html::button('<i class="fa fa-search"></i> Preview', ['value' => Url::toRoute(['/permohonan/permohonan/preview-dok', 'id' => $data->permohonan->id, 'filename' => $data->permohonan->file_surat_permohonan]), 'title' => 'Preview Dokumen', 'class' => 'btn btn-primary btn-sm showModalButton',]);
//                                }
//                            ],
                            // 'jumlah_kendaraan',
                            //'is_delete',
                            //'created_by',
                            //'updated_by',
                            //'created_at',
                            //'updated_at',

                            ['class' => 'yii\grid\ActionColumn', 'header' => 'Aksi', 'template' => '{view}', 'contentOptions' => ['style' => 'width:80px; text-align:center;'], 'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<i class="fas fa-eye"></i>', Url::toRoute(['/permohonan/permohonan/view', 'id' => $model->permohonan->id]));
                                },
                            ]],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>

    </div>

<?php
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'options' => ['tabindex' => false],
    'closeButton' => ['tag' => 'close', 'label' => 'x'],
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
]);
echo "<div id='modalContent' style='padding:2px;'></div>";
Modal::end();
?>