<?php

namespace backend\modules\report\controllers;

use backend\modules\permohonan\models\PermohonanSearch;
use backend\modules\report\models\PermohonanVerifikasiSearch;
use yii\web\Controller;
use Yii;

/**
 * Default controller for the `Report` module
 */
class ReportController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPermohonanTerverifikasi()
    {
        $searchModel = new PermohonanVerifikasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['diverifikasi_oleh' => Yii::$app->user->identity->getId()]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }
}
