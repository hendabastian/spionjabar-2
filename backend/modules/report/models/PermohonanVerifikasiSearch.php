<?php

namespace backend\modules\report\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\permohonan\PermohonanVerifikasi;

/**
 * PermohonanVerifikasiSearch represents the model behind the search form of `common\models\permohonan\PermohonanVerifikasi`.
 */
class PermohonanVerifikasiSearch extends PermohonanVerifikasi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'permohonan_id', 'status_verifikasi', 'diverifikasi_oleh', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['tgl_verifikasi', 'catatan_verifikasi', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PermohonanVerifikasi::find()->joinWith('permohonan');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'permohonan_id' => $this->permohonan_id,
            'tgl_verifikasi' => $this->tgl_verifikasi,
            'status_verifikasi' => $this->status_verifikasi,
            'diverifikasi_oleh' => $this->diverifikasi_oleh,
            'is_delete' => $this->is_delete,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'catatan_verifikasi', $this->catatan_verifikasi]);

        return $dataProvider;
    }
}