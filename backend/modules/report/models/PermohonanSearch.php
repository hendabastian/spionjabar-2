<?php

namespace backend\modules\report\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\permohonan\Permohonan;

/**
 * PermohonanSearch represents the model behind the search form of `common\models\permohonan\Permohonan`.
 */
class PermohonanSearch extends Permohonan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tipe_permohonan_id', 'jumlah_kendaraan', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['no_surat_permohonan', 'tgl_surat_permohonan', 'file_surat_permohonan', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Permohonan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tipe_permohonan_id' => $this->tipe_permohonan_id,
            'tgl_surat_permohonan' => $this->tgl_surat_permohonan,
            'jumlah_kendaraan' => $this->jumlah_kendaraan,
            'is_delete' => 0,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'no_surat_permohonan', $this->no_surat_permohonan])
            ->andFilterWhere(['like', 'file_surat_permohonan', $this->file_surat_permohonan]);

        return $dataProvider;
    }
}
