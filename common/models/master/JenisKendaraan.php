<?php

namespace common\models\master;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "m_jenis_kendaraan".
 *
 * @property int $id
 * @property string $jenis_kendaraan
 * @property int $kapasitas_min
 * @property int $kapasitas_max
 * @property string|null $keterangan
 * @property int $is_active
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class JenisKendaraan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_jenis_kendaraan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_kendaraan', 'kapasitas_min', 'kapasitas_max'], 'required'],
            [['kapasitas_min', 'kapasitas_max', 'is_active', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['jenis_kendaraan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_kendaraan' => 'Jenis Kendaraan',
            'kapasitas_min' => 'Kapasitas Min',
            'kapasitas_max' => 'Kapasitas Max',
            'keterangan' => 'Keterangan',
            'is_active' => 'Is Active',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

}
