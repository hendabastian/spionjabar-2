<?php

namespace common\models\master;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "m_terminal".
 *
 * @property int $id
 * @property int $tipe_terminal_id
 * @property string $kode_terminal
 * @property string $nama_terminal
 * @property string|null $alamat
 * @property int $kabupaten_id
 * @property string|null $latitude
 * @property string|null $longitude
 * @property string|null $kepala_terminal
 * @property string|null $no_tlp
 * @property string|null $keterangan
 * @property int $is_active
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Terminal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_terminal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipe_terminal_id', 'kode_terminal', 'nama_terminal', 'kabupaten_id'], 'required'],
            [['tipe_terminal_id', 'kabupaten_id', 'is_active', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['alamat', 'keterangan'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['kode_terminal', 'nama_terminal', 'latitude', 'longitude', 'kepala_terminal', 'no_tlp'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipe_terminal_id' => 'Tipe Terminal ID',
            'kode_terminal' => 'Kode Terminal',
            'nama_terminal' => 'Nama Terminal',
            'alamat' => 'Alamat',
            'kabupaten_id' => 'Kabupaten ID',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'kepala_terminal' => 'Kepala Terminal',
            'no_tlp' => 'No Tlp',
            'keterangan' => 'Keterangan',
            'is_active' => 'Is Active',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

}
