<?php

namespace common\models\master;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "m_trayek".
 *
 * @property int $id
 * @property string $kode_trayek
 * @property string $nama_trayek
 * @property int|null $terminal_asal
 * @property int|null $terminal_tujuan
 * @property string|null $rute_trayek
 * @property int|null $jarak_rute
 * @property int $kuota
 * @property int $jenis_angkutan_id
 * @property string|null $keterangan
 * @property int $is_active
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Trayek extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_trayek';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_trayek', 'nama_trayek', 'kuota', 'jenis_angkutan_id'], 'required'],
            [['terminal_asal', 'terminal_tujuan', 'jarak_rute', 'kuota', 'jenis_angkutan_id', 'is_active', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['rute_trayek', 'keterangan'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['kode_trayek', 'nama_trayek'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_trayek' => 'Kode Trayek',
            'nama_trayek' => 'Nama Trayek',
            'terminal_asal' => 'Terminal Asal',
            'terminal_tujuan' => 'Terminal Tujuan',
            'rute_trayek' => 'Rute Trayek',
            'jarak_rute' => 'Jarak Rute',
            'kuota' => 'Kuota',
            'jenis_angkutan_id' => 'Jenis Angkutan ID',
            'keterangan' => 'Keterangan',
            'is_active' => 'Is Active',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getTerminalAsal()
    {
        return $this->hasOne(Terminal::class, ['id' => 'terminal_asal_id']);
    }

    public function getTerminalTujuan()
    {
        return $this->hasOne(Terminal::class, ['id' => 'terminal_tujuan_id']);
    }

    public function getJenisAngkutan()
    {
        return $this->hasOne(JenisAngkutan::class, ['id' => 'jenis_angkutan_id']);
    }

}
