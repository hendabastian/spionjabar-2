<?php

namespace common\models\master;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "m_jenis_angkutan".
 *
 * @property int $id
 * @property int $jenis_layanan_id
 * @property string $jenis_angkutan
 * @property string|null $keterangan
 * @property int $is_active
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class JenisAngkutan extends \yii\db\ActiveRecord
{
    const AKDP = 1;
    const AJDP = 2;
    const ASK = 3;
    const TAXI = 4;
    const AKAP = 5;
    const PARIWISATA = 6;
    const AJAP = 7;
    const LAINNYA = 8;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_jenis_angkutan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_layanan_id', 'jenis_angkutan'], 'required'],
            [['jenis_layanan_id', 'is_active', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['jenis_angkutan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_layanan_id' => 'Jenis Layanan ID',
            'jenis_angkutan' => 'Jenis Angkutan',
            'keterangan' => 'Keterangan',
            'is_active' => 'Is Active',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getJenisLayanan()
    {
        return $this->hasOne(JenisLayanan::class, ['id' => 'jenis_layanan_id']);
    }
}
