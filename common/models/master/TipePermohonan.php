<?php

namespace common\models\master;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "m_tipe_permohonan".
 *
 * @property int $id
 * @property int $jenis_permohonan_id
 * @property string $tipe_permohonan
 * @property string|null $keterangan
 * @property int $is_active
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class TipePermohonan extends \yii\db\ActiveRecord
{
    const AKDP_IJIN_PRINSIP_BARU = 1;
    const AKDP_PERPANJANGAN_IJIN_PRINSIP = 2;
    const AKDP_INFO_REALISASI = 3;
    const AKDP_PEREMAJAAN = 4;
    const AKDP_BBN = 5;
    const AKDP_PINDAH_TRAYEK = 6;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_tipe_permohonan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_permohonan_id', 'tipe_permohonan'], 'required'],
            [['jenis_permohonan_id', 'is_active', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['tipe_permohonan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_permohonan_id' => 'Jenis Permohonan ID',
            'tipe_permohonan' => 'Tipe Permohonan',
            'keterangan' => 'Keterangan',
            'is_active' => 'Is Active',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getJenisPermohonan()
    {
        return $this->hasOne(JenisPermohonan::class, ['id' => 'jenis_permohonan_id']);
    }
}
