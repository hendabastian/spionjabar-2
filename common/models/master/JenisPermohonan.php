<?php

namespace common\models\master;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "m_jenis_permohonan".
 *
 * @property int $id
 * @property int $jenis_angkutan_id
 * @property string $jenis_permohonan
 * @property int $is_active
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class JenisPermohonan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_jenis_permohonan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_angkutan_id', 'jenis_permohonan'], 'required'],
            [['jenis_angkutan_id', 'is_active', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['jenis_permohonan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_angkutan_id' => 'Jenis Angkutan ID',
            'jenis_permohonan' => 'Jenis Permohonan',
            'is_active' => 'Is Active',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getJenisAngkutan()
    {
        return $this->hasOne(JenisAngkutan::class, ['id' => 'jenis_angkutan_id']);
    }
}
