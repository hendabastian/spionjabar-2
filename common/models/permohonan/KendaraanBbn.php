<?php

namespace common\models\permohonan;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_kendaraan_bbn".
 *
 * @property int $id
 * @property int $permohonan_id
 * @property int $perusahaan_id
 * @property string $nama_pemilik_lama
 * @property string $alamat_pemilik_lama
 * @property string $no_kendaraan_lama
 * @property string $no_uji_kendaraan_lama
 * @property string $merk_kendaraan_lama
 * @property string $tahun_kendaraan_lama
 * @property int $lintasan_trayek_lama
 * @property string $nama_pemilik_baru
 * @property string $alamat_pemilik_baru
 * @property string $no_kendaraan_baru
 * @property string $no_uji_kendaraan_baru
 * @property string $merk_kendaraan_baru
 * @property string $tahun_kendaraan_baru
 * @property int $lintasan_trayek_baru
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class KendaraanBbn extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_kendaraan_bbn';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['permohonan_id', 'perusahaan_id', 'nama_pemilik_lama', 'alamat_pemilik_lama', 'no_kendaraan_lama', 'no_uji_kendaraan_lama', 'merk_kendaraan_lama', 'tahun_kendaraan_lama', 'lintasan_trayek_lama', 'nama_pemilik_baru', 'alamat_pemilik_baru', 'no_kendaraan_baru', 'no_uji_kendaraan_baru', 'merk_kendaraan_baru', 'tahun_kendaraan_baru', 'lintasan_trayek_baru'], 'required'],
            [['permohonan_id', 'perusahaan_id', 'lintasan_trayek_lama', 'lintasan_trayek_baru', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama_pemilik_lama', 'alamat_pemilik_lama', 'no_kendaraan_lama', 'no_uji_kendaraan_lama', 'merk_kendaraan_lama', 'tahun_kendaraan_lama', 'nama_pemilik_baru', 'alamat_pemilik_baru', 'no_kendaraan_baru', 'no_uji_kendaraan_baru', 'merk_kendaraan_baru', 'tahun_kendaraan_baru'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'permohonan_id' => 'Permohonan ID',
            'perusahaan_id' => 'Perusahaan ID',
            'nama_pemilik_lama' => 'Nama Pemilik Lama',
            'alamat_pemilik_lama' => 'Alamat Pemilik Lama',
            'no_kendaraan_lama' => 'No Kendaraan Lama',
            'no_uji_kendaraan_lama' => 'No Uji Kendaraan Lama',
            'merk_kendaraan_lama' => 'Merk Kendaraan Lama',
            'tahun_kendaraan_lama' => 'Tahun Kendaraan Lama',
            'lintasan_trayek_lama' => 'Wilayah OperasiLama',
            'nama_pemilik_baru' => 'Nama Pemilik Baru',
            'alamat_pemilik_baru' => 'Alamat Pemilik Baru',
            'no_kendaraan_baru' => 'No Kendaraan Baru',
            'no_uji_kendaraan_baru' => 'No Mesin Kendaraan Baru',
            'merk_kendaraan_baru' => 'Merk Kendaraan Baru',
            'tahun_kendaraan_baru' => 'Tahun Kendaraan Baru',
            'lintasan_trayek_baru' => 'Wilayah Operasi Baru',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }
    
    public function getPermohonan()
    {
        return $this->hasOne(Permohonan::class, ['id' => 'permohonan_id']);
    }
}
