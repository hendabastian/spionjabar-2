<?php

namespace common\models\permohonan;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_kendaraan_info_realisasi".
 *
 * @property int $id
 * @property int $permohonan_id
 * @property int $perusahaan_id
 * @property string $no_rangka
 * @property string $no_mesin
 * @property string $merk_kendaraan
 * @property string $tahun_kendaraan
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class KendaraanInfoRealisasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_kendaraan_info_realisasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['permohonan_id', 'perusahaan_id', 'no_rangka', 'no_mesin', 'merk_kendaraan', 'tahun_kendaraan'], 'required'],
            [['permohonan_id', 'perusahaan_id', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['no_rangka', 'no_mesin', 'merk_kendaraan', 'tahun_kendaraan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'permohonan_id' => 'Permohonan ID',
            'perusahaan_id' => 'Perusahaan ID',
            'no_rangka' => 'No Rangka',
            'no_mesin' => 'No Mesin',
            'merk_kendaraan' => 'Merk Kendaraan',
            'tahun_kendaraan' => 'Tahun Kendaraan',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getPermohonan()
    {
        return $this->hasOne(Permohonan::class, ['id' => 'permohonan_id']);
    }

}
