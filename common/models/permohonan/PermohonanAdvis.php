<?php

namespace common\models\permohonan;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_permohonan_advis".
 *
 * @property int $id
 * @property int $permohonan_id
 * @property string|null $no_surat
 * @property string|null $perihal
 * @property string|null $kendaraan_disetujui
 * @property string|null $file_surat
 * @property string|null $no_advis
 * @property string|null $tgl_advis
 * @property string|null $file_advis
 * @property int|null $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class PermohonanAdvis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_permohonan_advis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['permohonan_id'], 'required'],
            [['permohonan_id', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['tgl_advis', 'created_at', 'updated_at'], 'safe'],
            [['no_surat', 'perihal', 'kendaraan_disetujui', 'file_surat', 'no_advis', 'file_advis'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'permohonan_id' => 'Permohonan ID',
            'no_surat' => 'No Surat',
            'perihal' => 'Perihal',
            'kendaraan_disetujui' => 'Kendaraan Yang Disetujui',
            'file_surat' => 'File Surat',
            'no_advis' => 'No Advis',
            'tgl_advis' => 'Tgl Advis',
            'file_advis' => 'File Advis',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getAdvisEmail()
    {
        return $this->hasMany(PermohonanAdvisEmail::class, ['permohonan_advis_id' => 'id']);
    }
}
