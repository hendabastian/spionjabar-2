<?php

namespace common\models\permohonan;

use common\models\perusahaan\Perusahaan;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_permohonan_approved".
 *
 * @property int $id
 * @property int|null $perusahaan_id
 * @property int|null $permohonan_id
 * @property int|null $jenis_permohonan
 * @property string|null $no_surat
 * @property string|null $tgl_cetak
 * @property string|null $tgl_expire
 * @property int|null $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PermohonanApproved extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_permohonan_approved';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['perusahaan_id', 'permohonan_id', 'jenis_permohonan', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['tgl_cetak', 'tgl_expire', 'created_at', 'updated_at'], 'safe'],
            [['no_surat'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'perusahaan_id' => 'Perusahaan ID',
            'permohonan_id' => 'Permohonan ID',
            'jenis_permohonan' => 'Jenis Permohonan',
            'no_surat' => 'No Surat',
            'tgl_cetak' => 'Tgl Cetak',
            'tgl_expire' => 'Tgl Expire',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getPermohonan()
    {
        return $this->hasOne(Permohonan::class, ['id' => 'permohonan_id']);
    }

    public function getPerusahaan()
    {
        return $this->hasOne(Perusahaan::class, ['id' => 'perusahaan_id']);
    }
}
