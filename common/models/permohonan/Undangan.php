<?php

namespace common\models\permohonan;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_undangan".
 *
 * @property int $id
 * @property string $permohonan_id
 * @property string|null $no_surat
 * @property string|null $sifat
 * @property string|null $lampiran
 * @property string|null $hal
 * @property string|null $waktu_undangan
 * @property string|null $tempat
 * @property string|null $acara
 * @property string|null $bukti_rapat
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Undangan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_undangan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['permohonan_id'], 'required'],
            [['waktu_undangan', 'created_at', 'updated_at'], 'safe'],
            [['is_delete', 'created_by', 'updated_by'], 'integer'],
            [['permohonan_id', 'no_surat', 'sifat', 'lampiran', 'hal', 'tempat', 'acara', 'bukti_rapat'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'permohonan_id' => 'Permohonan ID',
            'no_surat' => 'No Surat',
            'sifat' => 'Sifat',
            'lampiran' => 'Lampiran',
            'hal' => 'Hal',
            'waktu_undangan' => 'Waktu Undangan',
            'tempat' => 'Tempat',
            'acara' => 'Acara',
            'bukti_rapat' => 'Bukti Rapat Presentasi Kepengusahaan',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }
}
