<?php

namespace common\models\permohonan;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_permohonan_verifikasi".
 *
 * @property int $id
 * @property int|null $permohonan_id
 * @property string|null $tgl_verifikasi
 * @property int|null $status_verifikasi
 * @property int|null $diverifikasi_oleh
 * @property string|null $catatan_verifikasi
 * @property int|null $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PermohonanVerifikasi extends \yii\db\ActiveRecord
{
    public $perusahaan_id;
    public $tipe_permohonan_id;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_permohonan_verifikasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['permohonan_id', 'status_verifikasi', 'diverifikasi_oleh', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['tgl_verifikasi', 'created_at', 'updated_at'], 'safe'],
            [['catatan_verifikasi'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'permohonan_id' => 'Permohonan ID',
            'tgl_verifikasi' => 'Tgl Verifikasi',
            'status_verifikasi' => 'Status Verifikasi',
            'diverifikasi_oleh' => 'Diverifikasi Oleh',
            'catatan_verifikasi' => 'Catatan Verifikasi',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getPermohonan()
    {
        return $this->hasOne(Permohonan::class, ['id' => 'permohonan_id']);
    }
}
