<?php

namespace common\models\permohonan;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_permohonan_advis_email".
 *
 * @property int $id
 * @property int|null $permohonan_advis_id
 * @property string $kepada
 * @property string $email
 * @property int|null $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class PermohonanAdvisEmail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_permohonan_advis_email';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['permohonan_advis_id', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['kepada', 'email'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['kepada', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'permohonan_advis_id' => 'Permohonan Advis ID',
            'kepada' => 'Kepada',
            'email' => 'Email',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

}
