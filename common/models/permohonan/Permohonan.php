<?php

namespace common\models\permohonan;

use common\models\master\JenisKendaraan;
use common\models\master\Kabupaten;
use common\models\master\Terminal;
use common\models\master\TipePermohonan;
use common\models\master\Trayek;
use common\models\perusahaan\Perusahaan;
use common\models\UserRole;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_permohonan".
 *
 * @property int $id
 * @property int $perusahaan_id
 * @property int $jenis_angkutan_id
 * @property int $tipe_permohonan_id
 * @property string $no_surat_permohonan
 * @property string $tgl_surat_permohonan
 * @property string $file_surat_permohonan
 * @property int|null $jumlah_kendaraan
 * @property string|null $proposal_pengoperasian_angkutan
 * @property string|null $file_pernyataan_kesanggupan_kendaraan
 * @property string|null $file_pernyataan_kesanggupan_pool
 * @property string|null $dok_pernyataan
 * @property int|null $jenis_kendaraan
 * @property int|null $trayek_id
 * @property int|null $trayek_baru_id
 * @property int|null $kota_asal_id
 * @property int|null $kota_tujuan_id
 * @property string|null $no_surat_persetujuan
 * @property string|null $tgl_surat_persetujuan
 * @property string|null $file_surat_persetujuan
 * @property string|null $file_sk_kp
 * @property string|null $no_advis_asal
 * @property string|null $tgl_advis_asal
 * @property string|null $file_advis_asal
 * @property string|null $no_advis_tujuan
 * @property string|null $tgl_advis_tujuan
 * @property string|null $file_advis_tujuan
 * @property int|null $prinsip_lama_id
 * @property int|null $wilayah_operasi_id
 * @property string|null $file_surat_pelepasan_hak
 * @property string|null $file_pernyataan_kebenaran_data
 * @property string|null $file_prinsip_kementerian
 * @property string|null $no_prinsip_kementerian
 * @property string|null $tgl_prinsip_kementerian
 * @property int|null $this->status_verifikasi_verifikasi
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Permohonan extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 0;
    const STATUS_PERUSAHAAN_SUBMITTED = 1;
    const STATUS_VERIFIKATOR_APPROVED = 2;
    const STATUS_VERIFIKATOR_REJECTED = 20;
    const STATUS_KASI_APPROVED = 3;
    const STATUS_KASI_REJECTED = 30;
    const STATUS_KABID_APPROVED = 4;
    const STATUS_KABID_REJECTED = 40;
    const STATUS_ADVIS_SUBMITTED = 41;
    const STATUS_ADVIS_APPROVED = 42;
    const STATUS_ADVIS_REJECTED = 43;
    const STATUS_KADIS_APPROVED = 5;
    const STATUS_KADIS_REJECTED = 50;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_permohonan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['perusahaan_id', 'jenis_angkutan_id', 'tipe_permohonan_id', 'no_surat_permohonan', 'tgl_surat_permohonan', 'file_surat_permohonan'], 'required'],
            [['perusahaan_id', 'jenis_angkutan_id', 'tipe_permohonan_id', 'jumlah_kendaraan', 'jenis_kendaraan', 'trayek_id', 'trayek_baru_id', 'kota_asal_id', 'kota_tujuan_id', 'wilayah_operasi_id', 'prinsip_lama_id', 'status_verifikasi', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['tgl_surat_permohonan', 'tgl_advis_asal', 'tgl_advis_tujuan', 'created_at', 'updated_at'], 'safe'],
            [['no_surat_permohonan', 'file_surat_permohonan', 'proposal_pengoperasian_angkutan', 'file_pernyataan_kesanggupan_kendaraan', 'file_pernyataan_kesanggupan_pool', 'dok_pernyataan', 'no_surat_persetujuan', 'tgl_surat_persetujuan', 'file_surat_persetujuan', 'file_sk_kp', 'no_advis_asal', 'file_advis_asal', 'no_advis_tujuan', 'file_advis_tujuan', 'file_surat_pelepasan_hak', 'file_pernyataan_kebenaran_data', 'file_prinsip_kementerian', 'no_prinsip_kementerian', 'tgl_prinsip_kementerian'], 'string', 'max' => 255],
            [['file_surat_permohonan'], 'file', 'extensions' => 'pdf, docx, doc, png, jpg, jpeg', 'maxSize' => 4120000, 'tooBig' => 'Maximum File Size 4MB']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'perusahaan_id' => 'Perusahaan',
            'jenis_angkutan_id' => 'Jenis Angkutan',
            'tipe_permohonan_id' => 'Tipe Permohonan',
            'no_surat_permohonan' => 'No Surat Permohonan',
            'tgl_surat_permohonan' => 'Tgl Surat Permohonan',
            'file_surat_permohonan' => 'File Surat Permohonan',
            'jumlah_kendaraan' => 'Jumlah Kendaraan',
            'proposal_pengoperasian_angkutan' => 'Proposal Pengoperasian Angkutan',
            'file_pernyataan_kesanggupan_kendaraan' => 'File Pernyataan Kesanggupan Kendaraan',
            'file_pernyataan_kesanggupan_pool' => 'File Pernyataan Kesanggupan Pool',
            'dok_pernyataan' => 'Dok Pernyataan',
            'jenis_kendaraan' => 'Jenis Kendaraan',
            'trayek_id' => 'Wilayah Operasi',
            'trayek_baru_id' => 'Wilayah Operasi Baru',
            'kota_asal_id' => 'Kota Asal',
            'kota_tujuan_id' => 'Kota Tujuan',
            'no_surat_persetujuan' => 'No Surat Persetujuan',
            'tgl_surat_persetujuan' => 'Tgl Surat Persetujuan',
            'file_surat_persetujuan' => 'File Surat Persetujuan',
            'file_sk_kp' => 'File SK / KP',
            'no_advis_asal' => 'No Advis Asal',
            'tgl_advis_asal' => 'Tgl Advis Asal',
            'file_advis_asal' => 'File Advis Asal',
            'no_advis_tujuan' => 'No Advis Tujuan',
            'tgl_advis_tujuan' => 'Tgl Advis Tujuan',
            'file_advis_tujuan' => 'File Advis Tujuan',
            'prinsip_lama_id' => 'Surat Persetujuan Pengusahaan Lama',
            'wilayah_operasi_id' => 'Wilayah Operasi',
            'file_surat_pelepasan_hak' => 'File Surat Pelepasan Hak',
            'file_pernyataan_kebenaran_data' => 'File Pernyataan Kebenaran Data',
            'file_prinsip_kementerian' => 'File Prinsip Kementerian',
            'no_prinsip_kementerian' => 'No Prinsip Kementerian',
            'tgl_prinsip_kementerian' => 'Tgl Prinsip Kementerian',
            'status_verifikasi' => 'Status Verifikasi',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord) {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        } else {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getTipePermohonan()
    {
        return $this->hasOne(TipePermohonan::class, ['id' => 'tipe_permohonan_id']);
    }

    public function getTrayek()
    {
        return $this->hasOne(Trayek::class, ['id' => 'trayek_id']);
    }

    public function getTrayekBaru()
    {
        return $this->hasOne(Trayek::class, ['id' => 'trayek_baru_id']);
    }

    public function getKotaAsal()
    {
        return $this->hasOne(Kabupaten::class, ['id' => 'kota_asal_id']);
    }

    public function getKotaTujuan()
    {
        return $this->hasOne(Kabupaten::class, ['id' => 'kota_tujuan_id']);
    }

    public function getJenisKendaraan()
    {
        return $this->hasOne(JenisKendaraan::class, ['id' => 'jenis_kendaraan']);
    }

    public function getVerifikasi()
    {
        return $this->hasMany(PermohonanVerifikasi::class, ['permohonan_id' => 'id']);
    }

    public function getPerusahaan()
    {
        return $this->hasOne(Perusahaan::class, ['id' => 'perusahaan_id']);
    }

    public function getAdvis()
    {
        return $this->hasOne(PermohonanAdvis::class, ['permohonan_id' => 'id']);
    }

    public function getPrinsipLama()
    {
        return $this->hasOne(PermohonanApproved::class, ['id' => 'prinsip_lama_id']);
    }

    public function getUndangan()
    {
        return $this->hasOne(Undangan::class, ['permohonan_id' => 'id']);
    }

    public function labelStatus()
    {
        $badge = "";

        if (Yii::$app->user->identity->user_role_id == UserRole::PERUSAHAAN) {
            switch ($this->status_verifikasi) {
                case self::STATUS_DRAFT:
                    $badge = '<div class="label label-warning">Belum Diajukan</div>';
                    break;
                case self::STATUS_PERUSAHAAN_SUBMITTED:
                    $badge = '<div class="label label-primary">Diajukan</div>';
                    break;
                case self::STATUS_VERIFIKATOR_APPROVED:
                    $badge = '<div class="label label-primary">Menunggu Verifikasi Kepala Seksi</div>';
                    break;
                case self::STATUS_KASI_APPROVED:
                    $badge = '<div class="label label-primary">Menunggu Verifikasi Kepala Bidang Angkutan</div>';
                    break;
                case self::STATUS_KABID_APPROVED:
                    $badge = '<div class="label label-primary">Menunggu Persetujuan Kepala Dinas</div>';
                    break;
                case self::STATUS_ADVIS_SUBMITTED:
                    $badge = '<div class="label label-primary">Menunggu Persetujuan Advis Kab/Kota</div>';
                    break;
                case self::STATUS_ADVIS_APPROVED:
                    $badge = '<div class="label label-primary">Menunggu Verifikasi Kepala Dinas</div>';
                    break;
                case self::STATUS_KADIS_APPROVED:
                    $badge = '<div class="label label-success">Permohonan Disetujui</div>';
                    break;
            }
        } else {
            switch ($this->status_verifikasi) {
                case self::STATUS_DRAFT:
                    $badge = '<div class="badge badge-warning">Belum Diajukan</div>';
                    break;
                case self::STATUS_PERUSAHAAN_SUBMITTED:
                    $badge = '<div class="badge badge-primary">Diajukan</div>';
                    break;
                case self::STATUS_VERIFIKATOR_APPROVED:
                    $badge = '<div class="badge badge-primary">Menunggu Verifikasi Kepala Seksi</div>';
                    break;
                case self::STATUS_KASI_APPROVED:
                    $badge = '<div class="badge badge-primary">Menunggu Verifikasi Kepala Bidang Angkutan</div>';
                    break;
                case self::STATUS_KABID_APPROVED:
                    $badge = '<div class="badge badge-primary">Menunggu Persetujuan Kepala Dinas</div>';
                    break;
                case self::STATUS_ADVIS_SUBMITTED:
                    $badge = '<div class="badge badge-primary">Menunggu Persetujuan Advis Kab/Kota</div>';
                    break;
                case self::STATUS_ADVIS_APPROVED:
                    $badge = '<div class="badge badge-primary">Menunggu Verifikasi Kepala Dinas</div>';
                    break;
                case self::STATUS_KADIS_APPROVED:
                    $badge = '<div class="badge badge-success">Permohonan Disetujui</div>';
                    break;
            }
        }

        return $badge;
    }
}
