<?php

namespace common\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "user_role".
 *
 * @property int $id
 * @property string|null $role
 * @property string|null $keterangan
 * @property int $is_active
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class UserRole extends \yii\db\ActiveRecord
{
    const ADMINSTRATOR = 1;
    const PERUSAHAAN = 2;
    const VERIFIKATOR = 3;
    const JASA_RAHARJA = 4;
    const KEPALA_SEKSI = 5;
    const KEPALA_BIDANG = 6;
    const KEPALA_DINAS = 7;
    const PERCETAKAN = 8;
    const SEKRETARIS = 9;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['keterangan'], 'string'],
            [['is_active', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['role'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role' => 'Role',
            'keterangan' => 'Keterangan',
            'is_active' => 'Is Active',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

}
