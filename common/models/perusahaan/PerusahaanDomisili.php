<?php

namespace common\models\perusahaan;

use common\models\master\Kabupaten;
use common\models\master\Provinsi;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_perusahaan_domisili".
 *
 * @property int $id
 * @property int $perusahaan_id
 * @property string $jenis_domisili
 * @property string $alamat
 * @property int $alamat_utama 0 = Tidak, 1 = Ya
 * @property int $provinsi_id
 * @property int $kabupaten_id
 * @property string $tlp
 * @property string|null $fax
 * @property string $kode_pos
 * @property int $is_active 1 = Aktif, 0 = Tidak Aktif
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PerusahaanDomisili extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_perusahaan_domisili';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['perusahaan_id', 'jenis_domisili', 'alamat', 'alamat_utama', 'provinsi_id', 'kabupaten_id', 'tlp', 'kode_pos'], 'required'],
            [['perusahaan_id', 'alamat_utama', 'provinsi_id', 'kabupaten_id', 'is_active', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['alamat'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['jenis_domisili', 'tlp', 'fax', 'kode_pos'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'perusahaan_id' => 'Perusahaan ID',
            'jenis_domisili' => 'Jenis Domisili',
            'alamat' => 'Alamat',
            'alamat_utama' => 'Alamat Utama',
            'provinsi_id' => 'Provinsi ID',
            'kabupaten_id' => 'Kabupaten ID',
            'tlp' => 'Tlp',
            'fax' => 'Fax',
            'kode_pos' => 'Kode Pos',
            'is_active' => 'Is Active',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getProvinsi()
    {
        return $this->hasOne(Provinsi::class, ['id' => 'provinsi_id']);
    }

    public function getKabupaten()
    {
        return $this->hasOne(Kabupaten::class, ['id' => 'kabupaten_id']);
    }

    public function getDokumenDomisili($id)
    {
        // jenis_dokumen_perusahaan_id 9 = dokumen domisili
        $dokumen = PerusahaanDokumen::find()->where(['is_delete' => 0, 'perusahaan_id' => $id, 'jenis_dokumen_perusahaan_id' => 9])->one();
        if($dokumen !== NULL){
            return $dokumen;
        }

        return NULL;
    }

}
