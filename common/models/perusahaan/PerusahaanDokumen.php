<?php

namespace common\models\perusahaan;

use common\models\master\JenisDokumenPerusahaan;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_perusahaan_dokumen".
 *
 * @property int $id
 * @property int $perusahaan_id
 * @property int $jenis_dokumen_perusahaan_id
 * @property string $nama_dokumen
 * @property string $no_dokumen
 * @property string|null $tgl_dokumen
 * @property string $filename
 * @property string|null $keterangan
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PerusahaanDokumen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_perusahaan_dokumen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['perusahaan_id', 'jenis_dokumen_perusahaan_id', 'nama_dokumen', 'no_dokumen', 'filename'], 'required'],
            [['perusahaan_id', 'jenis_dokumen_perusahaan_id', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['tgl_dokumen', 'created_at', 'updated_at'], 'safe'],
            [['keterangan', 'nama_dokumen'], 'string'],
            [['nama_dokumen', 'no_dokumen', 'filename'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'perusahaan_id' => 'Perusahaan ID',
            'jenis_dokumen_perusahaan_id' => 'Jenis Dokumen Perusahaan ID',
            'nama_dokumen' => 'Nama Dokumen',
            'no_dokumen' => 'No Dokumen',
            'tgl_dokumen' => 'Tgl Dokumen',
            'filename' => 'Filename',
            'keterangan' => 'Keterangan',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord) {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        } else {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getJenisDokumen()
    {
        return $this->hasOne(JenisDokumenPerusahaan::class, ['id' => 'jenis_dokumen_perusahaan_id']);
    }

    public function getLabelTgl($jenisDokId)
    {
        $label = "";
        if ($jenisDokId == 3) {
            $label = "Tgl Terbit";
        } elseif ($jenisDokId == 5) {
            $label = "Tgl Terbit Perubahaan";
        } elseif ($jenisDokId == 7) {
            $label = "Tgl Exp SIUP";
        } elseif ($jenisDokId == 8) {
            $label = "Tgl Terbit TDP";
        } else {
            $label = "Tgl Terbit";
        }

        return $label;
    }

}
