<?php

namespace common\models\perusahaan;

use common\models\master\JenisAngkutan;
use common\models\master\Trayek;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_sk_perusahaan".
 *
 * @property int $id
 * @property int $perusahaan_id
 * @property int $jenis_angkutan_id
 * @property int|null $trayek
 * @property string $no_sk
 * @property string|null $tgl_terbit
 * @property string $exp_sk
 * @property string $file_sk
 * @property int $is_active
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class SkPerusahaan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_sk_perusahaan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['file_sk'], 'required', 'on' => 'create'],
            [['perusahaan_id', 'jenis_angkutan_id', 'no_sk', 'exp_sk'], 'required'],
            [['perusahaan_id', 'jenis_angkutan_id', 'trayek', 'is_active', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['tgl_terbit', 'exp_sk', 'created_at', 'updated_at'], 'safe'],
            [['no_sk', 'file_sk'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'perusahaan_id' => 'Perusahaan',
            'jenis_angkutan_id' => 'Jenis Angkutan',
            'trayek' => 'Trayek',
            'no_sk' => 'No Sk',
            'tgl_terbit' => 'Tgl Terbit',
            'exp_sk' => 'Exp Sk',
            'file_sk' => 'File Sk',
            'is_active' => 'Is Active',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getJenisAngkutan()
    {
        return $this->hasOne(JenisAngkutan::class, ['id' => 'jenis_angkutan_id']);
    }

    public function getTrayek()
    {
        return $this->hasOne(Trayek::class, ['id' => 'trayek']);
    }

    public function getPerusahaan()
    {
        return $this->hasOne(Perusahaan::class, ['id' => 'perusahaan_id']);
    }

}
