<?php

namespace common\models\perusahaan;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_perusahaan".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $jenis_badan_usaha_id
 * @property string|null $kode_perusahaan
 * @property string $nama_perusahaan
 * @property string $npwp
 * @property string $email
 * @property string $hp
 * @property string|null $website
 * @property int|null $jenis_perusahaan 1 = perusahaan lama, 2 = perusahaan baru
 * @property int $data_version
 * @property int $is_verified 0 = belum terverifikasi, 1 = diajukan 2 = sudah terverifikasi, 5 = ditolak
 * @property int $status 0=Disable (Perubahan), 1=Aktif
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Perusahaan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_perusahaan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'jenis_badan_usaha_id', 'jenis_perusahaan', 'data_version', 'is_verified', 'status', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['jenis_badan_usaha_id', 'nama_perusahaan', 'npwp', 'email', 'hp'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['kode_perusahaan', 'nama_perusahaan', 'npwp', 'email', 'hp', 'website'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributebadges()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'jenis_badan_usaha_id' => 'Jenis Badan Usaha ID',
            'kode_perusahaan' => 'Kode Perusahaan',
            'nama_perusahaan' => 'Nama Perusahaan',
            'npwp' => 'Npwp',
            'email' => 'Email',
            'hp' => 'Hp',
            'website' => 'Website',
            'jenis_perusahaan' => 'Jenis Perusahaan',
            'data_version' => 'Data Version',
            'is_verified' => 'Is Verified',
            'status' => 'Status',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function checkStatus($status)
    {
        $badge = "";

        if ($status == 0) {
            $badge = '<span class="badge badge-warning">Belum Terverifikasi</span>';
        } elseif ($status == 1) {
            $badge = '<span class="badge badge-primary">Diajukan</span>';
        } elseif ($status == 2) {
            $badge = '<span class="badge badge-success">Terverifikasi</span>';
        } elseif ($status == 3) {
            $badge = '<span class="badge badge-warning">Diproses</span>';
        } elseif ($status == 4) {
            $badge = '<span class="badge badge-danger">Dikembalikan</span>';
        } elseif ($status == 5) {
            $badge = '<span class="badge badge-danger">Ditolak</span>';
        }

        return $badge;
    }

    public function getDomisili()
    {
        return $this->hasOne(PerusahaanDomisili::className(), ['perusahaan_id' => 'id'])->andOnCondition(['alamat_utama' => 1]);;
    }

    public function getPengurus()
    {
        return $this->hasOne(PerusahaanPengurus::className(), ['perusahaan_id' => 'id']);
    }

    public function getJenisAngkutan()
    {
        return $this->hasOne(JenisAngkutan::className(), ['id' => 'jenis_angkutan_id']);
    }

    public function getUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_id']);
    }

    public function getJenisBadanUsaha()
    {
        return $this->hasOne(\common\models\master\JenisBadanUsaha::className(), ['id' => 'jenis_badan_usaha_id']);
    }
}
