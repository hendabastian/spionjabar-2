<?php

namespace common\models\perusahaan;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_perusahaan_pemegang_saham".
 *
 * @property int $id
 * @property int $perusahaan_id
 * @property string|null $nik
 * @property string|null $file_ktp
 * @property string $nama_lengkap
 * @property string|null $npwp
 * @property float $persentase
 * @property string|null $keterangan
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PerusahaanPemegangSaham extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_perusahaan_pemegang_saham';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['perusahaan_id', 'nama_lengkap', 'persentase'], 'required'],
            [['perusahaan_id', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['persentase'], 'number'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['nik', 'file_ktp', 'nama_lengkap', 'npwp'], 'string', 'max' => 255],
            [['file_npwp', 'file_ktp'], 'file', 'extensions' => 'pdf, docx, doc, png, jpg, jpeg', 'maxSize' => 4120000, 'tooBig' => 'Maximum File Size 4MB']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'perusahaan_id' => 'Perusahaan ID',
            'nik' => 'Nik',
            'file_ktp' => 'File Ktp',
            'nama_lengkap' => 'Nama Lengkap',
            'npwp' => 'Npwp',
            'persentase' => 'Persentase',
            'keterangan' => 'Keterangan',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

}
