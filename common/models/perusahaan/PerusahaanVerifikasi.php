<?php

namespace common\models\perusahaan;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_perusahaan_verifikasi".
 *
 * @property int $id
 * @property int $perusahaan_id
 * @property string|null $tgl_verifikasi
 * @property int|null $status_verifikasi
 * @property string|null $catatan_verifikasi
 * @property int|null $diverifikasi_oleh
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PerusahaanVerifikasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_perusahaan_verifikasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['perusahaan_id'], 'required'],
            [['perusahaan_id', 'status_verifikasi', 'diverifikasi_oleh', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['tgl_verifikasi', 'created_at', 'updated_at'], 'safe'],
            [['catatan_verifikasi'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'perusahaan_id' => 'Perusahaan ID',
            'tgl_verifikasi' => 'Tgl Verifikasi',
            'status_verifikasi' => 'Status Verifikasi',
            'catatan_verifikasi' => 'Catatan Verifikasi',
            'diverifikasi_oleh' => 'Diverifikasi Oleh',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getPerusahaan()
    {
        return $this->hasOne(Perusahaan::class, ['id' => 'perusahaan_id']);
    }

    public function checkStatus($status)
    {
        $badge = "";

        if ($status == 0) {
            $badge = '<span class="badge badge-warning">Belum Terverifikasi</span>';
        } elseif ($status == 1) {
            $badge = '<span class="badge badge-primary">Diajukan</span>';
        } elseif ($status == 2) {
            $badge = '<span class="badge badge-success">Terverifikasi</span>';
        } elseif ($status == 3) {
            $badge = '<span class="badge badge-warning">Diproses</span>';
        } elseif ($status == 4) {
            $badge = '<span class="badge badge-danger">Dikembalikan</span>';
        } elseif ($status == 5) {
            $badge = '<span class="badge badge-danger">Ditolak</span>';
        }

        return $badge;
    }

}
