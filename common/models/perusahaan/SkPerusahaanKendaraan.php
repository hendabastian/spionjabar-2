<?php

namespace common\models\perusahaan;

use common\models\master\JenisKendaraan;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_sk_perusahaan_kendaraan".
 *
 * @property int $id
 * @property int $sk_id
 * @property string|null $no_srut
 * @property string|null $tgl_srut
 * @property string $no_kendaraan
 * @property string $no_rangka
 * @property string|null $no_uji
 * @property string|null $expire_uji
 * @property string $no_mesin
 * @property string|null $tahun
 * @property string|null $merk
 * @property string|null $nama_pemilik
 * @property int $jenis_kendaraan_id
 * @property int|null $seat
 * @property string|null $stnk
 * @property string|null $kir
 * @property string|null $bukti_jasa_raharja
 * @property int $is_active
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class SkPerusahaanKendaraan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_sk_perusahaan_kendaraan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sk_id', 'no_kendaraan', 'no_rangka', 'no_mesin', 'jenis_kendaraan_id'], 'required'],
            [['sk_id', 'jenis_kendaraan_id', 'seat', 'is_active', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['expire_uji', 'created_at', 'updated_at'], 'safe'],
            [['no_srut', 'tgl_srut', 'no_kendaraan', 'no_rangka', 'no_uji', 'no_mesin', 'tahun', 'merk', 'nama_pemilik', 'stnk', 'kir', 'bukti_jasa_raharja'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sk_id' => 'Sk ID',
            'no_srut' => 'No Srut',
            'tgl_srut' => 'Tgl Srut',
            'no_kendaraan' => 'No Kendaraan',
            'no_rangka' => 'No Rangka',
            'no_uji' => 'No Uji',
            'expire_uji' => 'Expire Uji',
            'no_mesin' => 'No Mesin',
            'tahun' => 'Tahun',
            'merk' => 'Merk',
            'nama_pemilik' => 'Nama Pemilik',
            'jenis_kendaraan_id' => 'Jenis Kendaraan ID',
            'seat' => 'Seat',
            'stnk' => 'Stnk',
            'kir' => 'Kir',
            'bukti_jasa_raharja' => 'Bukti Jasa Raharja',
            'is_active' => 'Is Active',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

    public function getSk()
    {
        return $this->hasOne(SkPerusahaan::class, ['id' => 'sk_id']);
    }

    public function getJenisKendaraan()
    {
        return $this->hasOne(JenisKendaraan::class, ['id' => 'jenis_kendaraan_id']);
    }

}
