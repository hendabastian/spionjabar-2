<?php

namespace common\models\perusahaan;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "t_perusahaan_pengurus".
 *
 * @property int $id
 * @property int $perusahaan_id
 * @property string $nik
 * @property string|null $file_ktp
 * @property string $nama_lengkap
 * @property string $jabatan
 * @property string|null $periode_mulai
 * @property string|null $periode_selesai
 * @property string $npwp
 * @property string $file_npwp
 * @property string|null $keterangan
 * @property int $is_active
 * @property int $is_delete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class PerusahaanPengurus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_perusahaan_pengurus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['perusahaan_id', 'nik', 'nama_lengkap', 'jabatan', 'npwp', 'file_npwp'], 'required'],
            [['perusahaan_id', 'is_active', 'is_delete', 'created_by', 'updated_by'], 'integer'],
            [['periode_mulai', 'periode_selesai', 'created_at', 'updated_at'], 'safe'],
            [['keterangan'], 'string'],
            [['nik', 'file_ktp', 'nama_lengkap', 'jabatan', 'npwp', 'file_npwp'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'perusahaan_id' => 'Perusahaan ID',
            'nik' => 'Nik',
            'file_ktp' => 'File Ktp',
            'nama_lengkap' => 'Nama Lengkap',
            'jabatan' => 'Jabatan',
            'periode_mulai' => 'Periode Mulai',
            'periode_selesai' => 'Periode Selesai',
            'npwp' => 'Npwp',
            'file_npwp' => 'File Npwp',
            'keterangan' => 'Keterangan',
            'is_active' => 'Is Active',
            'is_delete' => 'Is Delete',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        if ($this->isNewRecord)
        {
            $this->created_by = Yii::$app->user->id;
            $this->updated_by = Yii::$app->user->id;
            $this->created_at = new Expression('NOW()');
            $this->updated_at = new Expression('NOW()');
        }
        else
        {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = new Expression('NOW()');
        }
        return true;
    }

}
