<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $perusahaan->user->verification_token]);
?>
Hello <?= $perusahaan->nama_perusahaan ?>,

Berikut Info Akun Anda Untuk Melakukan Login,

Username : <?= $perusahaan->user->username ?>
Password : <?= $password ?>

Silakan klik link di bawah ini untuk melakukan verifikasi:

<?= $verifyLink ?>