<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<div class="verify-email">
    <p>Hello <?= Html::encode($perusahaan->nama_perusahaan) ?>,</p>

    <p>Terima kasih telah mengajukan permohonan pada aplikasi SPIONJABAR,</p>

    <p>Untuk melanjutkan proses permohonan anda perlu melakukan presentasi sesuai dengan jadwal pada undangan terlampir.</p>
    <p>Terima kasih</p>
</div>