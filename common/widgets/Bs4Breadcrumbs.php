<?php
namespace common\widgets;

use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;

class Bs4Breadcrumbs extends Breadcrumbs {
    public function init()
    {
        $this->clientOptions = false;
        Html::addCssClass($this->options, ['widget' => 'breadcrumb breadcrumb-links breadcrumb-dark']);
        parent::init();
    }
}