<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_permohonan_approved}}`.
 */
class m201021_014035_create_t_permohonan_approved_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_permohonan_approved}}', [
            'id' => $this->primaryKey(),
            'perusahaan_id' => $this->integer(),
            'permohonan_id' => $this->integer(),
            'jenis_permohonan' => $this->integer(),
            'no_surat' => $this->string(),
            'tgl_cetak' => $this->date(),
            'tgl_expire' => $this->date(),
            'is_delete' => $this->integer()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_permohonan_approved}}');
    }
}
