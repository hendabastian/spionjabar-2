<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_jenis_layanan}}`.
 */
class m200717_100206_create_m_jenis_layanan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%m_jenis_layanan}}', [
            'id' => $this->primaryKey(),
            'jenis_layanan' => $this->string(),
            'keterangan' => $this->text(),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%m_jenis_layanan}}');
    }
}
