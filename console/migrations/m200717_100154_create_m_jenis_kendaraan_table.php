<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_jenis_kendaraan}}`.
 */
class m200717_100154_create_m_jenis_kendaraan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%m_jenis_kendaraan}}', [
            'id' => $this->primaryKey(),
            'jenis_kendaraan' => $this->string()->notNull(),
            'kapasitas_min' => $this->integer()->notNull(),
            'kapasitas_max' => $this->integer()->notNull(),
            'keterangan' => $this->text(),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%m_jenis_kendaraan}}');
    }
}
