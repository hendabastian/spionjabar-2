<?php

use yii\db\Migration;

/**
 * Class m210125_050934_add_attendance_status_to_to_undangan_table
 */
class m210125_050934_add_attendance_status_to_to_undangan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%t_undangan}}', 'perusahaan_id', $this->integer()->after('permohonan_id'));
        $this->addColumn('{{%t_undangan}}', 'attendance_status', $this->string()->after('acara'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%t_undangan}}', 'perusahaan_id');
        $this->dropColumn('{{%t_undangan}}', 'attendance_status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210125_050934_add_attendance_status_to_to_undangan_table cannot be reverted.\n";

        return false;
    }
    */
}
