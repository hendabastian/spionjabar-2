<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_terminal}}`.
 */
class m200717_104900_create_m_terminal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%m_terminal}}', [
            'id' => $this->primaryKey(),
            'tipe_terminal_id' => $this->integer()->notNull(),
            'kode_terminal' => $this->string()->notNull(),
            'nama_terminal' => $this->string()->notNull(),
            'alamat' => $this->text(),
            'kabupaten_id' => $this->integer()->notNull(),
            'latitude' => $this->string(),
            'longitude' => $this->string(),
            'kepala_terminal' => $this->string(),
            'no_tlp' => $this->string(),
            'keterangan' => $this->text(),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%m_terminal}}');
    }
}
