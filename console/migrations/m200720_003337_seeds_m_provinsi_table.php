<?php

use yii\db\Migration;

/**
 * Class m200720_003337_seeds_provinsi_table
 */
class m200720_003337_seeds_m_provinsi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            INSERT INTO `m_provinsi` (`id`, `kode`, `nama`, `keterangan`, `is_active`, `is_delete`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
            (1, '11', 'ACEH', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (2, '12', 'SUMATERA UTARA', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (3, '13', 'SUMATERA BARAT', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (4, '14', 'RIAU', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (5, '15', 'JAMBI', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (6, '16', 'SUMATERA SELATAN', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (7, '17', 'BENGKULU', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (8, '18', 'LAMPUNG', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (9, '19', 'KEPULAUAN BANGKA BELITUNG', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (10, '21', 'KEPULAUAN RIAU', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (11, '31', 'DKI JAKARTA', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (12, '32', 'JAWA BARAT', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (13, '33', 'JAWA TENGAH', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (14, '34', 'DAERAH ISTIMEWA', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (15, '35', 'JAWA TIMUR', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (16, '36', 'BANTEN', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (17, '51', 'BALI', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (18, '52', 'NUSA TENGGARA BARAT', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (19, '53', 'NUSA TENGGARA TIMUR', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (20, '61', 'KALIMANTAN BARAT', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (21, '62', 'KALIMANTAN TENGAH', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (22, '63', 'KALIMANTAN SELATAN', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (23, '64', 'KALIMANTAN TIMUR', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (24, '65', 'KALIMANTAN UTARA', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (25, '71', 'SULAWESI UTARA', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (26, '72', 'SULAWESI TENGAH', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (27, '73', 'SULAWESI SELATAN', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (28, '74', 'SULAWESI TENGGARA', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (29, '75', 'GORONTALO', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (30, '76', 'SULAWESI BARAT', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (31, '81', 'MALUKU', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (32, '82', 'MALUKU UTARA', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (33, '91', 'P A P U A', NULL, 1, 0, NULL, NULL, NULL, NULL),
            (34, '92', 'PAPUA BARAT', NULL, 1, 0, NULL, NULL, NULL, NULL);
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('m_provinsi');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200720_003337_seeds_provinsi_table cannot be reverted.\n";

        return false;
    }
    */
}
