<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_tipe_permohonan}}`.
 */
class m200717_100322_create_m_tipe_permohonan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%m_tipe_permohonan}}', [
            'id' => $this->primaryKey(),
            'jenis_permohonan_id' => $this->integer()->notNull(),
            'tipe_permohonan'=> $this->string()->notNull(),
            'keterangan' => $this->text(),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%m_tipe_permohonan}}');
    }
}
