<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_kabupaten}}`.
 */
class m200717_100338_create_m_kabupaten_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%m_kabupaten}}', [
            'id' => $this->primaryKey(),
            'provinsi_id' => $this->integer()->notNull(),
            'kode' => $this->string()->notNull(),
            'nama' => $this->string()->notNull(),
            'keterangan' => $this->text(),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%m_kabupaten}}');
    }
}
