<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_kendaraan_info_realisasi}}`.
 */
class m200811_082528_create_t_kendaraan_info_realisasi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_kendaraan_info_realisasi}}', [
            'id' => $this->primaryKey(),
            'permohonan_id' => $this->integer()->notNull(),
            'perusahaan_id' => $this->integer()->notNull(),
            'no_rangka' => $this->string()->notNull(),
            'no_mesin' => $this->string()->notNull(),
            'merk_kendaraan' => $this->string()->notNull(),
            'tahun_kendaraan' => $this->string()->notNull(),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_kendaraan_info_realisasi}}');
    }
}
