<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_kendaraan_peremajaan}}`.
 */
class m200811_082843_create_t_kendaraan_peremajaan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_kendaraan_peremajaan}}', [
            'id' => $this->primaryKey(),
            'permohonan_id' => $this->integer()->notNull(),
            'perusahaan_id' => $this->integer()->notNull(),
            'nama_pemilik_lama' => $this->string()->notNull(),
            'alamat_pemilik_lama' => $this->string()->notNull(),
            'no_kendaraan_lama' => $this->string()->notNull(),
            'no_uji_kendaraan_lama' => $this->string()->notNull(),
            'merk_kendaraan_lama' => $this->string()->notNull(),
            'tahun_kendaraan_lama' => $this->string()->notNull(),
            'lintasan_trayek_lama' => $this->integer()->notNull(),
            'status_kendaraan_lama' => $this->integer()->notNull(),
            'nama_pemilik_baru' => $this->string()->notNull(),
            'alamat_pemilik_baru' => $this->string()->notNull(),
            'no_rangka_baru' => $this->string()->notNull(),
            'no_mesin_kendaraan_baru' => $this->string()->notNull(),
            'merk_kendaraan_baru' => $this->string()->notNull(),
            'tahun_kendaraan_baru' => $this->string()->notNull(),
            'lintasan_trayek_baru' => $this->integer()->notNull(),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_kendaraan_peremajaan}}');
    }
}
