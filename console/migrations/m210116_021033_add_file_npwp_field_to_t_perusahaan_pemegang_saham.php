<?php

use yii\db\Migration;

/**
 * Class m210116_021033_add_file_npwp_field_to_t_perusahaan_pemegang_saham
 */
class m210116_021033_add_file_npwp_field_to_t_perusahaan_pemegang_saham extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%t_perusahaan_pemegang_saham}}', 'file_npwp', $this->string()->after('npwp'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%t_perusahaan_pemegang_saham}}', 'file_npwp');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210116_021033_add_file_npwp_field_to_t_perusahaan_pemegang_saham cannot be reverted.\n";

        return false;
    }
    */
}
