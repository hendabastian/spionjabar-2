<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_perusahaan_pengurus}}`.
 */
class m200717_105074_create_t_perusahaan_pengurus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_perusahaan_pengurus}}', [
            'id' => $this->primaryKey(),
            'perusahaan_id' => $this->integer()->notNull(),
            'nik' => $this->string()->notNull(),
            'file_ktp' => $this->string(),
            'nama_lengkap' => $this->string()->notNull(),
            'jabatan' => $this->string()->notNull(),
            'periode_mulai' => $this->date(),
            'periode_selesai' => $this->date(),
            'npwp' => $this->string()->notNull(),
            'file_npwp' => $this->string()->notNull(),
            'keterangan' => $this->text(),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_perusahaan_pengurus}}');
    }
}
