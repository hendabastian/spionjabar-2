<?php

use yii\db\Migration;

use function PHPSTORM_META\map;

/**
 * Class m200720_125053_seeds_user_table
 */
class m200720_125053_seeds_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert(
            'user',
            ['id', 'user_role_id', 'name', 'username', 'auth_key', 'password_hash', 'email', 'status', 'is_delete'],
            [
                [
                    1,
                    1,
                    'Dishub Jabar Darat',
                    'administrator',
                    Yii::$app->security->generateRandomString(),
                    Yii::$app->security->generatePasswordHash('123456789'),
                    'spionjabardev@gmail.com',
                    10,
                    0
                ],
                [
                    2,
                    3,
                    'Verifikator',
                    'verifikator',
                    Yii::$app->security->generateRandomString(),
                    Yii::$app->security->generatePasswordHash('123456789'),
                    'verifikator@test.com',
                    10,
                    0
                ],
                [
                    3,
                    4,
                    'Jasa Raharja',
                    'jasaraharja',
                    Yii::$app->security->generateRandomString(),
                    Yii::$app->security->generatePasswordHash('123456789'),
                    'jr@test.com',
                    10,
                    0
                ],
                [
                    4,
                    5,
                    'Kepala Seksi',
                    'kasi',
                    Yii::$app->security->generateRandomString(),
                    Yii::$app->security->generatePasswordHash('123456789'),
                    'kasi@test.com',
                    10,
                    0
                ],
                [
                    5,
                    6,
                    'Kepala Bidang Angkutan',
                    'kabid',
                    Yii::$app->security->generateRandomString(),
                    Yii::$app->security->generatePasswordHash('123456789'),
                    'kabid@test.com',
                    10,
                    0
                ],
                [
                    6,
                    7,
                    'Kepala Dinas',
                    'kadis',
                    Yii::$app->security->generateRandomString(),
                    Yii::$app->security->generatePasswordHash('123456789'),
                    'kadis@test.com',
                    10,
                    0
                ]
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200720_125053_seeds_user_table cannot be reverted.\n";

        return false;
    }
    */
}
