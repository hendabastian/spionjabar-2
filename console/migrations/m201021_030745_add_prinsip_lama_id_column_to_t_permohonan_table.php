<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%t_permohonan}}`.
 */
class m201021_030745_add_prinsip_lama_id_column_to_t_permohonan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%t_permohonan}}', 'prinsip_lama_id', $this->integer()->after('file_advis_tujuan'));
        $this->dropColumn('t_permohonan', 'no_surat_persetujuan_pengusahaan_lama');
        $this->dropColumn('t_permohonan', 'file_surat_persetujuan_pengusahaan_lama');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%t_permohonan}}', 'prinsip_lama_id');
        $this->addColumn('t_permohonan', 'file_surat_persetujuan_pengusahaan_lama', $this->string()->after('file_advis_tujuan'));
        $this->addColumn('t_permohonan', 'no_surat_persetujuan_pengusahaan_lama', $this->string()->after('file_advis_tujuan'));

    }
}
