<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_tipe_angkutan}}`.
 */
class m200717_104914_create_m_tipe_angkutan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%m_tipe_angkutan}}', [
            'id' => $this->primaryKey(),
            'jenis_angkutan_id' => $this->integer()->notNull(),
            'tipe_angkutan' => $this->string()->notNull(),
            'keterangan' => $this->text(),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%m_tipe_angkutan}}');
    }
}
