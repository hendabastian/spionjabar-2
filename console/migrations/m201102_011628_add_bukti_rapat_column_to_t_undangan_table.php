<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%t_undangan}}`.
 */
class m201102_011628_add_bukti_rapat_column_to_t_undangan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%t_undangan}}', 'bukti_rapat', $this->string()->after('acara'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%t_undangan}}', 'bukti_rapat');
    }
}
