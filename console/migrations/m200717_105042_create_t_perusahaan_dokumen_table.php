<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_perusahaan_dokumen}}`.
 */
class m200717_105042_create_t_perusahaan_dokumen_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_perusahaan_dokumen}}', [
            'id' => $this->primaryKey(),
            'perusahaan_id' => $this->integer()->notNull(),
            'jenis_dokumen_perusahaan_id' => $this->integer()->notNull(),
            'nama_dokumen' => $this->string()->notNull(),
            'no_dokumen' => $this->string()->notNull(),
            'tgl_dokumen' => $this->date(),
            'filename' => $this->string()->notNull(),
            'keterangan' => $this->text(),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_perusahaan_dokumen}}');
    }
}
