<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_jenis_permohonan}}`.
 */
class m200717_100318_create_m_jenis_permohonan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%m_jenis_permohonan}}', [
            'id' => $this->primaryKey(),
            'jenis_angkutan_id' => $this->integer()->notNull(),
            'jenis_permohonan' => $this->string()->notNull(),
            'keterangan' => $this->text(),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%m_jenis_permohonan}}');
    }
}
