<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_perusahaan_domisili}}`.
 */
class m200717_105051_create_t_perusahaan_domisili_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_perusahaan_domisili}}', [
            'id' => $this->primaryKey(),
            'perusahaan_id' => $this->integer()->notNull(),
            'jenis_domisili' => $this->string()->notNull(),
            'alamat' => $this->text()->notNull(),
            'alamat_utama' => $this->integer()->notNull()->comment('0 = Tidak, 1 = Ya'),
            'provinsi_id' => $this->integer()->notNull(),
            'kabupaten_id' => $this->integer()->notNull(),
            'tlp' => $this->string()->notNull(),
            'fax' => $this->string(),
            'kode_pos' => $this->string()->notNull(),
            'is_active' => $this->integer()->notNull()->defaultValue(1)->comment('1 = Aktif, 0 = Tidak Aktif'),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_perusahaan_domisili}}');
    }
}
