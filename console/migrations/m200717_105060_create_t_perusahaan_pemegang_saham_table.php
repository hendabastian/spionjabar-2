<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_perusahaan_pemegang_saham}}`.
 */
class m200717_105060_create_t_perusahaan_pemegang_saham_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_perusahaan_pemegang_saham}}', [
            'id' => $this->primaryKey(),
            'perusahaan_id' => $this->integer()->notNull(),
            'nik' => $this->string(),
            'file_ktp' => $this->string(),
            'nama_lengkap' => $this->string()->notNull(),
            'npwp' => $this->string(),
            'persentase' => $this->double()->notNull(),
            'keterangan' => $this->text(),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_perusahaan_pemegang_saham}}');
    }
}
