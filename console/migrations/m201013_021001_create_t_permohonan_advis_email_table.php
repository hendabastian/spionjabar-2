<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_permohonan_advis_tujuan}}`.
 */
class m201013_021001_create_t_permohonan_advis_email_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_permohonan_advis_email}}', [
            'id' => $this->primaryKey(),
            'permohonan_advis_id' => $this->integer(),
            'kepada' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'is_delete' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_permohonan_advis_email}}');
    }
}
