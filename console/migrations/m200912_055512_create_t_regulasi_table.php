<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_regulasi}}`.
 */
class m200912_055512_create_t_regulasi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%m_regulasi}}', [
            'id' => $this->primaryKey(),
            'jenis_layanan_id' => $this->integer(),
            'nama_regulasi' => $this->string(),
            'tahun' => $this->string(),
            'dok_regulasi' => $this->string(),
            'keterangan' => $this->text(),
            'is_publish' => $this->integer(),
            'is_delete' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%m_regulasi}}');
    }
}
