<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_undangan}}`.
 */
class m200717_105736_create_t_undangan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_undangan}}', [
            'id' => $this->primaryKey(),
            'permohonan_id' => $this->string()->notNull(),
            'no_surat' => $this->string(),
            'sifat' => $this->string(),
            'lampiran' => $this->string(),
            'hal' => $this->string(),
            'waktu_undangan' => $this->dateTime(),
            'tempat' => $this->string(),
            'acara' => $this->string(),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_undangan}}');
    }
}
