<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_kendaraan_info_penambahan}}`.
 */
class m200811_084010_create_t_kendaraan_info_penambahan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_kendaraan_info_penambahan}}', [
            'id' => $this->primaryKey(),
            'permohonan_id' => $this->integer()->notNull(),
            'perusahaan_id' => $this->integer()->notNull(),
            'nama_pemilik' => $this->string()->notNull(),
            'alamat_pemilik' => $this->string()->notNull(),
            'no_kendaraan' => $this->string()->notNull(),
            'no_uji_kendaraan' => $this->string()->notNull(),
            'merk_kendaraan' => $this->string()->notNull(),
            'tahun_kendaraan' => $this->string()->notNull(),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_kendaraan_info_penambahan}}');
    }
}
