<?php

use yii\db\Migration;

/**
 * Class m200805_044136_seeds_data_terminal
 */
class m200805_044136_seeds_data_terminal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        INSERT INTO `m_terminal` (`id`, `tipe_terminal_id`, `kode_terminal`, `nama_terminal`, `alamat`, `kabupaten_id`, `latitude`, `longitude`, `kepala_terminal`, `no_tlp`, `keterangan`, `is_active`, `is_delete`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
            (1, 3, 'TC-KBKLP-BDG', 'Kebon Kalapa', 'Jl. Dewi Sartika Bandung', 181, '-', '-', '-', '-', '', 1, 1, '2019-10-09 09:53:07', '2020-02-14 22:37:55', 1, 100),
            (2, 1, 'TA-CCHM-BDG', 'Cicaheum', 'Terminal Cicaheum Bandung', 181, '-6.902332353417856', '107.65661722568663', '', '', '', 1, 0, '2020-02-14 22:08:44', '2020-02-14 22:30:07', 1, 100),
            (3, 1, 'TA-LWPJG-BDG', 'Leuwi Panjang', 'Terminal Leuwipanjang Bandung', 181, '-6.946533639673635', '107.59318609098509', '', '', '', 1, 0, '2020-02-14 22:27:44', '2020-02-14 22:29:26', 100, 100),
            (4, 2, 'TB-CRYM-BDG', 'Ciroyom', 'Jl. Ciroyom, Ciroyom, Kota Bandung, Jawa Barat, Indonesia', 181, '-6.915130719409824', '107.58820469671173', '', '', '', 1, 0, '2020-02-14 22:35:16', '2020-02-14 22:38:05', 100, 100),
            (5, 2, 'TB-LDG-BDG', 'Ledeng', 'Jalan Doktor Setiabudi, Isola, Kota Bandung, Jawa Barat, Indonesia', 181, '-6.859178926967483', '107.59515036850892', '', '', '', 1, 0, '2020-02-14 22:40:51', '2020-02-14 22:40:58', 100, 100),
            (6, 3, 'TC-LMBG-KBB', 'Lembang', 'Jalan Panorama, Lembang, Kabupaten Bandung Barat, Jawa Barat, Indonesia', 177, '-6.816449667140874', '107.62292526322936', '', '', '', 1, 0, '2020-02-15 11:32:14', '2020-02-15 11:32:14', 1, 1),
            (7, 3, 'TC-PGLG-KBDG', 'Pangalengan', 'Jalan Raya Pangalengan, Pangalengan, Bandung, Jawa Barat, Indonesia', 164, '-7.176335754751985', '107.57477619115448', '', '', '', 1, 0, '2020-02-15 11:34:45', '2020-02-15 11:34:45', 1, 1),
            (8, 3, 'TC-PDRG-KBB', 'Padalarang', 'Jalan Raya Padalarang, Kabupaten Bandung Barat, Jawa Barat, Indonesia', 177, '-6.840481074044904', '107.4860102867859', '', '', '', 1, 0, '2020-02-15 11:36:19', '2020-02-15 11:36:19', 1, 1),
            (9, 1, 'TA-PTRMN-BJR', 'Patroman', 'Jalan Mayjen Didi Kartasasmita, Banjar, Kota Banjar, Jawa Barat, Indonesia', 345, '-7.363046297374314', '108.53335532988586', '', '', '', 1, 0, '2020-02-15 11:40:23', '2020-02-15 11:40:23', 1, 1),
            (10, 1, 'TA-BKS-BKS', 'Bekasi', 'Raya Juanda No.302 Bekasi', 176, '-6.249501687257454', '107.01400659739225', '', '', '', 1, 0, '2020-02-15 11:46:16', '2020-02-15 11:48:12', 1, 1),
            (11, 1, 'TA-BRSNG-BRG', 'Baranangsiang', 'Jalan Raya Bogor - Sukabumi, RT.04/RW.02, Kampung Parung Jambu, Baranangsiang, Kota Bogor, Jawa Barat, Indonesia', 179, '-6.6044753743461655', '106.8064967087372', '', '', '', 1, 0, '2020-02-15 11:59:16', '2020-02-15 11:59:16', 1, 1),
            (12, 3, 'TC-SKSR-BGR', 'Sukasari', '', 179, '', '', '', '', '', 1, 0, '2020-02-15 12:23:58', '2020-02-15 12:23:58', 1, 1),
            (13, 3, 'TC-CRG-BGR', 'Cicurug', '', 161, '', '', '', '', '', 1, 0, '2020-02-15 12:24:40', '2020-02-15 12:24:40', 1, 1),
            (14, 3, 'TC-RMYN-BGR', 'Ramayana', '', 179, '', '', '', '', '', 1, 0, '2020-02-15 12:25:23', '2020-02-15 12:25:23', 1, 1),
            (15, 3, 'TC-CSR-BGR', 'Cisarua', '', 161, '', '', '', '', '', 1, 0, '2020-02-15 12:26:22', '2020-02-15 12:26:22', 1, 1),
            (16, 3, 'TC-CPS-BGR', 'Ciapus', '', 161, '', '', '', '', '', 1, 0, '2020-02-15 12:26:57', '2020-02-15 12:26:57', 1, 1),
            (17, 3, 'TC-CBG-BGR', 'Cibedug', '', 161, '', '', '', '', '', 1, 0, '2020-02-15 12:27:38', '2020-02-15 12:27:38', 1, 1),
            (18, 3, 'TC-CHDG-BGR', 'Cihideung', '', 161, '', '', '', '', '', 1, 0, '2020-02-15 12:28:13', '2020-02-15 12:28:13', 1, 1),
            (19, 3, 'TC-MRDK-BGR', 'Merdeka', '', 179, '', '', '', '', '', 1, 0, '2020-02-15 12:28:49', '2020-02-15 12:28:49', 1, 1),
            (20, 3, 'TC-CMS-BGR', 'Ciomas', '', 161, '', '', '', '', '', 1, 0, '2020-02-15 12:29:16', '2020-02-15 12:29:16', 1, 1),
            (21, 3, 'TC-PRG-BGR', 'Parung', '', 161, '', '', '', '', '', 1, 0, '2020-02-15 12:30:06', '2020-02-15 12:30:06', 1, 1),
            (22, 3, 'TC-CTRP-BGR', 'Citeureup', '', 161, '', '', '', '', '', 1, 0, '2020-02-15 12:30:53', '2020-02-15 12:30:53', 1, 1),
            (23, 2, 'TB-RWBG-CJR', 'Rawa Bango', '', 163, '', '', '', '', '', 1, 0, '2020-02-15 12:32:16', '2020-02-15 12:32:16', 1, 1),
            (24, 3, 'TC-JGL-BGR', 'Jonggol', '', 161, '', '', '', '', '', 1, 0, '2020-02-15 12:33:23', '2020-02-15 12:33:23', 1, 1);
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('m_terminal');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200805_044136_seeds_data_terminal cannot be reverted.\n";

        return false;
    }
    */
}
