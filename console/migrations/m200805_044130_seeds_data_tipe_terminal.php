<?php

use yii\db\Migration;

/**
 * Class m200805_044130_seeds_data_tipe_terminal
 */
class m200805_044130_seeds_data_tipe_terminal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        INSERT INTO `m_tipe_terminal` (`id`, `tipe_terminal`, `keterangan`, `is_active`, `is_delete`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
        (1, 'Terminal A', '', 1, 0, '2019-07-11 11:51:01', '2019-10-08 16:03:34', 1, 1),
        (2, 'Terminal B', '', 1, 0, '2019-10-08 16:03:02', '2019-10-08 16:03:02', 1, 1),
        (3, 'Terminal C', '', 1, 0, '2019-10-08 16:03:47', '2019-10-08 16:03:47', 1, 1);
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('m_tipe_terminal');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200805_044130_seeds_data_tipe_terminal cannot be reverted.\n";

        return false;
    }
    */
}
