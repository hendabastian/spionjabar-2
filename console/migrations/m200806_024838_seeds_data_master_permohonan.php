<?php

use yii\db\Migration;

/**
 * Class m200806_024838_seeds_data_master_permohonan
 */
class m200806_024838_seeds_data_master_permohonan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        INSERT INTO `m_jenis_angkutan` (`id`, `jenis_layanan_id`, `jenis_angkutan`, `keterangan`, `is_active`, `is_delete`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
        (1, 1, 'AKDP', 'Angkutan Kota Dalam Provinsi', 1, 0, 1, 1, '2020-07-20 23:41:41', '2020-07-20 23:41:41'),
        (2, 1, 'AJDP', 'Antar Jemput Dalam Provinsi', 1, 0, 1, 1, '2020-07-20 23:42:08', '2020-07-20 23:42:08'),
        (3, 1, 'ASK', 'Angkutan Sewa Khusus', 1, 0, 1, 1, '2020-07-20 23:42:35', '2020-07-20 23:51:20'),
        (4, 1, 'TAXI', 'Taxi', 1, 0, 1, 1, '2020-07-20 23:42:47', '2020-07-20 23:51:27'),
        (5, 2, 'AKAP', 'Antar Kota Antar Provinsi', 1, 0, 1, 1, '2020-07-20 23:43:09', '2020-07-20 23:51:38'),
        (6, 2, 'Pariwisata', 'Pariwisata', 1, 0, 1, 1, '2020-07-20 23:43:25', '2020-07-20 23:43:25'),
        (7, 2, 'AJAP', 'Antar Jemput Antar Provinsi', 1, 0, 1, 1, '2020-07-20 23:43:40', '2020-07-20 23:43:40'),
        (8, 2, 'Lainnya', '', 1, 0, 1, 1, '2020-07-20 23:51:49', '2020-07-20 23:51:49');

        INSERT INTO `m_jenis_kendaraan` (`id`, `jenis_kendaraan`, `kapasitas_min`, `kapasitas_max`, `keterangan`, `is_active`, `is_delete`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
        (1, 'Bus Besar', 0, 0, '', 1, 0, 1, 1, '2020-08-05 11:15:49', '2020-08-05 11:15:49'),
        (2, 'Bus Sedang', 0, 0, '', 1, 0, 1, 1, '2020-08-05 11:16:00', '2020-08-05 11:16:00'),
        (3, 'Bus Kecil', 0, 0, '', 1, 0, 1, 1, '2020-08-05 11:16:21', '2020-08-05 11:16:21');

        INSERT INTO `m_jenis_layanan` (`id`, `jenis_layanan`, `keterangan`, `is_active`, `is_delete`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
        (1, 'Permohonan Ijin', '', 1, 0, 1, 1, '2020-07-20 23:40:52', '2020-07-20 23:50:35'),
        (2, 'Permohonan Rekom', 'Permohonan Rekomendasi Perusahaan', 1, 0, 1, 1, '2020-07-20 23:41:04', '2020-07-20 23:50:54');

        INSERT INTO `m_jenis_permohonan` (`id`, `jenis_angkutan_id`, `jenis_permohonan`, `is_active`, `is_delete`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
        (1, 1, 'Permohonan Izin Prinsip', 1, 0, 1, 1, '2020-07-20 23:52:24', '2020-07-20 23:52:24'),
        (2, 1, 'Permohonan Surat Informasi Kepengusahaan', 1, 0, 1, 1, '2020-07-20 23:44:23', '2020-07-20 23:52:42'),
        (3, 2, 'Permohonan Izin Prinsip', 1, 0, 1, 1, '2020-07-20 23:56:17', '2020-07-20 23:56:17'),
        (4, 2, 'Permohonan Surat Informasi Kepengusahaan', 1, 0, 1, 1, '2020-07-20 23:56:29', '2020-07-20 23:56:29'),
        (5, 3, 'Permohonan Izin Prinsip', 1, 0, 1, 1, '2020-07-27 08:14:51', '2020-07-27 08:14:51'),
        (6, 4, 'Permohonan Izin Prinsip', 1, 0, 1, 1, '2020-08-01 12:00:26', '2020-08-01 12:00:26'),
        (7, 5, 'Permohonan Rekomendasi', 1, 0, 1, 1, '2020-08-04 20:20:27', '2020-08-04 20:20:27'),
        (8, 6, 'Permohonan Rekomendasi', 1, 0, 1, 1, '2020-08-04 20:21:13', '2020-08-04 20:21:13'),
        (9, 7, 'Permohonan Rekomendasi', 1, 0, 1, 1, '2020-08-04 20:21:28', '2020-08-04 20:21:28');

        insert into `m_tipe_permohonan` (`id`, `jenis_permohonan_id`, `tipe_permohonan`, `keterangan`, `is_active`, `is_delete`, `created_by`, `updated_by`, `created_at`, `updated_at`)
        values  (1, 1, 'Permohonan Izin Baru', '', 1, 0, 1, 1, '2020-07-20 23:53:57', '2020-07-20 23:54:33'),
                (2, 1, 'Permohonan Perpanjangan Izin', '', 1, 0, 1, 1, '2020-07-20 23:54:28', '2020-07-20 23:54:28'),
                (3, 2, 'Info Realisasi', '', 1, 0, 1, 1, '2020-07-20 23:54:56', '2020-07-20 23:54:56'),
                (4, 2, 'Permohonan Peremajaan', '', 1, 0, 1, 1, '2020-07-20 23:55:07', '2020-07-20 23:55:07'),
                (5, 2, 'BBN', '', 1, 0, 1, 1, '2020-07-20 23:55:14', '2020-07-20 23:55:14'),
                (6, 2, 'Perubahan Trayek', '', 1, 0, 1, 1, '2020-07-20 23:55:26', '2020-07-20 23:55:26'),
                (7, 3, 'Permohonan Izin Baru', '', 1, 0, 1, 1, '2020-07-20 23:58:54', '2020-07-20 23:58:54'),
                (8, 3, 'Permohonan Perpanjangan Izin', '', 1, 0, 1, 1, '2020-07-20 23:59:06', '2020-07-20 23:59:06'),
                (9, 4, 'Info Realisasi', '', 1, 0, 1, 1, '2020-07-20 23:59:19', '2020-07-20 23:59:19'),
                (10, 4, 'Permohonan Peremajaan', '', 1, 0, 1, 1, '2020-07-20 23:59:30', '2020-07-20 23:59:30'),
                (11, 4, 'BBN', '', 1, 0, 1, 1, '2020-07-20 23:59:40', '2020-07-20 23:59:40'),
                (12, 4, 'Perubahan Trayek', '', 1, 0, 1, 1, '2020-07-21 00:00:01', '2020-07-21 00:00:01'),
                (13, 5, 'Permohonan Izin Baru', '', 1, 0, 1, 1, '2020-07-27 08:15:26', '2020-08-01 11:59:50'),
                (14, 5, 'Permohonan Peremajaan', '', 1, 0, 1, 1, '2020-07-27 08:15:46', '2020-07-27 08:15:46'),
                (15, 5, 'BBN', '', 1, 0, 1, 1, '2020-08-01 11:57:39', '2020-08-01 11:57:39'),
                (16, 6, 'Permohonan Izin Prinsip', '', 1, 0, 1, 1, '2020-08-01 12:00:55', '2020-08-01 12:00:55'),
                (17, 6, 'Permohonan Peremajaan', '', 1, 0, 1, 1, '2020-08-01 12:01:09', '2020-08-01 12:01:09'),
                (18, 6, 'BBN', '', 1, 0, 1, 1, '2020-08-01 12:01:24', '2020-08-01 12:01:24'),
                (19, 7, 'Permohonan Info Penambahan', '', 1, 0, 1, 1, '2020-08-04 20:22:14', '2020-08-04 20:22:14'),
                (20, 7, 'Peremajaan', '', 1, 0, 1, 1, '2020-08-04 20:22:34', '2020-08-04 20:22:34'),
                (21, 8, 'Permohonan Info Penambahan', '', 1, 0, 1, 1, '2020-08-04 20:22:59', '2020-08-04 20:22:59'),
                (22, 8, 'Peremajaan', '', 1, 0, 1, 1, '2020-08-04 20:23:20', '2020-08-04 20:23:20'),
                (23, 9, 'Permohonan Info Penambahan', '', 1, 0, 1, 1, '2021-01-16 05:12:32', '2021-01-16 05:12:32'),
                (24, 9, 'Peremajaan', '', 1, 0, 1, 1, '2021-01-16 05:12:57', '2021-01-16 05:12:57');
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('m_tipe_permohonan');
        $this->truncateTable('m_jenis_permohonan');
        $this->truncateTable('m_jenis_layanan');
        $this->truncateTable('m_jenis_kendaraan');
        $this->truncateTable('m_jenis_angkutan');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200806_024838_seeds_data_master_permohonan cannot be reverted.\n";

        return false;
    }
    */
}
