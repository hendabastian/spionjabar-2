<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_permohonan_advis}}`.
 */
class m201013_020952_create_t_permohonan_advis_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_permohonan_advis}}', [
            'id' => $this->primaryKey(),
            'permohonan_id' => $this->integer()->notNull(),
            'no_surat' => $this->string(),
            'perihal' => $this->string(),
            'kendaraan_disetujui' => $this->string(),
            'file_surat' => $this->string(),
            'no_advis' => $this->string(),
            'tgl_advis' => $this->date(),
            'file_advis' => $this->string(),
            'is_delete' => $this->integer()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_permohonan_advis}}');
    }
}
