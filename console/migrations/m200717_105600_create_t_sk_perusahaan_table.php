<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_sk_perusahaan}}`.
 */
class m200717_105600_create_t_sk_perusahaan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_sk_perusahaan}}', [
            'id' => $this->primaryKey(),
            'perusahaan_id' => $this->integer()->notNull(),
            'jenis_angkutan_id' => $this->integer()->notNull(),
            'trayek' => $this->integer(),
            'no_sk' => $this->string()->notNull(),
            'tgl_terbit' => $this->date(),
            'exp_sk' => $this->date()->notNull(),
            'file_sk' => $this->string()->notNull(),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_sk_perusahaan}}');
    }
}
