<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_permohonan}}`.
 */
class m200717_105015_create_t_permohonan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_permohonan}}', [
            'id' => $this->bigPrimaryKey(),
            'perusahaan_id' => $this->integer()->notNull(),
            'jenis_angkutan_id' => $this->integer()->notNull(),
            'tipe_permohonan_id' => $this->integer()->notNull(),
            'no_surat_permohonan' => $this->string()->notNull(),
            'tgl_surat_permohonan' => $this->date()->notNull(),
            'file_surat_permohonan' => $this->string()->notNull(),
            'jumlah_kendaraan' => $this->integer(),
            'proposal_pengoperasian_angkutan' => $this->string(),
            'file_pernyataan_kesanggupan_kendaraan' => $this->string(),
            'file_pernyataan_kesanggupan_pool' => $this->string(),
            'dok_pernyataan' => $this->string(),
            'jenis_kendaraan' => $this->integer(),
            'trayek_id' => $this->integer(),
            'trayek_baru_id' => $this->integer(),
            'kota_asal_id' => $this->integer(),
            'kota_tujuan_id' => $this->integer(),
            'no_surat_persetujuan' => $this->string(),
            'tgl_surat_persetujuan' => $this->string(),
            'file_surat_persetujuan' => $this->string(),
            'file_sk_kp' => $this->string(),
            'no_advis_asal' => $this->string(),
            'tgl_advis_asal' => $this->date(),
            'file_advis_asal' => $this->string(),
            'no_advis_tujuan' => $this->string(),
            'tgl_advis_tujuan' => $this->date(),
            'file_advis_tujuan' => $this->string(),
            'no_surat_persetujuan_pengusahaan_lama' => $this->string(),
            'file_surat_persetujuan_pengusahaan_lama' => $this->string(),
            'wilayah_operasi_id' => $this->integer(),
            'file_surat_pelepasan_hak' => $this->string(),
            'file_pernyataan_kebenaran_data' => $this->string(),
            'file_prinsip_kementerian' => $this->string(),
            'no_prinsip_kementerian' => $this->string(),
            'tgl_prinsip_kementerian' => $this->string(),
            'status_verifikasi' => $this->integer(),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_permohonan}}');
    }
}
