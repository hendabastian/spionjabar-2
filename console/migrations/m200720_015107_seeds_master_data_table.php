<?php

use yii\db\Migration;

/**
 * Class m200720_015107_seeds_master_data_table
 */
class m200720_015107_seeds_master_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            INSERT INTO `m_jenis_badan_usaha` (`id`, `jenis_badan_usaha`, `keterangan`, `is_active`, `is_delete`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
            (2, 'PT', '', 1, 0, '2019-07-23 09:51:38', NULL, 1, NULL),
            (3, 'CV', '', 1, 0, '2019-07-23 09:52:13', NULL, 1, NULL),
            (4, 'PD', '', 1, 0, '2019-07-23 09:52:27', NULL, 1, NULL);

            INSERT INTO `m_jenis_dokumen_perusahaan` (`id`, `jenis_dokumen`, `keterangan`, `is_active`, `is_delete`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
            (1, 'NPWP Direktur', '', 1, 0, '2019-07-16 16:06:18', NULL, 1, NULL),
            (2, 'NPWP Perusahaan', '', 1, 0, '2019-07-16 16:06:37', NULL, 1, NULL),
            (3, 'Akte Pendirian', '', 1, 0, '2019-07-16 16:06:57', NULL, 1, NULL),
            (4, 'Kumham Pendirian', '', 1, 0, '2019-07-16 16:07:17', NULL, 1, NULL),
            (5, 'Akte Perubahan (Opsional)', '', 1, 0, '2019-07-16 16:07:54', NULL, 1, NULL),
            (6, 'Kumham Perubahan Perubahan (Opsional)', '', 1, 0, '2019-07-16 16:08:21', NULL, 1, NULL),
            (7, 'SIUP', '', 1, 0, '2019-07-16 16:08:39', NULL, 1, NULL),
            (8, 'TDP', '', 1, 0, '2019-07-16 16:09:22', NULL, 1, NULL),
            (9, 'Domisili', '', 1, 0, '2019-07-16 16:09:45', NULL, 1, NULL),
            (10, 'SK Perusahaan (Untuk Perusahaan Lama)', '', 0, 1, '2019-10-07 11:34:59', '2019-12-06 09:58:45', 1, 1),
            (11, 'Kontrak Bengkel', '', 1, 0, '2019-10-07 11:35:11', NULL, 1, NULL);
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200720_015107_seeds_master_data_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200720_015107_seeds_master_data_table cannot be reverted.\n";

        return false;
    }
    */
}
