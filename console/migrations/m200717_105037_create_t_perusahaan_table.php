<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_perusahaan}}`.
 */
class m200717_105037_create_t_perusahaan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_perusahaan}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'jenis_badan_usaha_id' => $this->integer()->notNull(),
            'kode_perusahaan' => $this->string(),
            'nama_perusahaan' => $this->string()->notNull(),
            'npwp' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'hp' => $this->string()->notNull(),
            'website' => $this->string(),
            'jenis_perusahaan' => $this->integer()->comment('1 = perusahaan lama, 2 = perusahaan baru'),
            'data_version' => $this->integer()->notNull()->defaultValue(1),
            'is_verified' => $this->integer()->notNull()->defaultValue(0)->comment('0 = belum terverifikasi, 1 = diajukan 2 = sudah terverifikasi, 5 = ditolak'),
            'status' => $this->integer()->notNull()->defaultValue(1)->comment('0=Disable (Perubahan), 1=Aktif'),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_perusahaan}}');
    }
}
