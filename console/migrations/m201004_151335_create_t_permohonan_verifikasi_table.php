<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_permohonan_verifikasi}}`.
 */
class m201004_151335_create_t_permohonan_verifikasi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_permohonan_verifikasi}}', [
            'id' => $this->primaryKey(),
            'permohonan_id' => $this->integer()->notNull(),
            'tgl_verifikasi' => $this->date()->notNull(),
            'status_verifikasi' => $this->integer()->notNull(),
            'diverifikasi_oleh' => $this->integer()->notNull(),
            'catatan_verifikasi' => $this->string(),
            'is_delete' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_permohonan_verifikasi}}');
    }
}
