<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_sk_perusahaan_kendaraan}}`.
 */
class m200717_105620_create_t_sk_perusahaan_kendaraan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_sk_perusahaan_kendaraan}}', [
            'id' => $this->primaryKey(),
            'sk_id' => $this->integer()->notNull(),
            'no_srut' => $this->string(),
            'tgl_srut' => $this->string(),
            'no_kendaraan' => $this->string()->notNull(),
            'no_rangka' => $this->string()->notNull(),
            'no_uji' => $this->string(),
            'expire_uji' => $this->date(),
            'no_mesin' => $this->string()->notNull(),
            'tahun' => $this->string(),
            'merk' => $this->string(),
            'nama_pemilik' => $this->string(),
            'jenis_kendaraan_id' => $this->integer()->notNull(),
            'seat' => $this->integer(),
            'stnk' => $this->string(),
            'kir' => $this->string(),
            'bukti_jasa_raharja' => $this->string(),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_sk_perusahaan_kendaraan}}');
    }
}
