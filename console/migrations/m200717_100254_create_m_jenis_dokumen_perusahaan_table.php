<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_jenis_dokumen_perusahaan}}`.
 */
class m200717_100254_create_m_jenis_dokumen_perusahaan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%m_jenis_dokumen_perusahaan}}', [
            'id' => $this->primaryKey(),
            'jenis_dokumen' => $this->string(),
            'keterangan' => $this->text(),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%m_jenis_dokumen_perusahaan}}');
    }
}
