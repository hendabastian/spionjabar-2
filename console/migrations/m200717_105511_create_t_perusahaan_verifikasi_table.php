<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%t_perusahaan_verifikasi}}`.
 */
class m200717_105511_create_t_perusahaan_verifikasi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%t_perusahaan_verifikasi}}', [
            'id' => $this->primaryKey(),
            'perusahaan_id' => $this->integer()->notNull(),
            'tgl_verifikasi' => $this->date(),
            'status_verifikasi' => $this->integer(),
            'catatan_verifikasi' => $this->text(),
            'diverifikasi_oleh' => $this->integer(),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%t_perusahaan_verifikasi}}');
    }
}
