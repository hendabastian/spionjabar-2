<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_trayek}}`.
 */
class m200717_104959_create_m_trayek_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%m_trayek}}', [
            'id' => $this->primaryKey(),
            'kode_trayek' => $this->string()->notNull(),
            'nama_trayek' => $this->string()->notNull(),
            'terminal_asal' => $this->integer(),
            'terminal_tujuan' => $this->integer(),
            'rute_trayek' => $this->text(),
            'jarak_rute' => $this->integer(),
            'kuota' => $this->integer()->notNull(),
            'jenis_angkutan_id' => $this->integer()->notNull(),
            'keterangan' => $this->text(),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%m_trayek}}');
    }
}
