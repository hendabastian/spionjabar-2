<?php

use yii\db\Migration;

/**
 * Class m200720_124748_seeds_user_role_table
 */
class m200720_124748_seeds_user_role_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        INSERT INTO `user_role` (`id`, `role`, `keterangan`, `is_delete`, `is_active`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
                (1, 'Administrator', NULL, 0, 1, '2019-07-11 23:00:00', '2019-07-11 23:00:00', 1, 1),
                (2, 'Perusahaan Otobus', NULL, 0, 1, '2019-07-11 23:00:00', '2019-07-11 23:00:00', 1, 1),
                (3, 'Verifikator', NULL, 0, 1, '2019-07-11 23:00:00', '2019-07-11 23:00:00', 1, 1),
                (4, 'Jasa Raharja', NULL, 0, 1, '2019-10-14 10:18:34', '2019-10-14 10:18:34', 1, 1),
                (5, 'Kepala Seksi', NULL, 0, 1, '2019-10-14 13:55:16', '2020-01-08 08:26:25', 1, 1),
                (6, 'Kepala Bidang Angkutan', NULL, 0, 1, '2019-10-14 13:55:39', '2019-10-14 13:55:39', 1, 1),
                (7, 'Kepala Dinas', NULL, 0, 1, '2019-10-14 13:55:52', '2019-10-14 13:55:52', 1, 1),
                (8, 'Bid. Percetakan', NULL, 0, 1, '2019-10-14 13:56:04', '2019-10-14 13:56:04', 1, 1),
                (9, 'Sekretaris', NULL, 0, 1, '2019-12-04 11:12:18', '2019-12-04 11:12:18', 1, 1);
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('user_role');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200720_124748_seeds_user_role_table cannot be reverted.\n";

        return false;
    }
    */
}
