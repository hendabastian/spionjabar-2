<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_tipe_terminal}}`.
 */
class m200717_104943_create_m_tipe_terminal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%m_tipe_terminal}}', [
            'id' => $this->primaryKey(),
            'tipe_terminal' => $this->string()->notNull(),
            'keterangan' => $this->text(),
            'is_active' => $this->integer()->notNull()->defaultValue(1),
            'is_delete' => $this->integer()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%m_tipe_terminal}}');
    }
}
